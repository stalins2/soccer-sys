<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="pages/assets/js/jquery.min.js"></script>
        <title>Sistema FohfaOi</title>
    </head>
    <body>
        <section>
            <div class="loginBox">
                <div class="glass">
                    <!-- <img src="user.png" class="user"> -->
                    <h3>Login</h3>
                    <form name="login_form" id="login_form" action="pages/assets/includes/valida_login.php" method="post">
                    <!-- <form method="post" id="login_form" name="login_form" enctype="multipart/form-data"> -->
                        <div class="inputBox">
                            <input type="text" name="email" placeholder="E-mail">
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        </div>                    
                        <div class="inputBox">
                            <input type="password" name="senha" placeholder="Senha">
                            <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                        </div>
                        <input type="submit" name="fazer_login" class="fazer_login" value="Entrar">
                    </form>
                    <?php
                        if(isset($_GET['login']) && $_GET['login'] == 'err'){
                    ?>
                        <div style="text-align: center; padding: 5px 0;"> Usuário ou senha inválido(s). </div>
                    <?php } ?>
                    
                    <?php
                        if(isset($_GET['login']) && $_GET['login'] == 'err2'){
                    ?>
                        <div style="text-align: center; padding: 5px 0;"> Faça login antes de acessar o sistema. </div>
                    <?php } ?>
                    <a href="#" class="esqueci">Esqueci a Senha</a>
                </div>
            </div> 
        </section>
        <section>
            <div class="modal-tabelinha-hidden">
                <div class="fecha-modal"> X Fechar </div>
                    <section class="tabelinha-nova">	
                        <form method="post" id="recover_form" name="recover_form" enctype="multipart/form-data">
                            <h3>Insira o e-mail para recuperação</h3>
                            <div>
                                <input type="text" name="recuperar_email" id="recuperar_email" placeholder="E-mail"/>
                            </div>
                            <div>
                                <input type="submit" class="recuperar" name="recuperar" value="Enviar">
                            </div>
                        </form>
                        <section class="confirma">
            
                        </section>   
                    </section>         
                </div> 
            </div>
        </section>  
        <section>
            <script>
                $('.fecha-modal').on('click', function(){
                    fechaModal(); 
                }); 
                function fechaModal(){                
                    $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
                    $('body').css('overflow','auto');
                    $('.tabelinha-nova form').fadeOut();
                    $('.tabelinha-nova').css("margin-top", "0vh");
                    
                }

                $(document).on('click', '.esqueci', function(e){
                    e.preventDefault();
                    $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
                    $('body').css('overflow','hidden');
                    $('.tabelinha-nova form').fadeIn();
                    $('.tabelinha-nova').css("margin-top", "50vh");
                });


                $(document).on('click', '.recuperar', function(){		
                    event.preventDefault();                    
                    var formdata = new FormData($("form[name='recover_form']")[0]);
                    //  console.log('formdata', formdata);
                    var confirmaCount = `<div id="countdown"></div>`;
                    $(".confirma").html("Verificando...");
                    if(confirm("Você deseja receber sua senha por e-mail?"))
                    {
                        $('.fecha-modal').fadeOut();
                        $.ajax({
                            url:"pages/recuperar_senha.php",
                            method:"POST",
                            data: formdata,
                            contentType: false,
                            processData: false,
                            success:function(data)
                            {	
                                // console.log(data);
                                // alert(data);
                                $('.confirma').html(data);
                                $('.tabelinha-nova form').fadeOut();
                                setTimeout(function(){
                                    $('.confirma').append(confirmaCount);
                                    var timeleft = 15;
                                    var downloadTimer = setInterval(function(){
                                    if(timeleft <= 0){
                                        $('.fecha-modal').fadeIn();
                                        clearInterval(downloadTimer);
                                        document.getElementById("countdown").innerHTML = "0";
                                        $('#countdown').remove();
                                        $('.confirma').html("");
                                        fechaModal();
                                    } else {
                                        document.getElementById("countdown").innerHTML = "Essa janela irá se fechar em "+timeleft+" segundos.";
                                    }
                                    timeleft -= 1;
                                    }, 1000);
                                },100);
                            }
                        });
                    }
                    else
                    {
                        return false;	
                    }
                });


                // //Teste 

                // $(document).on('click', '.fazer_login', function(){		
                //     event.preventDefault();                    
                //     var formdata = new FormData($("form[name='login_form']")[0]);
                //     var pagina = "pages/assets/includes/valida_login.php";
                //     $.ajax({
                //         url:"pages/assets/includes/valida_login.php",
                //         method:"POST",
                //         data: formdata,
                //         contentType: false,
                //         processData: false,
                //         success:function(data)
                //         {	
                //            window.location.href = pagina;
                //             // console.log('dados', data);
                //         }
                //     });
                // });
                
            </script>
        </section>
    </body>
</html>