<?
session_start();
include('header_loaders.php');
include('new_lists.php');
?>

<div class="inner">
<!-- Header -->

    <form method="post" id="team_form" name="team_form" enctype="multipart/form-data">
    <header id="header">
        <a href="#" class="logo"><strong>Cadastrar </strong>Jogo (Súmula) - Código/Data</a>
        <?=newListaSu();?>
    </header>
<!-- Content -->
    <section>
        <div class="row gtr-200">
            <div class="col-6 col-12-medium">
                <h3>Time Azul 1º Tempo</h3>			
                <ul class="actions">
                    <li><div class="button primary botao1">Adicionar Jogadores</div></li>
                </ul>
                <div class="row lista-time-1">
                    <?=listarUsuarios("azul_1_");?>   
                </div>
            
                </div>
                <div class="col-6 col-12-medium">
                    <h3>Time Amarelo 1º Tempo</h3>											
                    <ul class="actions">
                        <li><div class="button primary botao2">Adicionar Jogadores</div></li>
                    </ul>                                            
                    <div class="row lista-time-2">
                        <?=listarUsuarios("amarelo_1_");?>   
                    </div>
                </div>
        </div>
        <hr class="major" />
        
        <div class="row gtr-200">
            <div class="col-6 col-12-medium">
                <h3>Time Azul 2º Tempo</h3>										
                <ul class="actions">
                    <li><div class="button primary botao3">Adicionar Jogadores</div></li>
                </ul>                                         
                <div class="row lista-time-3">
                    <?=listarUsuarios("azul_2_");?>   
                </div>
            
            </div>
            <div class="col-6 col-12-medium">
                <h3>Time Amarelo 2º Tempo</h3>										
                <ul class="actions">
                    <li><div class="button primary botao4">Adicionar Jogadores</div></li>
                </ul>                                         
                <div class="row lista-time-4">
                    <?=listarUsuarios("amarelo_2_");?>   
                </div>
            </div>
        </div>
        
                    <input type="hidden" name="operation" id="operation" />
                    <input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
                    <input type="submit" name="action" id="action" class="btn btn-success" value="Cadastrar os Times" />
        </form>
    </section>
    <section>
            <hr class="major" />
            <h4>Escalações dos Times</h4>
            <div class="row gtr-200">
                <div class="col-10 col-12-medium">
                <?include('times/index.php');?>  
                </div>                                
            </div>
    </section>
</div>
<script>
	$('#add_button').click(function(event){
        event.preventDefault();
		$('#team_form')[0].reset();
		$('#action').val("Adicionar");
		$('#operation').val("add");	
	});
    $(document).on('submit', '#team_form', function(event){
        event.preventDefault();
        var formdata = new FormData($("form[name='team_form']")[0]);
        
    if(confirm("Os times serão cadastrados de acordo com o preenchido, confirma?"))
		{
        $.ajax({
            url:"times/insert.php",
            type:'POST',
            data: formdata,
            contentType:false,
            processData:false,
            success:function(data){
                console.log("cadastro: ", data);
                alert(data);
                $('#team_form')[0].reset();
            }
        });
    }
    });

function funcoesTimes(){
    console.log('carregou as funções times');
var tabela;	
var botao;
    $(".botao1").on('click', function(){
        $('.lista-time-1').toggle("fast");
        var botao = ".botao1";
        textoCadTimes(botao);
    });
    $(".botao2").on('click', function(){
        $('.lista-time-2').toggle("fast");
        var botao = ".botao2";
        textoCadTimes(botao);
    });
    $(".botao3").on('click', function(){
        $('.lista-time-3').toggle("fast");
        var botao = ".botao3";
        textoCadTimes(botao);
    });
    $(".botao4").on('click', function(){
        $('.lista-time-4').toggle("fast");
        var botao = ".botao4";
        textoCadTimes(botao);
    });
    $(".vertime1").on('click', function(){
        $('.tabelinha-nova').append($('.tabela-time1'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
       
    });
    $(".vertime2").on('click', function(){        
        $('.tabelinha-nova').append($('.tabela-time2'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
    });
    $(".vertime3").on('click', function(){
        $('.tabelinha-nova').append($('.tabela-time3'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
    });
    $(".vertime4").on('click', function(){
        $('.tabelinha-nova').append($('.tabela-time4'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
    });
    function textoCadTimes(botao){  
        if($(botao).html() == "Adicionar Jogadores"){
            $(botao).html("Ocultar Lista");
        }else{
            $(botao).html("Adicionar Jogadores");
        }        
    }
}

$(document).ready(function(){
		var dataTable1 = $('#user_data').DataTable({		
		"processing":true,
		"serverSide":true,
		"searching": false,
		"responsive": true,
		"order":[],
		"ajax":{
			url:"times/azul1.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": [0],
				"orderable": false,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});
	var dataTable2 = $('#time2').DataTable({		
		"processing":true,
		"serverSide":true,
		"searching": false,
		"responsive": true,
		"order":[],
		"ajax":{
			url:"times/amarelo1.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": [0],
				"orderable": false,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});
	var dataTable3 = $('#time3').DataTable({		
		"processing":true,
		"serverSide":true,
		"searching": false,
		"responsive": true,
		"order":[],
		"ajax":{
			url:"times/azul2.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": [0],
				"orderable": false,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});
	var dataTable4 = $('#time4').DataTable({		
		"processing":true,
		"serverSide":true,
		"searching": false,
		"responsive": true,
		"order":[],
		"ajax":{
			url:"times/amarelo2.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": [0],
				"orderable": false,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});

	// var collapsedGroups = {};	

	$(document).on('click', '.update', function(){
		var user_id = $(this).attr("id");
		var tipo_time = $(this).attr("tipo");
		// console.log('tipo:', tipo_time);
		var dados;
		$.ajax({
			url:"times/fetch_single.php",
			method:"POST",
			data:{
				user_id:user_id,
				tipo_time:tipo_time
				},
			dataType:"json",
			success:function(data)
			{
				console.log("dados: ", data);
				dados = data;
			}
		})
		setTimeout(function(){
			let infos = dados;
			let tabelinha  = `			
			<div class="dados_titulo" style="font-size: 12px; text-transform: uppercase; ">
						<div width="2%">id</div>
						<div width="4%">Sumula</div>
						<div width="8%">Jogador 1</div>
						<div width="8%">Jogador 2</div>
						<div width="8%">Jogador 3</div>
						<div width="8%">Jogador 4</div>
						<div width="8%">Jogador 5</div>
						<div width="8%">Jogador 6</div>
						<div width="8%">Jogador 7</div>
						<div width="8%">Jogador 8</div>
						<div width="8%">Jogador 9</div>
						<div width="8%">Jogador 10</div>
						<div width="8%">Jogador 11</div>
						<div width="6%"> Excluir </div>
					</div>`;		
			for (let i = 0; i < infos.length; i++){
				tabelinha +=
				`	<div class="dados_jogo">
					<div> ${infos[i].id} </div>
					<div> ${infos[i].su_ref} </div>
					<div> ${infos[i].jog_1} </div>
					<div> ${infos[i].jog_2} </div>
					<div> ${infos[i].jog_3} </div>
					<div> ${infos[i].jog_4} </div>
					<div> ${infos[i].jog_5} </div>
					<div> ${infos[i].jog_6} </div>
					<div> ${infos[i].jog_7} </div>
					<div> ${infos[i].jog_8} </div>
					<div> ${infos[i].jog_9} </div>
					<div> ${infos[i].jog_10} </div>
					<div> ${infos[i].jog_11} </div>
					<div>
						<button class="excluir" name="excluir" id="${infos[i].id}" tipo="${infos[i].bd}"value="${infos[i].id}"> Excluir </button>
					</div>
				</div>
				`
			}
			$('.tabelinha-nova').prepend(tabelinha);
			// $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			// $('body').css('overflow','hidden');
		}, 800);
	});
	
	$(document).on('click', '.excluir', function(){
		var user_id = $(this).attr("id");
		var tipo_time = $(this).attr("tipo");
		if(confirm("Você quer excluir esse registro?"))
		{
			$.ajax({
				url:"times/delete.php",
				method:"POST",
				data:{
					user_id:user_id,
					tipo_time:tipo_time
				},
				success:function(data)
				{
					alert(data);
					dataTable1.ajax.reload();
					dataTable2.ajax.reload();
					dataTable3.ajax.reload();
					dataTable4.ajax.reload();
					$('.dados_jogo').remove();
					$('.dados_titulo').remove();
				}
			});
		}
		else
		{
			return false;	
		}
	});


	
$('.fecha-modal').on('click', function(){ 
	setTimeout(function(){
        $('.tabelinha-nova .tabela-carregada').fadeOut(100);
	},100);
	setTimeout(function(){		
		$('.tabelas-times').append($('.tabelinha-nova .tabela-carregada'));
	},200);
	setTimeout(function(){
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').html("");
		$('.dados_jogo').remove();
	},250);
        $('body').css('overflow','auto');
});    

});
</script>