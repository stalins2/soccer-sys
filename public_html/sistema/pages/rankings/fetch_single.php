<?php
include('db.php');
// $_POST["user_id"] = 33;
if(isset($_POST["user_id"]))
{
	$output = array();
	$statement = $connection->prepare(
		"SELECT
			-- Cadastros.id, 
			Sumula.id as sumula_jogo,
			Sumula.placar_amarelo as placar_amarelo,
			Sumula.placar_azul as placar_azul,
			Sumula.placar_penaltis_amarelo as placar_penaltis_amarelo,
			Sumula.placar_penaltis_azul as placar_penaltis_azul,
			Sumula.cartoes_amarelo as cartoes_time_amarelo,
			Sumula.cartoes_azul as cartoes_time_azul,

			arbitragem.sumula_referencia as sumula_arbitragem,
			IFNULL(nome_1.nome, 0) as arbitro, 
			IFNULL(nome_2.nome, 0) as auxiliar_1, 
			IFNULL(nome_3.nome, 0) as auxiliar_2, 
			IFNULL(nome_4.nome, 0) as arbitro_2, 
			IFNULL(nome_5.nome, 0) as auxiliar_3, 
			IFNULL(nome_6.nome, 0) as auxiliar_4,
			
			-- obs.sumula_referencia as sumula_obs,
			obs.presencas as presencas_obs,
			obs.punicoes as punicoes_obs,
			obs.destaques as destaques_obs,
			obs.observacoes as observacoes_obs,

			-- gols.sumula_referencia as sumula_gols,			
			nome_gols.nome as gols_jogador,
			gols.time as gols_jogador_time, 
			gols.primeiro_tempo as gols_primeiro,
			gols.segundo_tempo as gols_segundo

			FROM Sumula

			LEFT JOIN Arbitragem_1 as arbitragem ON arbitragem.sumula_referencia = Sumula.id
			LEFT JOIN Cadastros as nome_1 ON nome_1.id = arbitragem.arbitro
			LEFT JOIN Cadastros as nome_2 ON nome_2.id = arbitragem.auxiliar_1
			LEFT JOIN Cadastros as nome_3 ON nome_3.id = arbitragem.auxiliar_2
			LEFT JOIN Cadastros as nome_4 ON nome_4.id = arbitragem.arbitro_2
			LEFT JOIN Cadastros as nome_5 ON nome_5.id = arbitragem.auxiliar_3
			LEFT JOIN Cadastros as nome_6 ON nome_6.id = arbitragem.auxiliar_4

			LEFT JOIN Observacoes as obs ON obs.sumula_referencia = Sumula.id

			LEFT JOIN Gols as gols ON gols.sumula_referencia = Sumula.id
			LEFT JOIN Cadastros as nome_gols ON nome_gols.id = gols.jogador 

			WHERE Sumula.id = '".$_POST["user_id"]."'
			LIMIT 1"
	);
	
	$statement->execute();
	$result = $statement->fetchAll();
		

	$querygols = $connection->prepare(//gols da partida
		"SELECT
			nome_gols.nome as gols_jogador,
			gols.time as gols_jogador_time, 
			gols.primeiro_tempo as gols_primeiro,
			gols.segundo_tempo as gols_segundo

			FROM Gols as gols

			LEFT JOIN Sumula ON gols.sumula_referencia = Sumula.id
			LEFT JOIN Cadastros as nome_gols ON nome_gols.id = gols.jogador 

			WHERE Sumula.id = '".$_POST["user_id"]."' "
	);
	
	$querygols->execute();
	$resultgols = $querygols->fetchAll();

	$querytime = $connection->prepare(//time da partida
		"SELECT
			IFNULL(time_am1.jog_1 ,0) as jog_am1,
			IFNULL(time_am1.jog_2 ,0) as jog_am2,
			IFNULL(time_am1.jog_3 ,0) as jog_am3,
			IFNULL(time_am1.jog_4 ,0) as jog_am4,
			IFNULL(time_am1.jog_5 ,0) as jog_am5,
			IFNULL(time_am1.jog_6 ,0) as jog_am6,
			IFNULL(time_am1.jog_7 ,0) as jog_am7,
			IFNULL(time_am1.jog_8 ,0) as jog_am8,
			IFNULL(time_am1.jog_9 ,0) as jog_am9,
			IFNULL(time_am1.jog_10 ,0) as jog_am10,
			IFNULL(time_am1.jog_11 ,0) as jog_am11,

			IFNULL(time_az1.jog_1 ,0) as jog_az1,
			IFNULL(time_az1.jog_2 ,0) as jog_az2,
			IFNULL(time_az1.jog_3 ,0) as jog_az3,
			IFNULL(time_az1.jog_4 ,0) as jog_az4,
			IFNULL(time_az1.jog_5 ,0) as jog_az5,
			IFNULL(time_az1.jog_6 ,0) as jog_az6,
			IFNULL(time_az1.jog_7 ,0) as jog_az7,
			IFNULL(time_az1.jog_8 ,0) as jog_az8,
			IFNULL(time_az1.jog_9 ,0) as jog_az9,
			IFNULL(time_az1.jog_10 ,0) as jog_az10,
			IFNULL(time_az1.jog_11 ,0) as jog_az11,			

			IFNULL(time_am2.jog_1 ,0) as jog_am21,
			IFNULL(time_am2.jog_2 ,0) as jog_am22,
			IFNULL(time_am2.jog_3 ,0) as jog_am23,
			IFNULL(time_am2.jog_4 ,0) as jog_am24,
			IFNULL(time_am2.jog_5 ,0) as jog_am25,
			IFNULL(time_am2.jog_6 ,0) as jog_am26,
			IFNULL(time_am2.jog_7 ,0) as jog_am27,
			IFNULL(time_am2.jog_8 ,0) as jog_am28,
			IFNULL(time_am2.jog_9 ,0) as jog_am29,
			IFNULL(time_am2.jog_10 ,0) as jog_am210,
			IFNULL(time_am2.jog_11 ,0) as jog_am211,

			IFNULL(time_az2.jog_1 ,0) as jog_az21,
			IFNULL(time_az2.jog_2 ,0) as jog_az22,
			IFNULL(time_az2.jog_3 ,0) as jog_az23,
			IFNULL(time_az2.jog_4 ,0) as jog_az24,
			IFNULL(time_az2.jog_5 ,0) as jog_az25,
			IFNULL(time_az2.jog_6 ,0) as jog_az26,
			IFNULL(time_az2.jog_7 ,0) as jog_az27,
			IFNULL(time_az2.jog_8 ,0) as jog_az28,
			IFNULL(time_az2.jog_9 ,0) as jog_az29,
			IFNULL(time_az2.jog_10 ,0) as jog_az210,
			IFNULL(time_az2.jog_11 ,0) as jog_az211

			
			FROM Sumula
			LEFT JOIN v_time_am_1 as time_am1 ON time_am1.su_ref = Sumula.id
			LEFT JOIN v_time_az_1 as time_az1 ON time_az1.su_ref = Sumula.id
			LEFT JOIN v_time_am_2 as time_am2 ON time_am2.su_ref = Sumula.id
			LEFT JOIN v_time_az_2 as time_az2 ON time_az2.su_ref = Sumula.id

			WHERE Sumula.id = '".$_POST["user_id"]."'
		"
	);
	$querytime->execute();
	$resulttime = $querytime->fetchAll();

	
	$querypenaltis = $connection->prepare(//Penaltis da partida
		"SELECT
			nome_penaltis.nome as penaltis_jogador, 
			Penaltis.time as penaltis_jogador_time, 
			Penaltis.convertidos as convertidos,
			Penaltis.perdidos as perdidos

			FROM Penaltis
            
			LEFT JOIN Sumula ON Penaltis.sumula_referencia = Sumula.id
			LEFT JOIN Cadastros as nome_penaltis ON nome_penaltis.id = Penaltis.jogador 

			WHERE Sumula.id = '".$_POST["user_id"]."' "
	);
	
	$querypenaltis->execute();
	$resultpenaltis = $querypenaltis->fetchAll();

	$querycartoes = $connection->prepare(//Penaltis da partida
		"SELECT
			nome_cartoes.nome as cartoes_jogador, 
			Cartoes.amarelo as cartoes_amarelo, 
			Cartoes.azul as cartoes_azul,
			Cartoes.vermelho as cartoes_vermelho

			FROM Cartoes
            
			LEFT JOIN Sumula ON Cartoes.sumula_referencia = Sumula.id
			LEFT JOIN Cadastros as nome_cartoes ON nome_cartoes.id = Cartoes.jogador 

			WHERE Sumula.id = '".$_POST["user_id"]."' "
	);
	
	$querycartoes->execute();
	$resultcartoes = $querycartoes->fetchAll();

	$queryfotos = $connection->prepare(//Fotos dos Jogadores
		"SELECT 
			jog_az1_1.foto as foto1_1, jog_az1_2.foto as foto1_2, jog_az1_3.foto as foto1_3, jog_az1_4.foto as foto1_4, jog_az1_5.foto as foto1_5, jog_az1_6.foto as foto1_6, jog_az1_7.foto as foto1_7, jog_az1_8.foto as foto1_8, jog_az1_9.foto as foto1_9, jog_az1_10.foto as foto1_10, jog_az1_11.foto as foto1_11,

			jog_az2_1.foto as foto3_1, jog_az2_2.foto as foto3_2, jog_az2_3.foto as foto3_3, jog_az2_4.foto as foto3_4, jog_az2_5.foto as foto3_5, jog_az2_6.foto as foto3_6, jog_az2_7.foto as foto3_7, jog_az2_8.foto as foto3_8, jog_az2_9.foto as foto3_9, jog_az2_10.foto as foto3_10, jog_az2_11.foto as foto3_11,
			
			jog_am1_1.foto as foto2_1, jog_am1_2.foto as foto2_2, jog_am1_3.foto as foto2_3, jog_am1_4.foto as foto2_4, jog_am1_5.foto as foto2_5, jog_am1_6.foto as foto2_6, jog_am1_7.foto as foto2_7, jog_am1_8.foto as foto2_8, jog_am1_9.foto as foto2_9, jog_am1_10.foto as foto2_10, jog_am1_11.foto as foto2_11,
			
			jog_am2_1.foto as foto4_1, jog_am2_2.foto as foto4_2, jog_am2_3.foto as foto4_3, jog_am2_4.foto as foto4_4, jog_am2_5.foto as foto4_5, jog_am2_6.foto as foto4_6, jog_am2_7.foto as foto4_7, jog_am2_8.foto as foto4_8, jog_am2_9.foto as foto4_9, jog_am2_10.foto as foto4_10, jog_am2_11.foto as foto4_11
			
			FROM

			Time_Azul_1

			LEFT JOIN Time_Azul_2 ON Time_Azul_2.sumula_referencia = Time_Azul_1.sumula_referencia
			LEFT JOIN Time_Amarelo_1 ON Time_Amarelo_1.sumula_referencia = Time_Azul_1.sumula_referencia
			LEFT JOIN Time_Amarelo_2 ON Time_Amarelo_2.sumula_referencia = Time_Azul_1.sumula_referencia

            LEFT JOIN Cadastros as jog_az1_1 ON jog_az1_1.id = Time_Azul_1.jog_1 
            LEFT JOIN Cadastros as jog_az1_2 ON jog_az1_2.id = Time_Azul_1.jog_2 
            LEFT JOIN Cadastros as jog_az1_3 ON jog_az1_3.id = Time_Azul_1.jog_3 
            LEFT JOIN Cadastros as jog_az1_4 ON jog_az1_4.id = Time_Azul_1.jog_4 
            LEFT JOIN Cadastros as jog_az1_5 ON jog_az1_5.id = Time_Azul_1.jog_5 
            LEFT JOIN Cadastros as jog_az1_6 ON jog_az1_6.id = Time_Azul_1.jog_6 
            LEFT JOIN Cadastros as jog_az1_7 ON jog_az1_7.id = Time_Azul_1.jog_7 
            LEFT JOIN Cadastros as jog_az1_8 ON jog_az1_8.id = Time_Azul_1.jog_8 
            LEFT JOIN Cadastros as jog_az1_9 ON jog_az1_9.id = Time_Azul_1.jog_9 
            LEFT JOIN Cadastros as jog_az1_10 ON jog_az1_10.id = Time_Azul_1.jog_10 
            LEFT JOIN Cadastros as jog_az1_11 ON jog_az1_11.id = Time_Azul_1.jog_11

			LEFT JOIN Cadastros as jog_az2_1 ON jog_az2_1.id = Time_Azul_2.jog_1 
            LEFT JOIN Cadastros as jog_az2_2 ON jog_az2_2.id = Time_Azul_2.jog_2 
            LEFT JOIN Cadastros as jog_az2_3 ON jog_az2_3.id = Time_Azul_2.jog_3 
            LEFT JOIN Cadastros as jog_az2_4 ON jog_az2_4.id = Time_Azul_2.jog_4 
            LEFT JOIN Cadastros as jog_az2_5 ON jog_az2_5.id = Time_Azul_2.jog_5 
            LEFT JOIN Cadastros as jog_az2_6 ON jog_az2_6.id = Time_Azul_2.jog_6 
            LEFT JOIN Cadastros as jog_az2_7 ON jog_az2_7.id = Time_Azul_2.jog_7 
            LEFT JOIN Cadastros as jog_az2_8 ON jog_az2_8.id = Time_Azul_2.jog_8 
            LEFT JOIN Cadastros as jog_az2_9 ON jog_az2_9.id = Time_Azul_2.jog_9 
            LEFT JOIN Cadastros as jog_az2_10 ON jog_az2_10.id = Time_Azul_2.jog_10 
            LEFT JOIN Cadastros as jog_az2_11 ON jog_az2_11.id = Time_Azul_2.jog_11
			
            LEFT JOIN Cadastros as jog_am1_1 ON jog_am1_1.id = Time_Amarelo_1.jog_1 
            LEFT JOIN Cadastros as jog_am1_2 ON jog_am1_2.id = Time_Amarelo_1.jog_2 
            LEFT JOIN Cadastros as jog_am1_3 ON jog_am1_3.id = Time_Amarelo_1.jog_3 
            LEFT JOIN Cadastros as jog_am1_4 ON jog_am1_4.id = Time_Amarelo_1.jog_4 
            LEFT JOIN Cadastros as jog_am1_5 ON jog_am1_5.id = Time_Amarelo_1.jog_5 
            LEFT JOIN Cadastros as jog_am1_6 ON jog_am1_6.id = Time_Amarelo_1.jog_6 
            LEFT JOIN Cadastros as jog_am1_7 ON jog_am1_7.id = Time_Amarelo_1.jog_7 
            LEFT JOIN Cadastros as jog_am1_8 ON jog_am1_8.id = Time_Amarelo_1.jog_8 
            LEFT JOIN Cadastros as jog_am1_9 ON jog_am1_9.id = Time_Amarelo_1.jog_9 
            LEFT JOIN Cadastros as jog_am1_10 ON jog_am1_10.id = Time_Amarelo_1.jog_10 
            LEFT JOIN Cadastros as jog_am1_11 ON jog_am1_11.id = Time_Amarelo_1.jog_11

			LEFT JOIN Cadastros as jog_am2_1 ON jog_am2_1.id = Time_Amarelo_2.jog_1 
            LEFT JOIN Cadastros as jog_am2_2 ON jog_am2_2.id = Time_Amarelo_2.jog_2 
            LEFT JOIN Cadastros as jog_am2_3 ON jog_am2_3.id = Time_Amarelo_2.jog_3 
            LEFT JOIN Cadastros as jog_am2_4 ON jog_am2_4.id = Time_Amarelo_2.jog_4 
            LEFT JOIN Cadastros as jog_am2_5 ON jog_am2_5.id = Time_Amarelo_2.jog_5 
            LEFT JOIN Cadastros as jog_am2_6 ON jog_am2_6.id = Time_Amarelo_2.jog_6 
            LEFT JOIN Cadastros as jog_am2_7 ON jog_am2_7.id = Time_Amarelo_2.jog_7 
            LEFT JOIN Cadastros as jog_am2_8 ON jog_am2_8.id = Time_Amarelo_2.jog_8 
            LEFT JOIN Cadastros as jog_am2_9 ON jog_am2_9.id = Time_Amarelo_2.jog_9 
            LEFT JOIN Cadastros as jog_am2_10 ON jog_am2_10.id = Time_Amarelo_2.jog_10 
            LEFT JOIN Cadastros as jog_am2_11 ON jog_am2_11.id = Time_Amarelo_2.jog_11



			WHERE Time_Azul_1.sumula_referencia = '".$_POST["user_id"]."' "
	);
	
	$queryfotos->execute();
	$resultfotos = $queryfotos->fetchAll();

	$array_principal = array();
		$array_infos0 = array();
	foreach($result as $row){
		$sub_array = array();
		$sub_array["sumula_jogo"] = $row["sumula_jogo"];
		$sub_array["placar_amarelo"] = $row["placar_amarelo"];
		$sub_array["placar_azul"] = $row["placar_azul"];			
		$sub_array["placar_penaltis_amarelo"] = $row["placar_penaltis_amarelo"];			
		$sub_array["placar_penaltis_azul"] = $row["placar_penaltis_azul"];			
		$sub_array["cartoes_time_amarelo"] = $row["cartoes_time_amarelo"];			
		$sub_array["cartoes_time_azul"] = $row["cartoes_time_azul"];

		$sub_array["arbitro"] = $row["arbitro"];
		$sub_array["auxiliar_1"] = $row["auxiliar_1"];
		$sub_array["auxiliar_2"] = $row["auxiliar_2"];
		$sub_array["arbitro_2"] = $row["arbitro_2"];
		$sub_array["auxiliar_3"] = $row["auxiliar_3"];
		$sub_array["auxiliar_4"] = $row["auxiliar_4"];
		
		// $sub_array["sumula_obs"] = $row["sumula_obs"];
		$sub_array["presencas_obs"] = $row["presencas_obs"];
		$sub_array["punicoes_obs"] = $row["punicoes_obs"];
		$sub_array["destaques_obs"] = $row["destaques_obs"];
		$sub_array["observacoes_obs"] = $row["observacoes_obs"];
		$array_infos0[] = $sub_array;
	}
		$array_principal["cabecalho"] = $array_infos0; //cabecalho da sumula
		
		$array_infos1 = array();		
		foreach($resultgols as $gols){
			$array_gols = array();
			$array_gols["gols_jogador"] = $gols["gols_jogador"];
			$array_gols["gols_jogador_time"] = $gols["gols_jogador_time"];
			$array_gols["gols_primeiro"] = $gols["gols_primeiro"];
			$array_gols["gols_segundo"] = $gols["gols_segundo"];
			$array_infos1[] = $array_gols;
		}
		$array_principal["gols"] = $array_infos1;

		$array_infos2 = array();
		foreach($resulttime as $arraytimes){
			// $array_times= array();

				$array_time1 = array();
					$armazena1 = array();
						$armazena1[] = $array_time1["jog_am1"] = $arraytimes["jog_am1"];
						$armazena1[] = $array_time1["jog_am2"] = $arraytimes["jog_am2"];
						$armazena1[] = $array_time1["jog_am3"] = $arraytimes["jog_am3"];
						$armazena1[] = $array_time1["jog_am4"] = $arraytimes["jog_am4"];
						$armazena1[] = $array_time1["jog_am5"] = $arraytimes["jog_am5"];
						$armazena1[] = $array_time1["jog_am6"] = $arraytimes["jog_am6"];
						$armazena1[] = $array_time1["jog_am7"] = $arraytimes["jog_am7"];
						$armazena1[] = $array_time1["jog_am8"] = $arraytimes["jog_am8"];
						$armazena1[] = $array_time1["jog_am9"] = $arraytimes["jog_am9"];
						$armazena1[] = $array_time1["jog_am10"] = $arraytimes["jog_am10"];
						$armazena1[] = $array_time1["jog_am11"] = $arraytimes["jog_am11"];
				$array_infos2[] = $armazena1;

				$array_time2 = array();
					$armazena2 = array();
						$armazena2[] = $array_time2["jog_az1"] = $arraytimes["jog_az1"];
						$armazena2[] = $array_time2["jog_az2"] = $arraytimes["jog_az2"];
						$armazena2[] = $array_time2["jog_az3"] = $arraytimes["jog_az3"];
						$armazena2[] = $array_time2["jog_az4"] = $arraytimes["jog_az4"];
						$armazena2[] = $array_time2["jog_az5"] = $arraytimes["jog_az5"];
						$armazena2[] = $array_time2["jog_az6"] = $arraytimes["jog_az6"];
						$armazena2[] = $array_time2["jog_az7"] = $arraytimes["jog_az7"];
						$armazena2[] = $array_time2["jog_az8"] = $arraytimes["jog_az8"];
						$armazena2[] = $array_time2["jog_az9"] = $arraytimes["jog_az9"];
						$armazena2[] = $array_time2["jog_az10"] = $arraytimes["jog_az10"];
						$armazena2[] = $array_time2["jog_az11"] = $arraytimes["jog_az11"];		
				$array_infos2[] = $armazena2;
			
				$array_time3 = array();
					$armazena3 = array();
					$armazena3[] = $array_time3["jog_am21"] = $arraytimes["jog_am21"];
					$armazena3[] = $array_time3["jog_am22"] = $arraytimes["jog_am22"];
					$armazena3[] = $array_time3["jog_am23"] = $arraytimes["jog_am23"];
					$armazena3[] = $array_time3["jog_am24"] = $arraytimes["jog_am24"];
					$armazena3[] = $array_time3["jog_am25"] = $arraytimes["jog_am25"];
					$armazena3[] = $array_time3["jog_am26"] = $arraytimes["jog_am26"];
					$armazena3[] = $array_time3["jog_am27"] = $arraytimes["jog_am27"];
					$armazena3[] = $array_time3["jog_am28"] = $arraytimes["jog_am28"];
					$armazena3[] = $array_time3["jog_am29"] = $arraytimes["jog_am29"];
					$armazena3[] = $array_time3["jog_am210"] = $arraytimes["jog_am210"];
					$armazena3[] = $array_time3["jog_am211"] = $arraytimes["jog_am211"];
				$array_infos2[] = $armazena3;
				
				$array_time4 = array();
					$armazena4[] = $array_time4["jog_az21"] = $arraytimes["jog_az21"];
					$armazena4[] = $array_time4["jog_az22"] = $arraytimes["jog_az22"];
					$armazena4[] = $array_time4["jog_az23"] = $arraytimes["jog_az23"];
					$armazena4[] = $array_time4["jog_az24"] = $arraytimes["jog_az24"];
					$armazena4[] = $array_time4["jog_az25"] = $arraytimes["jog_az25"];
					$armazena4[] = $array_time4["jog_az26"] = $arraytimes["jog_az26"];
					$armazena4[] = $array_time4["jog_az27"] = $arraytimes["jog_az27"];
					$armazena4[] = $array_time4["jog_az28"] = $arraytimes["jog_az28"];
					$armazena4[] = $array_time4["jog_az29"] = $arraytimes["jog_az29"];
					$armazena4[] = $array_time4["jog_az210"] = $arraytimes["jog_az210"];
					$armazena4[] = $array_time4["jog_az211"] = $arraytimes["jog_az211"];
				$array_infos2[] = $armazena4;

			// $array_infos2[] = $array_times;
		}
		$array_principal["times"]	= $array_infos2;
		
		$array_infos3 = array();		
		foreach($resultpenaltis as $penaltis){
			$array_penaltis = array();
			$array_penaltis["penaltis_jogador"] = $penaltis["penaltis_jogador"];
			$array_penaltis["penaltis_jogador_time"] = $penaltis["penaltis_jogador_time"];
			$array_penaltis["convertidos"] = $penaltis["convertidos"];
			$array_penaltis["perdidos"] = $penaltis["perdidos"];

			$array_infos3[] = $array_penaltis;
		}
		$array_principal["penaltis"]	= $array_infos3;

		$array_infos4 = array();		
		foreach($resultcartoes as $cartoes){
			$array_cartoes = array();
			$array_cartoes["cartoes_jogador"] = $cartoes["cartoes_jogador"];
			$array_cartoes["cartoes_amarelo"] = $cartoes["cartoes_amarelo"];
			$array_cartoes["cartoes_azul"] = $cartoes["cartoes_azul"];
			$array_cartoes["cartoes_vermelho"] = $cartoes["cartoes_vermelho"];

			$array_infos4[] = $array_cartoes;
		}
		$array_principal["cartoes"]	= $array_infos4;

		$array_infos5 = array();		
		foreach($resultfotos as $fotos){

			$array_fotos1 = array();
				$guarda1 = array();//azul 1
					$guarda1[] = $array_fotos1["foto1_1"] = $fotos["foto1_1"];
					$guarda1[] = $array_fotos1["foto1_2"] = $fotos["foto1_2"];
					$guarda1[] = $array_fotos1["foto1_3"] = $fotos["foto1_3"];
					$guarda1[] = $array_fotos1["foto1_4"] = $fotos["foto1_4"];
					$guarda1[] = $array_fotos1["foto1_5"] = $fotos["foto1_5"];
					$guarda1[] = $array_fotos1["foto1_6"] = $fotos["foto1_6"];
					$guarda1[] = $array_fotos1["foto1_7"] = $fotos["foto1_7"];
					$guarda1[] = $array_fotos1["foto1_8"] = $fotos["foto1_8"];
					$guarda1[] = $array_fotos1["foto1_9"] = $fotos["foto1_9"];
					$guarda1[] = $array_fotos1["foto1_10"] = $fotos["foto1_10"];
					$guarda1[] = $array_fotos1["foto1_11"] = $fotos["foto1_11"];
			$array_infos5[] = $guarda1;

			$array_fotos2 = array();
				$guarda2 = array();
					$guarda2[] = $array_fotos2["foto2_1"] = $fotos["foto2_1"];
					$guarda2[] = $array_fotos2["foto2_2"] = $fotos["foto2_2"];
					$guarda2[] = $array_fotos2["foto2_3"] = $fotos["foto2_3"];
					$guarda2[] = $array_fotos2["foto2_4"] = $fotos["foto2_4"];
					$guarda2[] = $array_fotos2["foto2_5"] = $fotos["foto2_5"];
					$guarda2[] = $array_fotos2["foto2_6"] = $fotos["foto2_6"];
					$guarda2[] = $array_fotos2["foto2_7"] = $fotos["foto2_7"];
					$guarda2[] = $array_fotos2["foto2_8"] = $fotos["foto2_8"];
					$guarda2[] = $array_fotos2["foto2_9"] = $fotos["foto2_9"];
					$guarda2[] = $array_fotos2["foto2_10"] = $fotos["foto2_10"];
					$guarda2[] = $array_fotos2["foto2_11"] = $fotos["foto2_11"];		
			$array_infos5[] = $guarda2;
		
			$array_fotos3 = array();
				$guarda3 = array();
				$guarda3[] = $array_fotos3["foto3_1"] = $fotos["foto3_1"];
				$guarda3[] = $array_fotos3["foto3_2"] = $fotos["foto3_2"];
				$guarda3[] = $array_fotos3["foto3_3"] = $fotos["foto3_3"];
				$guarda3[] = $array_fotos3["foto3_4"] = $fotos["foto3_4"];
				$guarda3[] = $array_fotos3["foto3_5"] = $fotos["foto3_5"];
				$guarda3[] = $array_fotos3["foto3_6"] = $fotos["foto3_6"];
				$guarda3[] = $array_fotos3["foto3_7"] = $fotos["foto3_7"];
				$guarda3[] = $array_fotos3["foto3_8"] = $fotos["foto3_8"];
				$guarda3[] = $array_fotos3["foto3_9"] = $fotos["foto3_9"];
				$guarda3[] = $array_fotos3["foto3_10"] = $fotos["foto3_10"];
				$guarda3[] = $array_fotos3["foto3_11"] = $fotos["foto3_11"];
			$array_infos5[] = $guarda3;
			
			$array_fotos4 = array();
				$guarda4[] = $array_fotos4["foto4_1"] = $fotos["foto4_1"];
				$guarda4[] = $array_fotos4["foto4_2"] = $fotos["foto4_2"];
				$guarda4[] = $array_fotos4["foto4_3"] = $fotos["foto4_3"];
				$guarda4[] = $array_fotos4["foto4_4"] = $fotos["foto4_4"];
				$guarda4[] = $array_fotos4["foto4_5"] = $fotos["foto4_5"];
				$guarda4[] = $array_fotos4["foto4_6"] = $fotos["foto4_6"];
				$guarda4[] = $array_fotos4["foto4_7"] = $fotos["foto4_7"];
				$guarda4[] = $array_fotos4["foto4_8"] = $fotos["foto4_8"];
				$guarda4[] = $array_fotos4["foto4_9"] = $fotos["foto4_9"];
				$guarda4[] = $array_fotos4["foto4_10"] = $fotos["foto4_10"];
				$guarda4[] = $array_fotos4["foto4_11"] = $fotos["foto4_11"];
			$array_infos5[] = $guarda4;
		}

		$array_principal["fotos"] = $array_infos5;	
		
		// echo "<br> --- <pre>";
		// print_r($resultfotos);
		// echo "<br> --- </pre>";
		
		
		// echo "<pre> array_principal:";
		// print_r($array_principal);
		echo json_encode($array_principal, JSON_PRETTY_PRINT);
}
?>