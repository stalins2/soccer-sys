<?
require_once('../assets/includes/validador_acesso.php');
include('../header_loaders.php');
include('../new_lists.php');
?>
<html>

<head>
	<title> Sistema - Fohfaoi</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="../assets/css/new_view.css" />
	<style>
		tr.dtrg-start {
			cursor: pointer;
		}

		.modal-tabelinha-hidden{
			width: 0;
			height: 0;
			opacity: 0;
		}
		.modal-tabelinha{
			position: fixed;
			top: 0;
			left: 0;
			display: block;
			margin: 0 auto;
			width: 100vw;
			height: 100vh;
			overflow: auto;
			z-index: 9999;
			opacity: 1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		.tabelinha-nova{
			width: 90%;
			height: auto;
			padding: 20px;
			margin: 5% auto;
			background-color: #fff;
		}
		.fecha-modal{
			position: absolute;
			right: 10px;
			top: 10px;
			color: #fff;
			font-weight: bold;
			font-size: 15px;
			cursor: pointer;
			text-transform: uppercase;
		}
	
		.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
			display: grid; 
			grid-template-columns: 5% 5% 20% 20% 20% 20% 10%;
			/* grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); */
			grid-gap: 0px;
			padding: 10px;
		}
		.tabelinha-nova .dados_jogo div{
			padding: 5px;
		}
		.tabelinha-nova .dados_titulo {
			font-weight: bold;
			background-color: #fdfdfd;
		}

		.tabelinha-nova .dados_jogo div, .tabelinha-nova .dados_titulo div, {
			padding: 10px;
		}
		.tabelinha-nova .dados_jogo .excluir{
			width: 100%;
			padding: 0;
		}

		.tabelinha-nova .dados_jogo button {
			border: none;
			background-color: #fff;

		}
		.update{
			position: relative;
		}
		.campo-temporario-before:before {
			content: 'Detalhes Acima';
			padding: 8px;
			font-size: 11px;
			text-transform: uppercase;
			position: absolute;
			top: -60px;
			white-space: pre-wrap;
			border-radius: 6px;
			background: #000;
			color: #fff;
		}
	</style>
</head>

<body class="is-preload">
	<!-- Wrapper -->
	<div id="wrapper">
		<!-- Main -->
		<div id="main" class="conteudo-page">
			<div class="inner">
				<!-- Header -->
				<section>
					<form method="post" id="team_form" name="team_form" enctype="multipart/form-data">
					<!-- <form id="team_form" name="team_form" method="post" action="../controllers_new/cadastro_arbitragem_controller.php?acao=cadastrar"> -->
						<header id="header">
							<div class="col-4">
								<a href="#" class="logo"><strong>Ver </strong>Rankings/Súmulas</a>
							</div>
							<div class="col-4">
								<?=newListaSu();?>
							</div>
							<div class="col-4">
								<input type="hidden" name="operation" id="operation" />
								<input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
								<input type="submit" name="action" id="visu" class="btn" value="Visualizar Sumula"/>
							</div>
						</header>
						<!-- Content -->
						<br/>
						<div class="row">
							<div class="container-fluid box">
								<div class="container-sumula">
									<table class="sumula-escalacao">
										<thead>
											<tr>
												<th colspan="4">Sumula Pesquisada <span id="sumula_jogo"></span></th>
											</tr>
											<tr>
												<th>Posição</th>
												<th>
													Amarelo
												</th>
												<th>
													Azul
												</th>
												<th>Posição</th>
											</tr>
										</thead>
										<tbody class="listagem-primeiro-tempo">
											
										</tbody>
									</table>
								</div>
								<div class="container-sumula">
									<table class="sumula-escalacao">
										<thead>
											<tr>
												<th colspan="4">Segundo Tempo</th>
											</tr>
										</thead>
										<tbody class="listagem-segundo-tempo">
											
										</tbody>
									</table>
								</div>
								<div class="detalhes-1">
									<div class="container-sumula arbitragem">
										<table class="sumula-arbitragem first">
											<thead>
												<th colspan="2">
													Arbitragem 1º Tempo
												</th>
											</thead>
											<tbody class="arbitragem-1">

											</tbody>                    
										</table>
										<table class="sumula-arbitragem second">
											<thead>
												<tr>
													<th colspan="2">
														Arbitragem 2º Tempo
													</th>
												</tr>
											</thead>
											<tbody class="arbitragem-2">
											</tbody>                    
										</table>
									</div>
									<div class="container-sumula resultado">
										<table class="sumula-gols">
											<thead>
												<tr>
													<th colspan="3">
														Gols
													</th>
												</tr>
												<tr>
													<th>Marcador</th>
													<th>1º Tempo</th>
													<th>2º Tempo</th>
												</tr>
											</thead>
											<tbody class="gols">

											</tbody>                    
										</table>                

										<table class="sumula-penaltis">
											<thead>
												<tr>
													<th colspan="2">
														Cobranças de Penaltis
													</th>
												</tr>
												<tr>
													<th>Marcador</th>
													<th>Time</th>
													<th>Convertidos</th>
													<th>Perdidos</th>
												</tr>
											</thead>
											<tbody class="penaltis">
												
											</tbody>                    
										</table>

										<table class="sumula-complementares">
											<thead>
												<tr>
													<th colspan="2">
														Informações Complementares
													</th>
												</tr>
											</thead>
											<tbody class="observacoes">
												
											</tbody>                    
										</table>
									</div>
									<div class="container-sumula cartoes">
										<table class="sumula-cartoes">
											<thead>
												<tr>
													<th colspan="4" class="sumula-title">Cartões</th>
												</tr>
												<tr>
													<th>
														Jogador
													</th>
													<th>
														Amarelo
													</th>
													<th>
														Azul
													</th>
													<th>
														Vermelho
													</th>
												</tr>
											</thead>
											<tbody class="jogador-cartoes">
											
											</tbody>                    
										</table>
									</div>
								</div>
							</div>
						</div>
					</form>
				</section>
				<input type="submit" name="ranking" id="ranking" class="btn" value="Mostrar Rankings"/>
				<hr class="major"/>	
				<div class="row box rankings" style="display: none;">
					<table id="user_data" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="2%">ID</th>
								<th width="12%">jogador</th>
								<th width="8%">Cartões Amarelos</th>
								<th width="8%">Cartões Azuis</th>
								<th width="8%">Cartões Vermelhos</th>
								<th width="10%"><strong>Cartões Recebidos</strong></th>
								<th width="8%">Gols 1º Tempo</th>
								<th width="8%">Gols 2º Tempo</th>
								<th width="10%"><strong>Gols Marcados</strong></th>
								<th width="8%">Penaltis Convertidos</th>
								<th width="8%">Penaltis Perdidos</th>
								<th width="10%"><strong>Penaltis batidos</strong></th>
							</tr>
						</thead>
					</table>
				</div>
				
				<!-- <div class="modal-tabelinha-hidden">
					<div class="fecha-modal"> X Fechar </div>
					<section class="tabelinha-nova">		
					</section>
				</div> -->
			</div>
		</div>
		<?
		include('../new_menu.php');
		include('../libs.php');
		?>
	</div>
	<script>
		$(document).ready(function(){
			$('.fecha-modal').on('click', function(){
				$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
				$('.dados_titulo').remove();
				$('.dados_jogo').remove();
				$('body').css('overflow','auto');
			});
			// var collapsedGroups = {};
		$('#ranking').on('click', function(e){
			e.preventDefault();
			$('.rankings').toggle('fast');
			if($(this).val() == 'Ocultar Rankings'){
				$(this).val('Mostrar Rankings');
			}else{
				$(this).val('Ocultar Rankings');
			}
		})

			var dataTable = $('#user_data').DataTable({
				
				"processing":true,
				"serverSide":true,
				"responsive": {
					orthogonal: 'responsive'
				},
				"order":[[1, 'desc']],
				"ajax":{
					url:"fetch.php",
					type:"POST"
				},
				"columnDefs":[
					{
						"targets": 0,
						"orderable": false,
					},
				],
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
				},
			});

			
			$(document).on('submit', '#team_form', function(e){
				e.preventDefault();
				var user_id = $("#CodigoSumula").val();
				console.log('user_id:', user_id);
				var dados;
				$.ajax({
					url:"fetch_single.php",
					method:"POST",
					data:{user_id:user_id},
					dataType:"json",
					success:function(data)
					{
						console.log("dados: ", data);
						dados = data;
						
						let infos = dados;
						let tabelinha1  = "";
						let tabelinha2  = "";			
						let gols1 = "" ;
						let penaltis = "";
						let cartoes = "";

						$("#sumula_jogo").html(infos.cabecalho[0].sumula_jogo);
						let arbitragem1 = `
								<tr>
									<td>Árbitro</td><td>${infos.cabecalho[0].arbitro}</td>
								</tr>
								<tr>
									<td>Auxiliar 1</td><td>${infos.cabecalho[0].auxiliar_1}</td>
								</tr>
								<tr>
									<td>Auxiliar 2</td><td>${infos.cabecalho[0].auxiliar_2}</td>
								</tr>
						`;
						let arbitragem2 = `
								<tr>
									<td>Árbitro</td><td>${infos.cabecalho[0].arbitro_2}</td>
								</tr>
								<tr>
									<td>Auxiliar 1</td><td>${infos.cabecalho[0].auxiliar_3}</td>
								</tr>
								<tr>
									<td>Auxiliar 2</td><td>${infos.cabecalho[0].auxiliar_4}</td>
								</tr>
						`;

						let observacoes = `
								<tr>
									<td>Presenças</td><td>${infos.cabecalho[0].presencas_obs}</td>
								</tr>
								<tr>
									<td>Punições</td><td>${infos.cabecalho[0].punicoes_obs}</td>
								</tr>
								<tr>
									<td>Destaques do Jogo</td><td>${infos.cabecalho[0].destaques_obs}</td>
								</tr>
								<tr>
									<td>Observações</td><td>${infos.cabecalho[0].observacoes_obs}</td>
								</tr>
						`;
						for (let i = 0; i < infos.times[0].length; i++){
							var posicao = i+1;
							if(posicao == 1){
								posicao = 'Goleiro'
							};
							tabelinha1 +=
							`
								<tr>
									<td>${posicao}</td><td>${infos.times[0][i]}</td><td>${infos.times[1][i]}</td><td>${posicao}</td>
								</tr>
							`
							tabelinha2 +=
							`
								<tr>
									<td>${posicao}</td><td>${infos.times[2][i]}</td><td>${infos.times[3][i]}</td><td>${posicao}</td>
								</tr>
							`
						}
						
						for (let i = 0; i < infos.gols.length; i++){
								var marcador = infos.gols[i].gols_jogador;
								var golsPrimeiroTempo = infos.gols[i].gols_primeiro;
								if (golsPrimeiroTempo < 1){
									golsPrimeiroTempo = "0";
								}
								var golsSegundoTempo = infos.gols[i].gols_segundo;
								if (golsSegundoTempo < 1){
									golsSegundoTempo = "0";
								}
							gols1 += `
								<tr>
									<td>${marcador}</td><td>${golsPrimeiroTempo}</td><td>${golsSegundoTempo}</td>
								</tr>
							`
						}
						
						for (let i = 0; i < infos.penaltis.length; i++){
								var batedor = infos.penaltis[i].penaltis_jogador;
								var time = infos.penaltis[i].penaltis_jogador_time;
								if(time = 1){
									time = "Amarelo";
								}else{
									time = "Azul";
								}
								var golsConvertidos = infos.penaltis[i].convertidos;
								var golsPerdidos = infos.penaltis[i].perdidos;
								
							penaltis += `
								<tr>
									<td>${batedor}</td><td>${time}</td><td>${golsConvertidos}</td><td>${golsPerdidos}</td>
								</tr>
							`
						}
						for (let i = 0; i < infos.cartoes.length; i++){
								var penalizado = infos.cartoes[i].cartoes_jogador;
								var cartoesAmarelo = infos.cartoes[i].cartoes_amarelo;
								if (cartoesAmarelo < 1){
									cartoesAmarelo = "0";
								}
								var cartoesAzul = infos.cartoes[i].cartoes_azul;
								if (cartoesAzul < 1){
									cartoesAzul = "0";
								}
								var cartoesVermelho = infos.cartoes[i].cartoes_vermelho;
								if (cartoesVermelho < 1){
									cartoesVermelho = "0";
								}
							cartoes += `
								<tr>
									<td>${penalizado}</td><td>${cartoesAmarelo}</td><td>${cartoesAzul}</td><td>${cartoesVermelho}</td>
								</tr>
							`
						}
						
						$('.listagem-primeiro-tempo').html(tabelinha1);
						$('.listagem-segundo-tempo').html(tabelinha2);
						$('.arbitragem-1').html(arbitragem1);			
						$('.arbitragem-2').html(arbitragem2);
						$('.gols').html(gols1);
						$('.penaltis').html(penaltis);
						$('.observacoes').html(observacoes);
						$('.jogador-cartoes').html(cartoes);
						// $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
						// $('body').css('overflow','hidden');
					
					}
				})
			// 	setTimeout(function(){}, 800);
			// });
			});
		});
	</script>
	
</body>
</html>