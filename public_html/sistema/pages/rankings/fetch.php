<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
$query .= "SELECT 
				Cadastros.id as jogador_cod,
				Cadastros.nome as jogador_nome,

				sum(cartoes.amarelo) as cartoes_amarelo,
				sum(cartoes.azul) cartoes_azul,
				sum(cartoes.vermelho) cartoes_vermelho,
				(sum(cartoes.amarelo) + sum(cartoes.azul) + sum(cartoes.vermelho)) as cartoes_recebidos,

				sum(gols.primeiro_tempo) as gols_primeiro,
				sum(gols.segundo_tempo) as gols_segundo,
				(sum(gols.primeiro_tempo) + sum(gols.segundo_tempo)) as gols_marcados,

				sum(penaltis.convertidos) as penaltis_convertidos,
				sum(penaltis.perdidos) as penaltis_perdidos,
				(sum(penaltis.convertidos) + sum(penaltis.perdidos)) as penaltis_batidos

				FROM Cadastros

				LEFT JOIN Cartoes as cartoes ON cartoes.jogador = Cadastros.id
				LEFT JOIN Gols as gols ON gols.jogador = Cadastros.id
				LEFT JOIN Penaltis as penaltis on penaltis.jogador = Cadastros.id ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE Cadastros.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'GROUP BY jogador_nome ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
	// echo "<script> alert('".$query."')</script>";
}
else
{
	$query .= 'GROUP BY jogador_nome ORDER BY jogador_cod ASC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();

foreach($result as $row){
	
	if($row["cartoes_amarelo"] == null){
		$row["cartoes_amarelo"] = "0";
	}
	if($row["cartoes_azul"] == null){
		$row["cartoes_azul"] = "0";
	}
	if($row["cartoes_vermelho"] == null){
		$row["cartoes_vermelho"] = "0";
	}
	if($row["cartoes_recebidos"] == null){
		$row["cartoes_recebidos"] = "0";
	}
	if($row["gols_primeiro"] == null){
		$row["gols_primeiro"] = "0";
	}
	if($row["gols_segundo"] == null){
		$row["gols_segundo"] = "0";
	}
	if($row["gols_marcados"] == null){
		$row["gols_marcados"] = "0";
	}
	if($row["penaltis_convertidos"] == null){
		$row["penaltis_convertidos"] = "0";
	}
	if($row["penaltis_perdidos"] == null){
		$row["penaltis_perdidos"] = "0";
	}
	if($row["penaltis_batidos"] == null){
		$row["penaltis_batidos"] = "0";
	}

	$sub_array = array();	
	$sub_array[] = $row["jogador_cod"];	
	$sub_array[] = $row["jogador_nome"];
	$sub_array[] = $row["cartoes_amarelo"];
	$sub_array[] = $row["cartoes_azul"];
	$sub_array[] = $row["cartoes_vermelho"];
	$sub_array[] = $row["cartoes_recebidos"];
	$sub_array[] = $row["gols_primeiro"];
	$sub_array[] = $row["gols_segundo"];
	$sub_array[] = $row["gols_marcados"];
	$sub_array[] = $row["penaltis_convertidos"];
	$sub_array[] = $row["penaltis_perdidos"];
	$sub_array[] = $row["penaltis_batidos"];
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
// echo "<pre>";
echo json_encode($output);
?>