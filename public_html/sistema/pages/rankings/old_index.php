
<style>
tr.dtrg-start{
	cursor: pointer;
}
.modal-tabelinha-hidden{
	width: 0;
	height: 0;
	opacity: 0;
}
.modal-tabelinha{
	/* position: fixed; */
	/* top: 0;
	left: 0; */
	display: block;
	margin: 0 auto;
	width: 100%;
	height: auto;
	opacity: 1;
	/* background-color: rgba(0, 0, 0, 0.8); */
}
/* .tabelinha-nova{
	width: 80%;
	padding: 20px;
	margin: 10% auto;
	background-color: #fff;
} */
.fecha-modal{
	position: absolute;
	right: 10px;
	top: 10px;
	color: #fff;
	font-weight: bold;
	font-size: 15px;
	cursor: pointer;
	text-transform: uppercase;
}
.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
   display: grid; 
   grid-template-columns: 6% 6% 30% 14% 14% 14% 15%;
   padding: 5px;
}

.tabelinha-nova .dados_titulo{
	font-weight: bold;
	background-color: #fdfdfd;
}
.tabelinha-nova .dados_jogo{
	border: 1px solid;
	font-size: 13px;
	margin-top: 1px;	
}
.tabelinha-nova .dados_jogo button{
	border: none;
	background-color: #fff;

}
</style>


	<div class="container box">
	<?
	include('sumula.php');
	?>
		<table id="user_data" class="table table-bordered table-striped">
			<thead>
				<tr>
				<th width="2%">ID</th>
					<th width="12%">jogador</th>
					<th width="8%">Cartões Amarelos</th>
					<th width="8%">Cartões Azuis</th>
					<th width="8%">Cartões Vermelhos</th>
					<th width="10%"><strong>Cartões Recebidos</strong></th>
					<th width="8%">Gols 1º Tempo</th>
					<th width="8%">Gols 2º Tempo</th>
					<th width="10%"><strong>Gols Marcados</strong></th>
					<th width="8%">Penaltis Convertidos</th>
					<th width="8%">Penaltis Perdidos</th>
					<th width="10%"><strong>Penaltis batidos</strong></th>
				</tr>
			</thead>
		</table>
	</div>
	

<script type="text/javascript" language="javascript" >
$(document).ready(function(){

	$('.fecha-modal').on('click', function(){
		$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.dados_jogo').remove();
		$('body').css('overflow','auto');
	});
	// var collapsedGroups = {};
	
	var dataTable = $('#user_data').DataTable({
		
		"processing":true,
		"serverSide":true,
		"pageLength": 6,
		// "responsive": true,		
		"dom": 'Bfrtip',
		"lengthMenu": [
            [ 10, 25, 50, 500],
            [ '10 linhas', '25 linhas', '50 linhas', 'Todas' ]
        ],
		"buttons": [{extend: 'pageLength', text: 'Mostrar'}, {extend: 'excel', text: 'Exportar para planilha'}], 
		"order":[],
		"ajax":{
			url:"rankings/fetch.php",
			type:"post"
		},
		"columnDefs":[
			{
				"targets": [0, 1],
				"orderable": false,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});


	$(document).on('click', '.submit', function(){
		var user_id = $("#CodigoSumula").val();
		console.log('user_id:', user_id);
		var dados;
		$.ajax({
			url:"rankings/fetch_single.php",
			method:"POST",
			data:{user_id:user_id},
			dataType:"json",
			success:function(data)
			{
				console.log("dados: ", data);
				dados = data;
			}
		})
		setTimeout(function(){
			let infos = dados;
			let tabelinha1  = "";
			let tabelinha2  = "";			
			let gols1 = "" ;
			let penaltis = "";
			let cartoes = "";

			$("#sumula_jogo").html(infos.cabecalho[0].sumula_jogo);
			let arbitragem1 = `
					<tr>
						<td>Árbitro</td><td>${infos.cabecalho[0].arbitro}</td>
					</tr>
					<tr>
						<td>Auxiliar 1</td><td>${infos.cabecalho[0].auxiliar_1}</td>
					</tr>
					<tr>
						<td>Auxiliar 2</td><td>${infos.cabecalho[0].auxiliar_2}</td>
					</tr>
			`;
			let arbitragem2 = `
					<tr>
						<td>Árbitro</td><td>${infos.cabecalho[0].arbitro_2}</td>
					</tr>
					<tr>
						<td>Auxiliar 1</td><td>${infos.cabecalho[0].auxiliar_3}</td>
					</tr>
					<tr>
						<td>Auxiliar 2</td><td>${infos.cabecalho[0].auxiliar_4}</td>
					</tr>
			`;

			let observacoes = `
					<tr>
						<td>Presenças</td><td>${infos.cabecalho[0].presencas_obs}</td>
					</tr>
					<tr>
						<td>Punições</td><td>${infos.cabecalho[0].punicoes_obs}</td>
					</tr>
					<tr>
						<td>Destaques do Jogo</td><td>${infos.cabecalho[0].destaques_obs}</td>
					</tr>
					<tr>
						<td>Observações</td><td>${infos.cabecalho[0].observacoes_obs}</td>
					</tr>
			`;
			for (let i = 0; i < infos.times[0].length; i++){
				var posicao = i+1;
				if(posicao == 1){
					posicao = 'Goleiro'
				};
				tabelinha1 +=
				`
					<tr>
						<td>${posicao}</td><td>${infos.times[0][i]}</td><td>${infos.times[1][i]}</td><td>${posicao}</td>
					</tr>
				`
				tabelinha2 +=
				`
					<tr>
						<td>${posicao}</td><td>${infos.times[2][i]}</td><td>${infos.times[3][i]}</td><td>${posicao}</td>
					</tr>
				`
			}
			
			for (let i = 0; i < infos.gols.length; i++){
					var marcador = infos.gols[i].gols_jogador;
					var golsPrimeiroTempo = infos.gols[i].gols_primeiro;
					if (golsPrimeiroTempo < 1){
						golsPrimeiroTempo = "0";
					}
					var golsSegundoTempo = infos.gols[i].gols_segundo;
					if (golsSegundoTempo < 1){
						golsSegundoTempo = "0";
					}
				gols1 += `
					<tr>
						<td>${marcador}</td><td>${golsPrimeiroTempo}</td><td>${golsSegundoTempo}</td>
					</tr>
				`
			}
			
			for (let i = 0; i < infos.penaltis.length; i++){
					var batedor = infos.penaltis[i].penaltis_jogador;
					var time = infos.penaltis[i].penaltis_jogador_time;
					if(time = 1){
						time = "Amarelo";
					}else{
						time = "Azul";
					}
					var golsConvertidos = infos.penaltis[i].convertidos;
					var golsPerdidos = infos.penaltis[i].perdidos;
					
				penaltis += `
					<tr>
						<td>${batedor}</td><td>${time}</td><td>${golsConvertidos}</td><td>${golsPerdidos}</td>
					</tr>
				`
			}
			for (let i = 0; i < infos.cartoes.length; i++){
					var penalizado = infos.cartoes[i].cartoes_jogador;
					var cartoesAmarelo = infos.cartoes[i].cartoes_amarelo;
					if (cartoesAmarelo < 1){
						cartoesAmarelo = "0";
					}
					var cartoesAzul = infos.cartoes[i].cartoes_azul;
					if (cartoesAzul < 1){
						cartoesAzul = "0";
					}
					var cartoesVermelho = infos.cartoes[i].cartoes_vermelho;
					if (cartoesVermelho < 1){
						cartoesVermelho = "0";
					}
				cartoes += `
					<tr>
						<td>${penalizado}</td><td>${cartoesAmarelo}</td><td>${cartoesAzul}</td><td>${cartoesVermelho}</td>
					</tr>
				`
			}
			
			$('.listagem-primeiro-tempo').html(tabelinha1);
			$('.listagem-segundo-tempo').html(tabelinha2);
			$('.arbitragem-1').html(arbitragem1);			
			$('.arbitragem-2').html(arbitragem2);
			$('.gols').html(gols1);
			$('.penaltis').html(penaltis);
			$('.observacoes').html(observacoes);
			$('.jogador-cartoes').html(cartoes);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			// $('body').css('overflow','hidden');
		}, 800);
	});
	
	$(document).on('click', '.excluir', function(){
		var user_id = $(this).attr("id");
		if(confirm("Você quer excluir esse registro?"))
		{
			$.ajax({
				url:"rankings/delete.php",
				method:"POST",
				data:{user_id:user_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('.dados_jogo').remove();
					$('body').css('overflow','auto');
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>