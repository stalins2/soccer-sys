<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .carregar-sumula input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        .carregar-sumula .submit{
            width: 180px;
            height: 35px;
            line-height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            text-align: center;
            cursor: pointer;
            margin-bottom: 50px;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .carregar-sumula table{
            text-align: left !important;
        }
        .entry:not(:first-of-type)
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }
        .textareas textarea{
            font-size: 12px;
            line-height: 24px;           
        }
      

    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Visualização dos Rankings</h1>
                </div>
                <div class="container-fluid" style="width: 98%;">
                    <form class="carregar-sumula" id="carregar_sumula" name="carregar_sumula"  method="post" action="rankings.php?acao=exibir">                    
                        <!-- Seleção da súmula -->
                        <div class="row">
                            <table class="col table text-center field-wrapper">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"> Selecionar Jogo (Súmula) - Código/Data</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- código da súmula a ser escolhida -->
                            <div class="container-fluid lista-dados">
                                <div class="form-group row col-sm-12 lista-dados__infos" style="border: none;">
                                    <?=listaSumulas();?>
                                </div>                       
                            </div>
                        </div>
                        <!-- Fim da seleção da súmula -->
                        
                        <div class="row text-light bg-dark text-left">
                            <div class="col-sm-12"> 
                            </div> 
                            
                        </div>
                        <div class="submit cadastrar-sumula" id="submit">Visualizar Súmula</div>
                    </form>
                                                
                        <? include("rankings/index.php");?>
                        
                </div>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
        
        <script type="text/javascript">
            $(document).ready(function(){
                $('.textareas div textarea').on('keyup', function(){
                        var contando = $(this).parent().find('textarea').val().length;   
                        console.log('contando');
                        if (contando >= 500) {
                            $(this).parent().find('#charNum').text('*Limite Atingido');                            
                        } else {
                            $(this).parent().find('#charNum').text(500 - contando +' Caractere(s) restante(s)');
                        }                    
                });
            });
        </script>
    </body>
</html>