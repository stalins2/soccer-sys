<div class="modal-tabelinha-hidden">
		<section class="tabelinha-nova">
        <link rel="stylesheet" href="assets/css/sumula.css">    
         <div class="container">
            <div class="container-sumula">
                <table class="sumula-escalacao">
                    <thead>
                        <tr>
                            <th colspan="4">Sumula Pesquisada <span id="sumula_jogo"></span></th>
                        </tr>
                        <tr>
                            <th>Posição</th>
                            <th>
                                Amarelo
                            </th>
                            <th>
                                Azul
                            </th>
                            <th>Posição</th>
                        </tr>
                    </thead>
                    <tbody class="listagem-primeiro-tempo">
                        
                    </tbody>
                </table>
            </div>
            <div class="container-sumula">
                <table class="sumula-escalacao">
                    <thead>
                        <tr>
                            <th colspan="4">Segundo Tempo</th>
                        </tr>
                    </thead>
                    <tbody class="listagem-segundo-tempo">
                        
                    </tbody>
                </table>
            </div>
            <div class="detalhes-1">
                <div class="container-sumula arbitragem">
                    <table class="sumula-arbitragem first">
                        <thead>
                            <th colspan="2">
                                Arbitragem 1º Tempo
                            </th>
                        </thead>
                        <tbody class="arbitragem-1">

                        </tbody>                    
                    </table>
                    <table class="sumula-arbitragem second">
                            <thead>
                                <tr>
                                    <th colspan="2">
                                        Arbitragem 2º Tempo
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="arbitragem-2">
                            </tbody>                    
                        </table>
                </div>
                <div class="container-sumula resultado">
                    <table class="sumula-gols">
                        <thead>
                            <tr>
                                <th colspan="3">
                                    Gols
                                </th>
                            </tr>
                            <tr>
                                <th>Marcador</th>
                                <th>1º Tempo</th>
                                <th>2º Tempo</th>
                            </tr>
                        </thead>
                        <tbody class="gols">

                        </tbody>                    
                    </table>                

                    <table class="sumula-penaltis">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    Cobranças de Penaltis
                                </th>
                            </tr>
                            <tr>
                                <th>Marcador</th>
                                <th>Time</th>
                                <th>Convertidos</th>
                                <th>Perdidos</th>
                            </tr>
                        </thead>
                        <tbody class="penaltis">
                            
                        </tbody>                    
                    </table>

                    <table class="sumula-complementares">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    Informações Complementares
                                </th>
                            </tr>
                        </thead>
                        <tbody class="observacoes">
                            
                        </tbody>                    
                    </table>
                </div>
                <div class="container-sumula cartoes">
                        <table class="sumula-cartoes">
                            <thead>
                                <tr>
                                    <th colspan="4" class="sumula-title">Cartões</th>
                                </tr>
                                <tr>
                                    <th>
                                        Jogador
                                    </th>
                                    <th>
                                        Amarelo
                                    </th>
                                    <th>
                                        Azul
                                    </th>
                                    <th>
                                        Vermelho
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="jogador-cartoes">
                               
                            </tbody>                    
                        </table>
                    </div>
            </div>
        </div>
        <script src="pages/assets/js/script_sumula.js"></script>
    
        </section>
	</div>