<?
require_once('../assets/includes/validador_acesso.php');
include('../header_loaders.php');
include('../new_lists.php');
?>
<html>

<head>
	<title> Sistema - Fohfaoi</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="../assets/css/new_view.css" />
	<style>
		tr.dtrg-start {
			cursor: pointer;
		}

		.modal-tabelinha-hidden{
			width: 0;
			height: 0;
			opacity: 0;
		}
		.modal-tabelinha{
			position: fixed;
			top: 0;
			left: 0;
			display: block;
			margin: 0 auto;
			width: 100vw;
			height: 100vh;
			overflow: auto;
			z-index: 9999;
			opacity: 1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		.tabelinha-nova{
			width: 90%;
			height: auto;
			padding: 20px;
			margin: 5% auto;
			background-color: #fff;
		}
		.fecha-modal{
			position: absolute;
			right: 10px;
			top: 10px;
			color: #fff;
			font-weight: bold;
			font-size: 15px;
			cursor: pointer;
			text-transform: uppercase;
		}
		.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
			display: grid; 
			grid-template-columns: 6% 6% 30% 14% 14% 14% 15%;
			grid-gap: 0px;
			padding: 10px;
			text-align: center;
		}
		.titulos-cartoes{
			display: grid;
			grid-template-columns: 25%;
		}
		.tabelinha-nova .dados_titulo {
			font-weight: bold;
			background-color: #fdfdfd;
		}

		.tabelinha-nova .dados_jogo div, .tabelinha-nova .dados_titulo div, {
			padding: 10px;
		}
		.tabelinha-nova .dados_jogo .excluir{
			width: 100%;
			padding: 0;
		}

		.tabelinha-nova .dados_jogo button {
			border: none;
			background-color: #fff;

		}
		.update{
			position: relative;
		}
		.campo-temporario-before:before {
			content: 'Detalhes Acima';
			padding: 8px;
			font-size: 11px;
			text-transform: uppercase;
			position: absolute;
			top: -60px;
			white-space: pre-wrap;
			border-radius: 6px;
			background: #000;
			color: #fff;
		}
		
		/* .lista-time-1, .lista-time-2, .lista-time-3, .lista-time-4, .tabela-time1, .tabela-time2, .tabela-time3, .tabela-time4{ */
			/* .lista-time-1, .lista-time-2, .lista-time-3, .lista-time-4{ */
			/* display: none;		 */
			/* overflow: scroll; */
		/* } */
	</style>
</head>

<body class="is-preload">
	<!-- Wrapper -->
	<div id="wrapper">
		<!-- Main -->
		<div id="main" class="conteudo-page">
			<div class="inner">
				<!-- Header -->
				<form method="post" id="team_form" name="team_form" enctype="multipart/form-data">
				<!-- <form id="team_form" name="team_form" method="post" action="../controllers_new/cadastro_cartoes_controller.php?acao=cadastrar"> -->
					<header id="header">
						<a href="#" class="logo"><strong>Cadastrar </strong>Cartões</a>
						<?=newListaSu();?>
					</header>
					<!-- Content -->
					<div class="row gtr-200">	
						<div class="d-flex">
							<div class="row">
								<div class="row container-fluid d-flex no-wrap">
									<div class="row container-fluid">                          
										<div class="selects-cartoes form-group row container-fluid">&nbsp;</div>
										<br/>
									</div> 
								</div>
								<div class="row container-fluid">
									<div>		
									<br/>			
										<div class="button primary adiciona-jogador">Adicionar Jogador</div> <br/><br/>
										<input type="hidden" name="operation" id="operation" />
										<input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
										<input type="submit" name="action" id="action" class="btn" value="Cadastrar Cartões"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			
				<hr class="major"/>	
					<div class="row gtr-200">
						<div class="col-12 d-flex">
							<div class="container-fluid">
								<table id="user_data" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="5%">ID</th>
											<th width="5%">Sumula</th>
											<th width="20%">Jogador</th>
											<th width="20%">Amarelo</th>
											<th width="20%">Azul</th>
											<th width="20%">Vermelho</th>
											<th width="10%">Alterar</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				<div class="modal-tabelinha-hidden">
					<div class="fecha-modal"> X Fechar </div>
					<section class="tabelinha-nova">		
					</section>
				</div>
			</div>
		</div>
		<?
		include('../new_menu.php');
		include('../libs.php');
		?>
	</div>
	<script>
		$(document).ready(function(){

			var nm_id = 1; //ID inicial para as divs
                
			$('.adiciona-jogador').on('click', function(){
				if(nm_id <=10){
					$('.selects-cartoes').append(criarDiv('div_container_'+nm_id,'jogador-adicionado row col-sm-12', 'margin-top: 10px;'));  
					$('#div_container_'+nm_id).append(criarDivSelect('cartao_jogador_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'), 
					selecionar10('Cart&atilde;o(es)', 'Amarelo','amarelo_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'), 
					selecionar10('Cart&atilde;o(es)', 'Azul','azul_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'), 
					selecionar10('Cart&atilde;o(es)', 'Vermelho','vermelho_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'));
					nm_id++;                        
				}else{
					alert('Você atingiu o limite de jogadores para cadastrar por vez.'); 
				}
			});

			$('.fecha-modal').on('click', function(){
				$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
				$('.dados_titulo').remove();
				$('.dados_jogo').remove();
				$('body').css('overflow','auto');
			});
			var collapsedGroups = {};
	
			var dataTable = $('#user_data').DataTable({
				
				"processing":true,
				"serverSide":true,
				"order":[[1, 'asc']],
				// "order":[],
				// "rowGroup": {
				// 	dataSrc: 1,
				// 	startRender: function(rows, group) {
				// 		var collapsed = !!collapsedGroups[group];

				// 		rows.nodes().each(function (r) {
				// 		r.style.display = 'none';
				// 		if (collapsed) {
				// 			r.style.display = '';
				// 		}});

				// 		// Add category name to the <tr>. NOTE: Hardcoded colspan
				// 		return $('<tr/>')
				// 		.append('<td colspan="8"> Súmula ' + group + ' (' + rows.count() + ' Penalizado(s))</td>')
				// 		.attr('data-name', group)
				// 		.toggleClass('collapsed', collapsed);
				// 	}
				// },
				"ajax":{
					url:"fetch.php",
					type:"POST"
				},
				"columnDefs":[
					{
						"targets": 0,
						"orderable": false,
					},
				],
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
				},
			});
			// $('#user_data tbody').on('click', 'tr.dtrg-start', function() {
			// 	var name = $(this).data('name');
			// 	collapsedGroups[name] = !collapsedGroups[name];
			// 	dataTable.draw(false);
			// });

			
			$(document).on('submit', '#team_form', function(event){
				$('#operation').val("cadastrar");	
				event.preventDefault();
				var formdata = new FormData($("form[name='team_form']")[0]);
				
				if(confirm("Você confirma o cadastro dos dados preenchidos?")){
					$.ajax({
						url:"../controllers_new/cadastro_cartoes_controller.php?acao=cadastrar",
						type:'POST',
						data: formdata,
						contentType:false,
						processData:false,
						success:function(data){
							console.log("Cartoes: ", formdata);
							alert(data);
							$('#team_form')[0].reset();
							dataTable.ajax.reload();
						}
					});
				}
			});
			$('#user_data tbody').on('click', 'tr.dtrg-start', function() {
				var name = $(this).data('name');
				collapsedGroups[name] = !collapsedGroups[name];
				dataTable.draw(false);
			});


			$(document).on('click', '.update', function(){
				var user_id = $(this).attr("id");
				console.log('user_id:', user_id);
				var dados;
				$.ajax({
					url:"fetch_single.php",
					method:"POST",
					data:{user_id:user_id},
					dataType:"json",
					success:function(data)
					{
						console.log("dados: ", data);
						dados = data;
					}
				})
				setTimeout(function(){
					let infos = dados;
					let tabelinha  = `
						<div class="dados_titulo">
							<div> ID </div>
							<div> SUMULA </div>
							<div> JOGADOR </div>
							<div> AMARELO </div>
							<div> AZUL </div>
							<div> VERMELHO </div>
							<div> EXCLUIR </div>
						</div>
					`;		
					for (let i = 0; i < infos.length; i++){
						tabelinha +=
						`<div class="dados_jogo">
							<div> ${infos[i].id} </div>
							<div> ${infos[i].sumula} </div>
							<div> ${infos[i].jogador} </div>
							<div> ${infos[i].amarelo} </div>
							<div> ${infos[i].azul} </div>
							<div> ${infos[i].vermelho} </div>
							<div>
								<button class="excluir" name="excluir" id="${infos[i].id}" value="${infos[i].id}"> Excluir </button>
							</div>
						</div>
						`
					}
					$('.tabelinha-nova').append(tabelinha);
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('body').css('overflow','hidden');
				}, 800);
			});

			$(document).on('click', '.excluir', function(){
				var user_id = $(this).attr("id");
				if(confirm("Você quer excluir esse registro?"))
				{
					$.ajax({
						url:"delete.php",
						method:"POST",
						data:{user_id:user_id},
						success:function(data)
						{
							alert(data);
							dataTable.ajax.reload();
							$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
							$('.dados_titulo').remove();
							$('.dados_jogo').remove();
							$('body').css('overflow','auto');
						}
					});
				}
				else
				{
					return false;	
				}
			});


			});
	</script>
	<?
	include('../new_scripts.php');
	?>
	
	
</body>
</html>