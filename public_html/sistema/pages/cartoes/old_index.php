
<style>
tr.dtrg-start{
	cursor: pointer;
}
.modal-tabelinha-hidden{
	width: 0;
	height: 0;
	opacity: 0;
}
.modal-tabelinha{
	position: fixed;
	top: 0;
	left: 0;
	display: block;
	margin: 0 auto;
	width: 100vw;
	height: 100vh;
	opacity: 1;
	background-color: rgba(0, 0, 0, 0.8);
}
.tabelinha-nova{
	width: 80%;
	padding: 20px;
	margin: 10% auto;
	background-color: #fff;
}
.fecha-modal{
	position: absolute;
	right: 10px;
	top: 10px;
	color: #fff;
	font-weight: bold;
	font-size: 15px;
	cursor: pointer;
	text-transform: uppercase;
}
.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
   display: grid; 
   grid-template-columns: 6% 6% 30% 14% 14% 14% 15%;
   padding: 5px;
}

.tabelinha-nova .dados_titulo{
	font-weight: bold;
	background-color: #fdfdfd;
}
.tabelinha-nova .dados_jogo{
	border: 1px solid;
	font-size: 13px;
	margin-top: 1px;	
}
.tabelinha-nova .dados_jogo button{
	border: none;
	background-color: #fff;

}
</style>
	<div class="container box">
		<table id="user_data" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th width="5%">Sumula</th>
					<th width="20%">Jogador</th>
					<th width="20%">Amarelo</th>
					<th width="20%">Azul</th>
					<th width="20%">Vermelho</th>
					<th width="10%">Alterar</th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="modal-tabelinha-hidden">
		<section class="tabelinha-nova">
			<div class="fecha-modal"> X Fechar </div>
			<div class="dados_titulo">
				<div> ID </div>
				<div> SUMULA </div>
				<div> JOGADOR </div>
				<div> AMARELO </div>
				<div> AZUL </div>
				<div> VERMELHO </div>
				<div> EXCLUIR </div>
			</div>
		</section>
	</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){

	$('.fecha-modal').on('click', function(){
		$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.dados_jogo').remove();
		$('body').css('overflow','auto');
	});
	var collapsedGroups = {};
	
	var dataTable = $('#user_data').DataTable({
		
		"processing":true,
		"serverSide":true,
		"order":[[1, 'asc']],
		// "order":[],
		"rowGroup": {
            dataSrc: 1,
			startRender: function(rows, group) {
				var collapsed = !!collapsedGroups[group];

				rows.nodes().each(function (r) {
				r.style.display = 'none';
				if (collapsed) {
					r.style.display = '';
				}});

				// Add category name to the <tr>. NOTE: Hardcoded colspan
				return $('<tr/>')
				.append('<td colspan="8"> Súmula ' + group + ' (' + rows.count() + ' Penalizado(s))</td>')
				.attr('data-name', group)
				.toggleClass('collapsed', collapsed);
			}
        },
		"ajax":{
			url:"cartoes/fetch.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": '_all',
				"orderable": true,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});
	$('#user_data tbody').on('click', 'tr.dtrg-start', function() {
		var name = $(this).data('name');
		collapsedGroups[name] = !collapsedGroups[name];
		dataTable.draw(false);
	});


	$(document).on('click', '.update', function(){
		var user_id = $(this).attr("id");
		console.log('user_id:', user_id);
		var dados;
		$.ajax({
			url:"cartoes/fetch_single.php",
			method:"POST",
			data:{user_id:user_id},
			dataType:"json",
			success:function(data)
			{
				console.log("dados: ", data);
				dados = data;
			}
		})
		setTimeout(function(){
			let infos = dados;
			let tabelinha  = "";		
			for (let i = 0; i < infos.length; i++){
				tabelinha +=
				`<div class="dados_jogo">
					<div> ${infos[i].id} </div>
					<div> ${infos[i].sumula} </div>
					<div> ${infos[i].jogador} </div>
					<div> ${infos[i].amarelo} </div>
					<div> ${infos[i].azul} </div>
					<div> ${infos[i].vermelho} </div>
					<div>
						<button class="excluir" name="excluir" id="${infos[i].id}" value="${infos[i].id}"> Excluir </button>
					</div>
				</div>
				`
			}
			$('.tabelinha-nova').append(tabelinha);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('body').css('overflow','hidden');
		}, 800);
	});
	
	$(document).on('click', '.excluir', function(){
		var user_id = $(this).attr("id");
		if(confirm("Você quer excluir esse registro?"))
		{
			$.ajax({
				url:"cartoes/delete.php",
				method:"POST",
				data:{user_id:user_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('.dados_jogo').remove();
					$('body').css('overflow','auto');
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>