<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
$query .= "SELECT
			cartoes.id as id, 
			cartoes.sumula_referencia as sumula,
			Cadastros.nome as jogador, 
			cartoes.jogador as jogador_cod, 
			cartoes.amarelo as amarelo, 
			cartoes.azul as azul,
			cartoes.vermelho as vermelho			

			FROM Cartoes as cartoes
			LEFT JOIN Cadastros
			ON Cadastros.id = cartoes.jogador ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE Cadastros.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();

foreach($result as $row){
	
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = $row["sumula"];
	$sub_array[] = $row["jogador"];
	$sub_array[] = $row["amarelo"];
	$sub_array[] = $row["azul"];
	$sub_array[] = $row["vermelho"];	
	$sub_array[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="update">Alterar</button>';
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);

echo json_encode($output);
?>