<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-membros input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        .cadastro-membros .submit{
            width: 150px;
            height: 35px;
            line-height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            margin-bottom: 20px;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .cadastro-membros table{
            text-align: left !important;            
        }
        .lista-dados{
            margin-bottom: 200px;
        }

    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 d-flex text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Cadastro dos Membros</h1>
                </div>
                <div class="container-fluid" style="width: 98%;">
                    <form class="cadastro-membros" id="cadastro_sumula" name="cadastro_sumula" method="post" action="controllers/cadastro_membros_controller.php?acao=cadastrar">
                        <table class="table text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Nome</th>
                                    <th scope="col"> Apelido </th>
                                    <th scope="col"> Padrinho </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <input type="text" id="nome" name="nome" maxlength="50" />
                                    </td>
                                    <td>
                                        <input type="text" id="apelido" name="apelido" maxlength="30" />
                                    </td>
                                    <td>
                                        <input type="text" id="padrinho" name="padrinho" maxlength="30" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Posição </th>
                                    <!-- <th scope="col"> Chute </th> -->
                                    <!-- <th scope="col"> Altura (cm)</th> -->
                                    <!-- <th scope="col"> Peso (kg)</th> -->
                                    <th scope="col"> Data de Nascimento </th>
                                    <th scope="col"> Naturalidade </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <input type="text" id="posicao" name="posicao" maxlength="50" />
                                    </td>
                                    <!-- <td>
                                        <input type="text" id="chute" name="chute" maxlength="30" />
                                    </td>
                                    <td>
                                        <input type="text" id="altura" name="altura" maxlength="20" />
                                    </td>
                                    <td>
                                        <input type="text" id="peso" name="peso" maxlength="20" />
                                    </td> -->
                                    <td scope="row">
                                        <input type="date" id="data_nascimento" name="data_nascimento" maxlength="20" />
                                    </td>
                                    <td>
                                        <input type="text" id="natural_de" name="natural_de" maxlength="20" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Time que Torce</th>
                                    <th scope="col"> Endereço</th>
                                    <th scope="col"> Bairro </th>
                                    <th scope="col"> CEP </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" id="time_que_torce" name="time_que_torce" maxlength="50" />
                                    </td>
                                    <td>
                                        <input type="text" id="endereco" name="endereco" maxlength="50" />
                                    </td>
                                    <td>
                                        <input type="text" id="bairro" name="bairro" maxlength="30" />
                                    </td>
                                    <td>
                                        <input type="text" id="cep" name="cep" class="cep" maxlength="20" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Data de Entrada </th>
                                    <th scope="col"> Data de Saída</th>
                                    <th scope="col"> Status </th>
                                    <!-- <th scope="col"> CPF </th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <input type="date" id="data_entrada" name="data_entrada" maxlength="20" />
                                    </td>
                                    <td>
                                        <input type="date" id="data_saida" name="data_saida" maxlength="30" />
                                    </td>
                                    <td>
                                        <select class="form-control" id="status" name="status" onchange="afastadoDesde();">
                                            <option>Ativo</option>
                                            <option>Inativo</option>
                                            <option value="Afastado">Afastado</option>
                                        </select>
                                        <span id="data_afastado"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Celular</th>
                                    <th scope="col"> E-mail </th>
                                    <th scope="col"> Telefone </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <input type="text" id="celular" name="celular" class="tel" maxlength="50" />
                                    </td>
                                    <td>
                                        <input type="text" id="email" name="email" maxlength="30" />
                                    </td>
                                    <td>
                                        <input type="text" id="telefone" class="tel" name="telefone" maxlength="20" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <input class="submit" type="submit" id="submit" value="Cadastrar Membro" />
                </form>
                <div class="container-fluid lista-dados">
                    <div class="form-group row col-sm-12 lista-dados__titulos">
                        <!-- <label for="IdCadastro" class="col-sm-1 col-form-label">ID de Cadastro</label> -->
                        <label for="Nome" class="col-sm-1 col-form-label">Nome</label>
                        <label for="Apelido" class="col-sm-1 col-form-label">Apelido</label>
                        <label for="DataNascimento" class="col-sm-1 col-form-label">Data de Nascimento</label>
                        <label for="Padrinho" class="col-sm-1 col-form-label">Padrinho</label>
                        <label for="DataEntrada" class="col-sm-1 col-form-label">Data de Entrada</label>
                        <label for="TimeQueTorce" class="col-sm-1 col-form-label">Time que Torce</label>
                        <label for="TelCelular" class="col-sm-1 col-form-label">Tel. Celular</label>
                        <label for="Telefone" class="col-sm-1 col-form-label">Tel. Residencial</label>
                        <label for="Email" class="col-sm-1 col-form-label">Email</label>
                        <label for="Endereco" class="col-sm-1 col-form-label">Endereco</label>
                        <label for="Bairro" class="col-sm-1 col-form-label">Bairro</label>
                        <label for="Editar" class="col-sm-1 col-form-label">
                            <i class="fas fa-edit fa-lg"></i>
                            <i class="fas fa-trash-alt fa-lg float-right"></i>
                        </label>
                    </div>
                    <div class="form-group row col-sm-12 lista-dados__infos">
                <? foreach ($membros as $key => $lista){ ?>
                    <div class="container-fluid row lista-dados__linhas" style="padding:0; margin:0;" id="lista_<?=$lista->id;?>">
                        <!-- <label for="IdCadastro" class="col-sm-1 col-form-label id_num" id="IdCadastro"><//?=$lista->id;?></label> -->
                        <label for="Nome" class="col-sm-1 col-form-label" id="Nome"><?=$lista->nome;?></label>
                        <label for="Apelido" class="col-sm-1 col-form-label" id="Apelido"><?=$lista->apelido;?></label>
                        <label for="DataNascimento" class="col-sm-1 col-form-label" id="DataNascimento"><?=(new DateTime($lista->data_nascimento))->format('d/m/Y');?></label>
                        <label for="Padrinho" class="col-sm-1 col-form-label" id="Chute"><?=$lista->padrinho;?></label>
                        <label for="DataEntrada" class="col-sm-1 col-form-label" id="DataEntrada"><?=(new DateTime($lista->data_entrada))->format('d/m/Y');?></label>
                        <label for="TimeQueTorce" class="col-sm-1 col-form-label" id="TimeQueTorce"><?=$lista->time_que_torce;?></label>
                        <label for="TelCelular" class="col-sm-1 col-form-label" id="TelCelular"><?=$lista->celular;?></label>
                        <label for="Telefone" class="col-sm-1 col-form-label" id="Telefone"><?=$lista->telefone;?></label>
                        <!-- <label for="DiaCadastro" class="col-sm-1 col-form-label" id="DiaCadastro"><?//(new DateTime($lista->data_cadastro))->format('d/m/Y');?></label> -->
                        <label for="Email" class="col-sm-1 col-form-label" style="word-break: break-all;" id="Email"><?=$lista->email;?></label>   
                        <label for="Endereco" class="col-sm-1 col-form-label" id="Endereco"><?=$lista->endereco;?></label>     
                        <label for="Bairro" class="col-sm-1 col-form-label" id="Bairro"><?=$lista->bairro;?></label>  
                        <label for="Editar" class="col-sm-1 col-form-label float-left" id="Editar">
                            <i class="fas fa-edit fa-lg text-info" onclick="editar('<?=$lista->id;?>', '<?=$lista->nome;?>',
                            '<?=$lista->apelido;?>', '<?=$lista->padrinho;?>',
                            '<?=$lista->posicao;?>',  '<?=$lista->data_nascimento;?>',
                            '<?=$lista->natural_de;?>', '<?=$lista->time_que_torce;?>', 
                            '<?=$lista->endereco;?>', '<?=$lista->bairro;?>', 
                            '<?=$lista->cep;?>', '<?=$lista->data_entrada;?>', 
                            '<?=$lista->data_saida;?>', '<?=$lista->status;?>', 
                            '<?=$lista->data_afastamento;?>', '<?=$lista->celular;?>', 
                            '<?=$lista->email;?>', '<?=$lista->telefone;?>');"></i>
                            <i class="fas fa-lg fa-trash-alt text-danger float-right" onclick="confirmar_exclusao(<?=$lista->id;?>)"></i>
                        </label> 
                    </div>                   
                <?}?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('assets/includes/rodape.php');?>
    <script>
    function afastadoDesde(){ 
        let spanAfastado = document.querySelector("#data_afastado");
        let valStatus = document.querySelector("#status");
        if (valStatus.value == 'Afastado') {
            let inputAfastamento = document.createElement('input')
            inputAfastamento.type = 'date'
            inputAfastamento.name = 'data_afastamento'
            inputAfastamento.className = 'form-control mb-2' 
            
            spanAfastado.appendChild(inputAfastamento)

        } else if(valStatus.value != 'Afastado'){
            spanAfastado.innerHTML = ''
        }
       }      
    <? 
        include('assets/js/edit_membros.js');
    ?>
    </script>
</body>
</html>