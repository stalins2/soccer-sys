<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-sumula input{
            width: 60px;
            height: 30px;
            border-radius: 5px;
            text-align: center;
        }
        .cadastro-sumula input[type='date']{
            width: 150px;
        }
        .cadastro-sumula .submit{
            width: 150px;
            height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            margin-bottom: 50px;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0; margin-top: 0px;">
            <?php include("menu.php");?>
            <div class="container-content col-10 d-flex text-center flex-wrap" style="padding: 0;">
                <div class="text-center container-fluid" >
                    <h1> Cadastro de Súmula</h1>
                </div>
                <div class="text-center container-fluid" style="width: 90%;">
                    <form class="cadastro-sumula" id="cadastro_sumula" name="cadastro_sumula" method="post" action="controllers/cadastro_sumula_controller.php?acao=cadastrar">
                        <table class="table text-center">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Placar do Time Amarelo </th>
                                    <th scope="col"> Placar do Time Azul </th>
                                    <th scope="col"> Placar dos Pênaltis do Time Amarelo </th>
                                    <th scope="col"> Placar dos Pênaltis do Time Azul </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <input type="text" id="placar_amarelo" name="placar_amarelo" maxlength="2" />
                                    </td>
                                    <td>
                                        <input type="text" id="placar_azul" name="placar_azul" maxlength="2" />
                                    </td>
                                    <td>
                                        <input type="text" id="placar_penaltis_amarelo" name="placar_penaltis_amarelo" maxlength="2" />
                                    </td>
                                    <td>
                                        <input type="text" id="placar_penaltis_azul" name="placar_penaltis_azul" maxlength="2" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Cartões do Time Amarelo </th>
                                    <th scope="col"> Cartões do Time Azul </th>
                                    <th scope="col"> Data do Jogo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row">
                                        <input type="text" id="cartoes_amarelo" name="cartoes_amarelo" maxlength="2" />
                                    </td>
                                    <td>
                                        <input type="text" id="cartoes_azul" name="cartoes_azul" maxlength="2" />
                                    </td>
                                    <td>
                                        <input type="date" id="data_jogo" name="data_jogo" maxlength="" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <input class="submit" type="submit" id="submit" value="Cadastrar Sumula" />
                </form>
                <div class="container-fluid lista-dados">
                    <div class="form-group row col-sm-12 lista-dados__titulos">
                        <!-- <label for="IdCadastro" class="col-sm-1 col-form-label">ID de Cadastro</label> -->
                        <label for="SumulaJogo" class="col-sm-1 col-form-label">Sumula do Jogo</label>
                        <label for="PlacarTimeAmarelo" class="col-sm-1 col-form-label">Placar do Time Amarelo</label>
                        <label for="PlacarTimeAzul" class="col-sm-1 col-form-label">Placar do Time Azul</label>
                        <label for="PenaltisAmarelo" class="col-sm-1 col-form-label">Penaltis Amarelo</label>
                        <label for="PenaltisAzul" class="col-sm-1 col-form-label">Penaltis Azul</label>
                        <label for="CartoesTimeAmarelo" class="col-sm-1 col-form-label">Cartões Time Amarelo</label>
                        <label for="CartoesTimeAzul" class="col-sm-1 col-form-label">Cartões Time Azul</label>
                        <label for="DataJogo" class="col-sm-1 col-form-label">Data do Jogo</label>
                        <label for="DataCadastro" class="col-sm-1 col-form-label">Data do Cadastro</label>
                        <label for="CadastradoPor" class="col-sm-1 col-form-label">Cadastrado Por</label>
                        <label for="AlteradoPor" class="col-sm-1 col-form-label">Alterado Por</label>
                        <label for="Editar" class="col-sm-1 col-form-label">
                            <i class="fas fa-edit fa-lg"></i>
                            <i class="fas fa-trash-alt fa-lg float-right"></i>
                        </label>
                    </div>                    
                    <div class="form-group row col-sm-12 lista-dados__infos">
                        <? foreach ($sumulas as $key => $lista){ ?>
                            <div class="container-fluid row lista-dados__linhas" style="padding:0; margin:0;" id="lista_<?=$lista->id;?>">
                                <label for="SumulaJogo" class="col-sm-1 col-form-label id_num" id="SumulaJogo"><?=$lista->id;?></label>
                                <label for="PlacarTimeAmarelo" class="col-sm-1 col-form-label" id="PlacarTimeAmarelo"><?=$lista->placar_amarelo;?></label>
                                <label for="PlacarTimeAzul" class="col-sm-1 col-form-label" id="PlacarTimeAzul"><?=$lista->placar_azul;?></label>
                                <label for="PenaltisAmarelo" class="col-sm-1 col-form-label" id="PenaltisAmarelo"><?=$lista->placar_penaltis_amarelo;?></label>
                                <label for="PenaltisAzul" class="col-sm-1 col-form-label" id="PenaltisAzul"><?=$lista->placar_penaltis_azul;?></label>  
                                <label for="CartoesTimeAmarelo" class="col-sm-1 col-form-label" id="CartoesTimeAmarelo"><?=$lista->cartoes_amarelo;?></label>
                                <label for="CartoesTimeAmarelo" class="col-sm-1 col-form-label" id="TelCelular"><?=$lista->cartoes_azul;?></label>
                                <label for="DataJogo" class="col-sm-1 col-form-label" id="DataJogo"><?=(new DateTime($lista->data_jogo))->format('d/m/Y');?></label>
                                <label for="DataCadastro" class="col-sm-1 col-form-label" id="DataCadastro"><?=(new DateTime($lista->data_cadastro))->format('d/m/Y');?></label>
                                <label for="CadastradoPor" class="col-sm-1 col-form-label" id="CadastradoPor"><?=$lista->nome_cadastrador;?></label>  
                                <label for="AtualizadoPor" class="col-sm-1 col-form-label" id="AtualizadoPor"><?=$lista->atualizado_por;?></label>  
                                <label for="Editar" class="col-sm-1 col-form-label float-left" id="Editar">
                                    <i class="fas fa-edit fa-lg text-info" onclick="editar('<?=$lista->id;?>', '<?=$lista->placar_amarelo;?>',
                                    '<?=$lista->placar_azul;?>', '<?=$lista->placar_penaltis_amarelo;?>',
                                    '<?=$lista->placar_penaltis_azul;?>',  '<?=$lista->cartoes_amarelo;?>',
                                    '<?=$lista->cartoes_azul;?>', '<?=$lista->data_jogo;?>');"></i>
                                    <i class="fas fa-lg fa-trash-alt text-danger float-right" onclick="confirmar_exclusao(<?=$lista->id;?>)"></i>
                                </label> 
                            </div>                   
                        <?}?>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>   
        <script>
            <? 
                include('assets/js/edit_sumula.js');
            ?>
        </script>
</body>
</html>