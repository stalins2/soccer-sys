<?php
include('db.php');
include('function.php');
$query = '';
$output = array();
$query .= "SELECT * FROM Sumula ";
// if(isset($_POST["search"]["value"]))
// {
// 	$query .= 'WHERE nome LIKE "%'.$_POST["search"]["value"].'%" ';
// 	$query .= 'OR email LIKE "%'.$_POST["search"]["value"].'%" ';
// }
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id DESC ';
}
if($_POST["length"] != "")
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();
foreach($result as $row)
{
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = (new DateTime($row["data_jogo"]))->format('d/m/Y');
	$sub_array[] = $row["hora_jogo"];
	$sub_array[] = $row["placar_amarelo"];
	$sub_array[] = $row["placar_azul"];
	$sub_array[] = $row["placar_penaltis_amarelo"];
	$sub_array[] = $row["placar_penaltis_azul"];
	$sub_array[] = $row["cartoes_amarelo"];
	$sub_array[] = $row["cartoes_azul"];
	$sub_array[] = (new DateTime($row["data_cadastro"]))->format('d/m/Y');
	$sub_array[] = '<button type="button" name="update" class="update" id="'.$row["id"].'">Atualizar</button>';
	$sub_array[] = '<button type="button" name="delete" class="delete" id="'.$row["id"].'">Excluir</button>';
	$data[] = $sub_array;
}
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
echo json_encode($output);
?>