<?
require_once('../assets/includes/validador_acesso.php');
include('../header_loaders.php');
include('../new_lists.php');
?>
<html>

<head>
	<title> Sistema - Fohfaoi</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="../assets/css/new_view.css" />
    <style>
        .field-icon {
            float: right;
            margin-top: -25px;
            margin-right: 15px;
            position: relative;
            z-index: 2;
            cursor: pointer;
        }

        .container{
            padding-top:50px;
            margin: auto;
            position: relative;
        }
    </style>
</head>

<body class="is-preload">
	<!-- Wrapper -->
	<div id="wrapper">
		<!-- Main -->
			<div id="main" class="conteudo-page">
				<div class="inner">
					<header id="header">
						<a href="#" class="logo"><strong>Cadastrar </strong>Nova Sumula</a>
					</header>
					<!-- Content -->
					<div class="row gtr-200">
						<div class="col-12 d-flex text-center flex-wrap" style="padding: 0;">
							<div class="text-center" style="width: 100%;">
							</div>
							<br />
							<div class="row gtr-uniform">
                                <div class="col-11 col-12-xsmall">
                                    <div class="container box">
                                        <div class="table-responsive">
                                            <div align="right">
                                                <button type="button" id="add_button" data-toggle="modal" data-target="#userModal">Adicionar</button>
                                            </div>
                                            <br /><br />
                                            <table id="user_data" class="table table-bordered table-striped text-center">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">ID</th>
                                                        <th width="10%">Data do Jogo</th>
                                                        <th width="10%">Hora do Jogo</th>
                                                        <th width="8%">Placar Amarelo</th>
                                                        <th width="8%">Placar Azul</th>
                                                        <th width="8%"> Penaltis Amarelo</th>
                                                        <th width="8%">Penaltis Azul</th>
                                                        <th width="8%">Cartoes Time Amarelo</th>
                                                        <th width="8%">Cartoes Time Azul</th>
                                                        <th width="10%">Data do Jogo</th>
                                                        <th width="10%"></th>
                                                        <th width="7%"></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>

                                    <div id="userModal" class="modal fade">
                                        <div class="modal-dialog modal-lg">
                                            <form method="post" id="user_form" name="user_form" enctype="multipart/form-data">
                                                <div class="modal-content">
                                                    <div class="modal-body text-center;">
														<div class="form-group row">
															<div class="col">
																<label>Data do Jogo</label>
																<input type="date" name="data_jogo" id="data_jogo" class="form-control text-center" />
															</div>
															<div class="col">
																<label>Hora do Jogo (24h)</label>
																<input type="text" name="hora_jogo" id="hora_jogo" class="form-control text-center" />
															</div>
															<div class="col">
																<label>Placar Time Amarelo</label>
																<input type="text" name="placar_amarelo" id="placar_amarelo" class="form-control text-center placar" />
															</div>
															<div class="col">
																<label>Placar Time Azul</label>
																<input type="text" name="placar_azul" id="placar_azul" class="form-control text-center placar" />
															</div>
														</div>
                                                        <br />
														<div class="form-group row">
															<div class="col">
																<label>Penaltis Time Amarelo</label>
																<input type="text" name="placar_penaltis_amarelo" id="placar_penaltis_amarelo" class="form-control text-center placar" />
															</div>
															<div class="col">
																<label>Penaltis Time Azul</label>
																<input type="text" name="placar_penaltis_azul" id="placar_penaltis_azul" class="form-control text-center placar" />
															</div>
															<div class="col">
																<label>Cartões Time Amarelo</label>
																<input type="text" name="cartoes_amarelo" id="cartoes_amarelo" class="form-control text-center placar" />
															</div>
															<div class="col">
																<label>Cartões Time Azul</label>
																<input type="text" name="cartoes_azul" id="cartoes_azul" class="form-control text-center placar" />
															</div>
														</div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="hidden" name="user_id" id="user_id" />
                                                        <input type="hidden" name="operation" id="operation"/>
                                                        <input type="submit" name="action" id="action"  value="Adicionar" />
                                                        <input type="button" data-dismiss="modal" value="Fechar">

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		<?
		include('../new_menu.php');
		include('../libs.php');
		?>
    </div>

<script>


$(document).ready(function(){
	
	$('#placar').mask('000');
	$("#hora_jogo").mask("Hh:Mm",{
		translation: {
		'H': { pattern: /[0-2]/ },
		'h': { pattern: /[0-9]/ },
		'M': { pattern: /[0-5]/ },
		'm': { pattern: /[0-9]/ }
		}
	});
    $("#visualizar").on('click', function(){
        $("#visualizar").toggleClass("fa-eye fa-eye-slash");
        var input = $("#senha");
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Adicionar");
		$('#action').val("Adicionar");
		$('#operation').val("add");		
	});
		var dataTable = $('#user_data').DataTable({
			"processing":true,
			"serverSide":true,
				"pageLength": 10,
				"searching": false,
				"responsive": {
					orthogonal: 'responsive'
				},
				"order": [],
				"columnDefs":[{
					targets: [0],
					orderable: false
				}],
			"ajax":{
				url:"fetch.php",
				type:"POST"
			},
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
            }
		});
	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var formdata = new FormData($("form[name='user_form']")[0]);
	if($("#data_jogo").val() == ""){
		$("#data_jogo").attr('placeholder', 'Preencha esse campo!');
		$("#data_jogo").css('border', '1px solid red');
	}else if($("#hora_jogo").val() == ""){
		$("#hora_jogo").attr('placeholder', 'Preencha esse campo!');
		$("#hora_jogo").css('border', '1px solid red');
	}else if($("#placar_amarelo").val() == ""){
		$("#placar_amarelo").attr('placeholder', 'Preencha esse campo!');
		$("#placar_amarelo").css('border', '1px solid red');
	}else if($("#placar_azul").val() == ""){
		$("#placar_azul").attr('placeholder', 'Preencha esse campo!');
		$("#placar_azul").css('border', '1px solid red');
	}else if($("#cartoes_amarelo").val() == ""){
		$("#cartoes_amarelo").attr('placeholder', 'Preencha esse campo!');
		$("#cartoes_amarelo").css('border', '1px solid red');
	}else if($("#cartoes_azul").val() == ""){
		$("#cartoes_azul").attr('placeholder', 'Preencha esse campo!');
		$("#cartoes_azul").css('border', '1px solid red');
	}else if(confirm("Você confirma as informações?"))
	{
		$.ajax({
			url:"insert.php",
			type:'POST',
			data: formdata,
			contentType:false,
			processData:false,
			success:function(data){
				alert(data);
				console.log(data);
				$('#user_form')[0].reset();
				$('#userModal').modal('hide');
				dataTable.ajax.reload();
			}
		});
	}
	else
	{
		return false;	
	}
	});
	
	$(document).on('click', '.update', function(){
		var user_id = $(this).attr("id");
		$.ajax({
			url:"fetch_single.php",
			method:"POST",
			data:{user_id:user_id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#data_jogo').val(data.data_jogo);
				$('#hora_jogo').val(data.hora_jogo);
				$('#placar_amarelo').val(data.placar_amarelo);
				$('#placar_azul').val(data.placar_azul);
				$('#placar_penaltis_amarelo').val(data.placar_penaltis_amarelo);
				$('#placar_penaltis_azul').val(data.placar_penaltis_azul);
				$('#cartoes_amarelo').val(data.cartoes_amarelo);
				$('#cartoes_azul').val(data.cartoes_azul);
				// $('#data_cadastro').val(data.data_cadastro);
				$('.modal-title').text("Editar Súmula");
				$('#user_id').val(user_id);
				$('#action').val("Editar");
				$('#operation').val("edit");
			}
		});
	});
	
	$(document).on('click', '.delete', function(){
		var user_id = $(this).attr("id");
		if(confirm("Você tem certeza que quer excluir essa súmula? \n Todos os dados referentes a ela (Times, Arbitragem, Cartões, Gols, Penaltis e Observacoes) serão excluídos também!"))
		{
			$.ajax({
				url:"delete.php",
				method:"POST",
				data:{user_id:user_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});

</script>
</body>

</html>