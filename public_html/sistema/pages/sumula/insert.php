<?php
include('db.php');
include('function.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "add")
	{
			$dados = [
				'data_jogo' =>  $_POST['data_jogo'],
				'hora_jogo' => $_POST['hora_jogo'],
				'placar_amarelo' => $_POST['placar_amarelo'],
				'placar_azul' => $_POST['placar_azul'],
				'placar_penaltis_amarelo' => $_POST['placar_penaltis_amarelo'],
				'placar_penaltis_azul' => $_POST['placar_penaltis_azul'],
				'cartoes_amarelo' => $_POST['cartoes_amarelo'],
				'cartoes_azul' => $_POST['cartoes_azul']
			];
			$sql = "INSERT INTO Sumula (data_jogo, hora_jogo, placar_amarelo, placar_azul, placar_penaltis_amarelo, placar_penaltis_azul, cartoes_amarelo, cartoes_azul) VALUES (:data_jogo, :hora_jogo, :placar_amarelo, :placar_azul, :placar_penaltis_amarelo, :placar_penaltis_azul, :cartoes_amarelo, :cartoes_azul)";

			$stmt= $connection->prepare($sql);
			$stmt->execute($dados);

		if(!empty($stmt))
		{
			echo 'Cadastrado Realizado';
		}
	}
	if($_POST["operation"] == "edit")
	{
		
		$statement = $connection->prepare(
			"	UPDATE Sumula 
				SET 
					data_jogo = :data_jogo, 
					hora_jogo = :hora_jogo, 
					placar_amarelo = :placar_amarelo, 
					placar_azul = :placar_azul, 
					placar_penaltis_amarelo = :placar_penaltis_amarelo, 
					placar_penaltis_azul = :placar_penaltis_azul, 
					cartoes_amarelo = :cartoes_amarelo, 
					cartoes_azul = :cartoes_azul
				WHERE id = :id
			"
		);
		$result = $statement->execute(
			array(
				':data_jogo'					=>	$_POST["data_jogo"],
				':hora_jogo'					=>	$_POST["hora_jogo"],
				':placar_amarelo'				=>	$_POST["placar_amarelo"],
				':placar_azul'					=>	$_POST["placar_azul"],
				':placar_penaltis_amarelo'		=>	$_POST["placar_penaltis_amarelo"],
				':placar_penaltis_azul'			=>	$_POST["placar_penaltis_azul"],
				':cartoes_amarelo'				=>	$_POST["cartoes_amarelo"],
				':cartoes_azul'					=>	$_POST["cartoes_azul"],
				':id'							=>	$_POST["user_id"]
			)
		);
		if(!empty($result))
		{
			echo 'Dados Atualizados';
		}
	}
}

?>