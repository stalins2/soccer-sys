<?php
include('db.php');
include('function.php');
// $_POST["user_id"] = 0;
if(isset($_POST["user_id"]))
{
	$output = array();
	$statement = $connection->prepare(
		"SELECT
		arbitragem.id as id, 
		arbitragem.sumula_referencia as sumula,
		nome_1.nome as arbitro, 
		nome_2.nome as auxiliar_1, 
		nome_3.nome as auxiliar_2, 
		nome_4.nome as arbitro_2, 
		nome_5.nome as auxiliar_3, 
		nome_6.nome as auxiliar_4

		FROM Arbitragem_1 as arbitragem

		LEFT JOIN Cadastros as nome_1 ON nome_1.id = arbitragem.arbitro
		LEFT JOIN Cadastros as nome_2 ON nome_2.id = arbitragem.auxiliar_1
		LEFT JOIN Cadastros as nome_3 ON nome_3.id = arbitragem.auxiliar_2
		LEFT JOIN Cadastros as nome_4 ON nome_4.id = arbitragem.arbitro_2
		LEFT JOIN Cadastros as nome_5 ON nome_5.id = arbitragem.auxiliar_3
		LEFT JOIN Cadastros as nome_6 ON nome_6.id = arbitragem.auxiliar_4

		WHERE arbitragem.id = '".$_POST["user_id"]."'"
	);
	
	// WHERE jogador = '".$_POST["user_id"]."'"
	$statement->execute();
	$result = $statement->fetchAll();

	// echo "<pre>";
	// print_r($result);

	foreach($result as $row)
	{
		$sub_array = array();
		$sub_array["id"] = $row["id"];
		$sub_array["sumula"] = $row["sumula"];
		$sub_array["arbitro"] = $row["arbitro"];			
		$sub_array["auxiliar_1"] = $row["auxiliar_1"];			
		$sub_array["auxiliar_2"] = $row["auxiliar_2"];			
		$sub_array["arbitro_2"] = $row["arbitro_2"];			
		$sub_array["auxiliar_3"] = $row["auxiliar_3"];			
		$sub_array["auxiliar_4"] = $row["auxiliar_4"];			
		$output[] = $sub_array;
	}
	echo json_encode($output);
}
?>