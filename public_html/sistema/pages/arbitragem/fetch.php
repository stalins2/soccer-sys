<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
$query .= "SELECT
			arbitragem.id as id, 
			arbitragem.sumula_referencia as sumula,
			nome_1.nome as arbitro, 
			nome_2.nome as auxiliar_1, 
			nome_3.nome as auxiliar_2, 
			nome_4.nome as arbitro_2, 
			nome_5.nome as auxiliar_3, 
			nome_6.nome as auxiliar_4

			FROM Arbitragem_1 as arbitragem

			LEFT JOIN Cadastros as nome_1 ON nome_1.id = arbitragem.arbitro
			LEFT JOIN Cadastros as nome_2 ON nome_2.id = arbitragem.auxiliar_1
			LEFT JOIN Cadastros as nome_3 ON nome_3.id = arbitragem.auxiliar_2
			LEFT JOIN Cadastros as nome_4 ON nome_4.id = arbitragem.arbitro_2
			LEFT JOIN Cadastros as nome_5 ON nome_5.id = arbitragem.auxiliar_3
			LEFT JOIN Cadastros as nome_6 ON nome_6.id = arbitragem.auxiliar_4 ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE nome_1.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR nome_2.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR nome_3.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR nome_4.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR nome_5.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR nome_6.nome LIKE "%'.$_POST["search"]["value"].'%" ';
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();

foreach($result as $row){
	
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = $row["sumula"];
	$sub_array[] = $row["arbitro"];
	$sub_array[] = $row["auxiliar_1"];
	$sub_array[] = $row["auxiliar_2"];
	$sub_array[] = $row["arbitro_2"];
	$sub_array[] = $row["auxiliar_3"];
	$sub_array[] = $row["auxiliar_4"];
	$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="update">Alterar</button>';
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);

echo json_encode($output);
?>