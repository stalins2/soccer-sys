<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-membros input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        .cadastro-membros .submit{
            width: 150px;
            height: 35px;
            line-height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            margin-bottom: 250px;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .cadastro-membros table{
            text-align: left !important;
        }
        .entry:not(:first-of-type)
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }

    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 d-flex text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Cadastro dos Árbitros</h1>
                </div>
                <div class="container-fluid" style="width: 98%;">
                    <form class="cadastro-arbitragem" id="cadastro_arbitragem" name="cadastro_arbitragem" method="post" action="controllers/cadastro_arbitragem_controller.php?acao=cadastrar">
                    <div class="row">
                        <table class="col table text-center field-wrapper">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Jogo (Súmula)</th>
                                </tr>
                        </table>
                        
                        <!-- código da súmula a ser escolhida -->
                        <div class="container-fluid lista-dados">
                            <div class="form-group row col-sm-12 lista-dados__infos" style="border: none;">
                                <select class="form-control mb-2 col-sm-4" id="CodigoSumula" name="sumula_referencia">
                                    <? foreach($listaSumulas as $key => $lista){ ?>
                                        <option value="<?=$lista->id;?>">Código da Súmula: <?=$lista->id;?>  | Data do Jogo: <?=$lista->data_jogo;?></option>
                                    <? } ?>
                                </select>
                            </div>                       
                        </div>
                        <!-- Fim da seleção da súmula -->
                    </div>
                    <div class="row">
                        <table class="col-md-7 table text-left">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Primeiro Tempo</th>                                    
                                </tr>
                            </thead>
                        </table>
                        
                        <table class="col-md-5 table text-left">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Segundo Tempo</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="row text-left linha-times">
                        <div class="col-md-5 time arbitragem_1">
                            <div class="row">
                                <label for="arbitro_1_1">Árbitro</label>
                                <select class="form-control" id="arbitro_1_1" name="arbitro_1_1">
                                    <?=listarUsuarios();?>
                                </select>                                
                            </div>
                            <div class="row">
                                <label for="auxiliar_1_1">Auxiliar 1</label>
                                <select class="form-control" id="auxiliar_1_1" name="auxiliar_1_1">
                                    <option value="0">0 | (Ninguém) Selecione</option>
                                    <? 
                                    foreach($listarMembros as $key => $lista){ ?>
                                        <option value="<?=$lista->id;?>"><?=$lista->id;?>  | <?=$lista->nome;?></option>
                                    <? } ?>
                                </select>                                
                            </div>
                            <div class="row">
                                <label for="auxiliar_2_1">Auxiliar 2</label>
                                <select class="form-control" id="auxiliar_2_1" name="auxiliar_2_1">
                                    <option value="0">0 | (Ninguém) Selecione</option>
                                    <? 
                                    foreach($listarMembros as $key => $lista){ ?>
                                        <option value="<?=$lista->id;?>"><?=$lista->id;?>  | <?=$lista->nome;?></option>
                                    <? } ?>
                                </select>                                
                            </div>
                        </div>
                    <div class="col-md-2"></div>
                        <div class="col-md-5 time arbitragem_2">
                            <div class="row">
                                <label for="arbitro_1_2">Árbitro</label>
                                <select class="form-control" id="arbitro_1_2" name="arbitro_1_2">
                                    <option value="0">0 | (Ninguém) Selecione</option>
                                    <? 
                                    foreach($listarMembros as $key => $lista){ ?>
                                        <option value="<?=$lista->id;?>"><?=$lista->id;?>  | <?=$lista->nome;?></option>
                                    <? } ?>
                                </select>                                
                            </div>
                            <div class="row">
                                <label for="auxiliar_1_2">Auxiliar 1</label>
                                <select class="form-control" id="auxiliar_1_2" name="auxiliar_1_2">
                                    <option value="0">0 | (Ninguém) Selecione</option>
                                    <? 
                                    foreach($listarMembros as $key => $lista){ ?>
                                        <option value="<?=$lista->id;?>"><?=$lista->id;?>  | <?=$lista->nome;?></option>
                                    <? } ?>
                                </select>                                
                            </div>
                            <div class="row">
                                <label for="auxiliar_2_2">Auxiliar 2</label>
                                <select class="form-control" id="auxiliar_2_2" name="auxiliar_2_2">
                                    <option value="0">0 | (Ninguém) Selecione</option>
                                    <? 
                                    foreach($listarMembros as $key => $lista){ ?>
                                        <option value="<?=$lista->id;?>"><?=$lista->id;?>  | <?=$lista->nome;?></option>
                                    <? } ?>
                                </select>                                
                            </div>
                        </div>
                    </div>
                    <!-- fim da arbitragem primeiro tempo -->
                    
                    
                    <input class="submit" type="submit" id="submit" value="Cadastrar Arbitragem" />
                </form>
                </div>
                
                <?php
                    include('arbitragem/index.php');
                ?>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
        
        <script type="text/javascript">
         $(document).ready(function(){    
            var maxField = 11; //Input fields increment limitation    
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field-wrapper'); //Input field wrapper
            var contador = 0;    
            var x = 0; //Initial field counter is 1
            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if ( x <= maxField){
                    x++
                    contador++;
                    var fieldHTML = '<tr><td>'+ contador +'<input type="text" id="nome" name="field_name[]" maxlength="50" /><a href="javascript:void(0);" class="remove_button">Remover</a></td></tr>'; //New input field html
                    $(wrapper).append(fieldHTML); //Add field html
                }
                console.log(contador);
            });

    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).closest('tr').remove(); //Remove field html
        x--; //Decrement field counter
        contador --;
    });
});
</script>
</body>
</html>