<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
$query .= "SELECT
			gols.id as id, 
			gols.sumula_referencia as sumula,
			Cadastros.nome as jogador, 
			gols.jogador as jogador_cod, 
			gols.time as jogador_time, 
			gols.primeiro_tempo as primeiro,
			gols.segundo_tempo as segundo			

			FROM Gols as gols
			LEFT JOIN Cadastros
			ON Cadastros.id = gols.jogador ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE Cadastros.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();

foreach($result as $row){
	
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = $row["sumula"];
	$sub_array[] = $row["jogador"];
	if($row["jogador_time"] == 1){
		$sub_array[] = "Amarelo" ;
	}else{
		$sub_array[] = "Azul";
	}
	$sub_array[] = $row["primeiro"];
	$sub_array[] = $row["segundo"];	
	$sub_array[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="update">Alterar</button>';
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);

echo json_encode($output);
?>