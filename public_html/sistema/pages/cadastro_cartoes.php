<? 
    require_once('assets/includes/validador_acesso.php'); 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
?>


<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-cartoes input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        
        .cadastro-cartoes .submit{
            width: 150px;
            height: 40px;
            text-align: center;
            line-height: 40px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            cursor: pointer;
        }
        
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .cadastro-cartoes table{
            text-align: left !important;
        }
        .entry:not(:first-of-type) 
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }
      

    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0; margin-top: 0px;">
        <?php include("menu.php");?>
            <div class="container-content col-10 d-flex text-center flex-wrap" style="padding: 0;">
                <div class="text-center container-fluid">
                    <h1> Cadastro dos Cartões</h1>
                </div>
                <div class="text-center container-fluid" style="width: 90%;">
                    <form class="cadastro-cartoes" id="cadastro_cartoes" name="cadastro_cartoes" method="post" action="controllers/cadastro_cartoes_controller.php?acao=cadastrar">
                        <!-- Seleção da súmula -->
                            <table class="col table text-center field-wrapper">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"> Jogo (Súmula) - Código/Data</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- código da súmula a ser escolhida -->
                            <div class="container-fluid">
                                <div class="form-group row col-sm-12" style="border: none;">
                                    <select class="form-control mb-2 col-sm-4" id="CodigoSumula" name="sumula_referencia">
                                        <? foreach($listaSumulas as $key => $lista){ ?>
                                            <option value="<?=$lista->id;?>">Código da Súmula: <?=$lista->id;?>  | Data do Jogo: <?=$lista->data_jogo;?></option>
                                        <? } ?>
                                    </select>
                                </div>                       
                            </div>
                        <!-- Fim da seleção da súmula -->

                        <div class="row text-left linha-times">
                            <table class="col table text-center field-wrapper">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"> Jogador <th>
                                        <th scope="col"> Amarelo <th>
                                        <th scope="col"> Azul </th>
                                        <th scope="col"> Vermelho </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="row text-left">                          
                            <div class="selects-cartoes form-group row col-sm-12"></div>
                            <div class="row col-sm-12 text-center">
                                <div class="submit adiciona-jogador">Adicionar Jogador</div>                            
                            </div>
                        </div>                        
                        <input class="submit" type="submit" id="submit" value="Cadastrar Cartão" />
                    </form>
                </div> 
                <?php
                    include('cartoes/index.php');
                ?>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
        
        <script type="text/javascript">            
            $(document).ready(function(){
                var nm_id = 1; //ID inicial para as divs
                
                $('.adiciona-jogador').click(function(){
                    if(nm_id <=10){
                        $('.selects-cartoes').append(criarDiv('div_container_'+nm_id,'jogador-adicionado row col-sm-12', 'margin-top: 10px;'));  
                        $('#div_container_'+nm_id).append(criarDivSelect('cartao_jogador_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'), 
                        selecionar10('amarelo_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'), 
                        selecionar10('azul_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'), 
                        selecionar10('vermelho_'+nm_id,'col-sm-3 text-left', 'form-control col-sm-12'));
                        nm_id++;                        
                    }else{
                        alert('Você atingiu o limite de jogadores para cadastrar aqui.'); 
                    }
                });
            });
        </script>
    </body>
</html>