<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-gols input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        .cadastro-gols .submit{
            width: 150px;
            height: 35px;
            line-height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            cursor: pointer;
            margin-bottom: 50px;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .cadastro-gols table{
            text-align: left !important;
        }
        .entry:not(:first-of-type)
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }
      

    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Cadastro dos Gols</h1>
                </div>
                <div class="container-fluid" style="width: 98%;">
                    <form class="cadastro-gols" id="cadastro_gols" name="cadastro_gols"  method="post" action="controllers/cadastro_gols_controller.php?acao=cadastrar">                    
                        <!-- Seleção da súmula -->
                        <div class="row">
                            <table class="col table text-center field-wrapper">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"> Jogo (Súmula) - Código/Data</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- código da súmula a ser escolhida -->
                            <div class="container-fluid lista-dados">
                                <div class="form-group row col-sm-12 lista-dados__infos" style="border: none;">
                                    <?=listaSumulas();?>
                                </div>                       
                            </div>
                        </div>
                        <!-- Fim da seleção da súmula -->
                        
                        <div class="row text-light bg-dark text-left">
                            <div class="col-sm-3">Jogador (Primeiro tempo)</div> 
                            <div class="col-sm-3">Time</div>
                            <div class="col-sm-3">Gols Primeiro Tempo</div> 
                            <div class="col-sm-3">Gols Segundo tempo</div>
                        </div>
                        
                        <div class="row text-left"> 
                            <div class="row selects-gols-1 form-group col-sm-12"></div>
                            <div class="col-sm-6">
                                <div class="row text-center col-sm-12">
                                    <div class="submit adiciona-jogador-1">Adicionar Jogador</div>
                                </div>      
                            </div>
                        </div>
                        
                        <input class="submit cadastrar-gols" type="submit" id="submit" value="Cadastrar Gols" />
                        
                    </form>
                </div>
                <div class="container-fluid text-center">
                    <?php
                        include('gols/index.php');
                    ?>
                </div>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
        
        <script type="text/javascript">
            $(document).ready(function(){
                let nm_id = 1; //ID inicial para as divs
                
                $('.adiciona-jogador-1').click(function(){
                    if(nm_id <=10){
                            //criarDiv
                            let idDiv = 'div_container_'+nm_id; 
                            let classeDiv = 'jogador-adicionado container-fluid row col-sm-12';
                            let cssDiv = 'margin-top: 10px;';
                            //Escolher o jogador
                            let idSelect = 'jogador_'+nm_id;
                            let classDivSelect = 'col-sm-3 text-left';
                            let classSelect = 'form-control col-sm-12';
                            //Input de gols marcados
                            let idInput = 'gols_primeiro_tempo_'+nm_id;
                            let tipoInput = 'text';
                            let classInputDiv = 'col-sm-3 text-left';
                            let classInput = 'form-control col-sm-12';
                            let propInput = 'maxlength="2" value="0"';
                            
                            let idInput2 = 'gols_segundo_tempo_'+nm_id;
                            let tipoInput2 = 'text';
                            let classInputDiv2 = 'col-sm-3 text-left';
                            let classInput2 = 'form-control col-sm-12';
                            let propInput2 = 'maxlength="2" value="0"';

                            //Selecionar o time do jogador naquele jogo
                            let selecTimeId = 'time_gols_'+nm_id;
                            let selecTimeDiv = 'col-sm-3 text-left';
                            let classTimeSelect = 'form-control col-sm-12';

                            $('.selects-gols-1').append(criarDiv(idDiv, classeDiv, cssDiv));  
                            $('#'+idDiv).append(
                                criarDivSelect(idSelect, classDivSelect, classSelect),
                                selecionarTime(selecTimeId,selecTimeDiv,classTimeSelect),
                                selecionar10(idInput, classInputDiv, classInput),
                                selecionar10(idInput2, classInputDiv2, classInput2)
                                // criarInput(idInput, tipoInput, classInputDiv, classInput, propInput),
                                // criarInput(idInput2, tipoInput2, classInputDiv2, classInput2, propInput2)
                            );
                            
                            nm_id++;
                    }else{
                        alert('Você atingiu o limite de jogadores para cadastrar aqui.'); 
                    }
                });
            });
        </script>
    </body>
</html>