<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
function total_results()
{
    global $connection;
	$statement = $connection->prepare("SELECT
	* FROM v_time_az_1");
	$statement->execute();
	$result = $statement->fetchAll();
	return $statement->rowCount();
}


$query = '';
$output = array();
$query .= "SELECT
				v_time_az_1.id_az1 AS id_az1, 
				v_time_az_1.su_ref AS su_ref_az_1, 
				v_time_az_1.jog_1 AS jog_1_az1, 
				v_time_az_1.jog_2 AS jog_2_az1, 
				v_time_az_1.jog_3 AS jog_3_az1, 
			   	v_time_az_1.jog_4 AS jog_4_az1, 
			   	v_time_az_1.jog_5 AS jog_5_az1,
				v_time_az_1.jog_6 AS jog_6_az1, 
				v_time_az_1.jog_7 AS jog_7_az1, 
				v_time_az_1.jog_8 AS jog_8_az1, 
				v_time_az_1.jog_9 AS jog_9_az1, 
				v_time_az_1.jog_10 AS jog_10_az1, 
				v_time_az_1.jog_11 AS jog_11_az1, 
				v_time_az_1.cad AS cad_az1

                FROM  v_time_az_1 AS v_time_az_1 ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE v_time_az_1.su_ref LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id_az1 DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();


// $array_times = array();

	foreach($result as $azul1){
		$array_azul1 = array();
        $array_azul1[] = $azul1["id_az1"];
        $array_azul1[] = $azul1["su_ref_az_1"];
        $array_azul1[] = $azul1["jog_1_az1"];
        $array_azul1[] = $azul1["jog_2_az1"];
        $array_azul1[] = $azul1["jog_3_az1"];
        $array_azul1[] = $azul1["jog_4_az1"];
        $array_azul1[] = $azul1["jog_5_az1"];
        $array_azul1[] = $azul1["jog_6_az1"];
        $array_azul1[] = $azul1["jog_7_az1"];
        $array_azul1[] = $azul1["jog_8_az1"];
        $array_azul1[] = $azul1["jog_9_az1"];
        $array_azul1[] = $azul1["jog_10_az1"];
        $array_azul1[] = $azul1["jog_11_az1"];		
        $array_azul1[] = '<button type="button" tipo="v_time_az_1" name="update" id="'.$azul1["id_az1"].'" class="update">Alterar</button>';
		$data[] = $array_azul1;

	}

	
// $data[] = $array_times;
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	total_results(),
	"data"				=>	$data
);
// echo "<pre>";
echo json_encode($output, JSON_PRETTY_PRINT);
?>