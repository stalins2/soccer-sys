<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
function total_results()
{
    global $connection;
	$statement = $connection->prepare("SELECT
	* FROM v_time_am_2");
	$statement->execute();
	$result = $statement->fetchAll();
	return $statement->rowCount();
}


$query = '';
$output = array();
$query .= "SELECT
                v_time_am_2.id_am2 AS id_am2, v_time_am_2.su_ref AS su_ref_am_2, v_time_am_2.jog_1 AS jog_1_am2, v_time_am_2.jog_2 AS jog_2_am2, v_time_am_2.jog_3 AS jog_3_am2, v_time_am_2.jog_4 AS jog_4_am2, v_time_am_2.jog_5 AS jog_5_am2,
                v_time_am_2.jog_6 AS jog_6_am2, v_time_am_2.jog_7 AS jog_7_am2, v_time_am_2.jog_8 AS jog_8_am2, v_time_am_2.jog_9 AS jog_9_am2, v_time_am_2.jog_10 AS jog_10_am2, v_time_am_2.jog_11 AS jog_11_am2, v_time_am_2.cad AS cad_am2

                FROM  v_time_am_2 AS v_time_am_2 ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE v_time_am_2.su_ref LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id_am2 DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();


// $array_times = array();

	foreach($result as $amarelo2){

		$array_amarelo2 = array();
			$array_amarelo2[] = $amarelo2["id_am2"];
			$array_amarelo2[] = $amarelo2["su_ref_am_2"];
			$array_amarelo2[] = $amarelo2["jog_1_am2"];
			$array_amarelo2[] = $amarelo2["jog_2_am2"];
			$array_amarelo2[] = $amarelo2["jog_3_am2"];
			$array_amarelo2[] = $amarelo2["jog_4_am2"];
			$array_amarelo2[] = $amarelo2["jog_5_am2"];
			$array_amarelo2[] = $amarelo2["jog_6_am2"];
			$array_amarelo2[] = $amarelo2["jog_7_am2"];
			$array_amarelo2[] = $amarelo2["jog_8_am2"];
			$array_amarelo2[] = $amarelo2["jog_9_am2"];
			$array_amarelo2[] = $amarelo2["jog_10_am2"];
			$array_amarelo2[] = $amarelo2["jog_11_am2"];
			$array_amarelo2[] = '<button type="button" tipo="v_time_am_2" name="update" id="'.$amarelo2["id_am2"].'" class="update">Alterar</button>';
		$data[] = $array_amarelo2;

	}

	
// $data[] = $array_times;
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	total_results(),
	"data"				=>	$data
);
// echo "<pre>";
echo json_encode($output, JSON_PRETTY_PRINT);
?>