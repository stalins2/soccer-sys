<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 
// session_start();
include('db.php');
include('function.php');


$query = '';
$output = array();
$query .= "SELECT
                v_time_az_1.id_az1 AS id_az1, v_time_az_1.su_ref AS su_ref_az_1, v_time_az_1.jog_1 AS jog_1_az1, v_time_az_1.jog_2 AS jog_2_az1, v_time_az_1.jog_3 AS jog_3_az1, v_time_az_1.jog_4 AS jog_4_az1, v_time_az_1.jog_5 AS jog_5_az1,
                v_time_az_1.jog_6 AS jog_6_az1, v_time_az_1.jog_7 AS jog_7_az1, v_time_az_1.jog_8 AS jog_8_az1, v_time_az_1.jog_9 AS jog_9_az1, v_time_az_1.jog_10 AS jog_10_az1, v_time_az_1.jog_11 AS jog_11_az1, v_time_az_1.cad AS cad_az1,

                v_time_am_1.id_am1 AS id_am1, v_time_am_1.su_ref AS su_ref_am_1, v_time_am_1.jog_1 AS jog_1_am1, v_time_am_1.jog_2 AS jog_2_am1, v_time_am_1.jog_3 AS jog_3_am1, v_time_am_1.jog_4 AS jog_4_am1, v_time_am_1.jog_5 AS jog_5_am1,
                v_time_am_1.jog_6 AS jog_6_am1, v_time_am_1.jog_7 AS jog_7_am1, v_time_am_1.jog_8 AS jog_8_am1, v_time_am_1.jog_9 AS jog_9_am1, v_time_am_1.jog_10 AS jog_10_am1, v_time_am_1.jog_11 AS jog_11_am1, v_time_am_1.cad AS cad_am1,

                v_time_az_2.id_az2 AS id_az2, v_time_az_2.su_ref AS su_ref_az_2, v_time_az_2.jog_1 AS jog_1_az2, v_time_az_2.jog_2 AS jog_2_az2, v_time_az_2.jog_3 AS jog_3_az2, v_time_az_2.jog_4 AS jog_4_az2, v_time_az_2.jog_5 AS jog_5_az2,
                v_time_az_2.jog_6 AS jog_6_az2, v_time_az_2.jog_7 AS jog_7_az2, v_time_az_2.jog_8 AS jog_8_az2, v_time_az_2.jog_9 AS jog_9_az2, v_time_az_2.jog_10 AS jog_10_az2, v_time_az_2.jog_11 AS jog_11_az2, v_time_az_2.cad AS cad_az2,

                v_time_am_2.id_am2 AS id_am2, v_time_am_2.su_ref AS su_ref_am_2, v_time_am_2.jog_1 AS jog_1_am2, v_time_am_2.jog_2 AS jog_2_am2, v_time_am_2.jog_3 AS jog_3_am2, v_time_am_2.jog_4 AS jog_4_am2, v_time_am_2.jog_5 AS jog_5_am2,
                v_time_am_2.jog_6 AS jog_6_am2, v_time_am_2.jog_7 AS jog_7_am2, v_time_am_2.jog_8 AS jog_8_am2, v_time_am_2.jog_9 AS jog_9_am2, v_time_am_2.jog_10 AS jog_10_am2, v_time_am_2.jog_11 AS jog_11_am2, v_time_am_2.cad AS cad_am2

                FROM v_time_az_1 AS v_time_az_1
				LEFT JOIN Sumula ON Sumula.id = v_time_az_1.su_ref
                LEFT JOIN v_time_am_1 AS v_time_am_1 ON v_time_am_1.su_ref = v_time_az_1.su_ref
                LEFT JOIN v_time_az_2 AS v_time_az_2 ON v_time_az_2.su_ref = v_time_az_1.su_ref
                LEFT JOIN v_time_am_2 AS v_time_am_2 ON v_time_am_2.su_ref = v_time_az_2.su_ref ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE v_time_az_1.su_ref LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id_az1 DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();


$array_times = array();


	$array_azultime1 = array();
	foreach ($result as $azul1){	

		$array_azul1 = array();
			$array_azul1[] = $azul1["id_az1"];
			$array_azul1[] = $azul1["su_ref_az_1"];
			$array_azul1[] = $azul1["jog_1_az1"];
			$array_azul1[] = $azul1["jog_2_az1"];
			$array_azul1[] = $azul1["jog_3_az1"];
			$array_azul1[] = $azul1["jog_4_az1"];
			$array_azul1[] = $azul1["jog_5_az1"];
			$array_azul1[] = $azul1["jog_6_az1"];
			$array_azul1[] = $azul1["jog_7_az1"];
			$array_azul1[] = $azul1["jog_8_az1"];
			$array_azul1[] = $azul1["jog_9_az1"];
			$array_azul1[] = $azul1["jog_10_az1"];
			$array_azul1[] = $azul1["jog_11_az1"];		
			// $array_time1[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="btn btn-warning btn-xs update">Alterar</button>';
		$array_azultime1["azul primeiro tempo"] = $array_azul1;
	}
$array_times[] = $array_azultime1;

	$array_amarelotime1 = array();
	foreach($result as $amarelo1){

		$array_amarelo1 = array();
			$array_amarelo1[] = $amarelo1["id_am1"];
			$array_amarelo1[] = $amarelo1["su_ref_am_1"];
			$array_amarelo1[] = $amarelo1["jog_1_am1"];
			$array_amarelo1[] = $amarelo1["jog_2_am1"];
			$array_amarelo1[] = $amarelo1["jog_3_am1"];
			$array_amarelo1[] = $amarelo1["jog_4_am1"];
			$array_amarelo1[] = $amarelo1["jog_5_am1"];
			$array_amarelo1[] = $amarelo1["jog_6_am1"];
			$array_amarelo1[] = $amarelo1["jog_7_am1"];
			$array_amarelo1[] = $amarelo1["jog_8_am1"];
			$array_amarelo1[] = $amarelo1["jog_9_am1"];
			$array_amarelo1[] = $amarelo1["jog_10_am1"];
			$array_amarelo1[] = $amarelo1["jog_11_am1"];
			// $array_time1[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="btn btn-warning btn-xs update">Alterar</button>';
		$array_amarelotime1["amarelo primeiro tempo"] = $array_amarelo1;

	}
$array_times[] = $array_amarelotime1;

	$array_azultime2 = array();
	foreach ($result as $azul2){	

		$array_azul2 = array();
			$array_azul2[] = $azul2["id_az2"];
			$array_azul2[] = $azul2["su_ref_az_2"];
			$array_azul2[] = $azul2["jog_1_az2"];
			$array_azul2[] = $azul2["jog_2_az2"];
			$array_azul2[] = $azul2["jog_3_az2"];
			$array_azul2[] = $azul2["jog_4_az2"];
			$array_azul2[] = $azul2["jog_5_az2"];
			$array_azul2[] = $azul2["jog_6_az2"];
			$array_azul2[] = $azul2["jog_7_az2"];
			$array_azul2[] = $azul2["jog_8_az2"];
			$array_azul2[] = $azul2["jog_9_az2"];
			$array_azul2[] = $azul2["jog_10_az2"];
			$array_azul2[] = $azul2["jog_11_az2"];		
			// $array_time1[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="btn btn-warning btn-xs update">Alterar</button>';
		$array_azultime2["azul segundo tempo"] = $array_azul2;
	}
$array_times[] = $array_azultime2;

	$array_amarelotime2 = array();
	foreach($result as $amarelo2){

		$array_amarelo2 = array();
			$array_amarelo2[] = $amarelo2["id_am2"];
			$array_amarelo2[] = $amarelo2["su_ref_am_2"];
			$array_amarelo2[] = $amarelo2["jog_1_am2"];
			$array_amarelo2[] = $amarelo2["jog_2_am2"];
			$array_amarelo2[] = $amarelo2["jog_3_am2"];
			$array_amarelo2[] = $amarelo2["jog_4_am2"];
			$array_amarelo2[] = $amarelo2["jog_5_am2"];
			$array_amarelo2[] = $amarelo2["jog_6_am2"];
			$array_amarelo2[] = $amarelo2["jog_7_am2"];
			$array_amarelo2[] = $amarelo2["jog_8_am2"];
			$array_amarelo2[] = $amarelo2["jog_9_am2"];
			$array_amarelo2[] = $amarelo2["jog_10_am2"];
			$array_amarelo2[] = $amarelo2["jog_11_am2"];
			// $array_time2[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="btn btn-warning btn-xs update">Alterar</button>';
		$array_amarelotime2["amarelo segundo tempo"] = $array_amarelo2;
	}
$array_times[] = $array_amarelotime2;

	
$data[] = $array_times;
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
// echo "<pre>";
// echo "session: ". $_SESSION['user_id'];
// echo "<br>";
echo json_encode($output, JSON_PRETTY_PRINT);
?>