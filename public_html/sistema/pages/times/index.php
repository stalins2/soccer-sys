<?
require_once('../assets/includes/validador_acesso.php');
include('../header_loaders.php');
include('../new_lists.php');
?>
<html>

<head>
	<title> Sistema - Fohfaoi</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="../assets/css/new_view.css" />
	<style>
		tr.dtrg-start {
			cursor: pointer;
		}

		.modal-tabelinha-hidden{
			width: 0;
			height: 0;
			opacity: 0;
		}
		.modal-tabelinha{
			position: fixed;
			top: 0;
			left: 0;
			display: block;
			margin: 0 auto;
			width: 100vw;
			height: 100vh;
			overflow: auto;
			z-index: 9999;
			opacity: 1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		.tabelinha-nova{
			width: 90%;
			height: auto;
			padding: 20px;
			margin: 5% auto;
			background-color: #fff;
		}
		.fecha-modal{
			position: absolute;
			right: 10px;
			top: 10px;
			color: #fff;
			font-weight: bold;
			font-size: 15px;
			cursor: pointer;
			text-transform: uppercase;
		}
	
		.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
			display: grid; 
			grid-template-columns: 2% 4% 8% 8% 8% 8% 8% 8% 8% 8% 8% 8% 8% 6%;
			/* grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); */
			grid-gap: 0px;
		}
		.tabelinha-nova .dados_titulo {
			font-weight: bold;
			background-color: #fdfdfd;
		}

		.tabelinha-nova .dados_jogo div, .tabelinha-nova .dados_titulo div, {
			padding: 10px;
		}
		.tabelinha-nova .dados_jogo .excluir{
			width: 100%;
			padding: 0;
		}

		.tabelinha-nova .dados_jogo button {
			border: none;
			background-color: #fff;

		}
		.update{
			position: relative;
		}
		.campo-temporario-before:before {
			content: 'Detalhes Acima';
			padding: 8px;
			font-size: 11px;
			text-transform: uppercase;
			position: absolute;
			top: -60px;
			white-space: pre-wrap;
			border-radius: 6px;
			background: #000;
			color: #fff;
		}
		.lista-time-1, .lista-time-2, .lista-time-3, .lista-time-4, .tabela-time1, .tabela-time2, .tabela-time3, .tabela-time4{
			/* .lista-time-1, .lista-time-2, .lista-time-3, .lista-time-4{ */
			display: none;		
			overflow: scroll;
		}
	</style>
</head>

<body class="is-preload">
	<!-- Wrapper -->
	<div id="wrapper">
		<!-- Main -->
		<div id="main" class="conteudo-page">
			<div class="inner">
				<!-- Header -->
				<form method="post" id="team_form" name="team_form" enctype="multipart/form-data">
					<header id="header">
						<a href="#" class="logo"><strong>Cadastrar </strong>Times - Código/Data</a>
						<?=newListaSu();?>
					</header>
					<!-- Content -->
					<div class="row gtr-200">	
						<div class="d-flex">
							<div class="row">
								<div class="row container-fluid">
									<div>
										<h5>Time Azul 1º Tempo</h5>			
										<ul class="actions">
											<li><div class="button primary botao1">Adicionar Jogadores</div></li>
										</ul>
										<div class="lista-time-1">
											<?=listarUsuarios("azul_1_");?>   
										</div>								
									</div>
									<div>
										<h5>Time Amarelo 1º Tempo</h5>											
										<ul class="actions">
											<li>
												<div class="button primary botao2">Adicionar Jogadores</div>											
											</li>
										</ul>                                            
										<div class="lista-time-2">
											<?=listarUsuarios("amarelo_1_");?>   
										</div>
									</div>
								</div>
								<br/>
								<div class="row container-fluid">
									<div>
										<h5>Time Azul 2º Tempo</h5>										
										<ul class="actions">
											<li>
												<div class="button secundary copia_1">Copiar 1º Tempo</div>
												<div class="button primary botao3">Adicionar Jogadores</div>
											</li>
										</ul>                                         
										<div class="lista-time-3">
											<?=listarUsuarios("azul_2_");?>   
										</div>
									</div>
									<div>
										<h5>Time Amarelo 2º Tempo</h5>										
										<ul class="actions">
											<li>
												<div class="button secundary copia_2">Copiar 1º Tempo</div>
												<div class="button primary botao4">Adicionar Jogadores</div>
											</li>
										</ul>                                         
										<div class="lista-time-4">
											<?=listarUsuarios("amarelo_2_");?>   
										</div>
									</div>
								</div>
								<br/>
								<div class="row container-fluid">
									<div>					
										<input type="hidden" name="operation" id="operation" />
										<input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
										<input type="submit" name="action" id="action" class="btn" value="Cadastrar os Times" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			
				<hr class="major"/>	
					<div class="row gtr-200">
						<div class="col-12 d-flex">
							<div class="container-fluid">
								<div class="tabelas-times">
									<h3 class="vertime1 button small">Ver Times Azul Primeiro Tempo</h3>
									<div class="tabela-time1 tabela-carregada">
										<table id="user_data" class="table table-bordered table-striped">
											<thead>
												<tr>
													<!-- <th width="5%">ID</th> -->
													<th width="3%">id</th>
													<th width="3%">Sumula</th>
													<th width="8%">Jogador 1</th>
													<th width="8%">Jogador 2</th>
													<th width="8%">Jogador 3</th>
													<th width="8%">Jogador 4</th>
													<th width="8%">Jogador 5</th>
													<th width="8%">Jogador 6</th>
													<th width="8%">Jogador 7</th>
													<th width="8%">Jogador 8</th>
													<th width="8%">Jogador 9</th>
													<th width="8%">Jogador 10</th>
													<th width="8%">Jogador 11</th>
													<th width="6%">Alterar</th>
												</tr>
											</thead>
										</table>		
									</div>
									<h3 class="vertime2 button small">Ver Times Amarelo Primeiro Tempo</h3>
									<div class="tabela-time2 tabela-carregada">
										<table id="time2" class="table table-bordered table-striped">
											<thead>
												<tr>
													<!-- <th width="5%">ID</th> -->
													<th width="3%">id</th>
													<th width="3%">Sumula</th>
													<th width="8%">Jogador 1</th>
													<th width="8%">Jogador 2</th>
													<th width="8%">Jogador 3</th>
													<th width="8%">Jogador 4</th>
													<th width="8%">Jogador 5</th>
													<th width="8%">Jogador 6</th>
													<th width="8%">Jogador 7</th>
													<th width="8%">Jogador 8</th>
													<th width="8%">Jogador 9</th>
													<th width="8%">Jogador 10</th>
													<th width="8%">Jogador 11</th>
													<th width="6%">Alterar</th>
												</tr>
											</thead>
										</table>
									</div>
									<hr class="major" />
									<h3 class="vertime3 button small">Ver Times Azul Segundo Tempo</h3>
									<div class="tabela-time3 tabela-carregada">
										<table id="time3" class="table table-bordered table-striped">
											<thead>
												<tr>
													<!-- <th width="5%">ID</th> -->
													<th width="3%">id</th>
													<th width="3%">Sumula</th>
													<th width="8%">Jogador 1</th>
													<th width="8%">Jogador 2</th>
													<th width="8%">Jogador 3</th>
													<th width="8%">Jogador 4</th>
													<th width="8%">Jogador 5</th>
													<th width="8%">Jogador 6</th>
													<th width="8%">Jogador 7</th>
													<th width="8%">Jogador 8</th>
													<th width="8%">Jogador 9</th>
													<th width="8%">Jogador 10</th>
													<th width="8%">Jogador 11</th>
													<th width="6%">Alterar</th>
												</tr>
											</thead>
										</table>
									</div>
									<h3 class="vertime4 button small">Ver Times Amarelo Segundo Tempo</h3>
									<div class="tabela-time4 tabela-carregada">
										<table id="time4" class="table table-bordered table-striped">
											<thead>
												<tr>
													<!-- <th width="5%">ID</th> -->
													<th width="3%">id</th>
													<th width="3%">Sumula</th>
													<th width="8%">Jogador 1</th>
													<th width="8%">Jogador 2</th>
													<th width="8%">Jogador 3</th>
													<th width="8%">Jogador 4</th>
													<th width="8%">Jogador 5</th>
													<th width="8%">Jogador 6</th>
													<th width="8%">Jogador 7</th>
													<th width="8%">Jogador 8</th>
													<th width="8%">Jogador 9</th>
													<th width="8%">Jogador 10</th>
													<th width="8%">Jogador 11</th>
													<th width="6%">Alterar</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="modal-tabelinha-hidden">
					<div class="fecha-modal"> X Fechar </div>
					<section class="tabelinha-nova">		
					</section>
				</div>
			</div>
		</div>
		<?
		include('../new_menu.php');
		include('../libs.php');
		?>
	</div>
	<script>
		$('.copia_1').click(function(){
			for (var i = 1; i <= 11; i++){
				var time1 = $(".lista-time-1 #azul_1_"+i).val();
				$("#azul_2_"+i).val(time1);
			}
		})
		$('.copia_2').click(function(){
			for (var i = 1; i <= 11; i++){
				var time1 = $(".lista-time-2 #amarelo_1_"+i).val();
				$("#amarelo_2_"+i).val(time1);
			}
		})
		$('#add_button').click(function(event){
			event.preventDefault();
			$('#team_form')[0].reset();
			$('#action').val("Adicionar");
			$('#operation').val("add");	
		});
		$(document).on('submit', '#team_form', function(event){
			event.preventDefault();
			var formdata = new FormData($("form[name='team_form']")[0]);
			
		if(confirm("Os times serão cadastrados de acordo com o preenchido, confirma?"))
			{
			$.ajax({
				url:"insert.php",
				type:'POST',
				data: formdata,
				contentType:false,
				processData:false,
				success:function(data){
					console.log("cadastro: ", data);
					alert(data);
					$('#team_form')[0].reset();
					dataTable1.ajax.reload();
					dataTable2.ajax.reload();
					dataTable3.ajax.reload();
					dataTable4.ajax.reload();
				}
			});
		}
		});

		// console.log('carregou as funções times');
	var tabela;	
	var botao;
		$(".botao1").on('click', function(){
			$('.lista-time-1').toggle("fast");
			var botao = ".botao1";
			textoCadTimes(botao);
		});
		$(".botao2").on('click', function(){
			$('.lista-time-2').toggle("fast");
			var botao = ".botao2";
			textoCadTimes(botao);
		});
		$(".botao3").on('click', function(){
			$('.lista-time-3').toggle("fast");
			var botao = ".botao3";
			textoCadTimes(botao);
		});
		$(".botao4").on('click', function(){
			$('.lista-time-4').toggle("fast");
			var botao = ".botao4";
			textoCadTimes(botao);
		});
		$(".vertime1").on('click', function(){
			$('.tabelinha-nova').append($('.tabela-time1'));
			$('.tabelinha-nova .tabela-carregada').fadeIn(800);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('.tabelinha-nova').css('overflow','auto');
			$('body').css('overflow','hidden');
		
		});
		$(".vertime2").on('click', function(){        
			$('.tabelinha-nova').append($('.tabela-time2'));
			$('.tabelinha-nova .tabela-carregada').fadeIn(800);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('.tabelinha-nova').css('overflow','auto');
			$('body').css('overflow','hidden');
		});
		$(".vertime3").on('click', function(){
			$('.tabelinha-nova').append($('.tabela-time3'));
			$('.tabelinha-nova .tabela-carregada').fadeIn(800);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('.tabelinha-nova').css('overflow','auto');
			$('body').css('overflow','hidden');
		});
		$(".vertime4").on('click', function(){
			$('.tabelinha-nova').append($('.tabela-time4'));
			$('.tabelinha-nova .tabela-carregada').fadeIn(800);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('.tabelinha-nova').css('overflow','auto');
			$('body').css('overflow','hidden');
		});
		function textoCadTimes(botao){  
			if($(botao).html() == "Adicionar Jogadores"){
				$(botao).html("Ocultar Lista");
			}else{
				$(botao).html("Adicionar Jogadores");
			}        
		}

	$(document).ready(function(){
			var dataTable1 = $('#user_data').DataTable({		
			"processing":true,
			"serverSide":true,
			"searching": false,
			"responsive": true,
			"order":[],
			"ajax":{
				url:"azul1.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
			},
		});
		var dataTable2 = $('#time2').DataTable({		
			"processing":true,
			"serverSide":true,
			"searching": false,
			"responsive": true,
			"order":[],
			"ajax":{
				url:"amarelo1.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
			},
		});
		var dataTable3 = $('#time3').DataTable({		
			"processing":true,
			"serverSide":true,
			"searching": false,
			"responsive": true,
			"order":[],
			"ajax":{
				url:"azul2.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
			},
		});
		var dataTable4 = $('#time4').DataTable({		
			"processing":true,
			"serverSide":true,
			"searching": false,
			"responsive": true,
			"order":[],
			"ajax":{
				url:"amarelo2.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets": [0],
					"orderable": false,
				},
			],
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
			},
		});

		var counter = 0;
		var tabelinha = "";
		$(document).on('click', '.update', function(){
			var user_id = $(this).attr("id");
			var tipo_time = $(this).attr("tipo");
			$('.update').removeClass("campo-temporario-before");
			$(this).addClass("campo-temporario-before");
			$("#campo-temporario").remove();
			// counter+=1;
			$.ajax({
				url:"fetch_single.php",
				method:"POST",
				data:{
					user_id:user_id,
					tipo_time:tipo_time
					},
				dataType:"json",
				success:function(data)
				{
					console.log("dados: ", data);
					// dados = data;
					// setTimeout(function(){
					var infos = data;
					tabelinha  = `			
						<div class="dados_titulo" style="font-size: 12px; text-transform: uppercase; ">
									<div width="2%">id</div>
									<div width="4%">Sumula</div>
									<div width="8%">Jogador 1</div>
									<div width="8%">Jogador 2</div>
									<div width="8%">Jogador 3</div>
									<div width="8%">Jogador 4</div>
									<div width="8%">Jogador 5</div>
									<div width="8%">Jogador 6</div>
									<div width="8%">Jogador 7</div>
									<div width="8%">Jogador 8</div>
									<div width="8%">Jogador 9</div>
									<div width="8%">Jogador 10</div>
									<div width="8%">Jogador 11</div>
									<div width="6%"> Excluir </div>
								</div>`;		
						// for (var i = 0; i < infos.length; i++){;
							tabelinha +=
							`	<div class="dados_jogo">
								<div> ${infos[0].id} </div>
								<div> ${infos[0].su_ref} </div>
								<div> ${infos[0].jog_1} </div>
								<div> ${infos[0].jog_2} </div>
								<div> ${infos[0].jog_3} </div>
								<div> ${infos[0].jog_4} </div>
								<div> ${infos[0].jog_5} </div>
								<div> ${infos[0].jog_6} </div>
								<div> ${infos[0].jog_7} </div>
								<div> ${infos[0].jog_8} </div>
								<div> ${infos[0].jog_9} </div>
								<div> ${infos[0].jog_10} </div>
								<div> ${infos[0].jog_11} </div>
								<div>
									<button class="excluir" name="excluir" id="${infos[0].id}" tipo="${infos[0].bd}"value="${infos[0].id}"> Excluir </button>
								</div>
							</div>
							`
						// }
						var campoTemporario = "<div class='campo-temporario' id='campo-temporario'></div>";
						$('.tabelinha-nova').prepend(campoTemporario);
						$('.campo-temporario').prepend(tabelinha);
							// console.log('counter', counter);
					// }, 800);
					
			
				var tabelinha = " ";
				// var infos = undefined;
				// var counter = undefined;
				}
			});
			
			// var tabelinha = undefined;
			// var infos = undefined;
		});
		
		$(document).on('click', '.excluir', function(){
			var user_id = $(this).attr("id");
			var tipo_time = $(this).attr("tipo");			
			$(this).removeClass("campo-temporario-before");
			if(confirm("Você quer excluir esse registro?"))
			{
				$.ajax({
					url:"delete.php",
					method:"POST",
					data:{
						user_id:user_id,
						tipo_time:tipo_time
					},
					success:function(data)
					{
						alert(data);
						dataTable1.ajax.reload();
						dataTable2.ajax.reload();
						dataTable3.ajax.reload();
						dataTable4.ajax.reload();
						$('.dados_jogo').remove();
						$('.dados_titulo').remove();
					}
				});
			}
			else
			{
				return false;	
			}
		});


		
		$('.fecha-modal').on('click', function(){ 
			$(".update").removeClass("campo-temporario-before");
			setTimeout(function(){
				$('.tabelinha-nova .tabela-carregada').fadeOut(100);
			},100);
			setTimeout(function(){		
				$('.tabelas-times').append($('.tabelinha-nova .tabela-carregada'));
			},200);
			setTimeout(function(){
				$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
				$('.tabelinha-nova').html("");
				$('.dados_jogo').remove();
			},250);
				$('body').css('overflow','auto');
		});    

	});
	</script>
</body>
</html>