<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
function total_results()
{
    global $connection;
	$statement = $connection->prepare("SELECT
	* FROM v_time_az_2");
	$statement->execute();
	$result = $statement->fetchAll();
	return $statement->rowCount();
}

$query = '';
$output = array();
$query .= "SELECT
               v_time_az_2.id_az2 AS id_az2, v_time_az_2.su_ref AS su_ref_az_2, v_time_az_2.jog_1 AS jog_1_az2, v_time_az_2.jog_2 AS jog_2_az2, v_time_az_2.jog_3 AS jog_3_az2, v_time_az_2.jog_4 AS jog_4_az2, v_time_az_2.jog_5 AS jog_5_az2,
                v_time_az_2.jog_6 AS jog_6_az2, v_time_az_2.jog_7 AS jog_7_az2, v_time_az_2.jog_8 AS jog_8_az2, v_time_az_2.jog_9 AS jog_9_az2, v_time_az_2.jog_10 AS jog_10_az2, v_time_az_2.jog_11 AS jog_11_az2, v_time_az_2.cad AS cad_az1

                FROM  v_time_az_2 AS v_time_az_2 ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE v_time_az_2.su_ref LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.sumula LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id_az2 DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();


// $array_times = array();

	foreach($result as $azul2){
		$array_azul2 = array();
        $array_azul2[] = $azul2["id_az2"];
        $array_azul2[] = $azul2["su_ref_az_2"];
        $array_azul2[] = $azul2["jog_1_az2"];
        $array_azul2[] = $azul2["jog_2_az2"];
        $array_azul2[] = $azul2["jog_3_az2"];
        $array_azul2[] = $azul2["jog_4_az2"];
        $array_azul2[] = $azul2["jog_5_az2"];
        $array_azul2[] = $azul2["jog_6_az2"];
        $array_azul2[] = $azul2["jog_7_az2"];
        $array_azul2[] = $azul2["jog_8_az2"];
        $array_azul2[] = $azul2["jog_9_az2"];
        $array_azul2[] = $azul2["jog_10_az2"];
        $array_azul2[] = $azul2["jog_11_az2"];		
        $array_azul2[] = '<button type="button" tipo="v_time_az_2" name="update" id="'.$azul2["id_az2"].'" class="update">Alterar</button>';
		$data[] = $array_azul2;

	}

	
// $data[] = $array_times;
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	total_results(),
	"data"				=>	$data
);
// echo "<pre>";
echo json_encode($output, JSON_PRETTY_PRINT);
?>