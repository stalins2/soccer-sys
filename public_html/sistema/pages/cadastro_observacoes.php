<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-observacoes input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        .cadastro-observacoes .submit{
            width: 180px;
            height: 35px;
            line-height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            cursor: pointer;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .cadastro-observacoes table{
            text-align: left !important;
        }
        .entry:not(:first-of-type)
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }
        .textareas textarea{
            font-size: 12px;
            line-height: 24px;           
        }
      

    </style>
<body>
    <?php
        include('assets/includes/info_status.php');
    ?>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Observações Complementares</h1>
                </div>
                <div class="container-fluid" style="width: 98%;">
                    <form class="cadastro-observacoes" id="cadastro_observacoes" name="cadastro_observacoes"  method="post" action="controllers/cadastro_observacoes_controller.php?acao=cadastrar">                    
                        <!-- Seleção da súmula -->
                        <div class="row">
                            <table class="col table text-center field-wrapper">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"> Jogo (Súmula) - Código/Data</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- código da súmula a ser escolhida -->
                            <div class="container-fluid lista-dados">
                                <div class="form-group row col-sm-12 lista-dados__infos" style="border: none;">
                                    <?=listaSumulas();?>
                                </div>                       
                            </div>
                        </div>
                        <!-- Fim da seleção da súmula -->
                        
                        <div class="row text-light bg-dark text-left">
                            <div class="col-sm-3">Presenças </div> 
                            <div class="col-sm-3">Punições </div>
                            <div class="col-sm-3">Destaques do Jogo</div> 
                            <div class="col-sm-3">Observações</div>
                        </div>
                        
                        <div class="row text-left textareas"> 
                            <div class="row selects-observacoes-1 form-group col-sm-12" style="margin-top: 10px;">
                                <div class="col-sm-3"><textarea maxlength="500" class="form-control" id="presencas" name="presencas" rows="5"></textarea><div id="charNum">*Limite de 500 Caracteres</div></div>
                                <div class="col-sm-3"><textarea maxlength="500" class="form-control" id="punicoes" name="punicoes" rows="5"></textarea><div id="charNum">*Limite de 500 Caracteres</div> </div>
                                <div class="col-sm-3"><textarea maxlength="500" class="form-control" id="destaques" name="destaques" rows="5"></textarea><div id="charNum">*Limite de 500 Caracteres</div> </div> 
                                <div class="col-sm-3"><textarea maxlength="500" class="form-control" id="observacoes" name="observacoes" rows="5"></textarea><div id="charNum">*Limite de 500 Caracteres</div> </div>
                            </div>
                        </div>
                        
                        <input class="submit cadastrar-observacoes" type="submit" id="submit" value="Cadastrar Observações" />
                        
                    </form>
                </div>
                <div class="container-fluid text-center">
                    <?php
                        include('observacoes/index.php');
                    ?>
                </div>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
        
        <script type="text/javascript">
            $(document).ready(function(){
                $('.textareas div textarea').on('keyup', function(){
                        var contando = $(this).parent().find('textarea').val().length;   
                        console.log('contando');
                        if (contando >= 500) {
                            $(this).parent().find('#charNum').text('*Limite Atingido');                            
                        } else {
                            $(this).parent().find('#charNum').text(500 - contando +' Caractere(s) restante(s)');
                        }                    
                });
            });
        </script>
    </body>
</html>