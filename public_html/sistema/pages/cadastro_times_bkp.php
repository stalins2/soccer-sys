<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-membros input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: left;
        }
        .cadastro-membros .submit{
            width: 150px;
            height: 35px;
            line-height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
            margin-bottom: 20px;
        }
        .no-gutter > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .cadastro-membros table{
            text-align: left !important;
        }
        .entry:not(:first-of-type)
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }
      
        .linha-times{
            margin-bottom: 20px;
        }
        .time .row{
            margin-left: 0;
        }
        
        .time .row input{
            height: 35px;
        }
        .time .submit{
            margin-top: 20px;
            cursor: pointer;
        }
        .time .submit:hover{
            background-color: #f0f0f0;
        }
        .time .row:not(:first-child){
            display: none;
        }
        .columns-grid{
            width: 100%;
            display: grid;
            margin-bottom: 20px;
            border-bottom: 2px solid;
            grid-template-columns: 10% 15% 15% 15% 15% 15% 15%;            
        }
        /* .columns-grid:not(:first-child){
            display: none;
        } */
        .cell-time:nth-child(n+8) {
            display: none;
        }
        .mostra-time{
            display: block !important;
        }
        .lista-dados__infos{
            margin-bottom: 10px;
        }
        /* .column-player{
    
            
        } */
    </style>
<body>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Cadastro dos Times</h1>
                </div>
                <div class="container-fluid" style="width: 98%;">
                <form class="cadastro-membros" id="cadastro_times" name="cadastro_times" method="post" action="controllers/cadastro_times_controller.php?acao=cadastrar">
                    <!-- Seleção da súmula -->
                    <div class="row">
                        <table class="col table text-center field-wrapper">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Jogo (Súmula) - Código/Data</th>
                                </tr>
                            </thead>
                        </table>
                        <!-- código da súmula a ser escolhida -->
                        <div class="container-fluid lista-dados">
                            <div class="form-group row col-sm-12 lista-dados__infos" style="border: none;">
                                <?=listaSumulas();?>
                            </div>                       
                        </div>
                        <!-- Fim da seleção da súmula -->
                    </div>
                    <!-- Primeiro tempo de times -->
                    <div class="row">
                        <table class="col table text-center field-wrapper">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Primeiro Tempo</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="row text-left linha-times">
                        <div class="col-md-5 time time_azul_1">
                            <div class="row">
                                  Time Azul 1º Tempo  
                            </div>
                            <div class="text-left">
                                <div class="submit text-center">Preencher Time</div>
                            </div>
                            <div class="row">
                                <label for="azul_1_1">Goleiro</label>
                                <select class="form-control" id="azul_1_1" name="azul_1_1">
                                    <?=listarUsuarios();?>
                                </select>                                
                            </div>
                            <div class="row">
                                <label for="azul_1_2">Jogador 2</label>
                                <select class="form-control" id="azul_1_2" name="azul_1_2">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_3">Jogador 3</label>
                                <select class="form-control" id="azul_1_3" name="azul_1_3">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_4">Jogador 4</label>
                                <select class="form-control" id="azul_1_4" name="azul_1_4">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_5">Jogador 5</label>
                                <select class="form-control" id="azul_1_5" name="azul_1_5">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_6">Jogador 6</label>
                                <select class="form-control" id="azul_1_6" name="azul_1_6">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_7">Jogador 7</label>
                                <select class="form-control" id="azul_1_7" name="azul_1_7">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_8">Jogador 8</label>
                                <select class="form-control" id="azul_1_8" name="azul_1_8">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_9">Jogador 9</label>
                                <select class="form-control" id="azul_1_9" name="azul_1_9">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_1_10">Jogador 10</label>
                                <select class="form-control" id="azul_1_10" name="azul_1_10">
                                    <?=listarUsuarios();?>
                                </select>                             
                            </div>
                            <div class="row">
                                <label for="azul_1_11">Jogador 11</label>
                                <select class="form-control" id="azul_1_11" name="azul_1_11">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5 time time_amarelo_1">
                            <div class="row">
                                  Time Amarelo 1º Tempo  
                            </div>
                            <div class="text-left">
                                <div class="submit text-center">Preencher Time</div>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_1">Goleiro</label>
                                <select class="form-control" id="amarelo_1_1" name="amarelo_1_1">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_2">Jogador 2</label>
                                <select class="form-control" id="amarelo_1_2" name="amarelo_1_2">
                                    <?=listarUsuarios();?>
                                </select> 
                            </div>
                            <div class="row">
                                <label for="amarelo_1_3">Jogador 3</label>
                                <select class="form-control" id="amarelo_1_3" name="amarelo_1_3">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_4">Jogador 4</label>
                                <select class="form-control" id="amarelo_1_4" name="amarelo_1_4">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_5">Jogador 5</label>
                                <select class="form-control" id="amarelo_1_5" name="amarelo_1_5">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_6">Jogador 6</label>
                                <select class="form-control" id="amarelo_1_6" name="amarelo_1_6">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_7">Jogador 7</label>
                                <select class="form-control" id="amarelo_1_7" name="amarelo_1_7">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_8">Jogador 8</label>
                                <select class="form-control" id="amarelo_1_8" name="amarelo_1_8">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_9">Jogador 9</label>
                                <select class="form-control" id="amarelo_1_9" name="amarelo_1_9">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_10">Jogador 10</label>
                                <select class="form-control" id="amarelo_1_10" name="amarelo_1_10">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_1_11">Jogador 11</label>
                                <select class="form-control" id="amarelo_1_11" name="amarelo_1_11">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <table class="col table text-center field-wrapper">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"> Segundo Tempo</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="row text-left linha-times">
                        <div class="col-md-5 time time_azul_2">
                            <div class="row">
                                  Time Azul 2º Tempo  
                            </div>
                            <div class="text-left">
                                <div class="submit text-center">Preencher Time</div>
                            </div>
                            <div class="row">
                                <label for="azul_2_1">Goleiro</label>
                                <select class="form-control" id="azul_2_1" name="azul_2_1">
                                    <?=listarUsuarios();?>
                                </select>                                
                            </div>
                            <div class="row">
                                <label for="azul_2_2">Jogador 2</label>
                                <select class="form-control" id="azul_2_2" name="azul_2_2">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_3">Jogador 3</label>
                                <select class="form-control" id="azul_2_3" name="azul_2_3">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_4">Jogador 4</label>
                                <select class="form-control" id="azul_2_4" name="azul_2_4">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_5">Jogador 5</label>
                                <select class="form-control" id="azul_2_5" name="azul_2_5">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_6">Jogador 6</label>
                                <select class="form-control" id="azul_2_6" name="azul_2_6">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_7">Jogador 7</label>
                                <select class="form-control" id="azul_2_7" name="azul_2_7">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_8">Jogador 8</label>
                                <select class="form-control" id="azul_2_8" name="azul_2_8">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_9">Jogador 9</label>
                                <select class="form-control" id="azul_2_9" name="azul_2_9">
                                    <?=listarUsuarios();?>
                                </select>                            
                            </div>
                            <div class="row">
                                <label for="azul_2_10">Jogador 10</label>
                                <select class="form-control" id="azul_2_10" name="azul_2_10">
                                    <?=listarUsuarios();?>
                                </select>                             
                            </div>
                            <div class="row">
                                <label for="azul_2_11">Jogador 11</label>
                                <select class="form-control" id="azul_2_11" name="azul_2_11">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5 time time_amarelo_2">
                            <div class="row">
                                  Time Amarelo 2º Tempo  
                            </div>
                            <div class="text-left">
                                <div class="submit text-center">Preencher Time</div>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_1">Goleiro</label>
                                <select class="form-control" id="amarelo_2_1" name="amarelo_2_1">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_2">Jogador 2</label>
                                <select class="form-control" id="amarelo_2_2" name="amarelo_2_2">
                                    <?=listarUsuarios();?>
                                </select> 
                            </div>
                            <div class="row">
                                <label for="amarelo_2_3">Jogador 3</label>
                                <select class="form-control" id="amarelo_2_3" name="amarelo_2_3">
                                <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_4">Jogador 4</label>
                                <select class="form-control" id="amarelo_2_4" name="amarelo_2_4">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_5">Jogador 5</label>
                                <select class="form-control" id="amarelo_2_5" name="amarelo_2_5">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_6">Jogador 6</label>
                                <select class="form-control" id="amarelo_2_6" name="amarelo_2_6">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_7">Jogador 7</label>
                                <select class="form-control" id="amarelo_2_7" name="amarelo_2_7">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_8">Jogador 8</label>
                                <select class="form-control" id="amarelo_2_8" name="amarelo_2_8">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_9">Jogador 9</label>
                                <select class="form-control" id="amarelo_2_9" name="amarelo_2_9">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_10">Jogador 10</label>
                                <select class="form-control" id="amarelo_2_10" name="amarelo_2_10">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                            <div class="row">
                                <label for="amarelo_2_11">Jogador 11</label>
                                <select class="form-control" id="amarelo_2_11" name="amarelo_2_11">
                                    <?=listarUsuarios();?>
                                </select>
                            </div>
                        </div>
                    </div>                    
                    <input class="submit" type="submit" id="submit" value="Cadastrar Times" />
                </form>
                <!-- Lista dos times -->
                
                <div class="container-fluid lista-dados" style="margin-bottom: 50px;">
                    <div class="form-group row col-sm-12 lista-dados__titulos">
                        <!-- <label for="IdCadastro" class="col-sm-1 col-form-label">ID de Cadastro</label> -->
                        <label for="SumulaJogo" class="col-sm-1 col-form-label">Sumula do Jogo</label>
                        <label for="PlacarTimeAmarelo" class="col-sm-2 col-form-label">Time Azul 1 Tempo</label>
                        <label for="PlacarTimeAmarelo" class="col-sm-2 col-form-label">Time Amarelo 1 Tempo</label>
                        <label for="PlacarTimeAmarelo" class="col-sm-2 col-form-label">Time Azul 2 Tempo</label>
                        <label for="PlacarTimeAmarelo" class="col-sm-2 col-form-label">Time Amarelo 2 Tempo</label>
                        <label for="Editar" class="col-sm-1 col-form-label">
                            <i class="fas fa-edit fa-lg"></i>
                            <i class="fas fa-trash-alt fa-lg float-right"></i>
                        </label>
                    </div>                    
                    <div class="form-group row col-sm-12 lista-dados__infos">
                        <? foreach ($times as $key => $lista){ ?>
                        <div class="columns-grid">                            
                            <div class="cell-time" id="sumula_<?=$lista->su_ref_az_1;?>"><?=$lista->su_ref_az_1;?></div><div class="cell-time column-player"><?=$lista->jog_1_az1;?></div><div class="cell-time column-player"><?=$lista->jog_1_am1;?></div><div class="cell-time column-player"><?=$lista->jog_1_az2;?></div><div class="cell-time column-player"><?=$lista->jog_1_am2;?></div><div class="cell-time"> <i class="fas fa-edit fa-lg text-info" onclick="editar('<?=$lista->id;?>', '<?=$lista->sumula_referencia;?>', '<?=$lista->j_az1;?>', '<?=$lista->j_am1;?>', '<?=$lista->j_az2;?>', '<?=$lista->j_am2;?>');"></i></div><div class="cell-time"><i class="fas fa-lg fa-trash-alt text-danger float-right" onclick="confirmar_exclusao(<?=$lista->su_ref_az_1;?>)"></i></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_2_az1;?></div><div class="cell-time column-player"><?=$lista->jog_2_am1;?></div><div class="cell-time column-player"><?=$lista->jog_2_az2;?></div><div class="cell-time column-player"><?=$lista->jog_2_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_3_az1;?></div><div class="cell-time column-player"><?=$lista->jog_3_am1;?></div><div class="cell-time column-player"><?=$lista->jog_3_az2;?></div><div class="cell-time column-player"><?=$lista->jog_3_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_4_az1;?></div><div class="cell-time column-player"><?=$lista->jog_4_am1;?></div><div class="cell-time column-player"><?=$lista->jog_4_az2;?></div><div class="cell-time column-player"><?=$lista->jog_4_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_5_az1;?></div><div class="cell-time column-player"><?=$lista->jog_5_am1;?></div><div class="cell-time column-player"><?=$lista->jog_5_az2;?></div><div class="cell-time column-player"><?=$lista->jog_5_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_6_az1;?></div><div class="cell-time column-player"><?=$lista->jog_6_am1;?></div><div class="cell-time column-player"><?=$lista->jog_6_az2;?></div><div class="cell-time column-player"><?=$lista->jog_6_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_7_az1;?></div><div class="cell-time column-player"><?=$lista->jog_7_am1;?></div><div class="cell-time column-player"><?=$lista->jog_7_az2;?></div><div class="cell-time column-player"><?=$lista->jog_7_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_8_az1;?></div><div class="cell-time column-player"><?=$lista->jog_8_am1;?></div><div class="cell-time column-player"><?=$lista->jog_8_az2;?></div><div class="cell-time column-player"><?=$lista->jog_8_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_9_az1;?></div><div class="cell-time column-player"><?=$lista->jog_9_am1;?></div><div class="cell-time column-player"><?=$lista->jog_9_az2;?></div><div class="cell-time column-player"><?=$lista->jog_9_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_10_az1;?></div><div class="cell-time column-player"><?=$lista->jog_10_am1;?></div><div class="cell-time column-player"><?=$lista->jog_10_az2;?></div><div class="cell-time column-player"><?=$lista->jog_10_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                            <div class="cell-time"></div><div class="cell-time column-player"><?=$lista->jog_11_az1;?></div><div class="cell-time column-player"><?=$lista->jog_11_am1;?></div><div class="cell-time column-player"><?=$lista->jog_11_az2;?></div><div class="cell-time column-player"><?=$lista->jog_11_am2;?></div><div class="cell-time"> </div><div class="cell-time"></div>
                        </div> 
                        <?}?>
                    </div>
                </div>
                <!-- /Lista dos times -->
                </div>
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
        
        <script type="text/javascript">
        $(document).ready(function(){
            $('div.columns-grid').on('click', function(){
                // $(this).children('.cell-time').toggleClass('mostra-time');
                $(this).children('.cell-time').toggle(200);
            })
        });
            $(".time_azul_1 .submit").click(function(){ 
                $(".time_azul_1 .row").toggle("fast");
                $(this).html($(this).html()=="Preencher Time"?"Ocultar Campos":"Preencher Time");
            });
            $(".time_azul_2 .submit").click(function(){ 
                $(".time_azul_2 .row").toggle("fast");
                $(this).html($(this).html()=="Preencher Time"?"Ocultar Campos":"Preencher Time");
            });
            $(".time_amarelo_1 .submit").click(function(){ 
                $(".time_amarelo_1 .row").toggle("fast");
                $(this).html($(this).html()=="Preencher Time"?"Ocultar Campos":"Preencher Time");
            });
            $(".time_amarelo_2 .submit").click(function(){ 
                $(".time_amarelo_2 .row").toggle("fast");
                $(this).html($(this).html()=="Preencher Time"?"Ocultar Campos":"Preencher Time");
            });
            function excluir(id){
                location.href= "cadastro_times.php?acao=excluir&id="+id;
            }
        </script>
    </body>
</html>