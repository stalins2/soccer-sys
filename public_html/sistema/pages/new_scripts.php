<script>
var pagina;
    $(".menu-link").on('click', function(e){ //Carrega o item do value do menu
        e.preventDefault();
        var pagina = $(this).attr("value");
        $('.conteudo-page').html('Carregando...'); // Mostrar "Carregando..."
        $.ajax({
            url: pagina+".php"
        }).done(function(data) { // Página carregada do php
            $('.conteudo-page').html(data);
            // setTimeout(function(){
            //     loadFunctions();
            // },500)               
        });
        console.log('pagina: ',pagina);
    });
function loadFunctions(){
    if(pagina == "cad_times"){
        botoesTimes();
        console.log('cad times');
    }
    if(pagina == "cad_membros"){
        cadMembros();
        console.log('cad membros');
    }
}
function cadMembros(){
    $("#data_nascimento").datepicker({dateFormat: 'dd-mm-yy'});
}

function botoesTimes(){
	var tabela;	
    var botao;
    $(".botao1").on('click', function(){
        $('.lista-time-1').toggle("fast");
        var botao = ".botao1";
        textoCadTimes(botao);
    });
    $(".botao2").on('click', function(){
        $('.lista-time-2').toggle("fast");
        var botao = ".botao2";
        textoCadTimes(botao);
    });
    $(".botao3").on('click', function(){
        $('.lista-time-3').toggle("fast");
        var botao = ".botao3";
        textoCadTimes(botao);
    });
    $(".botao4").on('click', function(){
        $('.lista-time-4').toggle("fast");
        var botao = ".botao4";
        textoCadTimes(botao);
    });
    $(".vertime1").on('click', function(){
        $('.tabelinha-nova').append($('.tabela-time1'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
       
    });
    $(".vertime2").on('click', function(){        
        $('.tabelinha-nova').append($('.tabela-time2'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
    });
    $(".vertime3").on('click', function(){
        $('.tabelinha-nova').append($('.tabela-time3'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
    });
    $(".vertime4").on('click', function(){
        $('.tabelinha-nova').append($('.tabela-time4'));
        $('.tabelinha-nova .tabela-carregada').fadeIn(800);
        $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.tabelinha-nova').css('overflow','auto');
        $('body').css('overflow','hidden');
    });
    function textoCadTimes(botao){  
        if($(botao).html() == "Adicionar Jogadores"){
            $(botao).html("Ocultar Lista");
        }else{
            $(botao).html("Adicionar Jogadores");
        }        
    }
}

function selecionar10(tipo, label, selec10id, selec10Div, class10Select){
    let option = ""; 
    for(let i = 0; i <= 10; i++){            
        option += "<option value='"+i+"'>"+i+"</option>";
    }   
    let select = "<div class='"+selec10Div+"'><label for='"+selec10id+"'>"+tipo+" "+label+"</label><select name='"+selec10id+"' id='"+selec10id+"' class='"+class10Select+"'>"+option+"</select></div>";
    return select;
}
function criarDiv(idDiv, classeDiv, cssDiv){
    div = "<div class='"+classeDiv+"' id='"+idDiv+"' style='"+cssDiv+"'></div>";
    return div;        
}

let id = 1; //ID inicial para selects
function criarDivSelect(idSelect, classDivSelect, classSelect){            
    let options = "<?=listaResults();?>";
    let select = "<div class='"+classDivSelect+"'> <label for='"+idSelect+"'>Jogador</label> <select class='"+classSelect+"' id='"+idSelect+"' name='"+idSelect+"'>"+options+"</select></div>";        
    return select;
}

function selecionarTime(selecTimeId, selecTimeDiv, classTimeSelect){
    let option1 = "<option value='1'>Amarelo</option>";
    let option2 = "<option value='2'>Azul</option>";
    let select = "<div class='"+selecTimeDiv+"'><label for='"+selecTimeId+"'>Time</label><select name='"+selecTimeId+"' id='"+selecTimeId+"' class='"+classTimeSelect+"'>"+option1+""+option2+"</select></div>";
    return select;
}
</script>