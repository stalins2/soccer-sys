<?
require_once('../assets/includes/validador_acesso.php');
include('../header_loaders.php');
include('../new_lists.php');
?>
<html>

<head>
	<title> Sistema - Fohfaoi</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="../assets/css/new_view.css" />
	<style>
		tr.dtrg-start {
			cursor: pointer;
		}

		.modal-tabelinha-hidden{
			width: 0;
			height: 0;
			opacity: 0;
		}
		.modal-tabelinha{
			position: fixed;
			top: 0;
			left: 0;
			display: block;
			margin: 0 auto;
			width: 100vw;
			height: 100vh;
			overflow: auto;
			z-index: 9999;
			opacity: 1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		.tabelinha-nova{
			width: 90%;
			height: auto;
			padding: 20px;
			margin: 5% auto;
			background-color: #fff;
		}
		.fecha-modal{
			position: absolute;
			right: 10px;
			top: 10px;
			color: #fff;
			font-weight: bold;
			font-size: 15px;
			cursor: pointer;
			text-transform: uppercase;
		}
	

		.tabelinha-nova .dados_titulo {
			font-weight: bold;
			background-color: #fdfdfd;
		}

		.tabelinha-nova .dados_jogo {
			border: 1px solid;
			font-size: 13px;
			margin-top: 1px;
		}

		.tabelinha-nova .dados_jogo button {
			border: none;
			background-color: #fff;

		}

		input[type=date] {
			border-radius: 5px;
			height: 42px;
			width: 100%;
			line-height: 40px;
			text-align: center;
			outline: none;
		}

		input[type=date]:focus {
			border: 2px solid #f56a6a;
		}
	</style>
</head>

<body class="is-preload">
<!-- <form id="cadastro_sumula" name="cadastro_sumula" method="post" action="../controllers_new/cadastro_membros_controller.php?acao=cadastrar"> -->
<form method="post" id="member_form" name="member_form" enctype="multipart/form-data">
	<!-- Wrapper -->
	<div id="wrapper">
		<!-- Main -->
			<div id="main" class="conteudo-page">
				<div class="inner">
					<header id="header">
						<a href="#" class="logo"><strong>Cadastrar </strong>Novo Membro</a>
					</header>
					<!-- Content -->
					<div class="row gtr-200">
						<div class="">
							<div class="text-center" style="width: 100%;">
							</div>
							<br />
							<div class="row gtr-uniform">
								<div class="col-4 col-12-xsmall">
									<label for="nome">Nome</label>
									<input type="text" id="nome" name="nome" maxlength="50" placeholder="Nome" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="apelido">Apelido</label>
									<input type="text" id="apelido" name="apelido" maxlength="30"
										placeholder="Apelido" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="Padrinho">Padrinho</label>
									<input type="text" id="padrinho" name="padrinho" maxlength="30"
										placeholder="Padrinho" />
								</div>
								<!-- quebra -->
								<div class="col-4 col-12-xsmall">
									<label for="chute">Chute</label>
									<input type="text" id="chute" name="chute" maxlength="50" placeholder="Chute" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="altura">Altura</label>
									<input type="text" id="altura" name="altura" maxlength="30"
										placeholder="Altura" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="Peso">Peso</label>
									<input type="text" id="peso" name="peso" maxlength="30"
										placeholder="Peso" />
								</div>
								<!-- quebra -->
								<div class="col-4 col-12-xsmall">
									<label for="posicao">Posição</label>
									<input type="text" id="posicao" name="posicao" maxlength="50"
										placeholder="Posição" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="data_nascimento">Data de Nasciento</label>
									<input type="date" id="data_nascimento" name="data_nascimento"
										data-provide="datepicker" placeholder="Data de Nascimento" maxlength="20" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="natural_de">Naturalidade</label>
									<input type="text" id="natural_de" name="natural_de" maxlength="20"
										placeholder="Naturalidade" />
								</div>
								<!-- quebra -->
								<div class="col-3 col-12-xsmall">
									<label for="time_que_torce">Time que torce</label>
									<input type="text" id="time_que_torce" name="time_que_torce"
										placeholder="Time que torce" maxlength="50" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="endereco">Endereço</label>
									<input type="text" id="endereco" name="endereco" placeholder="Endereço"
										maxlength="50" />
								</div>
								<div class="col-3 col-12-xsmall">
									<label for="bairro">Bairro</label>
									<input type="text" id="bairro" name="bairro" placeholder="Bairro" maxlength="30" />
								</div>
								<div class="col-2 col-12-xsmall">
									<label for="cep">CEP</label>
									<input type="text" id="cep" name="cep" class="cep" placeholder="CEP"
										maxlength="20" />
								</div>
								<!-- quebra -->
								<div class="col-4 col-12-xsmall">
									<label for="data_entrada">Data de Entrada</label>
									<input type="date" id="data_entrada" name="data_entrada"
										placeholder="Data de Entrada" maxlength="20" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="data_saida">Data de Saída</label>
									<input type="date" id="data_saida" name="data_saida" placeholder="Data de Saída"
										maxlength="30" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="status">Status</label>
									<select class="form-control" id="status" name="status" onchange="afastadoDesde();">
										<option value="Ativo">Ativo</option>
										<option value="Inativo">Inativo</option>
										<option value="Afastado">Afastado</option>
									</select>
									<span id="data_afastado"></span>
								</div>
								<!-- quebra -->
								<div class="col-4 col-12-xsmall">
									<label for="celular">Celular</label>
									<input type="text" id="celular" name="celular" class="tel" placeholder="Celular"
										maxlength="50" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="email">E-mail</label>
									<input type="text" id="email" name="email" placeholder="E-mail" maxlength="50" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="tel">Telefone</label>
									<input type="text" id="telefone" class="tel" name="telefone" placeholder="Telefone"
										maxlength="20" />
								</div>
								<!-- quebra -->
								<div class="col-4 col-12-xsmall">
									<label for="rg">RG</label>
									<input type="text" id="rg" name="rg" placeholder="RG" maxlength="30" />
								</div>
								<div class="col-4 col-12-xsmall">
									<label for="cpf">CPF</label>
									<input type="text" id="cpf" class="cpf" name="cpf" placeholder="cpf"
										maxlength="20" />
								</div>
								<div class="col-4 col-12-xsmall">
									<!-- <label for="submit">Confirmar Cadastro</label> -->
									<input class="submit" type="submit" id="submit" value="Cadastrar Membro" />
									<input type="hidden" name="operation" id="operation" />
									<input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
								</div>
							</div>
						</div>						
					</div>
					</form>	
					<hr class="major" />	
					<h4>Lista de Membros</h4>
					<div class="row gtr-200">
						<div class="col-10">
							<div class="container-fluid">
								<?
								// include('sumula.php');
								?>
								<table id="user_data" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="2%">ID</th>
											<th width="12%">Nome</th>
											<th width="6%">Apelido</th>
											<th width="6%">Data de Nascimento</th>
											<th width="8%">Padrinho</th>
											<th width="6%">Data de Entrada</th>
											<th width="10%">Time que Torce</th>
											<th width="10%">Celular</th>
											<th width="10%">Telefone</strong></th>
											<th width="10%">Email</th>
											<th width="10%">Endereço </th>
											<th width="10%">Bairro</th>
											<th width="10%">Posição</th>
											<th width="10%">Chute</th>
											<th width="10%">Altura</th>
											<th width="10%">Peso</th>
											<th width="10%">CEP</th>
											<th width="10%">Naturalidade</th>
											<th width="10%">RG</th>
											<th width="10%">CPF</th>
											<th width="10%">Status</th>
											<th width="10%">Data de Afastamento</th>
											<th width="10%">Data do Cadastro</th>
											<th width="10%">Data de Saída</th>
											<th width="10%">Ultima Atualização</th>
											<th width="10%">Foto</th>
											<!-- <th width="7%">ID Cadastrador</th>
											<th width="7%">Atualizado Por</th> -->
											<th width="5%">Alterar</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-tabelinha-hidden">
						<div class="fecha-modal"> X Fechar </div>
						
						<form method="post" id="update_form" name="update_form" enctype="multipart/form-data">	
							<section class="tabelinha-nova">		
							</section>
						</form>
					</div>
				</div>
			</div>
		<?
		include('../new_menu.php');
		include('../libs.php');
		?>
	</div>
	

	

	<script>

		function afastadoDesde(){ 
			let spanAfastado = $("#data_afastado");
			let valStatus = $("#status").val();
			if (valStatus == 'Afastado') {
				let inputAfastamento = document.createElement('input')
				inputAfastamento.type = 'date'
				inputAfastamento.name = 'data_afastamento'
				inputAfastamento.id = 'data_afastamento'
				inputAfastamento.className = 'form-control' 		
				labelAfastamento = '<br><label for="data_afastamento">Data de afastamento</label>'
				
				$(spanAfastado).append(labelAfastamento);
				$(spanAfastado).append(inputAfastamento);

			} else if(valStatus != 'Afastado'){
				$(spanAfastado).html("");
			}
		}

		$(document).ready(function(){

			
			var SPMaskBehavior = function (val) {
 				return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			},
			spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(SPMaskBehavior.apply({}, arguments), options);
				}
			};
			$('#celular').mask(SPMaskBehavior, spOptions);
			$('#cep').mask('00000-000');
			$('#telefone').mask('(00) 0000-0000');
			$('#cpf').mask('000.000.000-00', {reverse: true});
			$('#altura').mask('0.00', {reverse: true});
			$('#peso').mask('000', {reverse: true});
			$('#rg').mask('00.000.000-0', {reverse: true});


					
			$('.fecha-modal').on('click', function(){ 
				setTimeout(function(){
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('.tabelinha-nova').html("");
				},100);
					$('body').css('overflow','auto');
			}); 
			// var collapsedGroups = {};

			var dataTable = $('#user_data').DataTable({

				"processing": true,
				"serverSide": true,
				"pageLength": 10,
				"responsive": {
					orthogonal: 'responsive'
				},
				"dom": 'Bfrtip',
				"lengthMenu": [
					[10, 25, 50, 500],
					['10 linhas', '25 linhas', '50 linhas', 'Todas']
				],
				"buttons": [{
					extend: 'pageLength',
					text: 'Mostrar'
				}, {
					extend: 'excel',
					text: 'Exportar para planilha'
				}],
				"order": [],
				"columnDefs":[{
					targets: [0],
					orderable: false
				}],
				"ajax": {
					url: "fetch.php",
					type: "post"
				},
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
				}
			});


			// $('#submit').click(function(event){
			// 	event.preventDefault();
			// 	// $('#member_form')[0].reset();
			// 	// $('#action').val("Adicionar");
			// });
			$(document).on('submit', '#member_form', function(event){
				$('#operation').val("cadastrar");	
				event.preventDefault();
				var formdata = new FormData($("form[name='member_form']")[0]);
				
				if(confirm("Você confirma o cadastro dos dados preenchidos?")){
					$.ajax({
						url:"../controllers_new/cadastro_membros_controller.php",
						type:'POST',
						data: formdata,
						contentType:false,
						processData:false,
						success:function(data){
							console.log("cadastro: ", data);
							alert(data);
							$('#member_form')[0].reset();
							dataTable.ajax.reload();
						}
					});
				}
			});



			//update loader
			// $('#update').click(function(event){
			// 	event.preventDefault();
			// 	// $('#member_form')[0].reset();
			// 	// $('#action').val("Adicionar");
			// });
			$(document).on('click', '.update', function(event){								
				// $('#operation').val("atualizar");		
				event.preventDefault();
				var user_id = $(this).attr("id");
				// var tipo_time = $(this).attr("tipo");
				// console.log('tipo:', tipo_time);
				var dados;
				$.ajax({
					url:"fetch_single.php",
					method:"POST",
					data:{
						user_id:user_id
						// tipo_time:tipo_time
						},
					dataType:"json",
					success:function(data){
						console.log("dados: ", data);
						dados = data;
						
						setTimeout(function(){
							let infos = dados;
							let tabelinha  = `
									<div class="row gtr-200">
										<div class="col-11 d-flex text-left flex-wrap">
											<div class="col-12 d-flex flex-wrap" style="padding: 0;">
												<div class="text-center" style="width: 100%;">
												</div>
												<br />
												<div class="row gtr-uniform">
													<div class="col-12 col-12-xsmall text-left">
													<label for="user_image">Foto: </label>
													<input type="hidden" name="hidden_user_image" id="hidden_user_image" value="${infos[0].hiddenFoto}" />
													<input type="file" name="user_image" id="user_image" />
													<span id="user_uploaded_image"></span>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="nome">Nome</label>
														<input type="text" id="nome" name="nome" maxlength="50" placeholder="Nome" value="${infos[0].nome}" />
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="apelido">Apelido</label>
														<input type="text" id="apelido" name="apelido" maxlength="30"
															placeholder="Apelido" value="${infos[0].apelido}" />
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="Padrinho">Padrinho</label>
														<input type="text" id="padrinho" name="padrinho" maxlength="30"
															placeholder="Padrinho" value="${infos[0].padrinho}"/>
													</div>
													<!-- quebra -->
													<div class="col-4 col-12-xsmall">
														<label for="chute">Chute</label>
														<input type="text" id="chute" name="chute" maxlength="50" placeholder="Chute" value="${infos[0].chute}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="altura">Altura (m.cm)</label>
														<input type="text" id="altura" name="altura" maxlength="30"
															placeholder="Altura" value="${infos[0].altura}" />
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="Peso">Peso (kg)</label>
														<input type="text" id="peso" name="peso" maxlength="30"
															placeholder="Peso" value="${infos[0].peso}" />
													</div>
													<!-- quebra -->
													<div class="col-4 col-12-xsmall">
														<label for="posicao">Posição</label>
														<input type="text" id="posicao" name="posicao" maxlength="50"
															placeholder="Posição" value="${infos[0].posicao}" />
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="data_nascimento">Data de Nasciento</label>
														<input type="date" id="data_nascimento" name="data_nascimento"
															data-provide="datepicker" placeholder="Data de Nascimento" maxlength="20" value="${infos[0].data_nascimento}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="natural_de">Naturalidade</label>
														<input type="text" id="natural_de" name="natural_de" maxlength="20"
															placeholder="Naturalidade" value="${infos[0].natural_de}"/>
													</div>
													<!-- quebra -->
													<div class="col-3 col-12-xsmall">
														<label for="time_que_torce">Time que torce</label>
														<input type="text" id="time_que_torce" name="time_que_torce"
															placeholder="Time que torce" maxlength="50" value="${infos[0].time_que_torce}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="endereco">Endereço</label>
														<input type="text" id="endereco" name="endereco" placeholder="Endereço"
															maxlength="50" value="${infos[0].endereco}" />
													</div>
													<div class="col-3 col-12-xsmall">
														<label for="bairro">Bairro</label>
														<input type="text" id="bairro" name="bairro" placeholder="Bairro" maxlength="30" value="${infos[0].bairro}"/>
													</div>
													<div class="col-2 col-12-xsmall">
														<label for="cep">CEP</label>
														<input type="text" id="cep" name="cep" class="cep" placeholder="CEP"
															maxlength="20" value="${infos[0].cep}"/>
													</div>
													<!-- quebra -->
													<div class="col-4 col-12-xsmall">
														<label for="data_entrada">Data de Entrada</label>
														<input type="date" id="data_entrada" name="data_entrada"
															placeholder="Data de Entrada" maxlength="20" value="${infos[0].data_entrada}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="data_saida">Data de Saída</label>
														<input type="date" id="data_saida" name="data_saida" placeholder="Data de Saída"
															maxlength="30" value="${infos[0].data_saida}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="status">Status</label>
														<select class="form-control" id="status" name="status" onchange="afastadoDesde();">
															<option value="Ativo">Ativo</option>
															<option value="Inativo">Inativo</option>
															<option value="Afastado">Afastado</option>
														</select>
														<span id="data_afastado">
														<label for="data_afastamento">Data de Afastamento</label>
															<input type="text" name="data_afastamento" id="data_afastamento" class="form-control" value="${infos[0].data_afastamento}">
														</span>
													</div>
													<!-- quebra -->
													<div class="col-4 col-12-xsmall">
														<label for="celular">Celular</label>
														<input type="text" id="celular" name="celular" class="tel" placeholder="Celular"
															maxlength="50" value="${infos[0].celular}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="email">E-mail</label>
														<input type="text" id="email" name="email" placeholder="E-mail" maxlength="50" value="${infos[0].email}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="tel">Telefone</label>
														<input type="text" id="telefone" class="tel" name="telefone" placeholder="Telefone"
															maxlength="20" value="${infos[0].telefone}"/>
													</div>
													<!-- quebra -->
													<div class="col-4 col-12-xsmall">
														<label for="rg">RG</label>
														<input type="text" id="rg" name="rg" placeholder="RG" maxlength="30" value="${infos[0].rg}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<label for="cpf">CPF</label>
														<input type="text" id="cpf" class="cpf" name="cpf" placeholder="cpf"
															maxlength="20" value="${infos[0].rg}"/>
													</div>
													<div class="col-4 col-12-xsmall">
														<input type="hidden" name="operation" id="operation" value="atualizar"/>
														<input type="hidden" name="id" id="id" value="${infos[0].id}" />
													</div>
													<div class="col-4 col-12-xsmall text-left">
														<input class="atualizar" type="submit" id="atualizar" value="Atualizar"/>
													</div>
													<div class="col-4 col-12-xsmall text-left">
														<input class="excluir" type="submit" id="excluir" value="Excluir Membro" />
													</div>

												</div>
											</div>	
										</div>
									</div>
								`			
							$('.tabelinha-nova').html(tabelinha);	

							$('.tabelinha-nova #status').val(infos[0].status);					
							$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
							$('body').css('overflow','hidden');							
							$('#data_afastamento').mask('00/00/0000');
						}, 800);
					}
				});
			});
			//end update loader

			$(document).on('click', '.atualizar', function(){		
				event.preventDefault();
				var user_id = $("#id").attr("value");
				var formdata = new FormData($("form[name='update_form']")[0]);
				if(confirm("Você confirma a atualização desse cadastro?"))
				{
					$.ajax({
						url:"../controllers_new/cadastro_membros_controller.php",
						method:"POST",
						data: formdata,
						contentType: false,
						processData: false,
						success:function(data)
						{	
							console.log(data);
							alert(data);
							dataTable.ajax.reload();
							setTimeout(function(){
								$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
								$('.tabelinha-nova').html("");
							},100);
								$('body').css('overflow','auto');
						}
					});
				}
				else
				{
					return false;	
				}
			});

			$(document).on('click', '.excluir', function(){
				event.preventDefault();
				var user_id = $("#update_form #id").val();
				if (confirm("Você realmente quer excluir esse cadastro?")){
					$.ajax({
						url: "delete.php",
						method: "POST",
						data: {
							user_id: user_id
						},
						success: function(data){
							console.log(data);
							alert(data);
							dataTable.ajax.reload();
							setTimeout(function(){
								$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
								$('.tabelinha-nova').html("");
							},100);
								$('body').css('overflow','auto');
						}
					});
				} else {
					return false;
				}
			});


		});
	</script>
</body>

</html>