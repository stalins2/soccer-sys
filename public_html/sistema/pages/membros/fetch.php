<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
$query .= "SELECT * FROM Cadastros ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE Cadastros.apelido LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR Cadastros.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR gols.id LIKE "%'.$_POST["search"]["value"].'%" ';	
}
if(isset($_POST["order"]))
{
	$query .= ' ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
	// echo "<script> alert('".$query."')</script>";
}
else
{
	$query .= ' ORDER BY id DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();

foreach($result as $row){
	
	$sub_array = array();	
	$sub_array[] = $row["id"];
	$sub_array[] = $row["nome"];	
	$sub_array[] = $row["apelido"];
	$sub_array[] = (new DateTime($row["data_nascimento"]))->format('d/m/Y');
	$sub_array[] = $row["padrinho"];
	$sub_array[] = (new DateTime($row["data_entrada"]))->format('d/m/Y');
	$sub_array[] = $row["time_que_torce"];
	$sub_array[] = $row["celular"];
	$sub_array[] = $row["telefone"];
	$sub_array[] = $row["email"];
	$sub_array[] = $row["endereco"];
	$sub_array[] = $row["bairro"];
	$sub_array[] = $row["posicao"];
	$sub_array[] = $row["chute"];
	$sub_array[] = $row["altura"];
	$sub_array[] = $row["peso"];
	$sub_array[] = $row["cep"];
	$sub_array[] = $row["natural_de"];
	$sub_array[] = $row["rg"];
	$sub_array[] = $row["cpf"];
	$sub_array[] = $row["status"];
	$sub_array[] = (new DateTime($row["data_afastamento"]))->format('d/m/Y');
	$sub_array[] = (new DateTime($row["data_cadastro"]))->format('d/m/Y');
	$sub_array[] = (new DateTime($row["data_saida"]))->format('d/m/Y');
	$sub_array[] = (new DateTime($row["ultima_atualizacao"]))->format('d/m/Y');
	if($row["foto"] != '')
		{
			$sub_array[] = '<img src="fotos/'.$row["foto"].'" class="img-thumbnail" width="300" height="170" />';
		}
		else
		{
			$sub_array[] = '<input type="hidden" name="foto" value="" />';
		}
	// $sub_array[] = $row["foto"];
	// $sub_array[] = $row["id_cadastrador"];
	// $sub_array[] = $row["atualizado_por"];
	$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="update">Alterar</button>';
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
// echo "<pre>";
echo json_encode($output);
?>