<?

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once('assets/includes/validador_acesso.php');
include('../../../classes_sistema/db.php');
// LISTA DE SÚMULAS
$query_su = $connection->prepare("
    SELECT sumula.id as id, sumula.data_jogo as data_jogo FROM Sumula as sumula ORDER BY id DESC LIMIT 1");
$query_su->execute();
$result_su = $query_su->fetchAll();


function lastSumula(){
    global $result_su;
     foreach($result_su as $lista){ 
        echo $lista["id"];
     } 
}
// FIM DA LISTA DE SÚMULAS
?>
<html>
	<head>
		<title> Sistema - Fohfaoi</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/new_view.css" />
	</head>
	<body class="is-preload">
		<!-- Wrapper -->
			<div id="wrapper">
				<!-- Main -->
					<div id="main" class="conteudo-page">
						<div class="inner">
                            <div style="margin: 0 auto; text-align: center;"> <img src="http://fohfaoi.com/images/logo.png" alt="Fohfa Oi F.C." /> </div>
                            <div class="row">
                                <div class="container-fluid box">
                                    <div class="container-sumula">
                                        <table class="sumula-escalacao">
                                            <thead>
                                                <tr>
                                                    <input type="hidden" value="<?=lastSumula();?>" id="lastsumula" name="lastsumula">
                                                    <th colspan="4">Última Súmula Cadastrada <span id="sumula_jogo"></span></th>
                                                </tr>
                                                <tr>
                                                    <th>Posição</th>
                                                    <th>
                                                        Amarelo
                                                    </th>
                                                    <th>
                                                        Azul
                                                    </th>
                                                    <th>Posição</th>
                                                </tr>
                                            </thead>
                                            <tbody class="listagem-primeiro-tempo">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="container-sumula">
                                        <table class="sumula-escalacao">
                                            <thead>
                                                <tr>
                                                    <th colspan="4">Segundo Tempo</th>
                                                </tr>
                                            </thead>
                                            <tbody class="listagem-segundo-tempo">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="detalhes-1">
                                        <div class="container-sumula arbitragem">
                                            <table class="sumula-arbitragem first">
                                                <thead>
                                                    <th colspan="2">
                                                        Arbitragem 1º Tempo
                                                    </th>
                                                </thead>
                                                <tbody class="arbitragem-1">

                                                </tbody>                    
                                            </table>
                                            <table class="sumula-arbitragem second">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">
                                                            Arbitragem 2º Tempo
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="arbitragem-2">
                                                </tbody>                    
                                            </table>
                                        </div>
                                        <div class="container-sumula resultado">
                                            <table class="sumula-gols">
                                                <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            Gols
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Marcador</th>
                                                        <th>1º Tempo</th>
                                                        <th>2º Tempo</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="gols">

                                                </tbody>                    
                                            </table>                

                                            <table class="sumula-penaltis">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">
                                                            Cobranças de Penaltis
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Marcador</th>
                                                        <th>Time</th>
                                                        <th>Convertidos</th>
                                                        <th>Perdidos</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="penaltis">
                                                    
                                                </tbody>                    
                                            </table>

                                            <table class="sumula-complementares">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">
                                                            Informações Complementares
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="observacoes">
                                                    
                                                </tbody>                    
                                            </table>
                                        </div>
                                        <div class="container-sumula cartoes">
                                            <table class="sumula-cartoes">
                                                <thead>
                                                    <tr>
                                                        <th colspan="4" class="sumula-title">Cartões</th>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                            Jogador
                                                        </th>
                                                        <th>
                                                            Amarelo
                                                        </th>
                                                        <th>
                                                            Azul
                                                        </th>
                                                        <th>
                                                            Vermelho
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody class="jogador-cartoes">
                                                
                                                </tbody>                    
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
					</div>
					<? 
						include('new_menu.php');						
					?>
			</div>

<!-- nova interface -->
<script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/jquery.mask.js"></script>
        <!-- /nova interface -->
            <!-- DataTables -->
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sp-1.0.1/datatables.min.css"/>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
            <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sp-1.0.1/datatables.min.js"></script>
            <!-- DataTables -->
            
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
        
			 <script>
                $(document).ready(function(){
					var janelaAtual;
                    var pagina;
                    var linkBase;
                        $(".menu-link").on('click', function(e){ //Carrega o item do value do menu
                            e.preventDefault();
                            var linkBase = window.location.href.split("pages/")[0];
                            var janelaAtual = window.location.href.split("pages/")[1];
                            var pagina = linkBase+"pages/"+$(this).attr("value");                            
                            if(janelaAtual == pagina){
                                pagina = linkBase+"pages/"+pagina;
                            }
                            $('.conteudo-page').html('Carregando...'); // Mostrar "Carregando..."
                            $.ajax().done(function(data) { // Página carregada do php
                                
                               window.location.href = pagina;
                            });
                        });




                        
				var user_id = $("#lastsumula").val();
				// console.log('user_id:', user_id);
				var dados;
				$.ajax({
					url:"rankings/fetch_single.php",
					method:"POST",
					data:{user_id:user_id},
					dataType:"json",
					success:function(data)
					{
						console.log("dados: ", data);
						dados = data;
						
						let infos = dados;
						let tabelinha1  = "";
						let tabelinha2  = "";			
						let gols1 = "" ;
						let penaltis = "";
						let cartoes = "";

						$("#sumula_jogo").html(infos.cabecalho[0].sumula_jogo);
						let arbitragem1 = `
								<tr>
									<td>Árbitro</td><td>${infos.cabecalho[0].arbitro}</td>
								</tr>
								<tr>
									<td>Auxiliar 1</td><td>${infos.cabecalho[0].auxiliar_1}</td>
								</tr>
								<tr>
									<td>Auxiliar 2</td><td>${infos.cabecalho[0].auxiliar_2}</td>
								</tr>
						`;
						let arbitragem2 = `
								<tr>
									<td>Árbitro</td><td>${infos.cabecalho[0].arbitro_2}</td>
								</tr>
								<tr>
									<td>Auxiliar 1</td><td>${infos.cabecalho[0].auxiliar_3}</td>
								</tr>
								<tr>
									<td>Auxiliar 2</td><td>${infos.cabecalho[0].auxiliar_4}</td>
								</tr>
						`;

						let observacoes = `
								<tr>
									<td>Presenças</td><td>${infos.cabecalho[0].presencas_obs}</td>
								</tr>
								<tr>
									<td>Punições</td><td>${infos.cabecalho[0].punicoes_obs}</td>
								</tr>
								<tr>
									<td>Destaques do Jogo</td><td>${infos.cabecalho[0].destaques_obs}</td>
								</tr>
								<tr>
									<td>Observações</td><td>${infos.cabecalho[0].observacoes_obs}</td>
								</tr>
						`;
						for (let i = 0; i < infos.times[0].length; i++){
							var posicao = i+1;
							if(posicao == 1){
								posicao = 'Goleiro'
							};
							tabelinha1 +=
							`
								<tr>
									<td>${posicao}</td><td>${infos.times[0][i]}</td><td>${infos.times[1][i]}</td><td>${posicao}</td>
								</tr>
							`
							tabelinha2 +=
							`
								<tr>
									<td>${posicao}</td><td>${infos.times[2][i]}</td><td>${infos.times[3][i]}</td><td>${posicao}</td>
								</tr>
							`
						}
						
						for (let i = 0; i < infos.gols.length; i++){
								var marcador = infos.gols[i].gols_jogador;
								var golsPrimeiroTempo = infos.gols[i].gols_primeiro;
								if (golsPrimeiroTempo < 1){
									golsPrimeiroTempo = "0";
								}
								var golsSegundoTempo = infos.gols[i].gols_segundo;
								if (golsSegundoTempo < 1){
									golsSegundoTempo = "0";
								}
							gols1 += `
								<tr>
									<td>${marcador}</td><td>${golsPrimeiroTempo}</td><td>${golsSegundoTempo}</td>
								</tr>
							`
						}
						
						for (let i = 0; i < infos.penaltis.length; i++){
								var batedor = infos.penaltis[i].penaltis_jogador;
								var time = infos.penaltis[i].penaltis_jogador_time;
								if(time = 1){
									time = "Amarelo";
								}else{
									time = "Azul";
								}
								var golsConvertidos = infos.penaltis[i].convertidos;
								var golsPerdidos = infos.penaltis[i].perdidos;
								
							penaltis += `
								<tr>
									<td>${batedor}</td><td>${time}</td><td>${golsConvertidos}</td><td>${golsPerdidos}</td>
								</tr>
							`
						}
						for (let i = 0; i < infos.cartoes.length; i++){
								var penalizado = infos.cartoes[i].cartoes_jogador;
								var cartoesAmarelo = infos.cartoes[i].cartoes_amarelo;
								if (cartoesAmarelo < 1){
									cartoesAmarelo = "0";
								}
								var cartoesAzul = infos.cartoes[i].cartoes_azul;
								if (cartoesAzul < 1){
									cartoesAzul = "0";
								}
								var cartoesVermelho = infos.cartoes[i].cartoes_vermelho;
								if (cartoesVermelho < 1){
									cartoesVermelho = "0";
								}
							cartoes += `
								<tr>
									<td>${penalizado}</td><td>${cartoesAmarelo}</td><td>${cartoesAzul}</td><td>${cartoesVermelho}</td>
								</tr>
							`
						}
						
						$('.listagem-primeiro-tempo').html(tabelinha1);
						$('.listagem-segundo-tempo').html(tabelinha2);
						$('.arbitragem-1').html(arbitragem1);			
						$('.arbitragem-2').html(arbitragem2);
						$('.gols').html(gols1);
						$('.penaltis').html(penaltis);
						$('.observacoes').html(observacoes);
						$('.jogador-cartoes').html(cartoes);
						// $('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
						// $('body').css('overflow','hidden');
					
					}
				    })
                    
                });
                </script>
		
	</body>
</html>