    <style>
        .menu-geral th:hover{
            background: #c0c0c0;
            cursor: pointer;
        }
        .menu-geral th:active{
            background: #7e7e7e;
        }
        .container-menu{
            background: #1e1e1e;
            font-size: 14px;
            height: 100%;
        }
        .menu a{
            text-decoration: none;
            color: #fff;
        }
        .text-link{
            width: 100%;
            height: 100%;
        }

    </style>
    <!-- <div class="container-fluid row"> -->
    <? 
    //  echo "user_id: ". $_SESSION['user_id']." | user_name: ". $_SESSION['user_nome'] ." | user_email: ". $_SESSION['user_email']." | user_nivel: ". $_SESSION['user_nivel'] ." | online: ". $_SESSION['online'];
    ?>
    <!-- </div> -->   


<div class="container-menu__wrap col-2" style="position: relative; padding: 0;">
    <div class="button-menu"></div>
    <div class="container-menu pos-f-t overflow-hidden">
    <div class="expanded" id="navbarToggleExternalContent">
        <div class="bg-dark p-4">
            <div class="menu">
                <table class="table table-striped table-dark menu-geral">
                    <thead>
                        <tr>
                            <th scope="col">FohFa Oi F.C.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row"><a href="cadastro_sumula.php"><div class="tex-link">Sumulas</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_membros.php"><div class="tex-link">Cadastros</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_times.php"><div class="tex-link">Times</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_arbitragem.php"><div class="tex-link">Arbitragem</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_cartoes.php"><div class="tex-link">Cartões</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_gols.php"><div class="tex-link">Gols</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_penaltis.php"><div class="tex-link">Penaltis</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_observacoes.php"><div class="tex-link">Informações Complementares</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="rankings.php"><div class="tex-link">Rankings</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="cadastro_acessos.php"><div class="tex-link">Acessos do Sistema</div></a></th>
                        </tr>
                        <tr>
                            <th scope="row"><a href="assets/includes/sair.php"><div class="tex-link">Sair do Sistema</div></a></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-dark bg-dark">
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"> -->
        <!-- <span class="navbar-toggler-icon"></span> -->
        </button>
    </nav>
    </div>    
</div>