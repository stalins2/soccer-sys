<? require_once('assets/includes/validador_acesso.php'); ?>
<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.75">
        <?php include('assets/includes/scripts.php');?>
    </head>
    <style>
        .cadastro-sumula input{
            width: 100%;
            height: 30px;
            border-radius: 5px;
            text-align: center;
        }
        .cadastro-sumula input[type='date']{
            width: 150px;
        }
        .cadastro-sumula .submit{
            width: 150px;
            height: 30px;
            color: #000;
            background: #fff;
            border: 1px solid #000;
            border-radius: 5px;
        }
    </style>
<body>
        <div class="row no-gutter container-fluid" style="padding: 0;">
            <?php include("menu.php");?>
            <div class="col-10 text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                    <h1> Cadastro de Súmula</h1>
                </div>
                <? include('acessos/old_index.php'); ?>
                
            </div>
        </div>
        <?php include('assets/includes/rodape.php');?>
</body>
</html>