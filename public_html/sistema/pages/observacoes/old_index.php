<style>

.modal-tabelinha-hidden{
	width: 0;
	height: 0;
	opacity: 0;
}
.modal-tabelinha{
	position: fixed;
	top: 0;
	left: 0;
	display: block;
	margin: 0 auto;
	width: 100vw;
	height: 100vh;
	opacity: 1;
	background-color: rgba(0, 0, 0, 0.8);
}
.tabelinha-nova{
	width: 80%;
	padding: 20px;
	margin: 10% auto;
	background-color: #fff;
}
.fecha-modal{
	position: absolute;
	right: 10px;
	top: 10px;
	color: #fff;
	font-weight: bold;
	font-size: 15px;
	cursor: pointer;
	text-transform: uppercase;
}
.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
   display: grid; 
   grid-template-columns: 6% 6% 30% 14% 14% 14% 15%;
   padding: 5px;
}

.tabelinha-nova .dados_titulo{
	font-weight: bold;
	background-color: #fdfdfd;
}
.tabelinha-nova .dados_jogo{
	border: 1px solid;
	font-size: 13px;
	margin-top: 1px;	
}
.tabelinha-nova .dados_jogo button{
	border: none;
	background-color: #fff;

}
</style>
	<div class="container box">
		<table id="user_data" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th width="5%">Sumula</th>
					<th width="20%">Presenças</th>
					<th width="20%">Punições</th>
					<th width="20%">Destaques do Jogo</th>
					<th width="20%">Observações</th>
					<th width="10%">Alterar</th>
				</tr>
			</thead>
		</table>
	</div>
	<div class="modal-tabelinha-hidden">
		<section class="tabelinha-nova">
			<div class="fecha-modal"> X Fechar </div>
			<div class="dados_titulo">
				<div> ID </div>
				<div> SUMULA </div>
				<div> PRESENÇAS </div>
				<div> PUNIÇÕES </div>
				<div> DESTAQUES DO JOGO </div>
				<div> OBSERVAÇÕES </div>
				<div> EXCLUIR </div>
			</div>
		</section>
	</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){

	$('.fecha-modal').on('click', function(){
		$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.dados_jogo').remove();
		$('body').css('overflow','auto');
	});

	
	var dataTable = $('#user_data').DataTable({
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url:"observacoes/fetch.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": '_all',
				"orderable":false,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});

	
	$(document).on('click', '.update', function(){
		var user_id = $(this).attr("id");
		console.log('user_id:', user_id);
		var dados;
		$.ajax({
			url:"observacoes/fetch_single.php",
			method:"POST",
			data:{user_id:user_id},
			dataType:"json",
			success:function(data)
			{
				console.log("dados: ", data);
				dados = data;
			}
		})
		setTimeout(function(){
			let infos = dados;
			let tabelinha  = "";		
			for (let i = 0; i < infos.length; i++){
				tabelinha +=
				`<div class="dados_jogo">
					<div> ${infos[i].id} </div>
					<div> ${infos[i].sumula} </div>
					<div> ${infos[i].presencas} </div>
					<div> ${infos[i].punicoes} </div>
					<div> ${infos[i].destaques} </div>
					<div> ${infos[i].observacoes} </div>
					<div>
						<button class="excluir" name="excluir" id="${infos[i].id}" value="${infos[i].id}"> Excluir </button>
					</div>
				</div>
				`
			}
			$('.tabelinha-nova').append(tabelinha);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('body').css('overflow','hidden');
		}, 800);
	});
	
	$(document).on('click', '.excluir', function(){
		var user_id = $(this).attr("id");
		if(confirm("Você quer excluir esse registro?"))
		{
			$.ajax({
				url:"observacoes/delete.php",
				method:"POST",
				data:{user_id:user_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('.dados_jogo').remove();
					$('body').css('overflow','auto');
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>