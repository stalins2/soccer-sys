<?
require_once('../assets/includes/validador_acesso.php');
include('../header_loaders.php');
include('../new_lists.php');
?>
<html>

<head>
	<title> Sistema - Fohfaoi</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="../assets/css/new_view.css" />
	<style>
		tr.dtrg-start {
			cursor: pointer;
		}

		.modal-tabelinha-hidden{
			width: 0;
			height: 0;
			opacity: 0;
		}
		.modal-tabelinha{
			position: fixed;
			top: 0;
			left: 0;
			display: block;
			margin: 0 auto;
			width: 100vw;
			height: 100vh;
			overflow: auto;
			z-index: 9999;
			opacity: 1;
			background-color: rgba(0, 0, 0, 0.8);
		}
		.tabelinha-nova{
			width: 90%;
			height: auto;
			padding: 20px;
			margin: 5% auto;
			background-color: #fff;
		}
		.fecha-modal{
			position: absolute;
			right: 10px;
			top: 10px;
			color: #fff;
			font-weight: bold;
			font-size: 15px;
			cursor: pointer;
			text-transform: uppercase;
		}
	
		.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
			display: grid; 
			grid-template-columns: 5% 5% 20% 20% 20% 20% 10%;
			/* grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); */
			grid-gap: 0px;
			padding: 10px;
		}
		.tabelinha-nova .dados_jogo div{
			padding: 5px;
		}
		.tabelinha-nova .dados_titulo {
			font-weight: bold;
			background-color: #fdfdfd;
		}

		.tabelinha-nova .dados_jogo div, .tabelinha-nova .dados_titulo div, {
			padding: 10px;
		}
		.tabelinha-nova .dados_jogo .excluir{
			width: 100%;
			padding: 0;
		}

		.tabelinha-nova .dados_jogo button {
			border: none;
			background-color: #fff;

		}
		.update{
			position: relative;
		}
		.campo-temporario-before:before {
			content: 'Detalhes Acima';
			padding: 8px;
			font-size: 11px;
			text-transform: uppercase;
			position: absolute;
			top: -60px;
			white-space: pre-wrap;
			border-radius: 6px;
			background: #000;
			color: #fff;
		}
	</style>
</head>

<body class="is-preload">
	<!-- Wrapper -->
	<div id="wrapper">
		<!-- Main -->
		<div id="main" class="conteudo-page">
			<div class="inner">
				<!-- Header -->
				<form method="post" id="team_form" name="team_form" enctype="multipart/form-data">
				<!-- <form id="team_form" name="team_form" method="post" action="../controllers_new/cadastro_arbitragem_controller.php?acao=cadastrar"> -->
					<header id="header">
						<a href="#" class="logo"><strong>Cadastrar </strong>Observações</a>
						<?=newListaSu();?>
					</header>
					<!-- Content -->
					<div class="row gtr-200">	
						<div class="col-12 d-flex">
							<div class="row container-fluid">
								<div class="row container-fluid">
									<div class="row text-left textareas container-fluid justify-content-around"> 
										<div class="row selects-observacoes-1 form-group col-sm-12" style="margin-top: 10px;">										
											<div class="col-sm-3">
												<label for="presencas">Presenças</label>
												<textarea maxlength="500" id="presencas" name="presencas" rows="5"></textarea>
												<div id="charNum">*Limite de 500 Caracteres</div>
											</div>
											<div class="col-sm-3">
												<label for="punicoes">Punições</label>
												<textarea maxlength="500" id="punicoes" name="punicoes" rows="5"></textarea>
												<div id="charNum">*Limite de 500 Caracteres</div>
											</div>
											<div class="col-sm-3">
												<label for="destaques">Destaques</label>
												<textarea maxlength="500" id="destaques" name="destaques" rows="5"></textarea>
												<div id="charNum">*Limite de 500 Caracteres</div>
											</div> 
											<div class="col-sm-3">
												<label for="observacoes">Observações</label>
												<textarea maxlength="500" id="observacoes" name="observacoes" rows="5"></textarea>
												<div id="charNum">*Limite de 500 Caracteres</div>
											</div>
										</div>
									</div>
								</div>
								<br/>
								<div class="row container-fluid">
									<div>		
									<br/>			
										<input type="hidden" name="operation" id="operation" />
										<input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
										<input type="submit" name="action" id="action" class="btn" value="Cadastrar Observações"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			
				<hr class="major"/>	
					<div class="row gtr-200">
						<div class="col-12 d-flex">
							<div class="container-fluid">
								<table id="user_data" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="5%">ID</th>
											<th width="10%">Sumula</th>
											<th width="20%">Presenças</th>
											<th width="20%">Punições</th>
											<th width="20%">Destaques do Jogo</th>
											<th width="20%">Observacoes</th>
											<th width="5%">Alterar</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				<div class="modal-tabelinha-hidden">
					<div class="fecha-modal"> X Fechar </div>
					<section class="tabelinha-nova">		
					</section>
				</div>
			</div>
		</div>
		<?
		include('../new_menu.php');
		include('../libs.php');
		?>
	</div>
	<script>
		$(document).ready(function(){
			$('.fecha-modal').on('click', function(){
				$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
				$('.dados_titulo').remove();
				$('.dados_jogo').remove();
				$('body').css('overflow','auto');
			});
			// var collapsedGroups = {};

			var dataTable = $('#user_data').DataTable({
				
				"processing":true,
				"serverSide":true,
				"order":[[1, 'desc']],
				"ajax":{
					url:"fetch.php",
					type:"POST"
				},
				"columnDefs":[
					{
						"targets": 0,
						"orderable": false,
					},
				],
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
				},
			});

			
			$(document).on('submit', '#team_form', function(event){
				$('#operation').val("cadastrar");	
				event.preventDefault();
				var formdata = new FormData($("form[name='team_form']")[0]);
				
				if(confirm("Você confirma o cadastro dos dados preenchidos?")){
					$.ajax({
						url:"../controllers_new/cadastro_observacoes_controller.php?acao=cadastrar",
						type:'POST',
						data: formdata,
						contentType:false,
						processData:false,
						success:function(data){
							console.log("Observacoes: ", formdata);
							alert(data);
							$('#team_form')[0].reset();
							dataTable.ajax.reload();
						}
					});
				}
			});


			$(document).on('click', '.update', function(){
				var user_id = $(this).attr("id");
				console.log('user_id:', user_id);
				var dados;
				$.ajax({
					url:"fetch_single.php",
					method:"POST",
					data:{user_id:user_id},
					dataType:"json",
					success:function(data)
					{
						console.log("dados: ", data);
						dados = data;
					}
				})
				setTimeout(function(){
					let infos = dados;
					let tabelinha  = `
						<div class="dados_titulo">
							<div> ID </div>
							<div> SUMULA </div>
							<div> PRESENÇAS </div>
							<div> PUNIÇÕES </div>
							<div> DESTAQUES DO JOGO </div>
							<div> OBSERVAÇÕES </div>
							<div> EXCLUIR </div>
						</div>
					`;		
					for (let i = 0; i < infos.length; i++){
						tabelinha +=
						`<div class="dados_jogo">
							<div> ${infos[i].id} </div>
							<div> ${infos[i].sumula} </div>
							<div> ${infos[i].presencas} </div>
							<div> ${infos[i].punicoes} </div>
							<div> ${infos[i].destaques} </div>
							<div> ${infos[i].observacoes} </div>
							<div>
								<button class="excluir" name="excluir" id="${infos[i].id}" value="${infos[i].id}"> Excluir </button>
							</div>
						</div>
						`
					}
					$('.tabelinha-nova').append(tabelinha);
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('body').css('overflow','hidden');
				}, 800);
			});

			$(document).on('click', '.excluir', function(){
				var user_id = $(this).attr("id");
				if(confirm("Você quer excluir esse registro?"))
				{
					$.ajax({
						url:"delete.php",
						method:"POST",
						data:{user_id:user_id},
						success:function(data)
						{
							alert(data);
							dataTable.ajax.reload();
							$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
							$('.dados_titulo').remove();
							$('.dados_jogo').remove();
							$('body').css('overflow','auto');
						}
					});
				}
				else
				{
					return false;	
				}
			});
			

			$('.textareas div textarea').on('keyup', function(){
				var contando = $(this).parent().find('textarea').val().length;   
				console.log('contando');
				if (contando >= 500) {
					$(this).parent().find('#charNum').text('*Limite Atingido');                            
				} else {
					$(this).parent().find('#charNum').text(500 - contando +' Caractere(s) restante(s)');
				}                    
			});

			});
	</script>
	
</body>
</html>