<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
// $query .= "SELECT * FROM Penaltis ";
$query .= "SELECT
			obs.id as id,
			obs.sumula_referencia as sumula,
			obs.presencas as presencas,
			obs.punicoes as punicoes,
			obs.destaques as destaques,
			obs.observacoes as observacoes
			
			FROM Observacoes as obs ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE obs.id LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR obs.presencas LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR obs.punicoes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR obs.destaques LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR obs.observacoes LIKE "%'.$_POST["search"]["value"].'%" ';
}
if(isset($_POST["order"]))
{
	$query .= ' ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= ' ORDER BY id DESC ';
}
if($_POST["length"] != "" )
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();

foreach($result as $row){
	$image = '';
	if($row["image"] != '')
	{
		$image = '<img src="upload/'.$row["image"].'" class="img-thumbnail" width="50" height="35" />';
	}
	else
	{
		$image = '';
	}
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = $row["sumula"];
	$sub_array[] = $row["presencas"];
	$sub_array[] = $row["punicoes"];
	$sub_array[] = $row["destaques"];
	$sub_array[] = $row["observacoes"];	
	$sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="update">Alterar</button>';
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);

echo json_encode($output);
?>