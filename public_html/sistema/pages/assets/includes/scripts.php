<? 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1); 
?>
    <!-- JQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- JQuery -->
    
    <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <!-- DataTables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sp-1.0.1/datatables.min.css"/>
 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-flash-1.6.1/b-html5-1.6.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sp-1.0.1/datatables.min.js"></script>
    <!-- DataTables -->
    <!-- FontAwesome -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
        <link rel="stylesheet" href="assets/includes/fontawesome/css/all.css" crossorigin="anonymous" >
        <script defer src="assets/includes/fontawesome/js/all.js"></script>        
    <!-- FontAwesome -->
    <!-- CSS Listas -->
        <link rel="stylesheet" href="assets/css/listas_dados.css">
        <link rel="stylesheet" href="assets/css/front_sumula.css">

        <!-- <link rel="stylesheet" href="assets/css/main.css"> -->
    <!-- Jquery Mask Plugin -->
        <script src="assets/js/jquery.mask.js"></script>

<!-- Controladores -->
<?

// echo "carregou os scripts!";
    require('controllers/sumula_list_controller.php');
    require('controllers/membros_list_controller.php');
    require('controllers/cadastro_sumula_controller.php');
    require('controllers/cadastro_membros_controller.php');
    require('controllers/cadastro_times_controller.php');
    require('controllers/cadastro_arbitragem_controller.php');
    require('controllers/cadastro_cartoes_controller.php');
    require('controllers/cadastro_gols_controller.php');   
    require('controllers/cadastro_penaltis_controller.php');   
    require('controllers/cadastro_observacoes_controller.php');

?>


<!-- Funções PHP-->
<? 
function listarUsuarios(){
    global $listarMembros;
    foreach($listarMembros as $key => $lista){ 
        echo "<option value='". $lista->id ."'>". $lista->id ." | ". $lista->nome ."</option>"; 
    }
}
function listaSumulas(){
    global $listaSumulas;
    echo "<select class='form-control mb-2 col-sm-4' id='CodigoSumula' name='sumula_referencia'>";
     foreach($listaSumulas as $key => $lista){ 
        echo "<option value='". $lista->id."'>Súmula Nº: ". $lista->id ."  | Data: ".  (new DateTime($lista->data_jogo))->format('d/m/Y') ."</option>";
     } 
    echo "</select>";
}


// function listarSelect(){
//     global $listarMembros;
//     $time_cor = 'azul_1_';
//     while $limite <= 11
//     foreach($listarMembros as $key => $lista){ 
//       if($key <= 11){
//         $id_select = $time_cor."".$key;
//         echo "<select class='form-control' id='".$id_select."' name='".$id_select."'>";
//         echo "<option value='". $lista->id ."'>". $lista->id ." | ". $lista->nome ."</option>";  
        
//         foreach($listarMembros as $idn => $lista){
//          echo "nomes". $idn->id; 
//          echo "<option value='". $lista->id ."'>". $lista->id ." | ". $lista->nome ."</option>";  
            
//         }
//         echo "</select>";
//       } 
//     }
// }
?>
<!-- Fim das funcoes PHP -->


<!-- Funcoes JavaScript -->
<script>
    $(document).ready(function(){
    var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('.tel').mask(SPMaskBehavior, spOptions);
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cep').mask('00000-000');
    });

    function confirmar_exclusao(id) {        
        const btn_confirm = confirm("Deseja REALMENTE excluir este registro? ("+id+") \nEssa ação é irreversível.");
        if (btn_confirm == true){
            excluir(id)
        } else(
            alert("Ação Cancelada")
        )
    }
    
    function criarDiv(idDiv, classeDiv, cssDiv){
        div = "<div class='"+classeDiv+"' id='"+idDiv+"' style='"+cssDiv+"'></div>";
        return div;        
    }

    let id = 1; //ID inicial para selects
    function criarDivSelect(idSelect, classDivSelect, classSelect){            
        let options = "<?=listarUsuarios();?>";
        let select = "<div class='"+classDivSelect+"'> <select class='"+classSelect+"' id='"+idSelect+"' name='"+idSelect+"'>"+options+"</select></div>";        
        return select;
    }

    function criarInput(idInput, tipoInput, classInputDiv, classInput, propInput){
        let input = "<div class='"+classInputDiv+"'> <input type="+tipoInput+" class='"+classInput+"' id='"+idInput+"' name='"+idInput+"' "+propInput+"/></div>";
        return input;
    }
    
    function selecionar10(selec10id, selec10Div, class10Select){
        let option = ""; 
        for(let i = 0; i <= 10; i++){            
            option += "<option value='"+i+"'>"+i+"</option>";
        }   
        let select = "<div class='"+selec10Div+"'><select name="+selec10id+" id="+selec10id+" class='"+class10Select+"'>"+option+"</select></div>";
        return select;
    }
    function selecionarTime(selecTimeId, selecTimeDiv, classTimeSelect){
        let option1 = "<option value='1'>Amarelo</option>";
        let option2 = "<option value='2'>Azul</option>";
        let select = "<div class='"+selecTimeDiv+"'><select name="+selecTimeId+" id="+selecTimeId+" class='"+classTimeSelect+"'>"+option1+""+option2+"</select></div>";
        return select;
    }
</script>

<script src="assets/js/script_sumula.js" async="true"></script>
