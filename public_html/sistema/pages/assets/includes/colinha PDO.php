<?php
//SELECT
$sql = "SELECT * FROM programadores";
$result = $PDO->query( $sql );
$rows = $result->fetchAll();
 
// print_r( $rows );
$rows = $result->fetchAll( PDO::FETCH_ASSOC );

//SELECT COM WHERE
$search = $_GET['search'];
$sql = "SELECT * FROM programadores WHERE site LIKE '%" . $search . "%'";
$result = $PDO->query( $sql );
$rows = $result->fetchAll( PDO::FETCH_ASSOC );

//RECUPERANDO UMA LISTAGEM COM FOREACH

foreach($conexao->query($query) as $chave => $valor){
    echo $valor['nome'];
}
// foreach($lista_usuarios as $key => $value){
//     echo $value['nome'];
//     //retorna o nome da tabela consultada
// }


//PREPARED STATEMENTS
$search = $_GET['search'];
$search = '%' . $search . '%';
$sql = "SELECT * FROM programadores WHERE site LIKE :search";
$stmt = $PDO->prepare( $sql );
$stmt->bindParam( ':search', $search );
$result = $stmt->execute();

//INSERT
$nome = 'Bill Gates';
$site = 'http://microsoft.com';
$sql = "INSERT INTO programadores(nome, site) VALUES(:nome, :site)";
$stmt = $PDO->prepare( $sql );
$stmt->bindParam( ':nome', $nome );
$stmt->bindParam( ':site', $site );
 
$result = $stmt->execute();
 
if ( ! $result )
{
    var_dump( $stmt->errorInfo() );
    exit;
}
 
echo $stmt->rowCount() . "linhas inseridas";

//UPDATE
$nome = 'Bill Gates';
$site = 'http://ruindows.com.br';
$sql = "UPDATE programadores set site = :site WHERE nome = :nome";
$stmt = $PDO->prepare( $sql );
$stmt->bindParam( ':nome', $nome );
$stmt->bindParam( ':site', $site );
 
$result = $stmt->execute();
 
if ( ! $result )
{
    var_dump( $stmt->errorInfo() );
    exit;
}
 
echo $stmt->rowCount() . "linhas alteradas";

//DELETE
$nome = 'Bill Gates';
$sql = "DELETE FROM programadores WHERE nome = :nome";
$stmt = $PDO->prepare( $sql );
$stmt->bindParam( ':nome', $nome );
 
$result = $stmt->execute();
 
if ( ! $result )
{
    var_dump( $stmt->errorInfo() );
    exit;
}
 
echo $stmt->rowCount() . "linhas removidas";
?>