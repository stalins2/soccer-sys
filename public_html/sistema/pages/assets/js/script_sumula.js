(function($){
    console.log("Is jQuery loaded? ", $);

    $(document).ready(function(){
        /*Activate Menu on load*/
        $(".button-menu").toggleClass("active");
        $(".container-menu__wrap").toggleClass("active");
        $(".container-content").toggleClass("active");

        $(".button-menu").on("click", function(){
            $(this).toggleClass("active");
            $(".container-menu__wrap").toggleClass("active");
            $(".container-content").toggleClass("active");
        });


        /*Toggle down/up*/
        $(".menu thead").on("click", function(){
            $(this).next("tbody").slideToggle("fast");            
        });

    });


})(jQuery);