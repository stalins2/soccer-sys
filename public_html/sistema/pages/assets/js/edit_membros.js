
    function editar(id, nome_value, apelido_value, padrinho_value, posicao_value, data_nascimento_value, naturalidade_value, time_que_torce_value, endereco_value, bairro_value, cep_value, data_entrada_value, data_saida_value, status_value, data_afastamento_value, celular_value, email_value, telefone_value) {
        //Montagem do formulário de edição
                
        let form = document.createElement('form')
        form.action = 'controllers/cadastro_membros_controller.php?acao=atualizar'
        form.method = 'post'
        form.className = 'container-fluid'

        let divLinha = document.createElement('div')
        divLinha.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        let divLinha2 = document.createElement('div')
        divLinha2.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        let divLinha3 = document.createElement('div')
        divLinha3.className ='col-sm-12 align-items-center d-flex justify-content-between'

        let divLinha4 = document.createElement('div')
        divLinha4.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        let divLinha5 = document.createElement('div')
        divLinha5.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        //let divLinha6 = document.createElement('div')
        //divLinha6.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        //criar um input hidden para guardar o id que está sendo editado
        let inputId = document.createElement('input')
        inputId.type = 'hidden'
        inputId.name = 'id'
        inputId.value = id

        let idEdicao = document.createElement('label')
        idEdicao.for = 'idCadastro'
        idEdicao.className = 'col-sm-1 titulo-campo'
        idEdicao.innerHTML = 'ID: '+inputId.value

        //criar inputs para entrada dos textos da primeira linha
        let inputNome = document.createElement('input')
        inputNome.type = 'text'
        inputNome.name = 'nome'
        inputNome.className = 'form-control mb-2'
        inputNome.value = nome_value
        
        let labelNome = document.createElement('label')
        labelNome.for = 'nome'
        labelNome.className = 'col-sm-1 titulo-campo'
        labelNome.innerHTML = 'Nome: '


        let inputApelido = document.createElement('input')
        inputApelido.type = 'text'
        inputApelido.name = 'apelido'
        inputApelido.className = 'form-control mb-2'
        inputApelido.value = apelido_value
        
        let labelApelido = document.createElement('label')
        labelApelido.for = 'apelido'
        labelApelido.className = 'col-sm-1 titulo-campo'
        labelApelido.innerHTML = 'Apelido: '


        let inputPosicao = document.createElement('input')
        inputPosicao.type = 'text'
        inputPosicao.name = 'posicao'
        inputPosicao.className = 'form-control mb-2'
        inputPosicao.value = posicao_value
        
        let labelPosicao = document.createElement('label')
        labelPosicao.for = 'posicao'
        labelPosicao.className = 'col-sm-1 titulo-campo'
        labelPosicao.innerHTML = 'Posição: '

        
        // let inputChute = document.createElement('input')
        // inputChute.type = 'text'
        // inputChute.name = 'chute'
        // inputChute.className = 'form-control mb-2'
        // inputChute.value = chute_value
        
        // let labelChute = document.createElement('label')
        // labelChute.for = 'chute'
        // labelChute.className = 'col-sm-1 titulo-campo'
        // labelChute.innerHTML = 'Chute: '

                
        // let inputAltura = document.createElement('input')
        // inputAltura.type = 'text'
        // inputAltura.name = 'altura'
        // inputAltura.className = 'form-control mb-2'
        // inputAltura.value = altura_value
        
        // let labelAltura = document.createElement('label')
        // labelAltura.for = 'altura'
        // labelAltura.className = 'col-sm-1 titulo-campo'
        // labelAltura.innerHTML = 'Altura: '

        
        // let inputPeso = document.createElement('input')
        // inputPeso.type = 'text'
        // inputPeso.name = 'peso'
        // inputPeso.className = 'form-control mb-2'
        // inputPeso.value = peso_value
        
        // let labelPeso = document.createElement('label')
        // labelPeso.for = 'peso'
        // labelPeso.className = 'col-sm-1 titulo-campo'
        // labelPeso.innerHTML = 'Peso: '

        
        let inputNascimento = document.createElement('input')
        inputNascimento.type = 'date'
        inputNascimento.name = 'data_nascimento'
        inputNascimento.className = 'form-control mb-2'
        inputNascimento.value = data_nascimento_value
        
        let labelNascimento = document.createElement('label')
        labelNascimento.for = 'data_nascimento'
        labelNascimento.className = 'col-sm-1 titulo-campo'
        labelNascimento.innerHTML = 'Data de Nascimento: '

        
        let inputEndereco = document.createElement('input')
        inputEndereco.type = 'text'
        inputEndereco.name = 'endereco'
        inputEndereco.className = 'form-control mb-2'
        inputEndereco.value = endereco_value
        
        let labelEndereco = document.createElement('label')
        labelEndereco.for = 'endereco'
        labelEndereco.className = 'col-sm-1 titulo-campo'
        labelEndereco.innerHTML = 'Endereço: '


        let inputBairro = document.createElement('input')
        inputBairro.type = 'text'
        inputBairro.name = 'bairro'
        inputBairro.className = 'form-control mb-2'
        inputBairro.value = bairro_value
        
        let labelBairro = document.createElement('label')
        labelBairro.for = 'bairro'
        labelBairro.className = 'col-sm-1 titulo-campo'
        labelBairro.innerHTML = 'Bairro: '

                
        let inputCep = document.createElement('input')
        inputCep.type = 'text'
        inputCep.name = 'cep'
        inputCep.className = 'form-control mb-2 cep'
        inputCep.value = cep_value
        
        let labelCep = document.createElement('label')
        labelCep.for = 'cep'
        labelCep.className = 'col-sm-1 titulo-campo'
        labelCep.innerHTML = 'CEP: '

        
        // let inputRg = document.createElement('input')
        // inputRg.type = 'text'
        // inputRg.name = 'rg'
        // inputRg.className = 'form-control mb-2'
        // inputRg.value = rg_value
        
        // let labelRg = document.createElement('label')
        // labelRg.for = 'rg'
        // labelRg.className = 'col-sm-1 titulo-campo'
        // labelRg.innerHTML = 'RG: '

        
        // let inputCpf = document.createElement('input')
        // inputCpf.type = 'text'
        // inputCpf.name = 'cpf'
        // inputCpf.className = 'form-control mb-2'
        // inputCpf.value = cpf_value
        
        // let labelCpf = document.createElement('label')
        // labelCpf.for = 'cpf'
        // labelCpf.className = 'col-sm-1 titulo-campo'
        // labelCpf.innerHTML = 'CPF: '

        
        let inputNaturalidade = document.createElement('input')
        inputNaturalidade.type = 'text'
        inputNaturalidade.name = 'natural_de'
        inputNaturalidade.className = 'form-control mb-2'
        inputNaturalidade.value = naturalidade_value
        
        let labelNaturalidade = document.createElement('label')
        labelNaturalidade.for = 'natural_de'
        labelNaturalidade.className = 'col-sm-1 titulo-campo'
        labelNaturalidade.innerHTML = 'Naturalidade: '

        
        let inputCelular = document.createElement('input')
        inputCelular.type = 'text'
        inputCelular.name = 'celular'
        inputCelular.className = 'form-control mb-2 tel'
        inputCelular.value = celular_value
        
        let labelCelular = document.createElement('label')
        labelCelular.for = 'celular'
        labelCelular.className = 'col-sm-1 titulo-campo'
        labelCelular.innerHTML = 'Celular: '

        
        let inputEmail = document.createElement('input')
        inputEmail.type = 'text'
        inputEmail.name = 'email'
        inputEmail.className = 'form-control mb-2'
        inputEmail.value = email_value
        
        let labelEmail = document.createElement('label')
        labelEmail.for = 'email'
        labelEmail.className = 'col-sm-1 titulo-campo'
        labelEmail.innerHTML = 'E-mail: '

        
        let inputTelefone = document.createElement('input')
        inputTelefone.type = 'text'
        inputTelefone.name = 'telefone'
        inputTelefone.className = 'form-control mb-2 tel'
        inputTelefone.value = telefone_value
        
        let labelTelefone = document.createElement('label')
        labelTelefone.for = 'telefone'
        labelTelefone.className = 'col-sm-1 titulo-campo'
        labelTelefone.innerHTML = 'Telefone: '

        let arrStatus =["Ativo", "Inativo", "Afastado"];

        let inputStatus = document.createElement('select')
        // inputStatus.type = 'text'
        inputStatus.id = "status_edit"
        inputStatus.name = 'status'
        inputStatus.className = 'form-control mb-2'

        for (let i = 0; i < arrStatus.length; i++){
            let option = document.createElement("option")
            option.text = arrStatus[i]
            inputStatus.appendChild(option)
            inputStatus.value = status_value
        }
        
        let labelStatus = document.createElement('label')
        labelStatus.for = 'status'
        labelStatus.className = 'col-sm-1 titulo-campo'
        labelStatus.innerHTML = 'Status: '
        
        
        let inputPadrinho = document.createElement('input')
        inputPadrinho.type = 'text'
        inputPadrinho.name = 'padrinho'
        inputPadrinho.className = 'form-control mb-2'
        inputPadrinho.value = padrinho_value
        
        let labelPadrinho = document.createElement('label')
        labelPadrinho.for = 'padrinho'
        labelPadrinho.className = 'col-sm-1 titulo-campo'
        labelPadrinho.innerHTML = 'Padrinho: '
        
        
        let inputTimequetorce = document.createElement('input')
        inputTimequetorce.type = 'text'
        inputTimequetorce.name = 'time_que_torce'
        inputTimequetorce.className = 'form-control mb-2'
        inputTimequetorce.value = time_que_torce_value
        
        let labelTimequetorce = document.createElement('label')
        labelTimequetorce.for = 'time_que_torce'
        labelTimequetorce.className = 'col-sm-1 titulo-campo'
        labelTimequetorce.innerHTML = 'Time que Torce: '
        
        
        let inputDataEntrada = document.createElement('input')
        inputDataEntrada.type = 'date'
        inputDataEntrada.name = 'data_entrada'
        inputDataEntrada.className = 'form-control mb-2'
        inputDataEntrada.value = data_entrada_value
        
        let labelDataEntrada = document.createElement('label')
        labelDataEntrada.for = 'data_entrada'
        labelDataEntrada.className = 'col-sm-1 titulo-campo'
        labelDataEntrada.innerHTML = 'Data de Entrada: '


        let inputDataSaida = document.createElement('input')
        inputDataSaida.type = 'date'
        inputDataSaida.name  = 'data_saida'
        inputDataSaida.className = 'form-control mb-2'
        inputDataSaida.value = data_saida_value
        
        let labelDataSaida = document.createElement('label')
        labelDataSaida.for = 'data_saida'
        labelDataSaida.className = 'col-sm-1 titulo-campo'
        labelDataSaida.innerHTML = 'Data de Saída: '


        let inputDataAfastamento = document.createElement('input')
        inputDataAfastamento.type = 'date'
        inputDataAfastamento.name  = 'data_afastamento'
        inputDataAfastamento.className = 'form-control mb-2'
        inputDataAfastamento.value = data_afastamento_value
        
        let labelDataAfastamento = document.createElement('label')
        labelDataAfastamento.for = 'data_afastamento'
        labelDataAfastamento.className = 'col-sm-1 titulo-campo'
        labelDataAfastamento.innerHTML = 'Data de Afastamento: '
        
        let spacer = document.createElement('div')        
        spacer.className = "col-sm-1"

        let spacerBig = document.createElement('div')        
        spacerBig.className = "col-sm-3"

        //criar um button para envio do form
        let button = document.createElement('button')
        button.type = 'submit'
        button.className = 'col-sm-2 mb-2 btn btn-info'
        button.innerHTML = 'Atualizar'

        let buttonCancel = document.createElement('button')
        buttonCancel.type = 'cancel'
        buttonCancel.className = 'col-sm-2 mb-2 btn bg-warning'
        buttonCancel.innerHTML = 'Cancelar'


        //incluir a div linha no form
        form.appendChild(divLinha) 
        form.appendChild(divLinha2)
        form.appendChild(divLinha3)
        form.appendChild(divLinha4) 
        form.appendChild(divLinha5)
        //form.appendChild(divLinha6)
        //incluir a div coluna na linha
        // divLinha.appendChild(divColuna)
        
        //incluir os itens na div coluna        
        divLinha.appendChild(inputId)        
        //divLinha.appendChild(idEdicao)

        //inputs
        divLinha.appendChild(labelNome)
        divLinha.appendChild(inputNome)
        divLinha.appendChild(labelApelido)
        divLinha.appendChild(inputApelido)  
        divLinha.appendChild(labelPadrinho)
        divLinha.appendChild(inputPadrinho)
        divLinha.appendChild(labelPosicao)
        divLinha.appendChild(inputPosicao) 

        divLinha2.appendChild(labelNascimento)
        divLinha2.appendChild(inputNascimento)
        divLinha2.appendChild(labelNaturalidade)
        divLinha2.appendChild(inputNaturalidade)  
        divLinha2.appendChild(labelTimequetorce)
        divLinha2.appendChild(inputTimequetorce)
        divLinha2.appendChild(labelEndereco)
        divLinha2.appendChild(inputEndereco)   
        
        divLinha3.appendChild(labelBairro)
        divLinha3.appendChild(inputBairro)
        divLinha3.appendChild(labelCep)
        divLinha3.appendChild(inputCep)
        divLinha3.appendChild(labelDataEntrada)
        divLinha3.appendChild(inputDataEntrada)  
        divLinha3.appendChild(labelDataSaida)
        divLinha3.appendChild(inputDataSaida) 
        
        
        divLinha4.appendChild(labelStatus)
        divLinha4.appendChild(inputStatus)
        divLinha4.appendChild(labelCelular)
        divLinha4.appendChild(inputCelular)
        divLinha4.appendChild(labelEmail)
        divLinha4.appendChild(inputEmail)
        divLinha4.appendChild(labelTelefone)
        divLinha4.appendChild(inputTelefone)

        // divLinha4.appendChild(labelCpf)
        // divLinha4.appendChild(inputCpf)
        // divLinha5.appendChild(labelChute)
        // divLinha5.appendChild(inputChute)
        // divLinha5.appendChild(labelAltura)
        // divLinha5.appendChild(inputAltura)
        // divLinha5.appendChild(labelPeso)
        // divLinha5.appendChild(inputPeso)      

        divLinha5.appendChild(labelDataAfastamento)
        divLinha5.appendChild(inputDataAfastamento)
        divLinha5.append(spacer)
        divLinha5.append(spacerBig)
        divLinha5.appendChild(buttonCancel)
        divLinha5.append(spacer.cloneNode(true))
        divLinha5.appendChild(button)
        //incluir button no form

        //teste
        //console.log(form)
        // alert(id)
        //selecionar a div tarefa
        let membro = document.getElementById('lista_'+id)
        let membroClone = membro.cloneNode(true)
        
        //limpar o texto da tarefa para inclusão do form
        membro.innerHTML = ''
        
        buttonCancel.onclick = function(){
                membro.innerHTML= ''
                membro.appendChild(membroClone)
            }

        //incluir form na página
            membro.insertBefore(form, membro[0])
        }
        function excluir(id){
            location.href= "cadastro_membros.php?acao=excluir&id="+id;
        }