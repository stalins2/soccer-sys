
    function editar(id, placar_amarelo_value, placar_azul_value, placar_penaltis_amarelo_value, placar_penaltis_azul_value, cartoes_amarelo_value, cartoes_azul_value, data_jogo_value) {
        //Montagem do formulário de edição
                
        let form = document.createElement('form')
        form.action = 'controllers/cadastro_sumula_controller.php?acao=atualizar'
        form.method = 'post'
        form.className = 'container-fluid'

        let divLinha = document.createElement('div')
        divLinha.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        let divLinha2 = document.createElement('div')
        divLinha2.className ='col-sm-12 align-items-center d-flex justify-content-between'

        let divLinha3 = document.createElement('div')
        divLinha2.className ='col-sm-12 align-items-center d-flex justify-content-between'
        
        
        //criar um input hidden para guardar o id que está sendo editado
        let inputId = document.createElement('input')
        inputId.type = 'hidden'
        inputId.name = 'id'
        inputId.value = id

        let idEdicao = document.createElement('label')
        idEdicao.for = 'SumulaJogo'
        idEdicao.className = 'col-sm-1 titulo-campo'
        idEdicao.innerHTML = 'ID: '+inputId.value

        //criar inputs para entrada dos textos da primeira linha
        let inputPlacarTimeAmarelo = document.createElement('input')
        inputPlacarTimeAmarelo.type = 'text'
        inputPlacarTimeAmarelo.name = 'placar_amarelo'
        inputPlacarTimeAmarelo.className = 'form-control mb-2'
        inputPlacarTimeAmarelo.value = placar_amarelo_value
        
        let labelPlacarTimeAmarelo = document.createElement('label')
        labelPlacarTimeAmarelo.for = 'placar_amarelo'
        labelPlacarTimeAmarelo.className = 'col-sm-1 titulo-campo'
        labelPlacarTimeAmarelo.innerHTML = 'Placar do Time Amarelo: '

        
        let inputPlacarTimeAzul = document.createElement('input')
        inputPlacarTimeAzul.type = 'text'
        inputPlacarTimeAzul.name = 'placar_azul'
        inputPlacarTimeAzul.className = 'form-control mb-2'
        inputPlacarTimeAzul.value = placar_azul_value
        
        let labelPlacarTimeAzul = document.createElement('label')
        labelPlacarTimeAzul.for = 'placar_azul'
        labelPlacarTimeAzul.className = 'col-sm-1 titulo-campo'
        labelPlacarTimeAzul.innerHTML = 'Placar do Time Azul: '

        
        let inputPlacarPenaltisAmarelo = document.createElement('input')
        inputPlacarPenaltisAmarelo.type = 'text'
        inputPlacarPenaltisAmarelo.name = 'placar_penaltis_amarelo'
        inputPlacarPenaltisAmarelo.className = 'form-control mb-2'
        inputPlacarPenaltisAmarelo.value = placar_penaltis_amarelo_value
        
        let labelPlacarPenaltisAmarelo = document.createElement('label')
        labelPlacarPenaltisAmarelo.for = 'placar_penaltis_amarelo'
        labelPlacarPenaltisAmarelo.className = 'col-sm-1 titulo-campo'
        labelPlacarPenaltisAmarelo.innerHTML = 'Penaltis Amarelo: '

        
        let inputPlacarPenaltisAzul = document.createElement('input')
        inputPlacarPenaltisAzul.type = 'text'
        inputPlacarPenaltisAzul.name = 'placar_penaltis_azul'
        inputPlacarPenaltisAzul.className = 'form-control mb-2'
        inputPlacarPenaltisAzul.value = placar_penaltis_azul_value
        
        let labelPlacarPenaltisAzul = document.createElement('label')
        labelPlacarPenaltisAzul.for = 'placar_penaltis_azul'
        labelPlacarPenaltisAzul.className = 'col-sm-1 titulo-campo'
        labelPlacarPenaltisAzul.innerHTML = 'Penaltis Azul: '

        
        let inputCartoesTimeAmarelo = document.createElement('input')
        inputCartoesTimeAmarelo.type = 'text'
        inputCartoesTimeAmarelo.name = 'cartoes_amarelo'
        inputCartoesTimeAmarelo.className = 'form-control mb-2'
        inputCartoesTimeAmarelo.value = cartoes_amarelo_value
        
        let labelCartoesTimeAmarelo = document.createElement('label')
        labelCartoesTimeAmarelo.for = 'cartoes_amarelo'
        labelCartoesTimeAmarelo.className = 'col-sm-1 titulo-campo'
        labelCartoesTimeAmarelo.innerHTML = 'Cartões do Time Amarelo: '

        
        let inputCartoesTimeAzul = document.createElement('input')
        inputCartoesTimeAzul.type = 'text'
        inputCartoesTimeAzul.name = 'cartoes_azul'
        inputCartoesTimeAzul.className = 'form-control mb-2'
        inputCartoesTimeAzul.value = cartoes_azul_value
        
        let labelCartoesTimeAzul = document.createElement('label')
        labelCartoesTimeAzul.for = 'cartoes_azul'
        labelCartoesTimeAzul.className = 'col-sm-1 titulo-campo'
        labelCartoesTimeAzul.innerHTML = 'Cartões do Time Azul: '

        
        let inputDataJogo = document.createElement('input')
        inputDataJogo.type = 'date'
        inputDataJogo.name = 'data_jogo'
        inputDataJogo.className = 'form-control mb-2'
        inputDataJogo.value = data_jogo_value
        
        let labelDataJogo = document.createElement('label')
        labelDataJogo.for = 'placar_time_amarelo'
        labelDataJogo.className = 'col-sm-1 titulo-campo'
        labelDataJogo.innerHTML = 'Data do Jogo: '

        let spacer = document.createElement('div')        
        spacer.className = "col-sm-1"

        let spacerBig = document.createElement('div')        
        spacerBig.className = "col-sm-3"

        //criar um button para envio do form
        let button = document.createElement('button')
        button.type = 'submit'
        button.className = 'col-sm-2 mb-2 btn btn-info'
        button.innerHTML = 'Atualizar'

        let buttonCancel = document.createElement('button')
        buttonCancel.type = 'cancel'
        buttonCancel.className = 'col-sm-2 mb-2 btn bg-warning'
        buttonCancel.innerHTML = 'Cancelar'

        form.appendChild(divLinha)
        form.appendChild(divLinha2)
        form.appendChild(divLinha3)

        divLinha.appendChild(inputId)

        divLinha.appendChild(labelPlacarTimeAmarelo)
        divLinha.appendChild(inputPlacarTimeAmarelo)
        divLinha.appendChild(labelPlacarTimeAzul)
        divLinha.appendChild(inputPlacarTimeAzul)
        divLinha.appendChild(labelPlacarPenaltisAmarelo)
        divLinha.appendChild(inputPlacarPenaltisAmarelo)
        divLinha.appendChild(labelPlacarPenaltisAzul)
        divLinha.appendChild(inputPlacarPenaltisAzul)

        
        divLinha2.appendChild(labelCartoesTimeAmarelo)
        divLinha2.appendChild(inputCartoesTimeAmarelo)
        divLinha2.appendChild(labelCartoesTimeAzul)
        divLinha2.appendChild(inputCartoesTimeAzul)
        divLinha2.appendChild(labelDataJogo)
        divLinha2.appendChild(inputDataJogo)
        divLinha2.appendChild(spacerBig)

        divLinha3.appendChild(button)        
        divLinha3.appendChild(buttonCancel)

        let sumula = document.getElementById('lista_'+id)
        let sumulaClone = sumula.cloneNode(true)

        sumula.innerHTML = ''
        buttonCancel.onclick = function(){
            sumula.innerHTML= ''
            sumula.appendChild(sumulaClone)
        }

        //incluir form na página
            sumula.insertBefore(form, sumula[0])
       
    }
    function excluir(id){
        location.href= "cadastro_sumula.php?acao=excluir&id="+id;
    }