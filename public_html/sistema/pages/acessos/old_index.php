<style>
.field-icon {
  float: right;
  margin-top: -25px;
  margin-right: 15px;
  position: relative;
  z-index: 2;
  cursor: pointer;
}

.container{
  padding-top:50px;
  margin: auto;
  position: relative;
}
</style>
	<body>
		<div class="container box">
			<div class="table-responsive">
				<div align="right">
					<button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-info btn-lg">Adicionar</button>
				</div>
				<br /><br />
				<table id="user_data" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="5%">ID</th>
							<th width="20%">Nome</th>
							<th width="20%">Email / Login</th>
							<th width="20%">Senha</th>
							<th width="15%">Nível</th>
							<th width="10%">Atualizar</th>
							<th width="10%">Excluir</th>
						</tr>
					</thead>
				</table>
				
			</div>
		</div>

<div id="userModal" class="modal fade">
	<div class="modal-dialog">
		<form method="post" id="user_form" name="user_form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Adicionar Acesso</h4>
				</div>
				<div class="modal-body">
					<label>Nome</label>
					<input type="text" name="nome" id="nome" class="form-control" />
					<br />
					<label>Email/Login</label>
					<input type="text" name="login" id="login" class="form-control" />
					<br />
					<label>Senha</label>
					<input type="password" name="senha" id="senha" class="form-control" />
					<span id="visualizar" toggle="#senha" class="fa fa-fw fa-eye field-icon visualizar"></span>
					<br />
					<label>Nível de Acesso</label>
					
					<select name="nivel" id="nivel" class="form-control">
						<option value="1">Administrador</option>
						<option value="2">Leitura</option>
					</select>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="user_id" id="user_id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" name="action" id="action" class="btn btn-success" value="Adicionar" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>


$(document).ready(function(){
	
	function verSenha(){
			$("#visualizar").on('click', function(){
				$("#visualizar").toggleClass("fa-eye fa-eye-slash");
				var input = $("#senha");
				if (input.attr("type") == "password") {
					input.attr("type", "text");
				} else {
					input.attr("type", "password");
				}
				});

		setTimeout(function(){
			verSenha();
		},300);
	}

	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Adicionar");
		$('#action').val("Adicionar");
		$('#operation').val("add");		
		verSenha();
		
	
	});
		var dataTable = $('#user_data').DataTable({
			"processing":true,
			"serverSide":true,
			"order":[],
			"ajax":{
				url:"acessos/fetch.php",
				type:"POST"
			}
		});
	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var formdata = new FormData($("form[name='user_form']")[0]);
		$.ajax({
			url:"acessos/insert.php",
			type:'POST',
			data: formdata,
			contentType:false,
			processData:false,
			success:function(data){
				alert(data);
				console.log(data);
				$('#user_form')[0].reset();
				$('#userModal').modal('hide');
				dataTable.ajax.reload();
			}
		});
	});
	
	$(document).on('click', '.update', function(){
		verSenha();
		var user_id = $(this).attr("id");
		$.ajax({
			url:"acessos/fetch_single.php",
			method:"POST",
			data:{user_id:user_id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#nome').val(data.nome);
				$('#login').val(data.login);
				$('#senha').val(data.senha);
				$('#nivel').val(data.nivel);
				$('.modal-title').text("Alterar Acesso");
				$('#user_id').val(user_id);
				// $('#user_uploaded_image').html(data.user_image);
				$('#action').val("Editar");
				$('#operation').val("edit");
			}
		});
	});
	
	$(document).on('click', '.delete', function(){
		var user_id = $(this).attr("id");
		if(confirm("Você tem certeza que quer excluir esse acesso?"))
		{
			$.ajax({
				url:"acessos/delete.php",
				method:"POST",
				data:{user_id:user_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});

</script>