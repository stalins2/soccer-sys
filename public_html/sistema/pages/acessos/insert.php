<?php
include('db.php');
include('function.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "add")
	{
			$dados = [
				'nome' =>  $_POST['nome'],
				'email' => $_POST['login'],
				'senha' => $_POST['senha'],
				'nivel' => $_POST['nivel']
			];
			$sql = "INSERT INTO Acessos (nome, email, senha, nivel) VALUES (:nome, :email, :senha, :nivel)";

			$stmt= $connection->prepare($sql);
			$stmt->execute($dados);

		if(!empty($stmt))
		{
			echo 'Cadastrado Realizado';
		}
	}
	if($_POST["operation"] == "edit")
	{
		
		$statement = $connection->prepare(
			"UPDATE Acessos 
			SET nome = :nome, email = :login, senha = :senha, nivel = :nivel
			WHERE id = :id
			"
		);
		$result = $statement->execute(
			array(
				':nome'	=>	$_POST["nome"],
				':login'	=>	$_POST["login"],
				':senha'	=>	$_POST["senha"],
				':nivel'	=>	$_POST["nivel"],
				':id'			=>	$_POST["user_id"]
			)
		);
		if(!empty($result))
		{
			echo 'Dados Atualizados';
		}
	}
}

?>