<?
session_start();
include('header_loaders.php');
include('new_lists.php');
?>
<div class="inner">
<!-- Header -->
<style>
    input[type=date]{
        border-radius: 5px;
        height: 42px; 
        width: 100%;
        line-height: 40px;
        text-align: center;
        outline: none;
    }
    input[type=date]:focus{
        border: 2px solid #f56a6a;
    }
</style>
    <form method="post" id="member_form" name="member_form" enctype="multipart/form-data">
    <header id="header">
        <a href="#" class="logo"><strong>Cadastrar </strong>Novos Membros</a>
    </header>
<!-- Content -->
    <section>
        <div class="row gtr-200">
        <div class="col-10 d-flex text-center flex-wrap" style="padding: 0;">
                <div class="text-center" style="width: 100%;">
                </div>
                <br/>
                    <form class="cadastro-membros form" id="cadastro_sumula" name="cadastro_sumula" method="post" action="controllers/cadastro_membros_controller.php?acao=cadastrar">
                        <div class="row gtr-uniform">
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="nome" name="nome" maxlength="50" placeholder="Nome" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="apelido" name="apelido" maxlength="30" placeholder="Apelido"/>
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="padrinho" name="padrinho" maxlength="30" placeholder="Padrinho"/>
                            </div>
                            <!-- quebra -->
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="posicao" name="posicao" maxlength="50" placeholder="Posição" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="date" id="data_nascimento" name="data_nascimento" data-provide="datepicker" placeholder="Data de Nascimento" maxlength="20" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="natural_de" name="natural_de" maxlength="20" placeholder="Naturalidade" />       
                            </div>
                            <!-- quebra -->
                            <div class="col-3 col-12-xsmall">
                                <input type="text" id="time_que_torce" name="time_que_torce" placeholder="Time que torce" maxlength="50" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="endereco" name="endereco" placeholder="Endereço" maxlength="50" />
                            </div>
                            <div class="col-3 col-12-xsmall">
                                <input type="text" id="bairro" name="bairro" placeholder="Bairro" maxlength="30" />  
                            </div>
                            <div class="col-2 col-12-xsmall">
                                <input type="text" id="cep" name="cep" class="cep" placeholder="CEP" maxlength="20" />
                            </div>                                
                            <!-- quebra -->
                            <div class="col-4 col-12-xsmall">
                                <input type="date" id="data_entrada" name="data_entrada" placeholder="Data de Entrada" maxlength="20" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="date" id="data_saida" name="data_saida" placeholder="Data de Saída" maxlength="30" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <select class="form-control" id="status" name="status" onchange="afastadoDesde();">
                                    <option>Ativo</option>
                                    <option>Inativo</option>
                                    <option value="Afastado">Afastado</option>
                                </select>       
                            <span id="data_afastado"></span>
                            </div>
                            <!-- quebra -->
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="celular" name="celular" class="tel" placeholder="Celular" maxlength="50" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="email" name="email" placeholder="E-mail" maxlength="30" />
                            </div>
                            <div class="col-4 col-12-xsmall">
                                <input type="text" id="telefone" class="tel" name="telefone" placeholder="Telefone" maxlength="20" />     
                            </div>
                        </div>
                        <br />
                        <br />
                        <input class="submit" type="submit" id="submit" value="Cadastrar Membro" />
                        <br />
                        <br />
                    </form>
                    <div class="col-12-xsmall">
                        <? include("membros/index.php");?>
                    </div>               
                </div>

        </div>
        <hr class="major" />
        
                    <input type="hidden" name="operation" id="operation" />
                    <input type="hidden" name="id_cadastrador" id="id_cadastrador" value="<?=$_SESSION['user_id']?>" />
        </form>
    </section>
</div>
<script>
    function cadMembros(){
        $("#data_nascimento").datepicker({dateFormat: 'dd-mm-yy'});
    }

	$('#add_button').click(function(event){
        event.preventDefault();
		$('#member_form')[0].reset();
		$('#action').val("Adicionar");
		$('#operation').val("add");	
	});

    $(document).on('submit', '#member_form', function(event){
        event.preventDefault();
        console.log('cadastrar membros');
        // var formdata = new FormData($("form[name='member_form']")[0]);
        
    });
</script>