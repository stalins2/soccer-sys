<?php

function upload_image()
{
	if(isset($_FILES["user_image"]))
	{
		$extension = explode('.', $_FILES['user_image']['name']);
		$new_name = rand() . '.' . $extension[1];
		$destination = './upload/' . $new_name;
		move_uploaded_file($_FILES['user_image']['tmp_name'], $destination);
		return $new_name;
	}
}

function get_image_name($user_id)
{
	include('db.php');
	$statement = $connection->prepare("SELECT foto FROM Cadastros WHERE id = '$user_id'");
	$statement->execute();
	$result = $statement->fetchAll();
	foreach($result as $row)
	{
		return $row["image"];
	}
}

function get_total_all_records()
{
	include('db.php');
	$statement = $connection->prepare("SELECT
	penaltis.id as id, 
	Cadastros.nome as jogador, 
	penaltis.time as jogador_time, 
	sum(penaltis.convertidos) as convertidos, 
	sum(penaltis.perdidos) as perdidos 

	FROM Penaltis as penaltis
	LEFT JOIN Cadastros
	ON Cadastros.id = penaltis.jogador
	GROUP BY jogador");
	$statement->execute();
	$result = $statement->fetchAll();
	return $statement->rowCount();
}

?>