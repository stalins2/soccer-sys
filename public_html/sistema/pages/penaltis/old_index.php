<style>

tr.dtrg-start{
	cursor: pointer;
}
.modal-tabelinha-hidden{
	width: 0;
	height: 0;
	opacity: 0;
}
.modal-tabelinha{
	position: fixed;
	top: 0;
	left: 0;
	display: block;
	margin: 0 auto;
	width: 100vw;
	height: 100vh;
	opacity: 1;
	background-color: rgba(0, 0, 0, 0.8);
}
.tabelinha-nova{
	width: 80%;
	padding: 20px;
	margin: 10% auto;
	background-color: #fff;
}
.fecha-modal{
	position: absolute;
	right: 10px;
	top: 10px;
	color: #fff;
	font-weight: bold;
	font-size: 15px;
	cursor: pointer;
	text-transform: uppercase;
}
.tabelinha-nova .dados_titulo, .tabelinha-nova .dados_jogo{
   display: grid; 
   grid-template-columns: 6% 6% 30% 14% 14% 14% 15%;
   padding: 5px;
}

.tabelinha-nova .dados_titulo{
	font-weight: bold;
	background-color: #fdfdfd;
   /* text-align: left; */
}
.tabelinha-nova .dados_jogo{
	border: 1px solid;
	font-size: 13px;
	margin-top: 1px;	
}
.tabelinha-nova .dados_jogo button{
	border: none;
	background-color: #fff;

}
</style>
		<div class="container box">
				<br /><br />
				<table id="user_data" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="5%">Id</th>
							<th width="10%">Sumula</th>
							<th width="20%">Nome</th>
							<th width="15%">Time</th>
							<th width="10%">Convertidos</th>
							<th width="10%">Perdidos</th>
							<th width="10%">Alterar</th>
							<!-- <th width="10%">Excluir</th> -->

						</tr>
					</thead>
				</table>
				
			</div>
		</div>
		<div class="modal-tabelinha-hidden">
			<section class="tabelinha-nova">
				<div class="fecha-modal"> X Fechar </div>
				<div class="dados_titulo">
					<div> ID </div>
					<div> SUMULA </div>
					<div> NOME </div>
					<div> TIME </div>
					<div> CONVERTIDOS </div>
					<div> PERDIDOS </div>
					<div> EXCLUIR </div>
				</div>
			</section>
		</div>
		

<div id="userModal" class="modal fade">
	<div class="modal-dialog">
		<form method="post" id="user_form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Cadastrar Penaltis</h4>
				</div>
				<div class="modal-body">
					<label>Nome do Jogador</label>
					<input type="text" name="jog_nome" id="jog_nome" class="form-control" />
					<br />
					<label>Time</label>
					<input type="text" name="jog_time" id="jog_time" class="form-control" />
					<br />
					<label>Convertidos</label>
					<input type="text" name="jog_convertidos" id="jog_convertidos" class="form-control" />
					<br />
					<label>Perdidos</label>
					<input type="text" name="jog_perdidos" id="jog_perdidos" class="form-control" />
					<br />
					<label> Foto</label>
					<input type="file" name="user_image" id="user_image" />
					<span id="user_uploaded_image"></span>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="user_id" id="user_id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" name="action" id="action" class="btn btn-success" value="Adicionar" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){

	$('.fecha-modal').on('click', function(){
		$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
		$('.dados_jogo').remove();
		$('body').css('overflow','auto');
	});
	var collapsedGroups = {};
	
	var dataTable = $('#user_data').DataTable({
		
		"processing":true,
		"serverSide":true,
		// "order":[[1, 'asc']],
		"order":[],
		"pageLength": 50,
		"rowGroup": {
            dataSrc: 1,
			startRender: function(rows, group) {
				var collapsed = !!collapsedGroups[group];

				rows.nodes().each(function (r) {
				r.style.display = 'none';
				if (collapsed) {
					r.style.display = '';
				}});
				return $('<tr/>')
				.append('<td colspan="8"> Súmula ' + group + ' (' + rows.count() + ' Marcador(es))</td>')
				.attr('data-name', group)
				.toggleClass('collapsed', collapsed);
			}
        },
		"ajax":{
			url:"penaltis/fetch.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets": '_all',
				"orderable": true,
			},
		],
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese.json"
		},
	});
	$('#user_data tbody').on('click', 'tr.dtrg-start', function() {
		var name = $(this).data('name');
		collapsedGroups[name] = !collapsedGroups[name];
		dataTable.draw(false);
	});
	
	$(document).on('click', '.update', function(){
		var user_id = $(this).attr("id");
		console.log('user_id:', user_id);
		var dados;
		$.ajax({
			url:"penaltis/fetch_single.php",
			method:"POST",
			data:{user_id:user_id},
			dataType:"json",
			success:function(data)
			{
				console.log("dados: ", data);
				dados = data;
			}
		})
		setTimeout(function(){
			let infos = dados;
			let tabelinha  = "";		
			for (let i = 0; i < infos.length; i++){
				tabelinha +=
				`<div class="dados_jogo">
					<div> ${infos[i].id} </div>
					<div> ${infos[i].sumula} </div>
					<div> ${infos[i].jogador} </div>
					<div> ${infos[i].jogador_time} </div>
					<div> ${infos[i].convertidos} </div>
					<div> ${infos[i].perdidos} </div>
					<div>
						<button class="excluir" name="excluir" id="${infos[i].id}" value="${infos[i].id}"> Excluir </button>
					</div>
				</div>
				`
			}
			$('.tabelinha-nova').append(tabelinha);
			$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
			$('body').css('overflow','hidden');
		}, 500);
	});
	
	$(document).on('click', '.excluir', function(){
		var user_id = $(this).attr("id");
		if(confirm("Você quer excluir esse registro?"))
		{
			$.ajax({
				url:"penaltis/delete.php",
				method:"POST",
				data:{user_id:user_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
					$('.modal-tabelinha-hidden').toggleClass('modal-tabelinha');
					$('.dados_jogo').remove();
					$('body').css('overflow','auto');
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>