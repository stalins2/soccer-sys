<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 

include('db.php');
include('function.php');


$query = '';
$output = array();
// $query .= "SELECT * FROM Penaltis ";
$query .= "SELECT
			penaltis.id as id, 
			penaltis.sumula_referencia as sumula,    
			Cadastros.nome as jogador, 
			penaltis.time as jogador_time, 
			penaltis.jogador as jogador_cod,
			penaltis.convertidos as convertidos, 
			penaltis.perdidos as perdidos 

			FROM Penaltis as penaltis
			LEFT JOIN Cadastros
			ON Cadastros.id = penaltis.jogador ";
		
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE Cadastros.nome LIKE "%'.$_POST["search"]["value"].'%" ';
	// $query .= 'OR jogador LIKE "%'.$_POST["search"]["value"].'%" ';
}
if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY sumula DESC ';
}
if($_POST["length"] != "")
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
// echo "query: ". $query;
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
// echo "query: ". $query;
// echo "<pre>";
// print_r($result);


$data = array();
$filtered_rows = $statement->rowCount();




foreach($result as $row){
	$sub_array = array();
	$sub_array[] = $row["id"];
	$sub_array[] = $row["sumula"];
	$sub_array[] = $row["jogador"];
	if($row["jogador_time"] == 1){
		$sub_array[] = "Amarelo";
	}else{
		$sub_array[] = "Azul";
	}
	$sub_array[] = $row["convertidos"];
	$sub_array[] = $row["perdidos"];
	$sub_array[] = '<button type="button" name="update" id="'.$row["jogador_cod"].'" class="update">Atualizar</button>';
	$data[] = $sub_array;
}

$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);

echo json_encode($output);
?>