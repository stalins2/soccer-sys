<?php
    class CadastroService {
        private $conexao;
        private $cadastroSumula;
        private $atualizaSumula;

        public function __construct(Conexao $conexao, $cadastroSumula){
            $this->conexao = $conexao->conectar();
            $this->cadastroSumula = $cadastroSumula;
        }

        public function inserir(){ //create
            $query = "insert into
             Sumula(placar_amarelo, placar_azul, placar_penaltis_amarelo, placar_penaltis_azul, cartoes_amarelo, cartoes_azul, data_jogo, id_cadastrador)
             values
             (:placar_amarelo, :placar_azul, :placar_penaltis_amarelo, :placar_penaltis_azul, :cartoes_amarelo, :cartoes_azul, :data_jogo, :id_cadastrador)";

            //ALTER TABLE Sumula AUTO_INCREMENT = 1; // Para reiniciar o contador de ID
            $stmt = $this->conexao->prepare($query);
            $stmt->bindValue(':placar_amarelo', $this->cadastroSumula->__get('placar_amarelo'), PDO::PARAM_INT);
            $stmt->bindValue(':placar_azul', $this->cadastroSumula->__get('placar_azul'),PDO::PARAM_INT);
            $stmt->bindValue(':placar_penaltis_amarelo', $this->cadastroSumula->__get('placar_penaltis_amarelo'),PDO::PARAM_INT);
            $stmt->bindValue(':placar_penaltis_azul', $this->cadastroSumula->__get('placar_penaltis_azul'),PDO::PARAM_INT);
            $stmt->bindValue(':cartoes_amarelo', $this->cadastroSumula->__get('cartoes_amarelo'),PDO::PARAM_INT);
            $stmt->bindValue(':cartoes_azul', $this->cadastroSumula->__get('cartoes_azul'),PDO::PARAM_INT);
            $stmt->bindValue(':data_jogo', $this->cadastroSumula->__get('data_jogo'));
            $stmt->bindValue(':id_cadastrador', $this->cadastroSumula->__get('user_id'),PDO::PARAM_INT);
            $stmt->execute();
        }

        public function listar(){ //read
            $sql = "
            SELECT sumula.id as id, sumula.placar_amarelo, sumula.placar_azul, sumula.placar_penaltis_amarelo, sumula.placar_penaltis_azul, sumula.cartoes_amarelo, sumula.cartoes_azul, sumula.data_jogo, sumula.data_cadastro, acessos.nome as atualizado_por, acessos2.nome as nome_cadastrador
 
            FROM Sumula as sumula
            LEFT JOIN Acessos as acessos
            ON acessos.id = sumula.atualizado_por
            LEFT JOIN Acessos as acessos2
            ON acessos2.id = sumula.id_cadastrador           

            ORDER BY id DESC LIMIT 10 ";
            $stmt = $this->conexao->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        public function atualizar(){ //update
            $sql = "UPDATE Sumula SET
            placar_amarelo = :placar_amarelo, placar_azul = :placar_azul, placar_penaltis_amarelo = :placar_penaltis_amarelo,
            placar_penaltis_azul = :placar_penaltis_azul, cartoes_amarelo = :cartoes_amarelo, cartoes_azul = :cartoes_azul, data_jogo = :data_jogo, atualizado_por = :atualizado_por WHERE id = :id";

            $stmt = $this->conexao->prepare($sql);
            $stmt->bindValue(':id', $this->cadastroSumula->__get('id'));
            $stmt->bindValue(':placar_amarelo', $this->cadastroSumula->__get('placar_amarelo'));
            $stmt->bindValue(':placar_azul', $this->cadastroSumula->__get('placar_azul'));
            $stmt->bindValue(':placar_penaltis_amarelo', $this->cadastroSumula->__get('placar_penaltis_amarelo'));
            $stmt->bindValue(':placar_penaltis_azul', $this->cadastroSumula->__get('placar_penaltis_azul'));
            $stmt->bindValue(':cartoes_amarelo', $this->cadastroSumula->__get('cartoes_amarelo'));
            $stmt->bindValue(':cartoes_azul', $this->cadastroSumula->__get('cartoes_azul'));
            $stmt->bindValue(':data_jogo', $this->cadastroSumula->__get('data_jogo'));
            $stmt->bindValue(':atualizado_por', $this->cadastroSumula->__get('atualizado_por'));
            return $stmt->execute();
        }

        public function excluir(){ //delete
            $sql = "DELETE FROM Sumula WHERE id = :id";
            $stmt = $this->conexao->prepare($sql);

            $stmt->bindValue(':id', $this->cadastroSumula->__get('id'));
            return $stmt->execute();            

        }

    }

?>