<?php
    class ListarMembros{
        private $id;
        private $nome_jogador;
        
        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
        
    }
    class ListaSumula{
        private $id;
        private $data_jogo;
        
        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
        
    }
    
    class CadastroSumula{
        private $id;
        private $placar_amarelo;
        private $placar_azul;
        private $placar_penaltis_amarelo;
        private $placar_penaltis_azul;
        private $cartoes_amarelo;
        private $cartoes_azul;
        private $data_jogo;
        private $atualizado_por;

        public function __get($atributo){
        return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }

    }

    class CadastroMembros{
        private $id;
        private $nome;
        private $apelido;
        private $posicao;
        private $chute;
        private $altura;
        private $peso;
        private $data_nascimento;
        private $endereco;
        private $bairro;
        private $cep;
        private $rg;
        private $cpf;
        private $natural_de;        
        private $celular;
        private $email;
        private $telefone;
        private $status;
        private $user_id;
        private $padrinho;
        private $time_que_torce;
        private $data_entrada;
        private $data_saida;
        private $data_afastamento;
        private $data_cadastro;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }

    
    class CadastroTimes{
        
        private $azul_1_1;
        private $azul_1_2;
        private $azul_1_3;
        private $azul_1_4;
        private $azul_1_5;
        private $azul_1_6;
        private $azul_1_7;
        private $azul_1_8;
        private $azul_1_9;
        private $azul_1_10;
        private $azul_1_11;        
        
        private $azul_2_1;
        private $azul_2_2;
        private $azul_2_3;
        private $azul_2_4;
        private $azul_2_5;
        private $azul_2_6;
        private $azul_2_7;
        private $azul_2_8;
        private $azul_2_9;
        private $azul_2_10;
        private $azul_2_11;
        
        private $amarelo_1_1;
        private $amarelo_1_2;
        private $amarelo_1_3;
        private $amarelo_1_4;
        private $amarelo_1_5;
        private $amarelo_1_6;
        private $amarelo_1_7;
        private $amarelo_1_8;
        private $amarelo_1_9;
        private $amarelo_1_10;
        private $amarelo_1_11;
        
        private $amarelo_2_1;
        private $amarelo_2_2;
        private $amarelo_2_3;
        private $amarelo_2_4;
        private $amarelo_2_5;
        private $amarelo_2_6;
        private $amarelo_2_7;
        private $amarelo_2_8;
        private $amarelo_2_9;
        private $amarelo_2_10;
        private $amarelo_2_11;        
        private $sumula_referencia;
        private $user_id;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }
    class CadastroArbitragem{
        private $arbitro_1_1;
        private $auxiliar_1_1;
        private $auxiliar_2_1;
                
        private $arbitro_1_2;
        private $auxiliar_1_2;
        private $auxiliar_2_2;

        private $sumula_referencia;
        private $user_id;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }
    
    class CadastroCartoes{
        private $cartao_jogador_1;
        private $amarelo_1;
        private $azul_1;
        private $vermelho_1;

        private $cartao_jogador_2;
        private $amarelo_2;
        private $azul_2;
        private $vermelho_2;

        private $cartao_jogador_3;
        private $amarelo_3;
        private $azul_3;
        private $vermelho_3;

        private $cartao_jogador_4;
        private $amarelo_4;
        private $azul_4;
        private $vermelho_4;

        private $cartao_jogador_5;
        private $amarelo_5;
        private $azul_5;
        private $vermelho_5;

        private $cartao_jogador_6;
        private $amarelo_6;
        private $azul_6;
        private $vermelho_6;

        private $cartao_jogador_7;
        private $amarelo_7;
        private $azul_7;
        private $vermelho_7;

        private $cartao_jogador_8;
        private $amarelo_8;
        private $azul_8;
        private $vermelho_8;

        private $cartao_jogador_9;
        private $amarelo_9;
        private $azul_9;
        private $vermelho_9;

        private $cartao_jogador_10;
        private $amarelo_10;
        private $azul_10;
        private $vermelho_10;
                
        private $sumula_referencia;
        private $user_id;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }
    class CadastroGols{
        private $jogador_1;
        private $gols_primeiro_tempo_1;
        private $gols_segundo_tempo_1;
        private $time_gols_1;

        private $jogador_2;
        private $gols_primeiro_tempo_2;
        private $gols_segundo_tempo_2;
        private $time_gols_2;

        private $jogador_3;
        private $gols_primeiro_tempo_3;
        private $gols_segundo_tempo_3;
        private $time_gols_3;

        private $jogador_4;
        private $gols_primeiro_tempo_4;
        private $gols_segundo_tempo_4;
        private $time_gols_4;

        private $jogador_5;
        private $gols_primeiro_tempo_5;
        private $gols_segundo_tempo_5;
        private $time_gols_5;

        private $jogador_6;
        private $gols_primeiro_tempo_6;
        private $gols_segundo_tempo_6;
        private $time_gols_6;

        private $jogador_7;
        private $gols_primeiro_tempo_7;
        private $gols_segundo_tempo_7;
        private $time_gols_7;

        private $jogador_8;
        private $gols_primeiro_tempo_8;
        private $gols_segundo_tempo_8;
        private $time_gols_8;

        private $jogador_9;
        private $gols_primeiro_tempo_9;
        private $gols_segundo_tempo_9;
        private $time_gols_9;

        private $jogador_10;
        private $gols_primeiro_tempo_10;
        private $gols_segundo_tempo_10;
        private $time_gols_10;
                
        private $sumula_referencia;
        private $user_id;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }
    class CadastroPenaltis{
        private $jogador_1;
        private $penaltis_convertidos_1;
        private $penaltis_perdidos_1;
        private $time_penaltis_1;

        private $jogador_2;
        private $penaltis_convertidos_2;
        private $penaltis_perdidos_2;
        private $time_penaltis_2;

        private $jogador_3;
        private $penaltis_convertidos_3;
        private $penaltis_perdidos_3;
        private $time_penaltis_3;

        private $jogador_4;
        private $penaltis_convertidos_4;
        private $penaltis_perdidos_4;
        private $time_penaltis_4;

        private $jogador_5;
        private $penaltis_convertidos_5;
        private $penaltis_perdidos_5;
        private $time_penaltis_5;

        private $jogador_6;
        private $penaltis_convertidos_6;
        private $penaltis_perdidos_6;
        private $time_penaltis_6;

        private $jogador_7;
        private $penaltis_convertidos_7;
        private $penaltis_perdidos_7;
        private $time_penaltis_7;

        private $jogador_8;
        private $penaltis_convertidos_8;
        private $penaltis_perdidos_8;
        private $time_penaltis_8;

        private $jogador_9;
        private $penaltis_convertidos_9;
        private $penaltis_perdidos_9;
        private $time_penaltis_9;

        private $jogador_10;
        private $penaltis_convertidos_10;
        private $penaltis_perdidos_10;
        private $time_gpenaltis10;
                
        private $sumula_referencia;
        private $user_id;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }
    class CadastroObservacoes{
        private $presencas;
        private $punicoes;
        private $destaques;
        private $observacoes;
                
        private $sumula_referencia;
        private $user_id;

        public function __get($atributo){
            return $this->$atributo;
        }

        public function __set($atributo, $valor){
            $this->$atributo = $valor;
        }
    }
?>