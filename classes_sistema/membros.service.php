<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1); 
// echo "<br />membros.service: ok<br />";
    class MembrosService {
        private $conexao;
        private $cadastroMembros;
        private $atualizaMembros;

        public function __construct(Conexao $conexao, $cadastroMembros){
            $this->conexao = $conexao->conectar();
            $this->cadastroMembros = $cadastroMembros;
        }

        public function inserir(){ //create
            $query = "
            insert into
             Cadastros(nome, apelido, posicao, chute, altura, peso, data_nascimento, endereco, bairro, cep, natural_de, rg, cpf, email, telefone, celular, status, data_afastamento, data_cadastro, padrinho, time_que_torce, data_entrada, data_saida, id_cadastrador)
            values
            (:nome, :apelido, :posicao, :chute, :altura, :peso, :data_nascimento, :endereco, :bairro, :cep, :natural_de, :rg, :cpf, :email, :telefone, :celular, :status, :data_afastamento, :data_cadastro, :padrinho, :time_que_torce, :data_entrada, :data_saida, :id_cadastrador)";

            //ALTER TABLE Sumula AUTO_INCREMENT = 1; // Para reiniciar o contador de ID
            $stmt = $this->conexao->prepare($query);
            $stmt->bindValue(':nome', $this->cadastroMembros->__get('nome'));
            $stmt->bindValue(':apelido', $this->cadastroMembros->__get('apelido'));
            $stmt->bindValue(':posicao', $this->cadastroMembros->__get('posicao'));
            $stmt->bindValue(':chute', $this->cadastroMembros->__get('chute'));
            $stmt->bindValue(':altura', $this->cadastroMembros->__get('altura'));
            $stmt->bindValue(':peso', $this->cadastroMembros->__get('peso'));
            $stmt->bindValue(':data_nascimento', $this->cadastroMembros->__get('data_nascimento'));
            $stmt->bindValue(':endereco', $this->cadastroMembros->__get('endereco'));
            $stmt->bindValue(':bairro', $this->cadastroMembros->__get('bairro'));
            $stmt->bindValue(':cep', $this->cadastroMembros->__get('cep'));
            $stmt->bindValue(':natural_de', $this->cadastroMembros->__get('natural_de'));
            $stmt->bindValue(':rg', $this->cadastroMembros->__get('rg'));
            $stmt->bindValue(':cpf', $this->cadastroMembros->__get('cpf'));
            $stmt->bindValue(':email', $this->cadastroMembros->__get('email'));
            $stmt->bindValue(':telefone', $this->cadastroMembros->__get('telefone'));
            $stmt->bindValue(':celular', $this->cadastroMembros->__get('celular'));
            $stmt->bindValue(':status', $this->cadastroMembros->__get('status'));
            $stmt->bindValue(':data_afastamento', $this->cadastroMembros->__get('data_afastamento'));
            $stmt->bindValue(':data_cadastro', $this->cadastroMembros->__get('data_cadastro'));
            $stmt->bindValue(':padrinho', $this->cadastroMembros->__get('padrinho'));
            $stmt->bindValue(':time_que_torce', $this->cadastroMembros->__get('time_que_torce'));
            $stmt->bindValue(':data_entrada', $this->cadastroMembros->__get('data_entrada'));
            $stmt->bindValue(':data_saida', $this->cadastroMembros->__get('data_saida'));
            $stmt->bindValue(':id_cadastrador', $this->cadastroMembros->__get('user_id'));
            $stmt->execute();

            // echo "<pre>";
            // print_r($stmt);
        }

        public function listar(){ //read
            $sql = "
            SELECT 
            cadastros.id as id, cadastros.nome, cadastros.apelido, cadastros.posicao, cadastros.chute, cadastros.altura, cadastros.peso, cadastros.data_nascimento, cadastros.endereco, cadastros.bairro, cadastros.cep, cadastros.natural_de, cadastros.rg, cadastros.cpf, cadastros.email, cadastros.telefone, cadastros.celular, cadastros.status, cadastros.data_afastamento, cadastros.data_cadastro,  cadastros.padrinho, cadastros.time_que_torce, cadastros.data_entrada, cadastros.data_saida, cadastros.ultima_atualizacao, cadastros.atualizado_por, acessos.nome as id_cadastrador
            
            FROM Cadastros as cadastros            
            LEFT JOIN Acessos AS acessos
            ON
            (cadastros.id_cadastrador = acessos.id)

            ORDER BY id DESC";
            $stmt = $this->conexao->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_OBJ);
            // echo "<pre>";
            // print_r($stmt);
            // echo "</pre>";
        }

        public function atualizar(){ //update
            
            $sql = "UPDATE Cadastros SET 
            nome = :nome, apelido = :apelido, padrinho = :padrinho, posicao = :posicao,
            data_nascimento = :data_nascimento, natural_de = :natural_de, time_que_torce = :time_que_torce, 
            endereco = :endereco, bairro = :bairro, cep = :cep, 
            data_entrada = :data_entrada, data_saida = :data_saida, status = :status,
            celular = :celular, email = :email, telefone = :telefone, 
            data_afastamento = :data_afastamento,
            atualizado_por = :atualizado_por WHERE id = :id";

            $stmt = $this->conexao->prepare($sql);
            $stmt->bindValue(':id', $this->cadastroMembros->__get('id'));
            $stmt->bindValue(':nome', $this->cadastroMembros->__get('nome'));
            $stmt->bindValue(':apelido', $this->cadastroMembros->__get('apelido'));
            $stmt->bindValue(':padrinho', $this->cadastroMembros->__get('padrinho'));
            $stmt->bindValue(':posicao', $this->cadastroMembros->__get('posicao'));
            $stmt->bindValue(':data_nascimento', $this->cadastroMembros->__get('data_nascimento'));
            $stmt->bindValue(':natural_de', $this->cadastroMembros->__get('natural_de'));
            $stmt->bindValue(':time_que_torce', $this->cadastroMembros->__get('time_que_torce'));
            $stmt->bindValue(':endereco', $this->cadastroMembros->__get('endereco'));
            $stmt->bindValue(':bairro', $this->cadastroMembros->__get('bairro'));
            $stmt->bindValue(':cep', $this->cadastroMembros->__get('cep'));
            $stmt->bindValue(':data_entrada', $this->cadastroMembros->__get('data_entrada'));
            $stmt->bindValue(':data_saida', $this->cadastroMembros->__get('data_saida'));
            $stmt->bindValue(':data_afastamento', $this->cadastroMembros->__get('data_afastamento'));
            $stmt->bindValue(':status', $this->cadastroMembros->__get('status'));
            $stmt->bindValue(':celular', $this->cadastroMembros->__get('celular'));
            $stmt->bindValue(':email', $this->cadastroMembros->__get('email'));
            $stmt->bindValue(':telefone', $this->cadastroMembros->__get('telefone'));
            $stmt->bindValue(':atualizado_por', $this->cadastroMembros->__get('user_id'));
            return $stmt->execute();
            // echo "<pre>";
            // //print_r($_POST);
            // print_r($this->cadastroMembros);

        }

        public function excluir(){ //delete
            $sql = "DELETE FROM Cadastros WHERE id = :id";
            $stmt = $this->conexao->prepare($sql);

            $stmt->bindValue(':id', $this->cadastroMembros->__get('id'));
            return $stmt->execute();            

        }

    }

?>