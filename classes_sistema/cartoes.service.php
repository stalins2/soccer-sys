<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1); 
// echo "<br />membros.service: ok<br />";
    class CartoesService {
        private $conexao;
        private $cadastroCartoes;
        private $atualizaCartoes;

        public function __construct(Conexao $conexao, $cadastroCartoes){
            $this->conexao = $conexao->conectar();
            $this->cadastroCartoes = $cadastroCartoes;
        }

        public function inserir(){ //create
            if(($_POST['cartao_jogador_10']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_5, :amarelo_5, :azul_5, :vermelho_5, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_6, :amarelo_6, :azul_6, :vermelho_6, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_7, :amarelo_7, :azul_7, :vermelho_7, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_8, :amarelo_8, :azul_8, :vermelho_8, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_9, :amarelo_9, :azul_9, :vermelho_9, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_10, :amarelo_10, :azul_10, :vermelho_10, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));    
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
                    $stmt_1->bindValue(':cartao_jogador_5', $this->cadastroCartoes->__get('cartao_jogador_5'));
                    $stmt_1->bindValue(':amarelo_5', $this->cadastroCartoes->__get('amarelo_5'));
                    $stmt_1->bindValue(':azul_5', $this->cadastroCartoes->__get('azul_5'));
                    $stmt_1->bindValue(':vermelho_5', $this->cadastroCartoes->__get('vermelho_5'));
                    
                    $stmt_1->bindValue(':cartao_jogador_6', $this->cadastroCartoes->__get('cartao_jogador_6'));
                    $stmt_1->bindValue(':amarelo_6', $this->cadastroCartoes->__get('amarelo_6'));
                    $stmt_1->bindValue(':azul_6', $this->cadastroCartoes->__get('azul_6'));
                    $stmt_1->bindValue(':vermelho_6', $this->cadastroCartoes->__get('vermelho_6'));
                    
                    $stmt_1->bindValue(':cartao_jogador_7', $this->cadastroCartoes->__get('cartao_jogador_7'));
                    $stmt_1->bindValue(':amarelo_7', $this->cadastroCartoes->__get('amarelo_7'));
                    $stmt_1->bindValue(':azul_7', $this->cadastroCartoes->__get('azul_7'));
                    $stmt_1->bindValue(':vermelho_7', $this->cadastroCartoes->__get('vermelho_7'));
                    
                    $stmt_1->bindValue(':cartao_jogador_8', $this->cadastroCartoes->__get('cartao_jogador_8'));
                    $stmt_1->bindValue(':amarelo_8', $this->cadastroCartoes->__get('amarelo_8'));
                    $stmt_1->bindValue(':azul_8', $this->cadastroCartoes->__get('azul_8'));
                    $stmt_1->bindValue(':vermelho_8', $this->cadastroCartoes->__get('vermelho_8'));
                    
                    $stmt_1->bindValue(':cartao_jogador_9', $this->cadastroCartoes->__get('cartao_jogador_9'));
                    $stmt_1->bindValue(':amarelo_9', $this->cadastroCartoes->__get('amarelo_9'));
                    $stmt_1->bindValue(':azul_9', $this->cadastroCartoes->__get('azul_9'));
                    $stmt_1->bindValue(':vermelho_9', $this->cadastroCartoes->__get('vermelho_9'));   
                    
                    $stmt_1->bindValue(':cartao_jogador_10', $this->cadastroCartoes->__get('cartao_jogador_10'));
                    $stmt_1->bindValue(':amarelo_10', $this->cadastroCartoes->__get('amarelo_10'));
                    $stmt_1->bindValue(':azul_10', $this->cadastroCartoes->__get('azul_10'));
                    $stmt_1->bindValue(':vermelho_10', $this->cadastroCartoes->__get('vermelho_10'));                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();


            }else if(($_POST['cartao_jogador_9']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_5, :amarelo_5, :azul_5, :vermelho_5, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_6, :amarelo_6, :azul_6, :vermelho_6, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_7, :amarelo_7, :azul_7, :vermelho_7, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_8, :amarelo_8, :azul_8, :vermelho_8, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_9, :amarelo_9, :azul_9, :vermelho_9, :id_cadastrador),"; 

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));    
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
                    $stmt_1->bindValue(':cartao_jogador_5', $this->cadastroCartoes->__get('cartao_jogador_5'));
                    $stmt_1->bindValue(':amarelo_5', $this->cadastroCartoes->__get('amarelo_5'));
                    $stmt_1->bindValue(':azul_5', $this->cadastroCartoes->__get('azul_5'));
                    $stmt_1->bindValue(':vermelho_5', $this->cadastroCartoes->__get('vermelho_5'));
                    
                    $stmt_1->bindValue(':cartao_jogador_6', $this->cadastroCartoes->__get('cartao_jogador_6'));
                    $stmt_1->bindValue(':amarelo_6', $this->cadastroCartoes->__get('amarelo_6'));
                    $stmt_1->bindValue(':azul_6', $this->cadastroCartoes->__get('azul_6'));
                    $stmt_1->bindValue(':vermelho_6', $this->cadastroCartoes->__get('vermelho_6'));
                    
                    $stmt_1->bindValue(':cartao_jogador_7', $this->cadastroCartoes->__get('cartao_jogador_7'));
                    $stmt_1->bindValue(':amarelo_7', $this->cadastroCartoes->__get('amarelo_7'));
                    $stmt_1->bindValue(':azul_7', $this->cadastroCartoes->__get('azul_7'));
                    $stmt_1->bindValue(':vermelho_7', $this->cadastroCartoes->__get('vermelho_7'));
                    
                    $stmt_1->bindValue(':cartao_jogador_8', $this->cadastroCartoes->__get('cartao_jogador_8'));
                    $stmt_1->bindValue(':amarelo_8', $this->cadastroCartoes->__get('amarelo_8'));
                    $stmt_1->bindValue(':azul_8', $this->cadastroCartoes->__get('azul_8'));
                    $stmt_1->bindValue(':vermelho_8', $this->cadastroCartoes->__get('vermelho_8'));
                    
                    $stmt_1->bindValue(':cartao_jogador_9', $this->cadastroCartoes->__get('cartao_jogador_9'));
                    $stmt_1->bindValue(':amarelo_9', $this->cadastroCartoes->__get('amarelo_9'));
                    $stmt_1->bindValue(':azul_9', $this->cadastroCartoes->__get('azul_9'));
                    $stmt_1->bindValue(':vermelho_9', $this->cadastroCartoes->__get('vermelho_9'));                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();

            }else if(($_POST['cartao_jogador_8']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_5, :amarelo_5, :azul_5, :vermelho_5, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_6, :amarelo_6, :azul_6, :vermelho_6, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_7, :amarelo_7, :azul_7, :vermelho_7, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_8, :amarelo_8, :azul_8, :vermelho_8, :id_cadastrador),"; 

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));    
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
                    $stmt_1->bindValue(':cartao_jogador_5', $this->cadastroCartoes->__get('cartao_jogador_5'));
                    $stmt_1->bindValue(':amarelo_5', $this->cadastroCartoes->__get('amarelo_5'));
                    $stmt_1->bindValue(':azul_5', $this->cadastroCartoes->__get('azul_5'));
                    $stmt_1->bindValue(':vermelho_5', $this->cadastroCartoes->__get('vermelho_5'));
                    
                    $stmt_1->bindValue(':cartao_jogador_6', $this->cadastroCartoes->__get('cartao_jogador_6'));
                    $stmt_1->bindValue(':amarelo_6', $this->cadastroCartoes->__get('amarelo_6'));
                    $stmt_1->bindValue(':azul_6', $this->cadastroCartoes->__get('azul_6'));
                    $stmt_1->bindValue(':vermelho_6', $this->cadastroCartoes->__get('vermelho_6'));
                    
                    $stmt_1->bindValue(':cartao_jogador_7', $this->cadastroCartoes->__get('cartao_jogador_7'));
                    $stmt_1->bindValue(':amarelo_7', $this->cadastroCartoes->__get('amarelo_7'));
                    $stmt_1->bindValue(':azul_7', $this->cadastroCartoes->__get('azul_7'));
                    $stmt_1->bindValue(':vermelho_7', $this->cadastroCartoes->__get('vermelho_7'));
                    
                    $stmt_1->bindValue(':cartao_jogador_8', $this->cadastroCartoes->__get('cartao_jogador_8'));
                    $stmt_1->bindValue(':amarelo_8', $this->cadastroCartoes->__get('amarelo_8'));
                    $stmt_1->bindValue(':azul_8', $this->cadastroCartoes->__get('azul_8'));
                    $stmt_1->bindValue(':vermelho_8', $this->cadastroCartoes->__get('vermelho_8'));
                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();


            }else if(($_POST['cartao_jogador_7']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_5, :amarelo_5, :azul_5, :vermelho_5, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_6, :amarelo_6, :azul_6, :vermelho_6, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_7, :amarelo_7, :azul_7, :vermelho_7, :id_cadastrador),"; 

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));    
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
                    $stmt_1->bindValue(':cartao_jogador_5', $this->cadastroCartoes->__get('cartao_jogador_5'));
                    $stmt_1->bindValue(':amarelo_5', $this->cadastroCartoes->__get('amarelo_5'));
                    $stmt_1->bindValue(':azul_5', $this->cadastroCartoes->__get('azul_5'));
                    $stmt_1->bindValue(':vermelho_5', $this->cadastroCartoes->__get('vermelho_5'));
                    
                    $stmt_1->bindValue(':cartao_jogador_6', $this->cadastroCartoes->__get('cartao_jogador_6'));
                    $stmt_1->bindValue(':amarelo_6', $this->cadastroCartoes->__get('amarelo_6'));
                    $stmt_1->bindValue(':azul_6', $this->cadastroCartoes->__get('azul_6'));
                    $stmt_1->bindValue(':vermelho_6', $this->cadastroCartoes->__get('vermelho_6'));
                    
                    $stmt_1->bindValue(':cartao_jogador_7', $this->cadastroCartoes->__get('cartao_jogador_7'));
                    $stmt_1->bindValue(':amarelo_7', $this->cadastroCartoes->__get('amarelo_7'));
                    $stmt_1->bindValue(':azul_7', $this->cadastroCartoes->__get('azul_7'));
                    $stmt_1->bindValue(':vermelho_7', $this->cadastroCartoes->__get('vermelho_7'));
                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();


            }else if(($_POST['cartao_jogador_6']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_5, :amarelo_5, :azul_5, :vermelho_5, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_6, :amarelo_6, :azul_6, :vermelho_6, :id_cadastrador),"; 

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));    
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
                    $stmt_1->bindValue(':cartao_jogador_5', $this->cadastroCartoes->__get('cartao_jogador_5'));
                    $stmt_1->bindValue(':amarelo_5', $this->cadastroCartoes->__get('amarelo_5'));
                    $stmt_1->bindValue(':azul_5', $this->cadastroCartoes->__get('azul_5'));
                    $stmt_1->bindValue(':vermelho_5', $this->cadastroCartoes->__get('vermelho_5'));
                    
                    $stmt_1->bindValue(':cartao_jogador_6', $this->cadastroCartoes->__get('cartao_jogador_6'));
                    $stmt_1->bindValue(':amarelo_6', $this->cadastroCartoes->__get('amarelo_6'));
                    $stmt_1->bindValue(':azul_6', $this->cadastroCartoes->__get('azul_6'));
                    $stmt_1->bindValue(':vermelho_6', $this->cadastroCartoes->__get('vermelho_6'));
                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();              


            }else if(($_POST['cartao_jogador_5']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_5, :amarelo_5, :azul_5, :vermelho_5, :id_cadastrador),";

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));    
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
                    $stmt_1->bindValue(':cartao_jogador_5', $this->cadastroCartoes->__get('cartao_jogador_5'));
                    $stmt_1->bindValue(':amarelo_5', $this->cadastroCartoes->__get('amarelo_5'));
                    $stmt_1->bindValue(':azul_5', $this->cadastroCartoes->__get('azul_5'));
                    $stmt_1->bindValue(':vermelho_5', $this->cadastroCartoes->__get('vermelho_5'));
                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();              


            }else if(($_POST['cartao_jogador_4']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_4, :amarelo_4, :azul_4, :vermelho_4, :id_cadastrador)"; 
                
                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                    $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                    $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));            
                    $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
    
                    $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                    $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                    $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));            
                    $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));                
                    
                    $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                    $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                    $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));            
                    $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));                
                    
                    $stmt_1->bindValue(':cartao_jogador_4', $this->cadastroCartoes->__get('cartao_jogador_4'));
                    $stmt_1->bindValue(':amarelo_4', $this->cadastroCartoes->__get('amarelo_4'));
                    $stmt_1->bindValue(':azul_4', $this->cadastroCartoes->__get('azul_4'));            
                    $stmt_1->bindValue(':vermelho_4', $this->cadastroCartoes->__get('vermelho_4'));
                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                    $stmt_1->execute();              

            }else if(($_POST['cartao_jogador_3']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_3, :amarelo_3, :azul_3, :vermelho_3, :id_cadastrador);
                "; 
                
                $stmt_1 = $this->conexao->prepare($query_1);
                $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));

                $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));            
                $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));

                $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));            
                $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));                
                
                $stmt_1->bindValue(':cartao_jogador_3', $this->cadastroCartoes->__get('cartao_jogador_3'));
                $stmt_1->bindValue(':amarelo_3', $this->cadastroCartoes->__get('amarelo_3'));
                $stmt_1->bindValue(':azul_3', $this->cadastroCartoes->__get('azul_3'));            
                $stmt_1->bindValue(':vermelho_3', $this->cadastroCartoes->__get('vermelho_3'));
                

                $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                $stmt_1->execute();

            }else if(($_POST['cartao_jogador_2']) != ""){
                $query_1 = "
                INSERT INTO
                    Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                VALUES
                    (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador),
                    (:sumula_referencia, :cartao_jogador_2, :amarelo_2, :azul_2, :vermelho_2, :id_cadastrador);
                "; 
                
                $stmt_1 = $this->conexao->prepare($query_1);
                $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));

                $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));            
                $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));
                $stmt_1->bindValue(':cartao_jogador_2', $this->cadastroCartoes->__get('cartao_jogador_2'));
                $stmt_1->bindValue(':amarelo_2', $this->cadastroCartoes->__get('amarelo_2'));
                $stmt_1->bindValue(':azul_2', $this->cadastroCartoes->__get('azul_2'));            
                $stmt_1->bindValue(':vermelho_2', $this->cadastroCartoes->__get('vermelho_2'));
                

                $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                $stmt_1->execute();

            }else if(($_POST['cartao_jogador_1']) != ""){
                $query_1 = "
                    INSERT INTO
                        Cartoes (sumula_referencia, jogador, amarelo, azul, vermelho, id_cadastrador)
                    VALUES
                        (:sumula_referencia, :cartao_jogador_1, :amarelo_1, :azul_1, :vermelho_1, :id_cadastrador);
                ";

                $stmt_1 = $this->conexao->prepare($query_1);
                $stmt_1->bindValue(':sumula_referencia', $this->cadastroCartoes->__get('sumula_referencia'));
                // azul 1
                $stmt_1->bindValue(':cartao_jogador_1', $this->cadastroCartoes->__get('cartao_jogador_1'));
                $stmt_1->bindValue(':amarelo_1', $this->cadastroCartoes->__get('amarelo_1'));
                $stmt_1->bindValue(':azul_1', $this->cadastroCartoes->__get('azul_1'));            
                $stmt_1->bindValue(':vermelho_1', $this->cadastroCartoes->__get('vermelho_1'));

                $stmt_1->bindValue(':id_cadastrador', $this->cadastroCartoes->__get('user_id'));
                $stmt_1->execute();

            }
            
            // echo "<pre>";
            // print_r($stmt_1);
        }

        public function listar(){ //read
            $sql = "
                SELECT 
                v_time_az_1.id_az1 AS id_az1, v_time_az_1.su_ref AS su_ref_az_1, v_time_az_1.jog_1 AS jog_1_az1, v_time_az_1.jog_2 AS jog_2_az1, v_time_az_1.jog_3 AS jog_3_az1, v_time_az_1.jog_4 AS jog_4_az1, v_time_az_1.jog_5 AS jog_5_az1,
                v_time_az_1.jog_6 AS jog_6_az1, v_time_az_1.jog_7 AS jog_7_az1, v_time_az_1.jog_8 AS jog_8_az1, v_time_az_1.jog_9 AS jog_9_az1, v_time_az_1.jog_10 AS jog_10_az1, v_time_az_1.jog_11 AS jog_11_az1, v_time_az_1.cad AS cad_az1,

                v_time_am_1.id_am1 AS id_am1, v_time_am_1.su_ref AS su_ref_am_1, v_time_am_1.jog_1 AS jog_1_am1, v_time_am_1.jog_2 AS jog_2_am1, v_time_am_1.jog_3 AS jog_3_am1, v_time_am_1.jog_4 AS jog_4_am1, v_time_am_1.jog_5 AS jog_5_am1,
                v_time_am_1.jog_6 AS jog_6_am1, v_time_am_1.jog_7 AS jog_7_am1, v_time_am_1.jog_8 AS jog_8_am1, v_time_am_1.jog_9 AS jog_9_am1, v_time_am_1.jog_10 AS jog_10_am1, v_time_am_1.jog_11 AS jog_11_am1, v_time_am_1.cad AS cad_am1,

                v_time_az_2.id_az2 AS id_az2, v_time_az_2.su_ref AS su_ref_az_2, v_time_az_2.jog_1 AS jog_1_az2, v_time_az_2.jog_2 AS jog_2_az2, v_time_az_2.jog_3 AS jog_3_az2, v_time_az_2.jog_4 AS jog_4_az2, v_time_az_2.jog_5 AS jog_5_az2,
                v_time_az_2.jog_6 AS jog_6_az2, v_time_az_2.jog_7 AS jog_7_az2, v_time_az_2.jog_8 AS jog_8_az2, v_time_az_2.jog_9 AS jog_9_az2, v_time_az_2.jog_10 AS jog_10_az2, v_time_az_2.jog_11 AS jog_11_az2, v_time_az_2.cad AS cad_az2,

                v_time_am_2.id_am2 AS id_am2, v_time_am_2.su_ref AS su_ref_am_2, v_time_am_2.jog_1 AS jog_1_am2, v_time_am_2.jog_2 AS jog_2_am2, v_time_am_2.jog_3 AS jog_3_am2, v_time_am_2.jog_4 AS jog_4_am2, v_time_am_2.jog_5 AS jog_5_am2,
                v_time_am_2.jog_6 AS jog_6_am2, v_time_am_2.jog_7 AS jog_7_am2, v_time_am_2.jog_8 AS jog_8_am2, v_time_am_2.jog_9 AS jog_9_am2, v_time_am_2.jog_10 AS jog_10_am2, v_time_am_2.jog_11 AS jog_11_am2, v_time_am_2.cad AS cad_am2

                FROM v_time_az_1 AS v_time_az_1
                LEFT JOIN v_time_am_1 AS v_time_am_1 ON v_time_am_1.su_ref = v_time_az_1.su_ref
                LEFT JOIN v_time_az_2 AS v_time_az_2 ON v_time_az_2.su_ref = v_time_az_1.su_ref
                LEFT JOIN v_time_am_2 AS v_time_am_2 ON v_time_am_2.su_ref = v_time_az_2.su_ref
            ";
            
            
            $stmt_1 = $this->conexao->prepare($sql);
            $stmt_1->execute();            
            // $stmt_2 = $this->conexao->prepare($sql_az_2);
            // $stmt_2->execute();
            // $stmt_3 = $this->conexao->prepare($sql_am_1);
            // $stmt_3->execute();
            // $stmt_4 = $this->conexao->prepare($sql_am_2);
            // $stmt_4->execute();
            return $stmt_1->fetchAll(PDO::FETCH_OBJ);
            // return $stmt_2->fetchAll(PDO::FETCH_OBJ);
            // return $stmt_3->fetchAll(PDO::FETCH_OBJ);
            // return $stmt_4->fetchAll(PDO::FETCH_OBJ);
            
            // echo "<pre>";
            // print_r($sql);
            // echo "</pre>";
        }

        public function atualizar(){ //update
            $sql_az_1= "UPDATE Time_Azul_1 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
            
            $sql_az_2= "UPDATE Time_Azul_2 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
            
            $sql_am_1= "UPDATE Time_Amarelo_1 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
            
            $sql_am_2= "UPDATE Time_Amarelo_2 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
           

            $stmt = $this->conexao->prepare($sql);
            $stmt->bindValue(':id', $this->cadastroTimes->__get('id'));
            $stmt->bindValue(':nome', $this->cadastroTimes->__get('nome'));
            $stmt->bindValue(':apelido', $this->cadastroTimes->__get('apelido'));
            $stmt->bindValue(':padrinho', $this->cadastroTimes->__get('padrinho'));
            $stmt->bindValue(':posicao', $this->cadastroTimes->__get('posicao'));
            $stmt->bindValue(':data_nascimento', $this->cadastroTimes->__get('data_nascimento'));
            $stmt->bindValue(':natural_de', $this->cadastroTimes->__get('natural_de'));
            $stmt->bindValue(':time_que_torce', $this->cadastroTimes->__get('time_que_torce'));
            $stmt->bindValue(':endereco', $this->cadastroTimes->__get('endereco'));
            $stmt->bindValue(':bairro', $this->cadastroTimes->__get('bairro'));
            $stmt->bindValue(':cep', $this->cadastroTimes->__get('cep'));
            $stmt->bindValue(':data_entrada', $this->cadastroTimes->__get('data_entrada'));
            $stmt->bindValue(':data_saida', $this->cadastroTimes->__get('data_saida'));
            $stmt->bindValue(':data_afastamento', $this->cadastroTimes->__get('data_afastamento'));
            $stmt->bindValue(':status', $this->cadastroTimes->__get('status'));
            $stmt->bindValue(':celular', $this->cadastroTimes->__get('celular'));
            $stmt->bindValue(':email', $this->cadastroTimes->__get('email'));
            $stmt->bindValue(':telefone', $this->cadastroTimes->__get('telefone'));
            $stmt->bindValue(':atualizado_por', $this->cadastroTimes->__get('user_id'));
            return $stmt->execute();
            // echo "<pre>";
            // //print_r($_POST);
            // print_r($this->cadastroTimes);

        }

        public function excluir(){ //delete
            $table1 = "Time_Azul_1";
            $table2 = "Time_Azul_2";
            $table3 = "Time_Amarelo_1";
            $table4 = "Time_Amarelo_2";
            for($i = 0; $i <=4; $i++){
                $sql = "DELETE FROM $table[$i] WHERE sumula_referencia = :id";
                $stmt = $this->conexao->prepare($sql);
    
                $stmt->bindValue(':id', $this->cadastroTimes->__get('id'));
                return $stmt->execute();            
            }
            

        }

    }

?>