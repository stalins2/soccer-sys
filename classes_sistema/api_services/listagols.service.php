<?php
    class ListaGols {
        private $conexao;
        private $listaGols;

        public function __construct(Conexao $conexao, $listaGols){
            $this->conexao = $conexao->conectar();
            $this->listaGols = $listaGols;
        }

    public function listar(){ //read
         
        $sql = "SELECT
                gols.id AS gol_id,
                cadastros.nome AS gol_nome,
                gols.sumula_referencia AS gol_su,
                gols.time AS gol_time,
                gols.primeiro_tempo AS gol_primeiro_tempo,
                gols.segundo_tempo AS gol_segundo_tempo,
                gols.id_cadastrador AS gol_cad

                FROM Gols AS gols 
                left join Cadastros AS cadastros 
                ON (cadastros.id = gols.jogador) ORDER BY gol_id DESC";
        $stmt = $this->conexao->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
        // return $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "<pre>";
        // print_r($stmt);
        
    }
}
?>