<?php
    class ListaPenaltis {
        private $conexao;
        private $listaPenaltis;

        public function __construct(Conexao $conexao, $listaPenaltis){
            $this->conexao = $conexao->conectar();
            $this->listaPenaltis = $listaPenaltis;
        }

    public function listar(){ //read
         
        $sql = "SELECT
                penaltis.id AS penalti_id,
                cadastros.nome AS penalti_nome,
                penaltis.sumula_referencia AS penalti_su,
                penaltis.time AS penalti_time,
                penaltis.convertidos AS penalti_primeiro_tempo,
                penaltis.perdidos AS penalti_segundo_tempo,
                penaltis.id_cadastrador AS penalti_cad

                FROM Penaltis AS penaltis 
                left join Cadastros AS cadastros 
                ON (cadastros.id = penaltis.jogador) ORDER BY penalti_id DESC";
        $stmt = $this->conexao->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
        // return $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "<pre>";
        // print_r($stmt);
        
    }
}
?>