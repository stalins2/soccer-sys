<?php
    class ListaMembros {
        private $conexao;
        private $listaMembros;

        public function __construct(Conexao $conexao, $listaMembros){
            $this->conexao = $conexao->conectar();
            $this->listaMembros = $listaMembros;
        }

    public function listar(){ //read
        $sql = "SELECT 
                cadastros.id as id, 
                cadastros.nome as nome 
                FROM Cadastros as cadastros 
                ORDER BY id ASC";
        $stmt = $this->conexao->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
        // return $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "<pre>";
        // print_r($stmt);
        
    }
}
?>