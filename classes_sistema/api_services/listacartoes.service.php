<?php
    class ListaCartoes {
        private $conexao;
        private $listaCartoes;

        public function __construct(Conexao $conexao, $listaCartoes){
            $this->conexao = $conexao->conectar();
            $this->listaCartoes = $listaCartoes;
        }

    public function listar(){ //read
         
        $sql = "SELECT cartoes.id AS cartao_id, 
                cartoes.sumula_referencia AS cartao_su,
                cadastros.nome AS cartao_nome, 
                cartoes.amarelo AS cartao_am, 
                cartoes.azul AS cartao_az, 
                cartoes.vermelho AS cartao_ve, 
                cartoes.id_cadastrador AS cartao_cad 
                FROM Cartoes AS cartoes 
                left join Cadastros AS cadastros 
                ON (cadastros.id = cartoes.jogador) ORDER BY cartao_id DESC";
        $stmt = $this->conexao->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
        // return $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "<pre>";
        // print_r($stmt);
        
    }
}
?>