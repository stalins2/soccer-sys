<?php
    class ListaPenaltis {
        private $conexao;
        private $listaPenaltis;

        public function __construct(Conexao $conexao, $listaPenaltis){
            $this->conexao = $conexao->conectar();
            $this->listaPenaltis = $listaPenaltis;
        }

    public function listar(){ //read
         
        $sql = "SELECT
        Arbitragem_1.id AS arbitragem1_id,
        Arbitragem_1.arbitro as arbitro1_1,
        Arbitragem_1.auxiliar_1 as arbitro1_2,
        Arbitragem_1.auxiliar_2 as arbitro1_3,
        
        Arbitragem_2.id AS arbitragem2_id,
        Arbitragem_2.arbitro as arbitro2_1,
        Arbitragem_2.auxiliar_1 as arbitro2_2,
        Arbitragem_2.auxiliar_2 as arbitro2_3,

        Arbitragem_1.sumula_referencia AS arbitragem_su1,
        Arbitragem_2.sumula_referencia AS arbitragem_su2,

        nome_1.nome AS arbitro1_1_nome,
        nome_2.nome AS auxiliar1_1_nome,
        nome_3.nome AS auxiliar1_2_nome,
        
        nome_4.nome AS arbitro2_1_nome,
        nome_5.nome AS auxiliar2_1_nome,
        nome_6.nome AS auxiliar2_2_nome,
        
        Arbitragem_1.id_cadastrador AS arbitragem_cad

        FROM 
            Arbitragem_1,
            Arbitragem_2
        LEFT JOIN
            Cadastros AS nome_1 ON nome_1.id = arbitro1_1
        LEFT JOIN        
            Cadastros AS nome_2 ON nome_2.id = arbitro1_2
        LEFT JOIN
            Cadastros AS nome_3 ON nome_3.id = arbitro1_3
        LEFT JOIN            
            Cadastros AS nome_4 ON nome_4.id = arbitro2_1
        LEFT JOIN
            Cadastros AS nome_5 ON nome_5.id = arbitro2_2
        LEFT JOIN
            Cadastros AS nome_6 ON nome_6.id = arbitro2_3

        ORDER BY Arbitragem_1.id DESC



        LEFT JOIN Cadastros AS cadastros1 ON cadastros1.id = arbitragem1_1.arbitro
        LEFT JOIN Cadastros AS cadastros2 ON cadastros2.id = arbitragem1_2.auxiliar_1
        LEFT JOIN Cadastros AS cadastros3 ON cadastros3.id = arbitragem1_3.auxiliar_2
        LEFT JOIN Cadastros AS cadastros4 ON cadastros4.id = arbitragem2_1.arbitro
        LEFT JOIN Cadastros AS cadastros5 ON cadastros5.id = arbitragem2_2.auxiliar_1
        LEFT JOIN Cadastros AS cadastros6 ON cadastros6.id = arbitragem2_3.auxiliar_2

         ORDER BY arbitragem1_id DESC
                ";
        $stmt = $this->conexao->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
        // return $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "<pre>";
        // print_r($stmt);
        
    }
}
?>