<?php
    class ListaService {
        private $conexao;
        private $listaSumula;

        public function __construct(Conexao $conexao, $listaSumula){
            $this->conexao = $conexao->conectar();
            $this->listaSumula = $listaSumula;
        }

    public function listar(){ //read
        $sql = "SELECT sumula.id as id, sumula.data_jogo as data_jogo FROM Sumula as sumula ORDER BY id DESC";
        $stmt = $this->conexao->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
        // echo "<pre>";
        // print_r($stmt);
        
    }
}
?>