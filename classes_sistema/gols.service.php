<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1); 
// echo "<br />gols.service: ok<br />";
    class GolsService {
        private $conexao;
        private $cadastroGols;
        private $atualizaGols;

        public function __construct(Conexao $conexao, $cadastroGols){
            $this->conexao = $conexao->conectar();
            $this->cadastroGols = $cadastroGols;
        }

        public function inserir(){ //create
            if(($_POST['jogador_10']) != ""){ // se existir esse campo será registrado todos os inputs
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador),
                    (:sumula_referencia, :jogador_5, :time_gols_5, :gols_primeiro_tempo_5, :gols_segundo_tempo_5, :id_cadastrador),
                    (:sumula_referencia, :jogador_6, :time_gols_6, :gols_primeiro_tempo_6, :gols_segundo_tempo_6, :id_cadastrador),
                    (:sumula_referencia, :jogador_7, :time_gols_7, :gols_primeiro_tempo_7, :gols_segundo_tempo_7, :id_cadastrador),
                    (:sumula_referencia, :jogador_8, :time_gols_8, :gols_primeiro_tempo_8, :gols_segundo_tempo_8, :id_cadastrador),
                    (:sumula_referencia, :jogador_9, :time_gols_9, :gols_primeiro_tempo_9, :gols_segundo_tempo_9, :id_cadastrador),
                    (:sumula_referencia, :jogador_10, :time_gols_10, :gols_primeiro_tempo_10, :gols_segundo_tempo_10, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':jogador_5', $this->cadastroGols->__get('jogador_5'));
                    $stmt_1->bindValue(':time_gols_5', $this->cadastroGols->__get('time_gols_5'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_5', $this->cadastroGols->__get('gols_primeiro_tempo_5'));
                    $stmt_1->bindValue(':gols_segundo_tempo_5', $this->cadastroGols->__get('gols_segundo_tempo_5'));
                    
                    $stmt_1->bindValue(':jogador_6', $this->cadastroGols->__get('jogador_6'));
                    $stmt_1->bindValue(':time_gols_6', $this->cadastroGols->__get('time_gols_6'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_6', $this->cadastroGols->__get('gols_primeiro_tempo_6'));
                    $stmt_1->bindValue(':gols_segundo_tempo_6', $this->cadastroGols->__get('gols_segundo_tempo_6'));
                    
                    $stmt_1->bindValue(':jogador_7', $this->cadastroGols->__get('jogador_7'));
                    $stmt_1->bindValue(':time_gols_7', $this->cadastroGols->__get('time_gols_7'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_7', $this->cadastroGols->__get('gols_primeiro_tempo_7'));
                    $stmt_1->bindValue(':gols_segundo_tempo_7', $this->cadastroGols->__get('gols_segundo_tempo_7'));
                    
                    $stmt_1->bindValue(':jogador_8', $this->cadastroGols->__get('jogador_8'));
                    $stmt_1->bindValue(':time_gols_8', $this->cadastroGols->__get('time_gols_8'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_8', $this->cadastroGols->__get('gols_primeiro_tempo_8'));
                    $stmt_1->bindValue(':gols_segundo_tempo_8', $this->cadastroGols->__get('gols_segundo_tempo_8'));
                    
                    $stmt_1->bindValue(':jogador_9', $this->cadastroGols->__get('jogador_9'));
                    $stmt_1->bindValue(':time_gols_9', $this->cadastroGols->__get('time_gols_9'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_9', $this->cadastroGols->__get('gols_primeiro_tempo_9'));
                    $stmt_1->bindValue(':gols_segundo_tempo_9', $this->cadastroGols->__get('gols_segundo_tempo_9'));   
                    
                    $stmt_1->bindValue(':jogador_10', $this->cadastroGols->__get('jogador_10'));
                    $stmt_1->bindValue(':time_gols_10', $this->cadastroGols->__get('time_gols_10'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_10', $this->cadastroGols->__get('gols_primeiro_tempo_10'));
                    $stmt_1->bindValue(':gols_segundo_tempo_10', $this->cadastroGols->__get('gols_segundo_tempo_10'));                    
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();


            }else if(($_POST['jogador_9']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador),
                    (:sumula_referencia, :jogador_5, :time_gols_5, :gols_primeiro_tempo_5, :gols_segundo_tempo_5, :id_cadastrador),
                    (:sumula_referencia, :jogador_6, :time_gols_6, :gols_primeiro_tempo_6, :gols_segundo_tempo_6, :id_cadastrador),
                    (:sumula_referencia, :jogador_7, :time_gols_7, :gols_primeiro_tempo_7, :gols_segundo_tempo_7, :id_cadastrador),
                    (:sumula_referencia, :jogador_8, :time_gols_8, :gols_primeiro_tempo_8, :gols_segundo_tempo_8, :id_cadastrador),
                    (:sumula_referencia, :jogador_9, :time_gols_9, :gols_primeiro_tempo_9, :gols_segundo_tempo_9, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':jogador_5', $this->cadastroGols->__get('jogador_5'));
                    $stmt_1->bindValue(':time_gols_5', $this->cadastroGols->__get('time_gols_5'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_5', $this->cadastroGols->__get('gols_primeiro_tempo_5'));
                    $stmt_1->bindValue(':gols_segundo_tempo_5', $this->cadastroGols->__get('gols_segundo_tempo_5'));
                    
                    $stmt_1->bindValue(':jogador_6', $this->cadastroGols->__get('jogador_6'));
                    $stmt_1->bindValue(':time_gols_6', $this->cadastroGols->__get('time_gols_6'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_6', $this->cadastroGols->__get('gols_primeiro_tempo_6'));
                    $stmt_1->bindValue(':gols_segundo_tempo_6', $this->cadastroGols->__get('gols_segundo_tempo_6'));
                    
                    $stmt_1->bindValue(':jogador_7', $this->cadastroGols->__get('jogador_7'));
                    $stmt_1->bindValue(':time_gols_7', $this->cadastroGols->__get('time_gols_7'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_7', $this->cadastroGols->__get('gols_primeiro_tempo_7'));
                    $stmt_1->bindValue(':gols_segundo_tempo_7', $this->cadastroGols->__get('gols_segundo_tempo_7'));
                    
                    $stmt_1->bindValue(':jogador_8', $this->cadastroGols->__get('jogador_8'));
                    $stmt_1->bindValue(':time_gols_8', $this->cadastroGols->__get('time_gols_8'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_8', $this->cadastroGols->__get('gols_primeiro_tempo_8'));
                    $stmt_1->bindValue(':gols_segundo_tempo_8', $this->cadastroGols->__get('gols_segundo_tempo_8'));
                    
                    $stmt_1->bindValue(':jogador_9', $this->cadastroGols->__get('jogador_9'));
                    $stmt_1->bindValue(':time_gols_9', $this->cadastroGols->__get('time_gols_9'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_9', $this->cadastroGols->__get('gols_primeiro_tempo_9'));
                    $stmt_1->bindValue(':gols_segundo_tempo_9', $this->cadastroGols->__get('gols_segundo_tempo_9'));                      
    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();

            }else if(($_POST['jogador_8']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador),
                    (:sumula_referencia, :jogador_5, :time_gols_5, :gols_primeiro_tempo_5, :gols_segundo_tempo_5, :id_cadastrador),
                    (:sumula_referencia, :jogador_6, :time_gols_6, :gols_primeiro_tempo_6, :gols_segundo_tempo_6, :id_cadastrador),
                    (:sumula_referencia, :jogador_7, :time_gols_7, :gols_primeiro_tempo_7, :gols_segundo_tempo_7, :id_cadastrador),
                    (:sumula_referencia, :jogador_8, :time_gols_8, :gols_primeiro_tempo_8, :gols_segundo_tempo_8, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':jogador_5', $this->cadastroGols->__get('jogador_5'));
                    $stmt_1->bindValue(':time_gols_5', $this->cadastroGols->__get('time_gols_5'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_5', $this->cadastroGols->__get('gols_primeiro_tempo_5'));
                    $stmt_1->bindValue(':gols_segundo_tempo_5', $this->cadastroGols->__get('gols_segundo_tempo_5'));
                    
                    $stmt_1->bindValue(':jogador_6', $this->cadastroGols->__get('jogador_6'));
                    $stmt_1->bindValue(':time_gols_6', $this->cadastroGols->__get('time_gols_6'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_6', $this->cadastroGols->__get('gols_primeiro_tempo_6'));
                    $stmt_1->bindValue(':gols_segundo_tempo_6', $this->cadastroGols->__get('gols_segundo_tempo_6'));
                    
                    $stmt_1->bindValue(':jogador_7', $this->cadastroGols->__get('jogador_7'));
                    $stmt_1->bindValue(':time_gols_7', $this->cadastroGols->__get('time_gols_7'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_7', $this->cadastroGols->__get('gols_primeiro_tempo_7'));
                    $stmt_1->bindValue(':gols_segundo_tempo_7', $this->cadastroGols->__get('gols_segundo_tempo_7'));
                    
                    $stmt_1->bindValue(':jogador_8', $this->cadastroGols->__get('jogador_8'));
                    $stmt_1->bindValue(':time_gols_8', $this->cadastroGols->__get('time_gols_8'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_8', $this->cadastroGols->__get('gols_primeiro_tempo_8'));
                    $stmt_1->bindValue(':gols_segundo_tempo_8', $this->cadastroGols->__get('gols_segundo_tempo_8'));

                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();

            }else if(($_POST['jogador_7']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador),
                    (:sumula_referencia, :jogador_5, :time_gols_5, :gols_primeiro_tempo_5, :gols_segundo_tempo_5, :id_cadastrador),
                    (:sumula_referencia, :jogador_6, :time_gols_6, :gols_primeiro_tempo_6, :gols_segundo_tempo_6, :id_cadastrador),
                    (:sumula_referencia, :jogador_7, :time_gols_7, :gols_primeiro_tempo_7, :gols_segundo_tempo_7, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':jogador_5', $this->cadastroGols->__get('jogador_5'));
                    $stmt_1->bindValue(':time_gols_5', $this->cadastroGols->__get('time_gols_5'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_5', $this->cadastroGols->__get('gols_primeiro_tempo_5'));
                    $stmt_1->bindValue(':gols_segundo_tempo_5', $this->cadastroGols->__get('gols_segundo_tempo_5'));
                    
                    $stmt_1->bindValue(':jogador_6', $this->cadastroGols->__get('jogador_6'));
                    $stmt_1->bindValue(':time_gols_6', $this->cadastroGols->__get('time_gols_6'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_6', $this->cadastroGols->__get('gols_primeiro_tempo_6'));
                    $stmt_1->bindValue(':gols_segundo_tempo_6', $this->cadastroGols->__get('gols_segundo_tempo_6'));
                    
                    $stmt_1->bindValue(':jogador_7', $this->cadastroGols->__get('jogador_7'));
                    $stmt_1->bindValue(':time_gols_7', $this->cadastroGols->__get('time_gols_7'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_7', $this->cadastroGols->__get('gols_primeiro_tempo_7'));
                    $stmt_1->bindValue(':gols_segundo_tempo_7', $this->cadastroGols->__get('gols_segundo_tempo_7'));
                    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();


            }else if(($_POST['jogador_6']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador),
                    (:sumula_referencia, :jogador_5, :time_gols_5, :gols_primeiro_tempo_5, :gols_segundo_tempo_5, :id_cadastrador),
                    (:sumula_referencia, :jogador_6, :time_gols_6, :gols_primeiro_tempo_6, :gols_segundo_tempo_6, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':jogador_5', $this->cadastroGols->__get('jogador_5'));
                    $stmt_1->bindValue(':time_gols_5', $this->cadastroGols->__get('time_gols_5'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_5', $this->cadastroGols->__get('gols_primeiro_tempo_5'));
                    $stmt_1->bindValue(':gols_segundo_tempo_5', $this->cadastroGols->__get('gols_segundo_tempo_5'));
                    
                    $stmt_1->bindValue(':jogador_6', $this->cadastroGols->__get('jogador_6'));
                    $stmt_1->bindValue(':time_gols_6', $this->cadastroGols->__get('time_gols_6'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_6', $this->cadastroGols->__get('gols_primeiro_tempo_6'));
                    $stmt_1->bindValue(':gols_segundo_tempo_6', $this->cadastroGols->__get('gols_segundo_tempo_6'));
                    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();

            }else if(($_POST['jogador_5']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador),
                    (:sumula_referencia, :jogador_5, :time_gols_5, :gols_primeiro_tempo_5, :gols_segundo_tempo_5, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':jogador_5', $this->cadastroGols->__get('jogador_5'));
                    $stmt_1->bindValue(':time_gols_5', $this->cadastroGols->__get('time_gols_5'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_5', $this->cadastroGols->__get('gols_primeiro_tempo_5'));
                    $stmt_1->bindValue(':gols_segundo_tempo_5', $this->cadastroGols->__get('gols_segundo_tempo_5'));
                    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();

            }else if(($_POST['jogador_4']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador),
                    (:sumula_referencia, :jogador_4, :time_gols_4, :gols_primeiro_tempo_4, :gols_segundo_tempo_4, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':jogador_4', $this->cadastroGols->__get('jogador_4'));
                    $stmt_1->bindValue(':time_gols_4', $this->cadastroGols->__get('time_gols_4'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_4', $this->cadastroGols->__get('gols_primeiro_tempo_4'));    
                    $stmt_1->bindValue(':gols_segundo_tempo_4', $this->cadastroGols->__get('gols_segundo_tempo_4'));
                    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();            

            }else if(($_POST['jogador_3']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador),
                    (:sumula_referencia, :jogador_3, :time_gols_3, :gols_primeiro_tempo_3, :gols_segundo_tempo_3, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':jogador_3', $this->cadastroGols->__get('jogador_3'));
                    $stmt_1->bindValue(':time_gols_3', $this->cadastroGols->__get('time_gols_3'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_3', $this->cadastroGols->__get('gols_primeiro_tempo_3'));
                    $stmt_1->bindValue(':gols_segundo_tempo_3', $this->cadastroGols->__get('gols_segundo_tempo_3'));
                    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();            
            }else if(($_POST['jogador_2']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador),
                    (:sumula_referencia, :jogador_2, :time_gols_2, :gols_primeiro_tempo_2, :gols_segundo_tempo_2, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));
    
                    $stmt_1->bindValue(':jogador_2', $this->cadastroGols->__get('jogador_2'));
                    $stmt_1->bindValue(':time_gols_2', $this->cadastroGols->__get('time_gols_2'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_2', $this->cadastroGols->__get('gols_primeiro_tempo_2'));
                    $stmt_1->bindValue(':gols_segundo_tempo_2', $this->cadastroGols->__get('gols_segundo_tempo_2'));
                    
                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();            
                    

            }else if(($_POST['jogador_1']) != ""){
                $query_1 = "
                INSERT INTO
                    Gols (sumula_referencia, jogador, time, primeiro_tempo, segundo_tempo, id_cadastrador)
                VALUES
                    (:sumula_referencia, :jogador_1, :time_gols_1, :gols_primeiro_tempo_1, :gols_segundo_tempo_1, :id_cadastrador)";  

                    $stmt_1 = $this->conexao->prepare($query_1);
                    $stmt_1->bindValue(':sumula_referencia', $this->cadastroGols->__get('sumula_referencia'));
    
                    $stmt_1->bindValue(':jogador_1', $this->cadastroGols->__get('jogador_1'));
                    $stmt_1->bindValue(':time_gols_1', $this->cadastroGols->__get('time_gols_1'));
                    $stmt_1->bindValue(':gols_primeiro_tempo_1', $this->cadastroGols->__get('gols_primeiro_tempo_1'));
                    $stmt_1->bindValue(':gols_segundo_tempo_1', $this->cadastroGols->__get('gols_segundo_tempo_1'));

                    $stmt_1->bindValue(':id_cadastrador', $this->cadastroGols->__get('user_id'));
                    $stmt_1->execute();              

            }
            
            // echo "<pre>";
            // print_r($stmt_1);
        }

        public function listar(){ //read
            $sql = "
                SELECT 
                v_time_az_1.id_az1 AS id_az1, v_time_az_1.su_ref AS su_ref_az_1, v_time_az_1.jog_1 AS jog_1_az1, v_time_az_1.jog_2 AS jog_2_az1, v_time_az_1.jog_3 AS jog_3_az1, v_time_az_1.jog_4 AS jog_4_az1, v_time_az_1.jog_5 AS jog_5_az1,
                v_time_az_1.jog_6 AS jog_6_az1, v_time_az_1.jog_7 AS jog_7_az1, v_time_az_1.jog_8 AS jog_8_az1, v_time_az_1.jog_9 AS jog_9_az1, v_time_az_1.jog_10 AS jog_10_az1, v_time_az_1.jog_11 AS jog_11_az1, v_time_az_1.cad AS cad_az1,

                v_time_am_1.id_am1 AS id_am1, v_time_am_1.su_ref AS su_ref_am_1, v_time_am_1.jog_1 AS jog_1_am1, v_time_am_1.jog_2 AS jog_2_am1, v_time_am_1.jog_3 AS jog_3_am1, v_time_am_1.jog_4 AS jog_4_am1, v_time_am_1.jog_5 AS jog_5_am1,
                v_time_am_1.jog_6 AS jog_6_am1, v_time_am_1.jog_7 AS jog_7_am1, v_time_am_1.jog_8 AS jog_8_am1, v_time_am_1.jog_9 AS jog_9_am1, v_time_am_1.jog_10 AS jog_10_am1, v_time_am_1.jog_11 AS jog_11_am1, v_time_am_1.cad AS cad_am1,

                v_time_az_2.id_az2 AS id_az2, v_time_az_2.su_ref AS su_ref_az_2, v_time_az_2.jog_1 AS jog_1_az2, v_time_az_2.jog_2 AS jog_2_az2, v_time_az_2.jog_3 AS jog_3_az2, v_time_az_2.jog_4 AS jog_4_az2, v_time_az_2.jog_5 AS jog_5_az2,
                v_time_az_2.jog_6 AS jog_6_az2, v_time_az_2.jog_7 AS jog_7_az2, v_time_az_2.jog_8 AS jog_8_az2, v_time_az_2.jog_9 AS jog_9_az2, v_time_az_2.jog_10 AS jog_10_az2, v_time_az_2.jog_11 AS jog_11_az2, v_time_az_2.cad AS cad_az2,

                v_time_am_2.id_am2 AS id_am2, v_time_am_2.su_ref AS su_ref_am_2, v_time_am_2.jog_1 AS jog_1_am2, v_time_am_2.jog_2 AS jog_2_am2, v_time_am_2.jog_3 AS jog_3_am2, v_time_am_2.jog_4 AS jog_4_am2, v_time_am_2.jog_5 AS jog_5_am2,
                v_time_am_2.jog_6 AS jog_6_am2, v_time_am_2.jog_7 AS jog_7_am2, v_time_am_2.jog_8 AS jog_8_am2, v_time_am_2.jog_9 AS jog_9_am2, v_time_am_2.jog_10 AS jog_10_am2, v_time_am_2.jog_11 AS jog_11_am2, v_time_am_2.cad AS cad_am2

                FROM v_time_az_1 AS v_time_az_1
                LEFT JOIN v_time_am_1 AS v_time_am_1 ON v_time_am_1.su_ref = v_time_az_1.su_ref
                LEFT JOIN v_time_az_2 AS v_time_az_2 ON v_time_az_2.su_ref = v_time_az_1.su_ref
                LEFT JOIN v_time_am_2 AS v_time_am_2 ON v_time_am_2.su_ref = v_time_az_2.su_ref
            ";
            
            
            $stmt_1 = $this->conexao->prepare($sql);
            $stmt_1->execute();            
            // $stmt_2 = $this->conexao->prepare($sql_az_2);
            // $stmt_2->execute();
            // $stmt_3 = $this->conexao->prepare($sql_am_1);
            // $stmt_3->execute();
            // $stmt_4 = $this->conexao->prepare($sql_am_2);
            // $stmt_4->execute();
            return $stmt_1->fetchAll(PDO::FETCH_OBJ);
            // return $stmt_2->fetchAll(PDO::FETCH_OBJ);
            // return $stmt_3->fetchAll(PDO::FETCH_OBJ);
            // return $stmt_4->fetchAll(PDO::FETCH_OBJ);
            
            // echo "<pre>";
            // print_r($sql);
            // echo "</pre>";
        }

        public function atualizar(){ //update
            $sql_az_1= "UPDATE Time_Azul_1 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
            
            $sql_az_2= "UPDATE Time_Azul_2 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
            
            $sql_am_1= "UPDATE Time_Amarelo_1 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
            
            $sql_am_2= "UPDATE Time_Amarelo_2 SET
            1 = :j_1, 2 = :j_3, 3 = :j_3, 4 = :j_4, 5 = :j_5, 6 = :j_6, 7 = :j_7, 8 = :j_8, 9 = :j_9, 10 = :j_10, 11 = :j_11
            WHERE id = :id";
           

            $stmt = $this->conexao->prepare($sql);
            $stmt->bindValue(':id', $this->cadastroTimes->__get('id'));
            $stmt->bindValue(':nome', $this->cadastroTimes->__get('nome'));
            $stmt->bindValue(':apelido', $this->cadastroTimes->__get('apelido'));
            $stmt->bindValue(':padrinho', $this->cadastroTimes->__get('padrinho'));
            $stmt->bindValue(':posicao', $this->cadastroTimes->__get('posicao'));
            $stmt->bindValue(':data_nascimento', $this->cadastroTimes->__get('data_nascimento'));
            $stmt->bindValue(':natural_de', $this->cadastroTimes->__get('natural_de'));
            $stmt->bindValue(':time_que_torce', $this->cadastroTimes->__get('time_que_torce'));
            $stmt->bindValue(':endereco', $this->cadastroTimes->__get('endereco'));
            $stmt->bindValue(':bairro', $this->cadastroTimes->__get('bairro'));
            $stmt->bindValue(':cep', $this->cadastroTimes->__get('cep'));
            $stmt->bindValue(':data_entrada', $this->cadastroTimes->__get('data_entrada'));
            $stmt->bindValue(':data_saida', $this->cadastroTimes->__get('data_saida'));
            $stmt->bindValue(':data_afastamento', $this->cadastroTimes->__get('data_afastamento'));
            $stmt->bindValue(':status', $this->cadastroTimes->__get('status'));
            $stmt->bindValue(':celular', $this->cadastroTimes->__get('celular'));
            $stmt->bindValue(':email', $this->cadastroTimes->__get('email'));
            $stmt->bindValue(':telefone', $this->cadastroTimes->__get('telefone'));
            $stmt->bindValue(':atualizado_por', $this->cadastroTimes->__get('user_id'));
            return $stmt->execute();
            // echo "<pre>";
            // //print_r($_POST);
            // print_r($this->cadastroTimes);

        }

        public function excluir(){ //delete
            $table1 = "Time_Azul_1";
            $table2 = "Time_Azul_2";
            $table3 = "Time_Amarelo_1";
            $table4 = "Time_Amarelo_2";
            for($i = 0; $i <=4; $i++){
                $sql = "DELETE FROM $table[$i] WHERE sumula_referencia = :id";
                $stmt = $this->conexao->prepare($sql);
    
                $stmt->bindValue(':id', $this->cadastroTimes->__get('id'));
                return $stmt->execute();            
            }
            

        }

    }

?>