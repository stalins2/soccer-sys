<?php
session_start();
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
    require_once('../../../../../classes_sistema/controllers/conexao_controller.php');

    $conn = new Conexao();
    $conn = $conn->conectar();

    $sql = "SELECT * FROM Acessos WHERE";
    $sql .= " email = :email";
    $sql .= " AND senha = :senha";
    
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':email', $_POST['email']);
    $stmt->bindValue(':senha', $_POST['senha']);

    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    $usuario_autenticado = false;

    if($user['email'] == $_POST['email'] && $user['senha'] == $_POST['senha']) {
        $usuario_autenticado = true;
    }

    if($usuario_autenticado){      
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['user_nome'] = $user['nome'];
        $_SESSION['user_email'] = $user['email'];
        $_SESSION['user_nivel'] = $user['nivel'];
        $_SESSION['online'] = $usuario_autenticado;
        header('Location: ../../../pages/cadastro_sumula.php');
    }else{
        $_SESSION['online'] = $usuario_autenticado;        
        header('Location: ../../../../sistema/index.php?login=err');
    }
?>