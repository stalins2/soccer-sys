<?
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1); 
    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;
    // echo "cadastro times controller: carregou ok";

if($acao == 'cadastrar'){    
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/cartoes.service.php');


    $cadastroCartoes = new CadastroCartoes();

    $cadastroCartoes ->__set('sumula_referencia', $_POST['sumula_referencia']);

    $cadastroCartoes ->__set('cartao_jogador_1', $_POST['cartao_jogador_1']);
    $cadastroCartoes ->__set('cartao_jogador_2', $_POST['cartao_jogador_2']);
    $cadastroCartoes ->__set('cartao_jogador_3', $_POST['cartao_jogador_3']);
    $cadastroCartoes ->__set('cartao_jogador_4', $_POST['cartao_jogador_4']);
    $cadastroCartoes ->__set('cartao_jogador_5', $_POST['cartao_jogador_5']);
    $cadastroCartoes ->__set('cartao_jogador_6', $_POST['cartao_jogador_6']);
    $cadastroCartoes ->__set('cartao_jogador_7', $_POST['cartao_jogador_7']);
    $cadastroCartoes ->__set('cartao_jogador_8', $_POST['cartao_jogador_8']);
    $cadastroCartoes ->__set('cartao_jogador_9', $_POST['cartao_jogador_9']);
    $cadastroCartoes ->__set('cartao_jogador_10', $_POST['cartao_jogador_10']);

    
    $cadastroCartoes ->__set('amarelo_1', $_POST['amarelo_1']);
    $cadastroCartoes ->__set('amarelo_2', $_POST['amarelo_2']);
    $cadastroCartoes ->__set('amarelo_3', $_POST['amarelo_3']);
    $cadastroCartoes ->__set('amarelo_4', $_POST['amarelo_4']);
    $cadastroCartoes ->__set('amarelo_5', $_POST['amarelo_5']);
    $cadastroCartoes ->__set('amarelo_6', $_POST['amarelo_6']);
    $cadastroCartoes ->__set('amarelo_7', $_POST['amarelo_7']);
    $cadastroCartoes ->__set('amarelo_8', $_POST['amarelo_8']);
    $cadastroCartoes ->__set('amarelo_9', $_POST['amarelo_9']);
    $cadastroCartoes ->__set('amarelo_10', $_POST['amarelo_10']);

    
    $cadastroCartoes ->__set('azul_1', $_POST['azul_1']);
    $cadastroCartoes ->__set('azul_2', $_POST['azul_2']);
    $cadastroCartoes ->__set('azul_3', $_POST['azul_3']);
    $cadastroCartoes ->__set('azul_4', $_POST['azul_4']);
    $cadastroCartoes ->__set('azul_5', $_POST['azul_5']);
    $cadastroCartoes ->__set('azul_6', $_POST['azul_6']);
    $cadastroCartoes ->__set('azul_7', $_POST['azul_7']);
    $cadastroCartoes ->__set('azul_8', $_POST['azul_8']);
    $cadastroCartoes ->__set('azul_9', $_POST['azul_9']);
    $cadastroCartoes ->__set('azul_10', $_POST['azul_10']);

    
    $cadastroCartoes ->__set('vermelho_1', $_POST['vermelho_1']);
    $cadastroCartoes ->__set('vermelho_2', $_POST['vermelho_2']);
    $cadastroCartoes ->__set('vermelho_3', $_POST['vermelho_3']);
    $cadastroCartoes ->__set('vermelho_4', $_POST['vermelho_4']);
    $cadastroCartoes ->__set('vermelho_5', $_POST['vermelho_5']);
    $cadastroCartoes ->__set('vermelho_6', $_POST['vermelho_6']);
    $cadastroCartoes ->__set('vermelho_7', $_POST['vermelho_7']);
    $cadastroCartoes ->__set('vermelho_8', $_POST['vermelho_8']);
    $cadastroCartoes ->__set('vermelho_9', $_POST['vermelho_9']);
    $cadastroCartoes ->__set('vermelho_10', $_POST['vermelho_10']);



    $cadastroCartoes ->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $cartoesService = new CartoesService($conexao, $cadastroCartoes);
    $cartoesService->inserir();
    
    // echo "<pre>";
    // print_r($cadastroCartoes);
    
    header('Location:../cadastro_cartoes.php?cadastrado=1');

} else if($acao == 'exibir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/times.service.php');

    $cadastroTimes = new CadastroTimes();
    $conexao = new Conexao();

    $timesService = new TimesService($conexao, $cadastroTimes);
    $times = $timesService->listar();

} else if($acao == 'atualizar'){
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/membros.service.php');


    $cadastroMembros = new CadastroMembros();

    $cadastroMembros->__set('id', $_POST['id']);
    $cadastroMembros ->__set('nome', $_POST['nome']);
    $cadastroMembros ->__set('apelido', $_POST['apelido']);
    $cadastroMembros ->__set('posicao', $_POST['posicao']);
    $cadastroMembros ->__set('chute', $_POST['chute']);
    $cadastroMembros ->__set('altura', $_POST['altura']);
    $cadastroMembros ->__set('peso', $_POST['peso']);
    $cadastroMembros ->__set('data_nascimento', $_POST['data_nascimento']);
    $cadastroMembros ->__set('endereco', $_POST['endereco']);
    $cadastroMembros ->__set('bairro', $_POST['bairro']);
    $cadastroMembros ->__set('cep', $_POST['cep']);
    $cadastroMembros ->__set('natural_de', $_POST['natural_de']);
    $cadastroMembros ->__set('rg', $_POST['rg']);
    $cadastroMembros ->__set('cpf', $_POST['cpf']);
    $cadastroMembros ->__set('email', $_POST['email']);
    $cadastroMembros ->__set('telefone', $_POST['telefone']);
    $cadastroMembros ->__set('celular', $_POST['celular']);
    $cadastroMembros ->__set('status', $_POST['status']);
    $cadastroMembros ->__set('data_afastamento', $_POST['data_afastamento']);
    $cadastroMembros ->__set('data_cadastro', $_POST['data_cadastro']);
    $cadastroMembros ->__set('padrinho', $_POST['padrinho']);
    $cadastroMembros ->__set('time_que_torce', $_POST['time_que_torce']);
    $cadastroMembros ->__set('data_entrada', $_POST['data_entrada']);
    $cadastroMembros ->__set('data_saida', $_POST['data_saida']);
    $cadastroMembros->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $membrosService = new MembrosService($conexao, $cadastroMembros);
    $membrosService->atualizar();

    header('Location:../cadastro_membros.php?atualizado=1');

} else if($acao == 'excluir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/membros.service.php');

    $cadastroMembros = new CadastroMembros();
    $cadastroMembros->__set('id',$_GET['id']);

    $conexao = new Conexao();
    $membrosService = new membrosService($conexao, $cadastroMembros);
    $membrosService->excluir();

    header('Location:cadastro_times.php?excluido=1');

}
 ?>