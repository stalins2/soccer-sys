<?
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1); 
    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;
    // echo "cadastro times controller: carregou ok";

if($acao == 'cadastrar'){    
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/times.service.php');


    $cadastroTimes = new CadastroTimes();

    $cadastroTimes ->__set('sumula_referencia', $_POST['sumula_referencia']);
    $cadastroTimes ->__set('azul_1_1', $_POST['azul_1_1']);
    $cadastroTimes ->__set('azul_1_2', $_POST['azul_1_2']);
    $cadastroTimes ->__set('azul_1_3', $_POST['azul_1_3']);
    $cadastroTimes ->__set('azul_1_4', $_POST['azul_1_4']);
    $cadastroTimes ->__set('azul_1_5', $_POST['azul_1_5']);
    $cadastroTimes ->__set('azul_1_6', $_POST['azul_1_6']);
    $cadastroTimes ->__set('azul_1_7', $_POST['azul_1_7']);
    $cadastroTimes ->__set('azul_1_8', $_POST['azul_1_8']);
    $cadastroTimes ->__set('azul_1_9', $_POST['azul_1_9']);
    $cadastroTimes ->__set('azul_1_10', $_POST['azul_1_10']);
    $cadastroTimes ->__set('azul_1_11', $_POST['azul_1_11']);
    
    $cadastroTimes ->__set('amarelo_1_1', $_POST['amarelo_1_1']);
    $cadastroTimes ->__set('amarelo_1_2', $_POST['amarelo_1_2']);
    $cadastroTimes ->__set('amarelo_1_3', $_POST['amarelo_1_3']);
    $cadastroTimes ->__set('amarelo_1_4', $_POST['amarelo_1_4']);
    $cadastroTimes ->__set('amarelo_1_5', $_POST['amarelo_1_5']);
    $cadastroTimes ->__set('amarelo_1_6', $_POST['amarelo_1_6']);
    $cadastroTimes ->__set('amarelo_1_7', $_POST['amarelo_1_7']);
    $cadastroTimes ->__set('amarelo_1_8', $_POST['amarelo_1_8']);
    $cadastroTimes ->__set('amarelo_1_9', $_POST['amarelo_1_9']);
    $cadastroTimes ->__set('amarelo_1_10', $_POST['amarelo_1_10']);
    $cadastroTimes ->__set('amarelo_1_11', $_POST['amarelo_1_11']);
    
    $cadastroTimes ->__set('azul_2_1', $_POST['azul_2_1']);
    $cadastroTimes ->__set('azul_2_2', $_POST['azul_2_2']);
    $cadastroTimes ->__set('azul_2_3', $_POST['azul_2_3']);
    $cadastroTimes ->__set('azul_2_4', $_POST['azul_2_4']);
    $cadastroTimes ->__set('azul_2_5', $_POST['azul_2_5']);
    $cadastroTimes ->__set('azul_2_6', $_POST['azul_2_6']);
    $cadastroTimes ->__set('azul_2_7', $_POST['azul_2_7']);
    $cadastroTimes ->__set('azul_2_8', $_POST['azul_2_8']);
    $cadastroTimes ->__set('azul_2_9', $_POST['azul_2_9']);
    $cadastroTimes ->__set('azul_2_10', $_POST['azul_2_10']);
    $cadastroTimes ->__set('azul_2_11', $_POST['azul_2_11']);
    
    $cadastroTimes ->__set('amarelo_2_1', $_POST['amarelo_2_1']);
    $cadastroTimes ->__set('amarelo_2_2', $_POST['amarelo_2_2']);
    $cadastroTimes ->__set('amarelo_2_3', $_POST['amarelo_2_3']);
    $cadastroTimes ->__set('amarelo_2_4', $_POST['amarelo_2_4']);
    $cadastroTimes ->__set('amarelo_2_5', $_POST['amarelo_2_5']);
    $cadastroTimes ->__set('amarelo_2_6', $_POST['amarelo_2_6']);
    $cadastroTimes ->__set('amarelo_2_7', $_POST['amarelo_2_7']);
    $cadastroTimes ->__set('amarelo_2_8', $_POST['amarelo_2_8']);
    $cadastroTimes ->__set('amarelo_2_9', $_POST['amarelo_2_9']);
    $cadastroTimes ->__set('amarelo_2_10', $_POST['amarelo_2_10']);
    $cadastroTimes ->__set('amarelo_2_11', $_POST['amarelo_2_11']);

    $cadastroTimes ->__set('user_id', $_SESSION['user_id']);
    $conexao = new Conexao();

    $timesService = new TimesService($conexao, $cadastroTimes);
    $timesService->inserir();
    // echo "<pre>";
    // print_r($timesService);

    header('Location:../cadastro_times.php?cadastrado=1');

} else if($acao == 'exibir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/times.service.php');

    $cadastroTimes = new CadastroTimes();
    $conexao = new Conexao();

    $timesService = new TimesService($conexao, $cadastroTimes);
    $times = $timesService->listar();

} else if($acao == 'atualizar'){
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/membros.service.php');


    $cadastroMembros = new CadastroMembros();

    $cadastroMembros->__set('id', $_POST['id']);
    $cadastroMembros ->__set('nome', $_POST['nome']);
    $cadastroMembros ->__set('apelido', $_POST['apelido']);
    $cadastroMembros ->__set('posicao', $_POST['posicao']);
    $cadastroMembros ->__set('chute', $_POST['chute']);
    $cadastroMembros ->__set('altura', $_POST['altura']);
    $cadastroMembros ->__set('peso', $_POST['peso']);
    $cadastroMembros ->__set('data_nascimento', $_POST['data_nascimento']);
    $cadastroMembros ->__set('endereco', $_POST['endereco']);
    $cadastroMembros ->__set('bairro', $_POST['bairro']);
    $cadastroMembros ->__set('cep', $_POST['cep']);
    $cadastroMembros ->__set('natural_de', $_POST['natural_de']);
    $cadastroMembros ->__set('rg', $_POST['rg']);
    $cadastroMembros ->__set('cpf', $_POST['cpf']);
    $cadastroMembros ->__set('email', $_POST['email']);
    $cadastroMembros ->__set('telefone', $_POST['telefone']);
    $cadastroMembros ->__set('celular', $_POST['celular']);
    $cadastroMembros ->__set('status', $_POST['status']);
    $cadastroMembros ->__set('data_afastamento', $_POST['data_afastamento']);
    $cadastroMembros ->__set('data_cadastro', $_POST['data_cadastro']);
    $cadastroMembros ->__set('padrinho', $_POST['padrinho']);
    $cadastroMembros ->__set('time_que_torce', $_POST['time_que_torce']);
    $cadastroMembros ->__set('data_entrada', $_POST['data_entrada']);
    $cadastroMembros ->__set('data_saida', $_POST['data_saida']);
    $cadastroMembros->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $membrosService = new MembrosService($conexao, $cadastroMembros);
    $membrosService->atualizar();

    header('Location:../cadastro_membros.php?atualizado=1');

} else if($acao == 'excluir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/membros.service.php');

    $cadastroMembros = new CadastroMembros();
    $cadastroMembros->__set('id',$_GET['id']);

    $conexao = new Conexao();
    $membrosService = new membrosService($conexao, $cadastroMembros);
    $membrosService->excluir();

    header('Location:cadastro_times.php?excluido=1');

}
 ?>