<?
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1); 
    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;

if($acao == 'cadastrar'){    
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/cadastro.service.php');


    $cadastroSumula = new CadastroSumula();

    $cadastroSumula ->__set('placar_amarelo', $_POST['placar_amarelo']);
    $cadastroSumula ->__set('placar_azul', $_POST['placar_azul']);
    $cadastroSumula ->__set('placar_penaltis_amarelo', $_POST['placar_penaltis_amarelo']);
    $cadastroSumula ->__set('placar_penaltis_azul', $_POST['placar_penaltis_azul']);
    $cadastroSumula ->__set('cartoes_amarelo', $_POST['cartoes_amarelo']);
    $cadastroSumula ->__set('cartoes_azul', $_POST['cartoes_azul']);
    $cadastroSumula ->__set('data_jogo', $_POST['data_jogo']);
    $cadastroSumula ->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $cadastroService = new CadastroService($conexao, $cadastroSumula);
    $cadastroService->inserir();
    //print_r($cadastroService);
    header('Location:../cadastro_sumula.php?cadastrado=1');

} else if($acao == 'exibir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/cadastro.service.php');

    $cadastroSumula = new CadastroSumula();
    $conexao = new Conexao();

    $cadastroService = new CadastroService($conexao, $cadastroSumula);
    $sumulas = $cadastroService->listar();
} else if($acao == 'atualizar'){
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/cadastro.service.php');



    $cadastroSumula = new CadastroSumula();

    $cadastroSumula ->__set('id', $_POST['id']);
    $cadastroSumula ->__set('placar_amarelo', $_POST['placar_amarelo']);
    $cadastroSumula ->__set('placar_azul', $_POST['placar_azul']);
    $cadastroSumula ->__set('placar_penaltis_amarelo', $_POST['placar_penaltis_amarelo']);
    $cadastroSumula ->__set('placar_penaltis_azul', $_POST['placar_penaltis_azul']);
    $cadastroSumula ->__set('cartoes_amarelo', $_POST['cartoes_amarelo']);
    $cadastroSumula ->__set('cartoes_azul', $_POST['cartoes_azul']);
    $cadastroSumula ->__set('data_jogo', $_POST['data_jogo']);
    //$cadastroSumula ->__set('user_id', $_SESSION['user_id']);
    $cadastroSumula ->__set('atualizado_por', $_SESSION['user_id']);

    $conexao = new Conexao();

    $cadastroService = new CadastroService($conexao, $cadastroSumula);
    $cadastroService->atualizar();

    
    // echo "<pre>";
    // print_r($cadastroService);
    header('Location:../cadastro_sumula.php?atualizado=1');

} else if($acao == 'excluir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/cadastro.service.php');

    $cadastroSumula = new CadastroSumula();
    $cadastroSumula->__set('id',$_GET['id']);

    $conexao = new Conexao();
    $cadastroService = new CadastroService($conexao, $cadastroSumula);
    $cadastroService->excluir();

    header('Location:cadastro_sumula.php?excluido=1');

}
 ?>