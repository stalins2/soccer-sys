<?
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1); 
    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;
    // echo "cadastro penaltis controller: carregou ok";

if($acao == 'cadastrar'){    
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/penaltis.service.php');


    $cadastroPenaltis = new CadastroPenaltis();

    $cadastroPenaltis ->__set('sumula_referencia', $_POST['sumula_referencia']);

    $cadastroPenaltis ->__set('jogador_1', $_POST['jogador_1']);
    $cadastroPenaltis ->__set('jogador_2', $_POST['jogador_2']);
    $cadastroPenaltis ->__set('jogador_3', $_POST['jogador_3']);
    $cadastroPenaltis ->__set('jogador_4', $_POST['jogador_4']);
    $cadastroPenaltis ->__set('jogador_5', $_POST['jogador_5']);
    $cadastroPenaltis ->__set('jogador_6', $_POST['jogador_6']);
    $cadastroPenaltis ->__set('jogador_7', $_POST['jogador_7']);
    $cadastroPenaltis ->__set('jogador_8', $_POST['jogador_8']);
    $cadastroPenaltis ->__set('jogador_9', $_POST['jogador_9']);
    $cadastroPenaltis ->__set('jogador_10', $_POST['jogador_10']);

    
    $cadastroPenaltis ->__set('penaltis_convertidos_1', $_POST['penaltis_convertidos_1']);
    $cadastroPenaltis ->__set('penaltis_convertidos_2', $_POST['penaltis_convertidos_2']);
    $cadastroPenaltis ->__set('penaltis_convertidos_3', $_POST['penaltis_convertidos_3']);
    $cadastroPenaltis ->__set('penaltis_convertidos_4', $_POST['penaltis_convertidos_4']);
    $cadastroPenaltis ->__set('penaltis_convertidos_5', $_POST['penaltis_convertidos_5']);
    $cadastroPenaltis ->__set('penaltis_convertidos_6', $_POST['penaltis_convertidos_6']);
    $cadastroPenaltis ->__set('penaltis_convertidos_7', $_POST['penaltis_convertidos_7']);
    $cadastroPenaltis ->__set('penaltis_convertidos_8', $_POST['penaltis_convertidos_8']);
    $cadastroPenaltis ->__set('penaltis_convertidos_9', $_POST['penaltis_convertidos_9']);
    $cadastroPenaltis ->__set('penaltis_convertidos_10', $_POST['penaltis_convertidos_10']);

    
    $cadastroPenaltis ->__set('penaltis_perdidos_1', $_POST['penaltis_perdidos_1']);
    $cadastroPenaltis ->__set('penaltis_perdidos_2', $_POST['penaltis_perdidos_2']);
    $cadastroPenaltis ->__set('penaltis_perdidos_3', $_POST['penaltis_perdidos_3']);
    $cadastroPenaltis ->__set('penaltis_perdidos_4', $_POST['penaltis_perdidos_4']);
    $cadastroPenaltis ->__set('penaltis_perdidos_5', $_POST['penaltis_perdidos_5']);
    $cadastroPenaltis ->__set('penaltis_perdidos_6', $_POST['penaltis_perdidos_6']);
    $cadastroPenaltis ->__set('penaltis_perdidos_7', $_POST['penaltis_perdidos_7']);
    $cadastroPenaltis ->__set('penaltis_perdidos_8', $_POST['penaltis_perdidos_8']);
    $cadastroPenaltis ->__set('penaltis_perdidos_9', $_POST['penaltis_perdidos_9']);
    $cadastroPenaltis ->__set('penaltis_perdidos_10', $_POST['penaltis_perdidos_10']);

    
    $cadastroPenaltis ->__set('time_penaltis_1', $_POST['time_penaltis_1']);
    $cadastroPenaltis ->__set('time_penaltis_2', $_POST['time_penaltis_2']);
    $cadastroPenaltis ->__set('time_penaltis_3', $_POST['time_penaltis_3']);
    $cadastroPenaltis ->__set('time_penaltis_4', $_POST['time_penaltis_4']);
    $cadastroPenaltis ->__set('time_penaltis_5', $_POST['time_penaltis_5']);
    $cadastroPenaltis ->__set('time_penaltis_6', $_POST['time_penaltis_6']);
    $cadastroPenaltis ->__set('time_penaltis_7', $_POST['time_penaltis_7']);
    $cadastroPenaltis ->__set('time_penaltis_8', $_POST['time_penaltis_8']);
    $cadastroPenaltis ->__set('time_penaltis_9', $_POST['time_penaltis_9']);
    $cadastroPenaltis ->__set('time_penaltis_10', $_POST['time_penaltis_10']);



    $cadastroPenaltis ->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $penaltisService = new PenaltisService($conexao, $cadastroPenaltis);
    $penaltisService->inserir();
    
    // echo "<pre>";
    // print_r($penaltisService);
    
    header('Location:../cadastro_penaltis.php?cadastrado=1');

} else if($acao == 'exibir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/times.service.php');

    $cadastroTimes = new CadastroTimes();
    $conexao = new Conexao();

    $timesService = new TimesService($conexao, $cadastroTimes);
    $times = $timesService->listar();

} else if($acao == 'atualizar'){
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/membros.service.php');


    $cadastroMembros = new CadastroMembros();

    $cadastroMembros->__set('id', $_POST['id']);
    $cadastroMembros ->__set('nome', $_POST['nome']);
    $cadastroMembros ->__set('apelido', $_POST['apelido']);
    $cadastroMembros ->__set('posicao', $_POST['posicao']);
    $cadastroMembros ->__set('chute', $_POST['chute']);
    $cadastroMembros ->__set('altura', $_POST['altura']);
    $cadastroMembros ->__set('peso', $_POST['peso']);
    $cadastroMembros ->__set('data_nascimento', $_POST['data_nascimento']);
    $cadastroMembros ->__set('endereco', $_POST['endereco']);
    $cadastroMembros ->__set('bairro', $_POST['bairro']);
    $cadastroMembros ->__set('cep', $_POST['cep']);
    $cadastroMembros ->__set('natural_de', $_POST['natural_de']);
    $cadastroMembros ->__set('rg', $_POST['rg']);
    $cadastroMembros ->__set('cpf', $_POST['cpf']);
    $cadastroMembros ->__set('email', $_POST['email']);
    $cadastroMembros ->__set('telefone', $_POST['telefone']);
    $cadastroMembros ->__set('celular', $_POST['celular']);
    $cadastroMembros ->__set('status', $_POST['status']);
    $cadastroMembros ->__set('data_afastamento', $_POST['data_afastamento']);
    $cadastroMembros ->__set('data_cadastro', $_POST['data_cadastro']);
    $cadastroMembros ->__set('padrinho', $_POST['padrinho']);
    $cadastroMembros ->__set('time_que_torce', $_POST['time_que_torce']);
    $cadastroMembros ->__set('data_entrada', $_POST['data_entrada']);
    $cadastroMembros ->__set('data_saida', $_POST['data_saida']);
    $cadastroMembros->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $membrosService = new MembrosService($conexao, $cadastroMembros);
    $membrosService->atualizar();

    header('Location:../cadastro_membros.php?atualizado=1');

} else if($acao == 'excluir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/membros.service.php');

    $cadastroMembros = new CadastroMembros();
    $cadastroMembros->__set('id',$_GET['id']);

    $conexao = new Conexao();
    $membrosService = new membrosService($conexao, $cadastroMembros);
    $membrosService->excluir();

    header('Location:cadastro_times.php?excluido=1');

}
 ?>