<?
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1); 
    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;
    // echo "cadastro gols controller: carregou ok";

if($acao == 'cadastrar'){    
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/gols.service.php');


    $cadastroGols = new CadastroGols();

    $cadastroGols ->__set('sumula_referencia', $_POST['sumula_referencia']);

    $cadastroGols ->__set('jogador_1', $_POST['jogador_1']);
    $cadastroGols ->__set('jogador_2', $_POST['jogador_2']);
    $cadastroGols ->__set('jogador_3', $_POST['jogador_3']);
    $cadastroGols ->__set('jogador_4', $_POST['jogador_4']);
    $cadastroGols ->__set('jogador_5', $_POST['jogador_5']);
    $cadastroGols ->__set('jogador_6', $_POST['jogador_6']);
    $cadastroGols ->__set('jogador_7', $_POST['jogador_7']);
    $cadastroGols ->__set('jogador_8', $_POST['jogador_8']);
    $cadastroGols ->__set('jogador_9', $_POST['jogador_9']);
    $cadastroGols ->__set('jogador_10', $_POST['jogador_10']);

    
    $cadastroGols ->__set('gols_primeiro_tempo_1', $_POST['gols_primeiro_tempo_1']);
    $cadastroGols ->__set('gols_primeiro_tempo_2', $_POST['gols_primeiro_tempo_2']);
    $cadastroGols ->__set('gols_primeiro_tempo_3', $_POST['gols_primeiro_tempo_3']);
    $cadastroGols ->__set('gols_primeiro_tempo_4', $_POST['gols_primeiro_tempo_4']);
    $cadastroGols ->__set('gols_primeiro_tempo_5', $_POST['gols_primeiro_tempo_5']);
    $cadastroGols ->__set('gols_primeiro_tempo_6', $_POST['gols_primeiro_tempo_6']);
    $cadastroGols ->__set('gols_primeiro_tempo_7', $_POST['gols_primeiro_tempo_7']);
    $cadastroGols ->__set('gols_primeiro_tempo_8', $_POST['gols_primeiro_tempo_8']);
    $cadastroGols ->__set('gols_primeiro_tempo_9', $_POST['gols_primeiro_tempo_9']);
    $cadastroGols ->__set('gols_primeiro_tempo_10', $_POST['gols_primeiro_tempo_10']);

    
    $cadastroGols ->__set('gols_segundo_tempo_1', $_POST['gols_segundo_tempo_1']);
    $cadastroGols ->__set('gols_segundo_tempo_2', $_POST['gols_segundo_tempo_2']);
    $cadastroGols ->__set('gols_segundo_tempo_3', $_POST['gols_segundo_tempo_3']);
    $cadastroGols ->__set('gols_segundo_tempo_4', $_POST['gols_segundo_tempo_4']);
    $cadastroGols ->__set('gols_segundo_tempo_5', $_POST['gols_segundo_tempo_5']);
    $cadastroGols ->__set('gols_segundo_tempo_6', $_POST['gols_segundo_tempo_6']);
    $cadastroGols ->__set('gols_segundo_tempo_7', $_POST['gols_segundo_tempo_7']);
    $cadastroGols ->__set('gols_segundo_tempo_8', $_POST['gols_segundo_tempo_8']);
    $cadastroGols ->__set('gols_segundo_tempo_9', $_POST['gols_segundo_tempo_9']);
    $cadastroGols ->__set('gols_segundo_tempo_10', $_POST['gols_segundo_tempo_10']);

    
    $cadastroGols ->__set('time_gols_1', $_POST['time_gols_1']);
    $cadastroGols ->__set('time_gols_2', $_POST['time_gols_2']);
    $cadastroGols ->__set('time_gols_3', $_POST['time_gols_3']);
    $cadastroGols ->__set('time_gols_4', $_POST['time_gols_4']);
    $cadastroGols ->__set('time_gols_5', $_POST['time_gols_5']);
    $cadastroGols ->__set('time_gols_6', $_POST['time_gols_6']);
    $cadastroGols ->__set('time_gols_7', $_POST['time_gols_7']);
    $cadastroGols ->__set('time_gols_8', $_POST['time_gols_8']);
    $cadastroGols ->__set('time_gols_9', $_POST['time_gols_9']);
    $cadastroGols ->__set('time_gols_10', $_POST['time_gols_10']);



    $cadastroGols ->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $golsService = new GolsService($conexao, $cadastroGols);
    $golsService->inserir();
    
    // echo "<pre>";
    // print_r($golsService);
    
    header('Location:../cadastro_gols.php?cadastrado=1');

} else if($acao == 'exibir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/times.service.php');

    $cadastroTimes = new CadastroTimes();
    $conexao = new Conexao();

    $timesService = new TimesService($conexao, $cadastroTimes);
    $times = $timesService->listar();

} else if($acao == 'atualizar'){
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/membros.service.php');


    $cadastroMembros = new CadastroMembros();

    $cadastroMembros->__set('id', $_POST['id']);
    $cadastroMembros ->__set('nome', $_POST['nome']);
    $cadastroMembros ->__set('apelido', $_POST['apelido']);
    $cadastroMembros ->__set('posicao', $_POST['posicao']);
    $cadastroMembros ->__set('chute', $_POST['chute']);
    $cadastroMembros ->__set('altura', $_POST['altura']);
    $cadastroMembros ->__set('peso', $_POST['peso']);
    $cadastroMembros ->__set('data_nascimento', $_POST['data_nascimento']);
    $cadastroMembros ->__set('endereco', $_POST['endereco']);
    $cadastroMembros ->__set('bairro', $_POST['bairro']);
    $cadastroMembros ->__set('cep', $_POST['cep']);
    $cadastroMembros ->__set('natural_de', $_POST['natural_de']);
    $cadastroMembros ->__set('rg', $_POST['rg']);
    $cadastroMembros ->__set('cpf', $_POST['cpf']);
    $cadastroMembros ->__set('email', $_POST['email']);
    $cadastroMembros ->__set('telefone', $_POST['telefone']);
    $cadastroMembros ->__set('celular', $_POST['celular']);
    $cadastroMembros ->__set('status', $_POST['status']);
    $cadastroMembros ->__set('data_afastamento', $_POST['data_afastamento']);
    $cadastroMembros ->__set('data_cadastro', $_POST['data_cadastro']);
    $cadastroMembros ->__set('padrinho', $_POST['padrinho']);
    $cadastroMembros ->__set('time_que_torce', $_POST['time_que_torce']);
    $cadastroMembros ->__set('data_entrada', $_POST['data_entrada']);
    $cadastroMembros ->__set('data_saida', $_POST['data_saida']);
    $cadastroMembros->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $membrosService = new MembrosService($conexao, $cadastroMembros);
    $membrosService->atualizar();

    header('Location:../cadastro_membros.php?atualizado=1');

} else if($acao == 'excluir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/membros.service.php');

    $cadastroMembros = new CadastroMembros();
    $cadastroMembros->__set('id',$_GET['id']);

    $conexao = new Conexao();
    $membrosService = new membrosService($conexao, $cadastroMembros);
    $membrosService->excluir();

    header('Location:cadastro_times.php?excluido=1');

}
 ?>