<?
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1); 
    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;
    // echo "cadastro observacoes controller: carregou ok";

if($acao == 'cadastrar'){    
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/observacoes.service.php');


    $cadastroObservacoes = new CadastroObservacoes();

    $cadastroObservacoes ->__set('sumula_referencia', $_POST['sumula_referencia']);

    $cadastroObservacoes ->__set('presencas', $_POST['presencas']);
    $cadastroObservacoes ->__set('punicoes', $_POST['punicoes']);
    $cadastroObservacoes ->__set('destaques', $_POST['destaques']);
    $cadastroObservacoes ->__set('observacoes', $_POST['observacoes']);

    $cadastroObservacoes ->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $observacoesService = new ObservacoesService($conexao, $cadastroObservacoes);
    $observacoesService->inserir();
    
    // echo "<pre>";
    // print_r($observacoesService);
    
    header('Location:../cadastro_observacoes.php?cadastrado=1');

} else if($acao == 'exibir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/times.service.php');

    $cadastroTimes = new CadastroTimes();
    $conexao = new Conexao();

    $timesService = new TimesService($conexao, $cadastroTimes);
    $times = $timesService->listar();

} else if($acao == 'atualizar'){
    require_once('../../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../../classes_sistema/sistema.model.php');
    require_once('../../../../classes_sistema/membros.service.php');


    $cadastroMembros = new CadastroMembros();

    $cadastroMembros->__set('id', $_POST['id']);
    $cadastroMembros ->__set('nome', $_POST['nome']);
    $cadastroMembros ->__set('apelido', $_POST['apelido']);
    $cadastroMembros ->__set('posicao', $_POST['posicao']);
    $cadastroMembros ->__set('chute', $_POST['chute']);
    $cadastroMembros ->__set('altura', $_POST['altura']);
    $cadastroMembros ->__set('peso', $_POST['peso']);
    $cadastroMembros ->__set('data_nascimento', $_POST['data_nascimento']);
    $cadastroMembros ->__set('endereco', $_POST['endereco']);
    $cadastroMembros ->__set('bairro', $_POST['bairro']);
    $cadastroMembros ->__set('cep', $_POST['cep']);
    $cadastroMembros ->__set('natural_de', $_POST['natural_de']);
    $cadastroMembros ->__set('rg', $_POST['rg']);
    $cadastroMembros ->__set('cpf', $_POST['cpf']);
    $cadastroMembros ->__set('email', $_POST['email']);
    $cadastroMembros ->__set('telefone', $_POST['telefone']);
    $cadastroMembros ->__set('celular', $_POST['celular']);
    $cadastroMembros ->__set('status', $_POST['status']);
    $cadastroMembros ->__set('data_afastamento', $_POST['data_afastamento']);
    $cadastroMembros ->__set('data_cadastro', $_POST['data_cadastro']);
    $cadastroMembros ->__set('padrinho', $_POST['padrinho']);
    $cadastroMembros ->__set('time_que_torce', $_POST['time_que_torce']);
    $cadastroMembros ->__set('data_entrada', $_POST['data_entrada']);
    $cadastroMembros ->__set('data_saida', $_POST['data_saida']);
    $cadastroMembros->__set('user_id', $_SESSION['user_id']);

    $conexao = new Conexao();

    $membrosService = new MembrosService($conexao, $cadastroMembros);
    $membrosService->atualizar();

    header('Location:../cadastro_membros.php?atualizado=1');

} else if($acao == 'excluir'){
    require_once('../../../classes_sistema/controllers/conexao_controller.php');
    require_once('../../../classes_sistema/sistema.model.php');
    require_once('../../../classes_sistema/membros.service.php');

    $cadastroMembros = new CadastroMembros();
    $cadastroMembros->__set('id',$_GET['id']);

    $conexao = new Conexao();
    $membrosService = new membrosService($conexao, $cadastroMembros);
    $membrosService->excluir();

    header('Location:cadastro_times.php?excluido=1');

}
 ?>