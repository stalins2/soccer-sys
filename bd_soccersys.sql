-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 05/11/2019 às 11:42
-- Versão do servidor: 5.7.23-23
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `fohfaoi_system`
--
CREATE DATABASE IF NOT EXISTS `fohfaoi_system` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `fohfaoi_system`;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Acessos`
--

CREATE TABLE `Acessos` (
  `id` int(4) NOT NULL,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `Acessos`
--

INSERT INTO `Acessos` (`id`, `nome`, `email`, `senha`, `nivel`) VALUES
(1, 'Admin', 'admin@admin', 'stdmin123123!', 1),
(2, 'stalin', 'stalin@stalin.com', 'stdmin123123!', 1),
(3, 'Administrador de Cadastros', 'admin_cadastros@fohfaoi.com', 'fohfaoi123!', 1),
(4, 'Val', 'valdeiralves42@gmail.com', '12345dede', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Arbitragem_1`
--

CREATE TABLE `Arbitragem_1` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `arbitro` int(4) NOT NULL DEFAULT '0',
  `auxiliar_1` int(4) NOT NULL DEFAULT '0',
  `auxiliar_2` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Arbitragem_2`
--

CREATE TABLE `Arbitragem_2` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `arbitro` int(4) NOT NULL DEFAULT '0',
  `auxiliar_1` int(4) NOT NULL DEFAULT '0',
  `auxiliar_2` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `cadastro`
--

CREATE TABLE `cadastro` (
  `id_cad` int(5) UNSIGNED ZEROFILL NOT NULL,
  `nome` char(120) NOT NULL,
  `email` char(120) DEFAULT NULL,
  `dia` int(2) NOT NULL,
  `mes` int(2) NOT NULL,
  `ano` int(4) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `apelido` varchar(45) DEFAULT NULL,
  `posicao` varchar(45) NOT NULL,
  `altura` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `peso` varchar(8) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `local` varchar(45) NOT NULL,
  `civil` varchar(12) NOT NULL,
  `chute` varchar(12) NOT NULL,
  `totalgols` int(11) DEFAULT NULL,
  `endereco` varchar(45) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `rg` varchar(20) NOT NULL,
  `cpf` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `cadastro`
--

INSERT INTO `cadastro` (`id_cad`, `nome`, `email`, `dia`, `mes`, `ano`, `status`, `apelido`, `posicao`, `altura`, `peso`, `local`, `civil`, `chute`, `totalgols`, `endereco`, `bairro`, `telefone`, `celular`, `rg`, `cpf`) VALUES
(00001, 'Adilson de Lima', NULL, 18, 3, 1962, 'Ativo', 'Tiozinho', 'Meia', '1.70m', '62kg', 'Teixeiras - MG', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00002, 'Juraci Fernandes', NULL, 10, 3, 1970, 'Desligado', 'Jura', 'Lateral Direito', '1.72m', '95kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Celestino Joaquim da Costa, 212', 'Vila Santa Lúcia', '(11) 5833-3084', '', '23.207.094-5', '116.810.298-71'),
(00003, 'Ailton Alves Magalhães', 'ailton5000@ig.com.br', 10, 1, 1965, 'In Memory', 'Fubá', 'Goleiro', '1.78m', '74kg', 'São Pedro dos Ferros - MG', 'Casado', 'Destro', NULL, 'Rua Vale Jacui, 45', 'Jardim Marquesa', '(11) 5831-0110', '', '', ''),
(00004, 'Luciano Brito dos Santos', 'luciano@lewlara.com.br', 16, 10, 1974, 'Ativo', 'Lobão', 'Lateral Esquerdo', '1.70m', '65kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, 'Rua Eng. Antonio Alves Braga, 375', 'Riviera Paulista', '(11) 5517-7290', '', '21.962.771', ''),
(00005, 'Alex dos Santos', NULL, 8, 5, 1974, 'Ativo', 'Alex', 'Lateral Esquerdo', '1.60m', '64kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, '', '', '(11) 5831-8280', '', '23.759.586-2', '157.993.518-43'),
(00006, 'Marcos Rocha', NULL, 10, 11, 1969, 'Ativo', 'Marcão', 'Volante', '1.80m', '70kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira, 183', 'Jd. Copacabana', '(11) 5831-4742', '', '18.891.438', '136.185.148-16'),
(00007, 'Marcos Paranhos', NULL, 28, 2, 1966, 'Afastado', 'Aranha', 'Lateral Direito', '1.70m', '70kg', 'Guararapes - SP', 'Casado', 'Destro', NULL, 'Av. M\'Boi Mirim, 1832 - Apto. 81, Bloco A', 'Jardim das Flores', '(11) 5897-5070', '', '18.892.610', '183.952.348-30'),
(00008, 'Marcio Moreira', NULL, 8, 10, 1970, 'Desligado', 'Marcião', 'Lateral Direito', '1.69m', '69kg', 'São Pedro dos Ferros - MG', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00009, 'Benedito Santana', NULL, 16, 3, 1958, 'Ativo', 'Bahia', 'Zagueiro', '1.70m', '72kg', 'Governador Mangabeira - BA', 'Casado', 'Destro', NULL, 'Rua Marcelino Coelho, 52', 'Jardim Ângela', '0', '(11) 99272-4567', '', ''),
(00010, 'Edmilson Maciel', NULL, 30, 8, 1965, 'Desligado', 'Nambuco', 'Zagueiro', '1.78m', '73kg', 'Pesqueira - PE', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira, 94', 'Jd. Copacabana', '(11) 5831-2895', '', '3.181.856', '488.897.504-30'),
(00011, 'Adilson Magalhães', 'adilsonbaratamagalhaes@hotmail.com', 1, 6, 1963, 'Afastado', 'Barata', 'Lateral Direito', '1.84m', '88kg', 'São Pedro dos Ferros - MG', 'Casado', 'Destro', NULL, '', '', '(19) 3306-7557', '', '', ''),
(00012, 'Nerinaldo dos Santos', 'neri.zamolh@hotmail.com', 23, 2, 1973, 'Lesionado', 'Neire', 'Atacante', '1.64m', '69kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira, 85', 'Jd. Copacabana', '(11) 5831-0487', '', '25.484.405-4', '189.775.938-69'),
(00013, 'Rogério Fernandes', NULL, 30, 4, 1971, 'Lesionado', 'Beiçola', 'Atacante', '1.65m', '85kg', 'São Paulo - SP', 'Casado', 'Ambidestro', NULL, 'Rua Tanjoré, 62', 'Jardim Alfredo', '(11) 5892-7676', '', '12.345.678-9', ''),
(00014, 'Nereu Jesus das Mercês', NULL, 11, 10, 1971, 'Lesionado', 'Nereu', 'Lateral Direito', '1.72m', '75kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Cardoso Jarros, 9', 'Jardim da Pedreira', '(11) 5615-0617', '(11) 8290-9663', '22.664.954-4', ''),
(00015, 'Charles Araujo', NULL, 10, 11, 1977, 'Desligado', 'Brown', 'Meia', '1.77m', '75kg', 'Santos - SP', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00016, 'Orlando Barbosa', NULL, 15, 3, 1978, 'Desligado', 'Orlando', 'Volante', '1.79m', '65kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira, 66', 'Jd. Copacabana', '(11) 5831-2825', '', '27.934.326-7', ''),
(00017, 'Francisco José Filho', NULL, 13, 7, 1966, 'Lesionado', 'Chiquinho', 'Volante', '1.72m', '70kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Hafiz Abi Chedid, 18E', 'Vila Bom Jardim', '(11) 5834-1038', '', '15.758.854-3', '083.941.118-90'),
(00018, 'Luiz Carlos Quebra', NULL, 8, 7, 1979, 'Desligado', 'Passarinho', 'Meia', '1.78m', '70kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, '', '', '0', '', '', ''),
(00019, 'Daniel Bastos', NULL, 25, 3, 1956, 'Lesionado', 'Daniel Ortega', 'Volante', '1.75m', '70kg', 'Palmares - PE', 'Casado', 'Destro', NULL, 'Rua Jaraguari, 50', 'Jd. Copacabana', '(11) 5831-1702', '', '8.323.883', '756.529.278-87'),
(00020, 'Ronaldo Santos da Rocha', NULL, 28, 8, 1972, 'Desligado', 'Ronaldo', 'Lateral Esquerdo', '1.74m', '95kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, 'Rua José Gomes Ferreira, 183', 'Jd. Copacabana', '(11) 5831-4745', '', '22.336.520-8', '166.239.048-32'),
(00021, 'Anderson Magalhães', NULL, 30, 7, 1976, 'Desligado', 'Deco', 'Meia', '1.83m', '65kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00022, 'José Roque Santos', NULL, 9, 11, 1972, 'Desligado', 'Roque', 'Zagueiro', '1.80m', '83kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00023, 'Francisco da Silva', NULL, 7, 9, 1970, 'Desligado', 'Di Caprio', 'Goleiro', '1.67m', '70kg', 'Carinhanha - BA', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00024, 'Sebastião Robson Paulo Flores', NULL, 20, 1, 1963, 'Ativo', 'Rubicão', 'Volante', '1.80m', '72kg', 'Ibepeba - BA', 'Casado', 'Destro', NULL, 'Rua Bento Rodrigues, 634', 'Jardim Tupi', '(11) 5833-1404', '', '16.486.120', '051.133.328-52'),
(00025, 'Edson de Oliveira', NULL, 16, 8, 1972, 'Ativo', 'Edsinho', 'Atacante', '1.74m', '70kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Antônio Dionízio de Souza, 243', 'Jd. Copacabana', '0', '(11) 7462-2988', '21.484.040-2', '111.421.328-46'),
(00026, 'Silvano Bezerra da Silva', NULL, 15, 5, 1976, 'Afastado', 'Silvano', 'Zagueiro', '1.80m', '82kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, 'Rua Itapiocaba, 70', 'Capão Redondo', '0', '(11) 99401-0404', '', ''),
(00027, 'Gilmar Fernandes', NULL, 15, 10, 1974, 'Afastado', 'Gilmar', 'Meia', '1.76m', '85kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua dos Imigrantes Sírios Libaneses, 202', '', '(11) 4165-4575', '', '23.089.386-5', '189.774.978-33'),
(00028, 'Wagner Lima Domingues', NULL, 2, 7, 1975, 'Ativo', 'Wagner', 'Zagueiro', '1.75m', '80kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Antônio Dionízio de Souza, ', 'Jd. Copacabana', '(11) 5831-2317', '', '23.130.020-7', '165.677.368-60'),
(00029, 'Hélio Ferreira', NULL, 16, 9, 1954, 'Desligado', 'Hélio', 'Goleiro', '1.78m', '93kg', 'Carinhanha - BA', 'Casado', 'Destro', NULL, 'Rua 2A, 67B', '', '0', '(11) 9176-5300', '', ''),
(00030, 'Wilson Magalhães', 'venture2006@terra.com.br', 16, 12, 1967, 'Ativo', 'Wilson', 'Volante', '1.80m', '110kg', 'São Pedro dos Ferros - MG', 'Casado', 'Ambidestro', NULL, 'Rua José Gomes Ferreira, 61', 'Jd. Copacabana', '(11) 5833-7823', '(11) 3739-0052', '19.418.414-6', '085.556.388-52'),
(00031, 'Leandro da Silva', NULL, 16, 9, 1982, 'Ativo', 'Jaburu', 'Atacante', '1.62m', '70kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Leonardo Luiz Bahia', 'Parque Cerejeiras', '(11) 5839-0485', '(11) 99645-3641', '43.643.144-0', '228.536.158-04'),
(00032, 'Darcy Moreira', NULL, 10, 12, 1960, 'Ativo', 'Zizico', 'Zagueiro', '1.78m', '79kg', 'São Pedro dos Ferros - MG', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira, 270', 'Jd. Copacabana', '(11) 5831-5554', '', '37.279.928-0', '02.201.418-20'),
(00033, 'Joelson Barbosa', NULL, 1, 12, 1982, 'Desligado', 'Joelson', 'Zagueiro', '1.75m', '95kg', 'São Paulo - SP', 'Divorciado', 'Destro', NULL, 'Rua José Gomes Ferreira, 66', 'Jd. Copacabana', '(11) 5831-2825', '', '27.934.327-9', ''),
(00034, 'Ademir Marques', 'ademirol.2006@hotmail.com', 10, 3, 1970, 'Ativo', 'Palmeirense', 'Meia', '1.71m', '70kg', 'Curitiba - PR', 'Casado', 'Canhoto', NULL, 'Rua Nóbrega de Siqueira', 'Vila Bom Jardim', '0', '', '18.891.698-2', '124.802.288-27'),
(00035, 'Adriano Alves Magalhães', NULL, 15, 2, 1976, 'Ativo', 'Adriano', 'Atacante', '1.79m', '66kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Colonia Nova, 123', 'Jardim Ângela', '(11) 5831-7369', '', '23.760.102-3', '191.833.388-29'),
(00036, 'Antônio Cassio Rezende', NULL, 4, 11, 1964, 'Lesionado', 'Capacete', 'Meia', '1.78m', '82kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, 'Rua Francisca Nunes Rodrigues,79', 'Jd. Copacabana', '(11) 5831-5679', '', '19.455.945', ''),
(00037, 'Claudio P. Costa', NULL, 19, 10, 1973, 'Desligado', 'Cau', 'Volante', '1.70m', '80kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00038, 'Fabiano Bastos Doneida', 'binhodocopa@hotmail.com', 5, 1, 1982, 'Ativo', 'Fabinho', 'Goleiro', '1.84m', '116kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira, 108', 'Jd. Copacabana', '(11) 5835-2008', '', '33.019.365-X', '220.129.458-56'),
(00039, 'Manoelito Sena Junior', NULL, 24, 10, 1977, 'Afastado', 'Manoelito', 'Meia', '1.85m', '80kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, 'Rua Pedrina Maria da Silva Valente', 'Parque Munhoz', '(11) 5819-3994', '(11) 99935-2820', '26.312.447-7', '189.682.598-28'),
(00040, 'Marcos de Menezes', NULL, 4, 8, 1971, 'Desligado', 'Mezenga', 'Lateral Esquerdo', '1.82m', '79kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Antônio Dionízio de Souza, 124', 'Jd. Copacabana', '(11) 3452-2620', '(11) 7808-1975', '19.737.147-4', '142.286.058-26'),
(00041, 'Rildo Alves Moreira', 'bar.rildo@hotmail.com.', 21, 10, 1972, 'Desligado', 'Rildo Moreira', 'Atacante', '1.65m', '80kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Dr. Castro Laboreiro, 30', 'Jd. Copacabana', '(11) 5831-4689', '', '23.890.659-0', '136.706.818-50'),
(00042, 'Silio Guedes Lauton', NULL, 28, 4, 1971, 'Ativo', 'Gato', 'Volante', '1.70m', '70kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Afonso Rui, 205', 'Santa Lúcia', '(11) 5831-6353', '(11) 9746-4678', '20.523.054', '142.291.508-57'),
(00043, 'Fábio Luiz Cantuária', NULL, 28, 5, 1972, 'Desligado', 'Tubaina', 'Zagueiro', '1.75m', '100kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua João Fioravante, 7', 'Jd. Copacabana', '(11) 5833-1819', '', '23.683.783-7', '164.123.928-07'),
(00044, 'Pedro José dos Santos Junior', NULL, 15, 10, 1979, 'Ativo', 'Pedro Jr.', 'Atacante', '1.70m', '60kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Av. Celso dos Santos, 1036', 'Vila Constança', '(11) 5671-5108', '(11) 96702-7656', '28.780.374-0', '265.466.818-54'),
(00045, 'Rodolfo', NULL, 10, 10, 1972, 'Desligado', 'Rodolfo', 'Lateral Direito', '1.73m', '69kg', 'Recife - PE', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00046, 'Rodrigo', NULL, 10, 10, 1972, 'Desligado', 'Rodrigo', 'Volante', '1.69m', '78kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00047, 'Rondineli', NULL, 10, 10, 1972, 'Desligado', 'Rondineli', 'Volante', '1.80m', '78kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00048, 'Marcio Rogerio Feitosa de Oliveira', 'romarcio-11@hotmail.com', 17, 2, 1977, 'Lesionado', 'Cafú', 'Atacante', '1.68m', '70kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, 'Rua Celestino Joaquim da Costa, ', 'Vila Santa Lúcia', '(11) 5832-7111', '(11) 96701-2047', '26.222.658-3', '248.371.898-14'),
(00049, 'Rildo Pereira de França', NULL, 23, 5, 1973, 'Ativo', 'Rildo França', 'Volante', '1.70m', '65kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Bonifacio Fernandes, 97', 'Santa Lúcia', '(11) 5831-4954', '(11) 96914-4038', '22.976.212-8', '183.048.448-69'),
(00050, 'Edilson Leandro da Silva', NULL, 28, 6, 1981, 'Ativo', 'Romarinho', 'Atacante', '1.55m', '50kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Ribeirão Ponte Baixa,23 A', 'Jd. Ângela', '(11) 5891-0678', '(11) 98180-0140', '33.397.589-3', '220.940.278-64'),
(00051, 'Sergio Dantas Cirqueira', NULL, 2, 6, 1973, 'Ativo', 'Sergio', 'Lateral Direito', '1.77m', '90kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua do Contraponto,10 ', 'Kangohara', '(11) 5834-5453', '(11) 97879-9648', '23.372.909-4', '130.324.918-94'),
(00052, 'Sergio Henrique Oliveira Ribeiro', NULL, 22, 8, 1982, 'Ativo', 'Serginho', 'Atacante', '1.70m', '85kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, 'Rua Celso Magalhães, 100', 'Jardim Tohmas', '(11) 5897-8057', '(11) 97860-6313', '34.456.128-8', '319.397.748-96'),
(00053, 'Daniel Moreira Araujo Junior', 'jsmdespachante@yahoo.com.br', 11, 11, 1971, 'Aitvo', 'Daniel Araujo', 'Goleiro', '1.75m', '90kg', 'Ilheus - BA', 'Divorciado', 'Destro', NULL, 'Rua Gonçalo de Barros, 191', 'Jd. Soraia', '(11) 5875-1996', '', '26.223.433-6', '129.074.678-83'),
(00054, 'Daniel Theodoro', NULL, 10, 10, 1972, 'Ativo', 'Daniel Theodoro', 'Meia', '1.75m', '80kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, 'Rua Celestino Joaquim da Costa, 45', 'Vila Santa Lúcia', '(11) 5833-5863', '', '', ''),
(00055, 'Rubens S. Leite', NULL, 3, 8, 1971, 'Desligado', 'Ninha', 'Meia', '1.69m', '65kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Estrada de Itapecerica, 6395 - Apto 13', 'Parque Fernanda', '(11) 5824-5339', '(11) 99515-6165', '22.738.871-9', '127.115.068-94'),
(00056, 'Emerson Ortega', NULL, 10, 10, 1972, 'Ativo', 'Emerson', 'Zagueiro', '1.78m', '85kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00057, 'Adeildo Nascimento de Souza', NULL, 22, 6, 1955, 'Ativo', 'Marrom', 'Zagueiro', '1.73m', '85kg', 'Curitiba - PR', 'Casado', 'Destro', NULL, 'Rua José Gomesa Ferreira, 101', 'Jardim Copacabana', '(11) 5831-0577', '', '9.063.522', '033.546.568-22'),
(00058, 'Toco', NULL, 10, 10, 1972, 'Ativo', 'Toco', 'Goleiro', '1.74m', '78kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00059, 'Sergio Oliveira Pinto', NULL, 10, 10, 1972, 'Ativo', 'Totó', 'Zagueiro', '1.85m', '85kg', 'Salvador - BA', 'Casado', 'Destro', NULL, 'Rua Antonio Dinizio de souza,245', 'Jd. Copacabana', '0', '(11) 96522-3040', '53.923.378', '973.611.005-20'),
(00060, 'Cesár', NULL, 10, 10, 1972, 'Desligado', 'Cesár', 'Zagueiro', '1.80m', '110kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00061, 'Luiz Carlos Theodoro da Silva', 'theoth_32@hotmail.com', 4, 4, 1973, 'Ativo', 'Luiz Carlos', 'Volante', '1.78m', '72kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Celestino Joaquim da Costa, 115', 'Vila Santa Lúcia', '(11) 5833-5100', '(11) 99789-5764', '21.620.502-5', ''),
(00062, 'Osires Silva', NULL, 19, 5, 1977, 'Ativo', 'Cabañas', 'Atacante', '1.75m', '75kg', 'Divinópolis - MG', 'Casado', 'Destro', NULL, 'Rua Susana Adams, 32', 'Americanópolis', '(11) 98809-0025', '(11) 98710-7889', '37.951.032-7', '392.204.478-60'),
(00063, 'Antônio Coimbra de Souza', NULL, 17, 8, 1964, 'Ativo', 'Coimbra', 'Volante', '1.65m', '70kg', 'Água Branca - SP', 'Casado', 'Destro', NULL, 'Rua Antônio Dionízio de Souza, ', 'Jd. Copacabana', '(11) 5831-4805', '', '37.414.740.1', '116.251.228-88'),
(00064, 'Marisvaldo Pereira', NULL, 3, 1, 1972, 'Desligado', 'Marisvaldo', 'Lateral Direito', '1.70m', '65kg', 'Salvador - BA', 'Casado', 'Destro', NULL, 'Rua Antônio Dionízio de Souza, 124', 'Jd. Copacabana', '(11) 5831-4813', '', '', '52.115.119'),
(00065, 'Washington de Souza Raimundo', NULL, 9, 4, 1979, 'Desligado', 'Washington', 'Volante', '1.78m', '85kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Celestino Joaquim da Costa, ', 'Vila Santa Lúcia', '0', '(11) 99379-7086', '28.843.770-6', '268.233.818-70'),
(00066, 'Marcio', NULL, 10, 10, 1972, 'Desligado', 'Marcio', 'Zagueiro', '1.78m', '80kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00067, 'Junior', NULL, 10, 10, 1972, 'Desligado', 'Juninho', 'Goleiro', '1.78m', '85kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00068, 'Alexander Ketzmann M. Magalhães', 'ketzmann@gmail.com', 14, 2, 1989, 'Ativo', 'Alexander', 'Atacante', '1.72m', '62kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, 'Rua Vale do Jacui, 45', 'Jd. Marquesa', '(11) 5831-0110', '(11) 98651-3258', '44.694.917-6', '355.918.018-03'),
(00069, 'Fagner Rodrigues Magalhães', NULL, 2, 10, 1989, 'Ativo', 'Gansinho', 'Meia', '1.86m', '75kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00070, 'Frances Anderson Dosvaldo', NULL, 8, 8, 1965, 'Ativo', 'Frances', 'Atacante', '1.85m', '95kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '0', '', '', ''),
(00071, 'Arnaldo', NULL, 10, 7, 1969, 'Ativo', 'Arnaldo', 'Zagueiro', '1.80m', '115kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Antônio Dionízio de Souza, 165', 'Jd. Copacabana', '(11) 5834-2978', '', '', ''),
(00072, 'Ângelo Lucas', NULL, 15, 2, 1959, 'Ativo', 'Ângelo', 'Lateral Direito', '1.78m', '80kg', 'Alfenas - MG', 'Casado', 'Destro', NULL, 'Rua Antonio Victor de Oliveira, 107', 'Jd. Copacabana', '(11) 5831-5483', '', '', ''),
(00073, 'Paraguai', NULL, 10, 10, 1972, 'Desligado', 'Paraguai', 'Lateral Direito', '1.78m', '80kg', 'Canhanha - BA', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00074, 'Angelo Lucas Viana Junior', NULL, 10, 10, 1972, 'Ativo', 'Lucas', 'Zagueiro', '1.78m', '75kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Antonio Victor de Oliveira, 107', 'Jd. Copacabana', '(11) 5831-5483', '', '', ''),
(00075, 'Emerson Pires dos Santos', NULL, 19, 3, 1988, 'Ativo', 'Neguinho', 'Meia', '1.78m', '78kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00076, 'Piabá', NULL, 10, 10, 1972, 'Ativo', 'Piabá', 'Volante', '1.80m', '75kg', 'Canhanha - BA', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00077, 'Jó', NULL, 10, 10, 1972, 'Ativo', 'Jó', 'Meia', '1.75m', '73kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00078, 'William Vasconcelos Rodrigues', NULL, 29, 5, 1976, 'Desligado', 'William', 'Meia', '1.64m', '80kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Afonso Rui, 73', 'Vila Santa Lúcia', '(11) 5831-9109', '(11) 7870-8010', '25.620.230-8', '246.697.498-38'),
(00079, 'Sergio Barbosa Ramos da Silva', NULL, 27, 5, 1980, 'Ativo', 'Sergião', 'Atacante', '1.65m', '80kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua José Gomes Ferreira,95', 'Jd. Copacabana', '0', '(11) 98654-4446', '', ''),
(00080, 'Anderson', NULL, 10, 10, 1972, 'Ativo', 'Nikimba', 'Meia', '1.70m', '70kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '0', '', '', ''),
(00081, 'Clóvis Benedicto T. dos Santos Filho', NULL, 25, 2, 1972, 'Ativo', 'Cobrinha', 'Lateral Esquerdo', '1.72m', '67kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, 'Rua Antônio Victor de Oliveira, 268', 'Jd. Copacabana', '(11) 5831-9844', '(11) 98833-0250', '21.484.012-8', '135.336.078-44'),
(00082, 'Aderaldo Binha', NULL, 10, 10, 1972, 'Ativo', 'Binha', 'Meia', '1.75m', '76kg', 'Canhanha - BA', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00083, 'Robson Ruisa Moreira', NULL, 26, 6, 1976, 'Ativo', 'Robson', 'Meia', '1.75m', '75kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, '', '', '', '', '', ''),
(00084, 'Eduardo França Santos', NULL, 10, 10, 1972, 'Ativo', 'Eduardo', 'Meia', '1.75m', '75kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00085, 'Everton Moreira', NULL, 28, 10, 1984, 'Ativo', 'Everton', 'Meia', '1.75m', '72kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00086, 'Fernando Ribeiro Silva', NULL, 13, 7, 1985, 'Ativo', 'Fernando', 'Meia', '1.75m', '78kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00087, 'Edmilson Ortega', NULL, 20, 10, 1980, 'Ativo', 'Edmilson', 'Meia', '1.78m', '75kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00088, 'Edson Francisco dos Santos', NULL, 8, 10, 1982, 'Ativo', 'Edinho', 'Meia', '1.72m', '64kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00089, 'Victor', NULL, 16, 9, 1991, 'Ativo', 'Victor', 'Lateral Direito', '1.62m', '59kg', 'Chile', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00090, 'Clodoaldo', NULL, 19, 9, 1976, 'Ativo', '13', 'Atacante', '1.70m', '73kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00091, 'Daniel El Niño', NULL, 10, 5, 1995, 'Ativo', 'Daniel', 'Lateral Esquerdo', '1.49m', '56kg', 'Chile', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00092, 'Javier', NULL, 3, 12, 1990, 'Ativo', 'Javier', 'Meia', '1.63m', '59kg', 'Chile', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00093, 'Jorge', NULL, 10, 10, 2000, 'Ativo', 'Jorge', 'Zagueiro', '1.75m', '78kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00094, 'Kiko', NULL, 10, 10, 2000, 'Ativo', 'Kiko', 'Volante', '1.76m', '76kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00095, 'Lucas', NULL, 10, 10, 2000, 'Ativo', 'Lucas', 'Lateral Direito', '1.72m', '70kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00096, 'Nenê', NULL, 10, 10, 2000, 'Ativo', 'Nenê', 'Atacante', '1.77m', '100kg', 'Bahia', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00097, 'Anderson dos Santos', NULL, 26, 12, 1980, 'Ativo', 'Anderson', 'Lateral Esquerdo', '1.65m', '63kg', 'São Paulo - SP', 'Casado', 'Canhoto', NULL, '', '', '', '', '', ''),
(00098, 'Carlos  Enterpa', NULL, 10, 7, 1979, 'Ativo', 'Carlos', 'Atacante', '1.72m', '63kg', 'Bahia', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00099, 'Edmar Alves', NULL, 11, 11, 1992, 'Ativo', 'Edmar', 'Lateral Direito', '1.60m', '70kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00100, 'José Mozaniel Santana', NULL, 1, 6, 1987, 'Ativo', 'Zé', 'Lateral Direito', '1.70m', '75kg', 'Bahia', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00101, 'Leonardo Menezes de Souza', NULL, 28, 7, 1998, 'Ativo', 'Leonardo', 'Atacante', '1.65m', '63kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00102, 'Marcio Vinícius Souza de oliveira', NULL, 9, 6, 1998, 'Desligado', 'Marcio', 'Atacante', '1.65m', '63kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00103, 'Alex Souza França', NULL, 24, 9, 1979, 'Ativo', 'Alex', 'Zagueiro', '1.65m', '78kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00104, 'Richard', NULL, 28, 7, 1987, 'Desligado', 'Richard', 'Meia Atacante', '1.60m', '63kg', 'Bolivia', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00105, 'Pedro Maurício Medeiros Andrade', NULL, 28, 7, 1998, 'Desligado', 'Pedro', 'Atacante', '1.65m', '63kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00106, 'Andre Vilas Boas', NULL, 26, 12, 1995, 'Desligado', 'Andre', 'Atacante', '1.74m', '120kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00107, 'Jeferson Antonio S. Rezende', NULL, 19, 11, 1991, 'Ativo', 'Oreia', 'Zagueiro', '1.78m', '75kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00108, 'Ivan Aparecido N. Jesus', NULL, 19, 5, 1982, 'Ativo', 'Ivan', 'Volante', '1.75m', '72kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00109, 'Vitor Arruda Fernandes', NULL, 22, 6, 1998, 'Ativo', 'Alemão', 'Lateral', '1.70m', '64kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00110, 'Gilberlanio de Castro Dantas', NULL, 28, 1, 1991, 'Ativo', 'Gilberlanio', 'Volante', '1.59m', '62kg', 'Ceara', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00111, 'Alan de Oliveira Ferreira', NULL, 15, 12, 1994, 'Ativo', 'Alan', 'Goleiro', '1.74m', '73kg', 'Ceara', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00112, 'Luiz Régio da Silva', NULL, 22, 12, 1992, 'Ativo', 'Régio', 'Atacante', '1.65m', '68kg', 'Ceara', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00113, 'Valdeir Alves de Lima', NULL, 8, 8, 1997, 'Ativo', 'Val', 'Atacante', '1.74m', '72kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00114, 'Jonatan', NULL, 10, 10, 1990, 'Ativo', 'Jonatan', 'Volante', '1.74m', '70kg', 'Bahia', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00115, 'Jadson Araujo Almeida', NULL, 13, 1, 1993, 'Ativo', 'Jadson', 'Lateral', '1.75m', '78kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00116, 'Thiago da Conceição Oliveira', NULL, 4, 11, 1989, 'Ativo', 'Thiago', 'Atacante', '1.60m', '63kg', 'São Paulo - SP', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00117, 'Gustavo Jose da Silva Santos', NULL, 6, 3, 2000, 'Ativo', 'Gustavo', 'Atacante', '1.71m', '63kg', 'São Paulo - SP', 'Solteiro', 'Canhoto', NULL, '', '', '', '', '', ''),
(00118, 'Francisco Gilberto Lopes', NULL, 13, 8, 1979, 'Ativo', 'Neno', 'Volante', '1.75m', '73kg', 'Ceara', 'Casado', 'Destro', NULL, '', '', '', '', '', ''),
(00119, 'Ricardo Tomaz de Lima', NULL, 15, 2, 1993, 'Ativo', 'Ricardo', 'Volante', '1.75m', '78kg', 'Ceara', 'Solteiro', 'Destro', NULL, '', '', '', '', '', ''),
(00120, 'Marco Antonio Mantovani de Araujo', NULL, 16, 11, 2000, 'Ativo', 'Marquinhos', 'Goleiro', '1.70m', '77kg', 'São Paulo - SP', 'Solteiro', 'Destro', NULL, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Cadastros`
--

CREATE TABLE `Cadastros` (
  `id` int(4) NOT NULL,
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `apelido` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `posicao` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `chute` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `altura` int(4) DEFAULT NULL,
  `peso` int(3) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `endereco` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bairro` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `natural_de` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `rg` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_afastamento` date DEFAULT NULL,
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `padrinho` varchar(50) DEFAULT NULL,
  `time_que_torce` varchar(50) DEFAULT NULL,
  `data_entrada` date DEFAULT NULL,
  `data_saida` date DEFAULT NULL,
  `ultima_atualizacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_cadastrador` int(5) NOT NULL DEFAULT '0',
  `atualizado_por` int(5) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `Cadastros`
--

INSERT INTO `Cadastros` (`id`, `nome`, `apelido`, `posicao`, `chute`, `altura`, `peso`, `data_nascimento`, `endereco`, `bairro`, `cep`, `natural_de`, `rg`, `cpf`, `email`, `telefone`, `celular`, `status`, `data_afastamento`, `data_cadastro`, `padrinho`, `time_que_torce`, `data_entrada`, `data_saida`, `ultima_atualizacao`, `id_cadastrador`, `atualizado_por`) VALUES
(27, 'Edson Alves de Oliveira ', 'Edsinho', 'Atacante ', NULL, NULL, NULL, '1972-08-16', 'Rua antonio dionisio de sousa , 243', 'JD Angela ', '', 'São Paulo ', NULL, NULL, 'edsinhoao@gmail.com', '', '(11) 99116-9866', 'Ativo', NULL, NULL, 'Fuba', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-26 15:42:15', 4, 0),
(21, 'Pedro Jose Dos Santos Junior', 'Pedro Jr.', 'Atacante ', '', 0, 0, '1979-10-15', 'AV CELSO DOS SANTOS, 1036', 'VL. CONSTANÇA', '4658', 'São Paulo ', '', '', 'pedro.fabi15@gmail.com', '', '11967027656', NULL, '0000-00-00', '2019-09-19 00:20:26', 'Nambuco', ' São Paulo ', '2009-12-15', '0000-00-00', '2019-09-18 21:20:26', 3, 4),
(23, 'Clodoaldo jose dos Santos', '13', 'Lateral', NULL, NULL, NULL, '1976-09-19', 'Rua doutor Humberto pascal 347', '', '', 'São Paulo ', NULL, NULL, 'clodoaldojose1976@hotmail.com', '(58) 7162-77', '', 'Ativo', '0000-00-00', NULL, '', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-26 15:27:39', 4, 4),
(24, '', '', '', NULL, NULL, NULL, '0000-00-00', '', '', '', '', NULL, NULL, '', '', '', 'Ativo', NULL, NULL, '', '', '0000-00-00', '0000-00-00', '2019-09-26 15:27:46', 4, 0),
(25, 'Valdeir Alves de Lima', 'Val', 'Meio - Atacante ', NULL, NULL, NULL, '1997-08-08', 'Rua laco de fita , 20', '', '', 'São Paulo ', NULL, NULL, 'valdeiralves42@gmail.com', '', '(11) 94976-0308', 'Ativo', '0000-00-00', NULL, 'Pedro Jr', 'Corinthians', '0000-00-00', '0000-00-00', '2019-09-26 15:31:52', 4, 4),
(26, 'Thiago da Conceição de Oliveira', 'Pablo V.', 'Lateral', NULL, NULL, NULL, '1989-11-04', 'Rua Bento Rodrigues', '', '', '', NULL, NULL, 'mailto:Thiagodocopa@gamil.com', '', '(97) 7606-318', 'Ativo', NULL, NULL, 'Neno', ' São Paulo ', '0000-00-00', '0000-00-00', '2019-09-26 15:37:18', 4, 0),
(28, 'Francisco Carlos Nascimento', 'Carlinhos ', 'Meio - Campo ', NULL, NULL, NULL, '1980-08-22', 'Rua Bento Rodrigues: número 21', '', '', 'Rio Grande do Norte ', NULL, NULL, 'Fcnrbt5196@gmail.com', '', '(11) 95177-9665', 'Ativo', NULL, NULL, 'Neno', ' São Paulo ', '0000-00-00', '0000-00-00', '2019-09-26 15:47:15', 4, 0),
(29, 'Ronald Azevedo Nunes', 'Ronny', 'Goleiro / Meio-Campo', NULL, NULL, NULL, '2000-09-25', 'R. Álvaro Ferreira, 318 - Parque novo Santo Amaro', '', '', 'São Paulo ', NULL, NULL, 'ronaldwalker15@hotmail.com', '(58) 3178-99', '', 'Ativo', NULL, NULL, 'Daniel ', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-26 15:50:41', 4, 0),
(30, 'Francisco Gilberto Lopes  ', 'Neno', 'Zagueiro ', NULL, NULL, NULL, '1979-08-13', '', '', '', 'Rio Grande do Norte ', NULL, NULL, '', '', '', 'Ativo', '0000-00-00', NULL, 'Marron', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-26 15:54:46', 4, 4),
(31, ' Eduardo Santos da Silva metzker', 'Dudu Bony', 'Atacante ', NULL, NULL, NULL, '1986-11-01', 'Rua Porto Murtinho 39', '', '', 'São Paulo ', NULL, NULL, ' eduardohenry86@gmail.com', '', '(11) 97760-2088', 'Ativo', NULL, NULL, 'Edmar ', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-26 15:57:07', 4, 0),
(32, 'Rubens de Sousa Leite', 'Ninha', 'Atacante ', NULL, NULL, NULL, '1971-08-03', 'Estr. De Itapecerica 6395', '', '', 'São Paulo ', NULL, NULL, 'leiterubens@hotmail.com', '', '(11) 94709-4898', 'Ativo', NULL, NULL, 'Jura ', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-26 16:00:47', 4, 0),
(33, 'Jonathan de Menezes Correia', 'Correia', 'Meio - Campo ', NULL, NULL, NULL, '1998-08-11', 'Rua Antônio Dionísio de souza 124', '', '', 'São Paulo ', NULL, NULL, 'jonathancopa50@gmail.com', '(11) 5831-4813', '', 'Ativo', NULL, NULL, 'Edisinho', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-26 16:03:22', 4, 0),
(34, 'Edilson Leandro da silva', 'Romarinho', 'Atacante ', NULL, NULL, NULL, '1981-06-28', 'R ribeirao ponte baixa n 23', '', '', 'São Paulo ', NULL, NULL, ' leandroenane@ig.com.br', '(58) 9106-78', '', 'Ativo', NULL, NULL, 'Daniel', 'Santos', '0000-00-00', '0000-00-00', '2019-09-26 16:05:58', 4, 0),
(35, 'Edmar Alves de Oliveira', ' Ed balada', 'Atacante ', NULL, NULL, NULL, '1992-11-19', 'Antônio Dionízio de Souza', '', '', 'São Paulo ', NULL, NULL, 'edmarrato@gmail.com', '', '(11) 97729-5143', 'Afastado', '0000-00-00', NULL, 'Edisinho', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-26 16:07:56', 4, 4),
(36, 'Alan Pekson Quirino', 'Hulk', 'Meio - Campo ', NULL, NULL, NULL, '1989-06-30', 'Rua Candelária 117', '', '', 'São Paulo ', NULL, NULL, 'alanpekson@hotmail.com', '', '(11) 98976-9042', 'Ativo', '0000-00-00', NULL, 'Alexsander', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-26 16:12:45', 4, 4),
(37, 'Daniel Moreira   de Araujo  junior', 'Daniel', 'Goleiro', NULL, NULL, NULL, '1971-12-11', 'Rua dos mercanteis, 162 ap 13b', '', '', 'Bahia ', NULL, NULL, 'djunior481@gmail.com', '(11) 5827-3699', '', 'Ativo', '0000-00-00', NULL, '', 'Santos', '0000-00-00', '0000-00-00', '2019-09-26 16:16:03', 4, 4),
(38, 'Alex Alves dos Santos', 'Alexzinho', 'Lateral', NULL, NULL, NULL, '1974-05-08', 'Rua Antônio Dionísio de Souza 278', '', '', 'São Paulo ', NULL, NULL, 'alex_aliancalaser@hotmail.com.', '(11) 5831-8280', '', 'Ativo', NULL, NULL, '', 'Santos', '0000-00-00', '0000-00-00', '2019-09-26 16:19:31', 4, 0),
(39, 'Alexander Ketzmann Machado Magalhães', 'Alexander ', 'Volante ', NULL, NULL, NULL, '1989-02-14', 'Rua Francisco José da Silva, 292', '', '', 'São Paulo ', NULL, NULL, ' ketzmann@gmail.com', '', '', 'Ativo', NULL, NULL, 'Wilson', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-26 16:21:57', 4, 0),
(40, 'Sérgio Dantas cirqueira', 'Serjao', 'Lateral', NULL, NULL, NULL, '0000-00-00', 'Rua do contraponto n 10', '', '', 'São Paulo ', NULL, NULL, 'serjaodespachante@hotmail.com', '', '(11) 5831-6678', 'Ativo', NULL, NULL, 'Romarinho', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-26 16:24:33', 4, 0),
(41, 'Juan Araujo de Almeida', 'Juan', 'Meio - Campo ', NULL, NULL, NULL, '1990-08-10', 'Rua Antônio Victor de Oliveira 264', '', '', 'São Paulo ', NULL, NULL, ' juaalme@gmail.com', '(11) 5831-9999', '', 'Ativo', NULL, NULL, ' Jadson', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-26 16:26:55', 4, 0),
(42, ' RAFAEL ALVES FERREIRA', 'MONSTRO', 'Atacante ', NULL, NULL, NULL, '1992-07-07', 'RUA ANTÔNIO DIONÍZIO DE SOUZA 293', '', '', 'São Paulo ', NULL, NULL, 'raferreira927@gmail.com', '', '', 'Ativo', NULL, NULL, 'ALEX', ' São Paulo ', '0000-00-00', '0000-00-00', '2019-09-26 16:28:37', 4, 0),
(43, 'Isael Pereira da Silva', 'Isael ', 'Lateral', NULL, NULL, NULL, '1984-10-25', 'Rua José Gomes Ferreira 125', 'Jardim Copacabana', '', 'Piaui', NULL, NULL, 'isaelsilva.2510@gmail. Com', '', '(11) 96587-1406', 'Ativo', NULL, NULL, 'Neno', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-27 21:17:06', 4, 0),
(44, 'Rildo Pereira de França', 'Rildo ', 'Volante ', NULL, NULL, NULL, '1973-05-23', 'R. Bonifacio Fernandes ,97', '', '', 'São Paulo ', NULL, NULL, 'orcamentos@raceengenharia.com.', '(11) 5831-4954', '', 'Ativo', NULL, NULL, 'Gato ', ' São Paulo ', '0000-00-00', '0000-00-00', '2019-09-27 21:22:58', 4, 0),
(45, 'Sebastião Robson Paulo das flores', 'Rubicão ', 'Zagueiro ', NULL, NULL, NULL, '1963-01-20', 'rua Bento Rodrigues, 634', '', '', 'Bahia ', NULL, NULL, 'mailto:Sebastiao.robson20@gmail.com', '', '', 'Ativo', '0000-00-00', NULL, 'Aranha', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-27 21:23:54', 4, 4),
(46, 'Wilson Barbosa dos Santos', 'Garimpeiro ', 'Atacante ', NULL, NULL, NULL, '1978-03-04', 'Rua Dr Lauro Ribas Braga 245 ap 26 bloco C', '', '', 'São Paulo ', NULL, NULL, 'Wilsonbarbosasantos@gmail.com', '', '', 'Ativo', '0000-00-00', NULL, 'Pedro Jr', 'Santos', '0000-00-00', '0000-00-00', '2019-09-27 21:25:46', 4, 4),
(47, 'Juraci Fernandes de Sousa ', 'Jura ', 'Lateral', NULL, NULL, NULL, '0000-00-00', 'Rua Celestino Joaquim da Costa n. 212 Casa 2 Vila ', '', '', 'São Paulo ', NULL, NULL, 'jurasousa88@hotmail.com', '(11) 2052-4430', '', 'Ativo', NULL, NULL, ' Alex Alves', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-27 21:29:51', 4, 0),
(48, 'Emerson Ortega ', 'Emerson', 'Zagueiro ', NULL, NULL, NULL, '1979-08-18', 'Rua Elisa Antônia de Andrade número 14', '', '', 'São Paulo ', NULL, NULL, 'Emersonortega031@gmail.com', '(11) 5833-0536', '', 'Ativo', '0000-00-00', NULL, 'Edsinho', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-27 21:31:31', 4, 4),
(49, 'Jadson Araújo de Almeida ', 'Jadson', 'Lateral', NULL, NULL, NULL, '0000-00-00', 'Antônio Victor de oliveira n 264', '', '', 'São Paulo ', NULL, NULL, 'jadson.realce2017@gmail.com', '(11) 5831-9999', '', 'Ativo', '0000-00-00', NULL, '', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-27 21:34:31', 4, 4),
(50, 'Osires Antonio da Silva', 'Cabañas', 'Meio - Campo ', NULL, NULL, NULL, '1977-05-19', 'R Mario nizolli 120 jd modelo . Capao', '', '', 'São Paulo ', NULL, NULL, 'Osires.contabil@gmail.com', '(11) 5874-2963', '', 'Ativo', NULL, NULL, 'ALEX', 'Santos', '0000-00-00', '0000-00-00', '2019-09-27 21:36:06', 4, 0),
(51, 'Alex Sandro Souza de Sena', 'Alex ', 'Zagueiro ', NULL, NULL, NULL, '1979-09-24', '', '', '', 'São Paulo ', NULL, NULL, 'Alexsena11.@.yahoo.com.br', '', '', 'Afastado', '0000-00-00', NULL, 'Rubicão', 'Santos', '0000-00-00', '0000-00-00', '2019-09-27 21:37:51', 4, 4),
(52, 'Clovis Benedicto T. Dos Santos Filho', 'Cobrinha', 'Lateral', NULL, NULL, NULL, '1972-02-25', ' Rua Antônio Victor de Oliveira,268', '', '', 'São Paulo ', NULL, NULL, 'clovisb305@gmail.com', '', '(11) 94783-9536', 'Ativo', NULL, NULL, 'Lobão ', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-27 21:40:16', 4, 0),
(53, 'LUIS CARLOS THEODORO DA SILVA', 'LUIS', 'Meio - Campo ', NULL, NULL, NULL, '1973-04-04', ' Rua  Celestino Joaquim da Costa, 115', 'Vila Santa Lúcia', '', 'São Paulo ', NULL, NULL, ' theoth_32@hotmail.com', '(11) 5833-5100', '', 'Ativo', '0000-00-00', NULL, 'Gato ', 'Santos', '0000-00-00', '0000-00-00', '2019-09-27 21:42:10', 4, 4),
(54, 'Silio Guedes lauton', 'Gato ', 'Zagueiro ', NULL, NULL, NULL, '1971-04-22', 'Rua Afonso Rui n.205', '', '', 'Bahia ', NULL, NULL, '', '(11) 5831-6353', '', 'Ativo', NULL, NULL, 'Adriano', 'Flamengo', '0000-00-00', '0000-00-00', '2019-09-27 21:43:52', 4, 0),
(55, 'Francisco Gilberto Lopes ', 'Neno', 'Zagueiro ', NULL, NULL, NULL, '1979-08-13', '', '', '', 'Rio Grande do Norte ', NULL, NULL, '', '', '', 'Ativo', NULL, NULL, 'Marron', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-27 21:46:01', 4, 0),
(56, 'Jonatas souza santos', 'Jonatas ', 'Volante ', NULL, NULL, NULL, '1994-06-19', 'Av carlos lacerda JD.rosana', '', '', 'Bahia ', NULL, NULL, '', '', '(01) 19901-1451', 'Ativo', '0000-00-00', NULL, 'Carlos 12 ', 'Vitoria ', '0000-00-00', '0000-00-00', '2019-09-27 21:47:23', 4, 4),
(57, 'Fabiano Bastos Doneida', 'Fabinho', 'Goleiro', NULL, NULL, NULL, '1982-01-05', 'R. Jovina, 75, apto 82, Vila Mascote, São Paulo', '', '04363-080', 'São Paulo ', NULL, NULL, 'fdoneida@gmail.com ', '', '(11) 99145-6527', 'Ativo', NULL, NULL, '', ' São Paulo ', '0000-00-00', '0000-00-00', '2019-09-28 16:20:44', 4, 0),
(58, 'Sergio Oliveira ', 'Totó ', 'Zagueiro ', NULL, NULL, NULL, '1978-02-06', 'Estrada do m.boi Mirim. N.5098', '', '', 'Bahia ', NULL, NULL, 'So0066647@gmail.com ', '', '(11) 99381-3673', 'Ativo', NULL, NULL, '', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-28 16:22:53', 4, 0),
(59, 'Joel Ferreira Silva', 'Joel', 'Meio - Campo ', NULL, NULL, NULL, '1979-08-01', 'rua José da Costa Lima 23 jd copacabana', '', '', 'Piaui', NULL, NULL, ' efjconstrucoes@yahoo.com.br', '(11) 5834-8264', '', 'Ativo', NULL, NULL, 'Neno', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-28 16:25:14', 4, 0),
(60, 'Fagner Rodrigues Magalhães', 'Gansinho', 'Lateral', NULL, NULL, NULL, '1989-10-02', 'Henrique Chaves 291 apto 31b', '', '05372-050', 'São Paulo ', NULL, NULL, 'fagner.magalhaes@gmail.com', '', '(11) 95855-8759', 'Ativo', NULL, NULL, 'Wilson', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-28 16:50:09', 4, 0),
(61, 'Aureliano Alves de Souza  Neto', 'Aurélio', 'Zagueiro ', NULL, NULL, NULL, '1984-08-12', 'doutor João Baptista parmigiani', '', '05877-210', 'São Paulo ', NULL, NULL, '', '', '(11) 98412-3118', 'Ativo', '0000-00-00', NULL, 'Thiago', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-28 17:48:16', 4, 4),
(62, 'Luciano Brito dos Santos ', ' Lobão', 'Volante ', NULL, NULL, NULL, '1974-10-26', ' Rua Eng. Antonio Alves Braga, 375 Riviera', '', '', 'São Paulo ', NULL, NULL, 'ubrito122@hotmail.com', '(11) 5517-7290', '', 'Ativo', NULL, NULL, '', 'Palmeiras ', '0000-00-00', '0000-00-00', '2019-09-28 17:49:45', 4, 0),
(63, 'Lucas Ferreira Dos Reis Santos', 'Sonic', 'Meio - Campo', NULL, NULL, NULL, '1989-03-06', 'Rua Cataldo Parrilha n⁰03', '', '', 'São Paulo', NULL, NULL, 'b2k_lucas007@hotmail.com ', '', '(11) 96037-8476', 'Ativo', NULL, NULL, 'Romarinho ', 'São Paulo', '0000-00-00', '0000-00-00', '2019-09-28 19:31:54', 4, 0),
(64, 'Raphael lima dos Santos ', 'Neguinho ', 'Atacante ', NULL, NULL, NULL, '2003-01-15', 'Rua José Gomes Ferreira n°149', '', '', 'São Paulo', NULL, NULL, 'Raphael-0102@hotmail.com ', '', '', 'Ativo', NULL, NULL, 'Wilson', 'Corinthians ', '0000-00-00', '0000-00-00', '2019-09-28 20:59:57', 4, 0),
(65, 'Ronaldo ', 'Toco', 'Goleiro', NULL, NULL, NULL, '0000-00-00', 'Sem endereço', '', '', '', NULL, NULL, '', '', '', 'Ativo', NULL, NULL, 'Sem dado', 'Corinthians', '0000-00-00', '0000-00-00', '2019-09-30 14:16:04', 4, 0),
(66, 'Régio', 'Régio', 'Goleiro', NULL, NULL, NULL, '0000-00-00', '', '', '', '', NULL, NULL, '', '', '', 'Ativo', NULL, NULL, '', '', '0000-00-00', '0000-00-00', '2019-09-30 14:16:28', 4, 0),
(0, '(Ninguém) Selecione o Jogador', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-04 13:24:46', NULL, NULL, NULL, NULL, '2019-11-04 10:24:46', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Cartoes`
--

CREATE TABLE `Cartoes` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `jogador` int(4) NOT NULL DEFAULT '0',
  `amarelo` int(2) NOT NULL DEFAULT '0',
  `azul` int(2) NOT NULL DEFAULT '0',
  `vermelho` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Quantos cartões um jogador levou por jogo.';

-- --------------------------------------------------------

--
-- Estrutura para tabela `Gols`
--

CREATE TABLE `Gols` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `jogador` int(4) NOT NULL DEFAULT '0',
  `primeiro_tempo` int(2) NOT NULL DEFAULT '0',
  `segundo_tempo` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Informacoes_Complementares`
--

CREATE TABLE `Informacoes_Complementares` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `presencas` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `punicoes` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destaques` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacoes` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Penaltis`
--

CREATE TABLE `Penaltis` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `jogador` int(4) NOT NULL DEFAULT '0',
  `time` int(1) NOT NULL DEFAULT '0',
  `convertidos` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Sumula`
--

CREATE TABLE `Sumula` (
  `id` int(5) NOT NULL,
  `placar_amarelo` int(2) NOT NULL,
  `placar_azul` int(2) NOT NULL DEFAULT '0',
  `placar_penaltis_amarelo` int(2) NOT NULL DEFAULT '0',
  `placar_penaltis_azul` int(2) NOT NULL DEFAULT '0',
  `cartoes_amarelo` int(2) NOT NULL DEFAULT '0',
  `cartoes_azul` int(2) NOT NULL DEFAULT '0',
  `data_jogo` date NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_cadastrador` int(5) NOT NULL DEFAULT '0',
  `atualizado_por` int(5) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `Sumula`
--

INSERT INTO `Sumula` (`id`, `placar_amarelo`, `placar_azul`, `placar_penaltis_amarelo`, `placar_penaltis_azul`, `cartoes_amarelo`, `cartoes_azul`, `data_jogo`, `data_cadastro`, `id_cadastrador`, `atualizado_por`) VALUES
(22, 2, 2, 2, 2, 2, 2, '0002-02-02', '2019-09-19 00:13:52', 3, 1),
(26, 4, 4, 4, 4, 4, 4, '0004-04-04', '2019-09-26 01:14:58', 1, 0),
(28, 5, 2, 0, 0, 0, 0, '2019-10-15', '2019-10-29 19:42:22', 1, 0),
(24, 1, 1, 1, 1, 1, 1, '0001-01-01', '2019-09-26 01:14:32', 1, 0),
(27, 2, 2, 2, 2, 2, 2, '0002-02-02', '2019-09-27 17:07:08', 1, 0),
(25, 3, 3, 3, 3, 3, 3, '0003-03-03', '2019-09-26 01:14:43', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `Time_Amarelo_1`
--

CREATE TABLE `Time_Amarelo_1` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `1` int(4) NOT NULL DEFAULT '0',
  `2` int(4) NOT NULL DEFAULT '0',
  `3` int(4) NOT NULL DEFAULT '0',
  `4` int(4) NOT NULL DEFAULT '0',
  `5` int(4) NOT NULL DEFAULT '0',
  `6` int(4) NOT NULL DEFAULT '0',
  `7` int(4) NOT NULL DEFAULT '0',
  `8` int(4) NOT NULL DEFAULT '0',
  `9` int(4) NOT NULL DEFAULT '0',
  `10` int(4) NOT NULL DEFAULT '0',
  `11` int(4) NOT NULL DEFAULT '0',
  `12` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Time_Amarelo_2`
--

CREATE TABLE `Time_Amarelo_2` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `1` int(4) NOT NULL DEFAULT '0',
  `2` int(4) NOT NULL DEFAULT '0',
  `3` int(4) NOT NULL DEFAULT '0',
  `4` int(4) NOT NULL DEFAULT '0',
  `5` int(4) NOT NULL DEFAULT '0',
  `6` int(4) NOT NULL DEFAULT '0',
  `7` int(4) NOT NULL DEFAULT '0',
  `8` int(4) NOT NULL DEFAULT '0',
  `9` int(4) NOT NULL DEFAULT '0',
  `10` int(4) NOT NULL DEFAULT '0',
  `11` int(4) NOT NULL DEFAULT '0',
  `12` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Time_Azul_1`
--

CREATE TABLE `Time_Azul_1` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `1` int(4) NOT NULL DEFAULT '0',
  `2` int(4) NOT NULL DEFAULT '0',
  `3` int(4) NOT NULL DEFAULT '0',
  `4` int(4) NOT NULL DEFAULT '0',
  `5` int(4) NOT NULL DEFAULT '0',
  `6` int(4) NOT NULL DEFAULT '0',
  `7` int(4) NOT NULL DEFAULT '0',
  `8` int(4) NOT NULL DEFAULT '0',
  `9` int(4) NOT NULL DEFAULT '0',
  `10` int(4) NOT NULL DEFAULT '0',
  `11` int(4) NOT NULL DEFAULT '0',
  `12` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Time_Azul_2`
--

CREATE TABLE `Time_Azul_2` (
  `sumula_referencia` int(5) NOT NULL DEFAULT '0',
  `1` int(4) NOT NULL DEFAULT '0',
  `2` int(4) NOT NULL DEFAULT '0',
  `3` int(4) NOT NULL DEFAULT '0',
  `4` int(4) NOT NULL DEFAULT '0',
  `5` int(4) NOT NULL DEFAULT '0',
  `6` int(4) NOT NULL DEFAULT '0',
  `7` int(4) NOT NULL DEFAULT '0',
  `8` int(4) NOT NULL DEFAULT '0',
  `9` int(4) NOT NULL DEFAULT '0',
  `10` int(4) NOT NULL DEFAULT '0',
  `11` int(4) NOT NULL DEFAULT '0',
  `12` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Acessos`
--
ALTER TABLE `Acessos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `cadastro`
--
ALTER TABLE `cadastro`
  ADD PRIMARY KEY (`id_cad`);

--
-- Índices de tabela `Cadastros`
--
ALTER TABLE `Cadastros`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Sumula`
--
ALTER TABLE `Sumula`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Acessos`
--
ALTER TABLE `Acessos`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `cadastro`
--
ALTER TABLE `cadastro`
  MODIFY `id_cad` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT de tabela `Cadastros`
--
ALTER TABLE `Cadastros`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de tabela `Sumula`
--
ALTER TABLE `Sumula`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
